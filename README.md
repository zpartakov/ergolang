Эrgolang
========
[![Эrgolang](https://radeff.red/pics/logoergolang.jpg)](Эrgolang)

## Français
Avec Эrgolang, vous pouvez:

- consulter les dictionnaires, entrer de nouveaux mots et catégories, entraîner votre vocabulaire
- entrer et reviser des textes, en profitant de l'analyseur automatique de mots
- entrer en contact avec d'autres personnes apprenantes

Les deux idées centrales d'ergolang:

- une communauté d'apprenants, ce qui permet de partager les vocabulaires et de les enrichir
- une pédagogie flexible et personnalisée, basée sur les catégories de mots et de textes choisies par l'apprenant

## English
collaborative &amp; open-source learning languages / apprentissage de langues collaboratif &amp; open-source

## Documentation & demo
https://radeff.red/websites/ergolang/cake/ (french only)

## Téléchargement/Download
https://gitlab.com/zpartakov/ergolang

## Required
- Apache2 http://apache.org/
- MySQL5 https://www.mysql.com/downloads/
- PHP5 http://www.php.net/
- CakePhp 1.3.21 https://github.com/cakephp/cakephp/

## Credits
- Fred Radeff aka Zpartakov - https://radeff.red
- Hulin Thibaud / wistithi, http://traces.toile-libre.org/dokuwiki/
- Special thanks to Nicolas Rod for his help in cakePhp https://github.com/alaxos

## Credits / Russian language & Grammar
- Béatrice Crabère, RUSSE / РУССКИЙ ЯЗЫК, http://w3.adim.univ-tlse2.fr/russe/Accueil.htm
- Serge Arbiol
- stemming: http://snowball.tartarus.org/algorithms/russian/stemmer.html

## Install
After install required software, see
- copy /config/core.php.default to /config/core.php and adapt hashkey
- copy /config/database.php.default to /database/core.php and adapt MySqlsettings
- /config/sql/facturAcp.sql for populating the database

###########################################
# @package Эrgolang                                                                           #
# @version $Id: 1.0.1                                                                            #
# @author Fred Radeff fradeff@akademia.ch                                        #
# @copyright (c) 2013-2018 Fred Radeff, www.radeff.red                     #
# @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  #
###########################################

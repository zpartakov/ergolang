<?php
class JosErgolangRuMotsController extends AppController {

	var $name = 'JosErgolangRuMots';
	var $helpers = array('Html', 'Form');

	function index() {
		$this->JosErgolangRuMot->recursive = 0;
		$this->set('josErgolangRuMots', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid JosErgolangRuMot.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('josErgolangRuMot', $this->JosErgolangRuMot->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->JosErgolangRuMot->create();
			if ($this->JosErgolangRuMot->save($this->data)) {
				$this->Session->setFlash(__('The JosErgolangRuMot has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The JosErgolangRuMot could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid JosErgolangRuMot', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->JosErgolangRuMot->save($this->data)) {
				$this->Session->setFlash(__('The JosErgolangRuMot has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The JosErgolangRuMot could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->JosErgolangRuMot->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for JosErgolangRuMot', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->JosErgolangRuMot->del($id)) {
			$this->Session->setFlash(__('JosErgolangRuMot deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}


	function admin_index() {
		$this->JosErgolangRuMot->recursive = 0;
		$this->set('josErgolangRuMots', $this->paginate());
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid JosErgolangRuMot.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('josErgolangRuMot', $this->JosErgolangRuMot->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->JosErgolangRuMot->create();
			if ($this->JosErgolangRuMot->save($this->data)) {
				$this->Session->setFlash(__('The JosErgolangRuMot has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The JosErgolangRuMot could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid JosErgolangRuMot', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->JosErgolangRuMot->save($this->data)) {
				$this->Session->setFlash(__('The JosErgolangRuMot has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The JosErgolangRuMot could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->JosErgolangRuMot->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for JosErgolangRuMot', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->JosErgolangRuMot->del($id)) {
			$this->Session->setFlash(__('JosErgolangRuMot deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>
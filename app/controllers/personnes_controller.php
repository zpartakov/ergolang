<?php
/**
* @version        $Id: personnes_controller.php v1.0 12.01.2010 14:46:05 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
adapted from http://www.formation-cakephp.com/34/autocomplete-en-ajax
*/
class PersonnesController extends AppController {

	var $name = 'Personnes';
	var $helpers = array('Html', 'Form');

	function index() {
		$this->Personne->recursive = 0;
		$this->set('personnes', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Personne.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('personne', $this->Personne->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Personne->create();
			if ($this->Personne->save($this->data)) {
				$this->Session->setFlash(__('The Personne has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Personne could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Personne', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Personne->save($this->data)) {
				$this->Session->setFlash(__('The Personne has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Personne could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Personne->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Personne', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Personne->del($id)) {
			$this->Session->setFlash(__('Personne deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>

<?php
class ErgoMyVocsController extends AppController {

	var $name = 'ErgoMyVocs';
	var $helpers = array('Html', 'Form');
 
 /*function beforeFilter() {
	$this->Auth->allow('*'); //on autorise tout
	$this->Auth->deny('delete'); //sauf l'effacement; todo: admin doit pouvoir effacer
 }*/
	function index() {
		/*
		$langue=$_GET['langue'];
		$nomtable="ergo_" .$langue ."frs";
		$userid= $this->Session->read('Auth.User.id'); 
	
	
		$langue="(nomtable LIKE '" .$nomtable ."' AND user_id =" .$userid .") ORDER BY date DESC";
		$this->ErgoMyVoc->recursive = 0;
		$this->set('ergoMyVocs', $this->paginate());
		
		$this->set('ergoMyVocs',$this->ErgoMyVoc->find('all',array('conditions'=>array($langue), $this->paginate())));

$requetesql="
SELECT * FROM ergo_my_vocs 
WHERE (
ergo_my_vocs.nomtable LIKE '" .$langue ."' 
AND ergo_my_vocs.user_id =" .$userid .")";
#echo $requetesql;
	$this->set('ergoMyVocs',$this->ErgoMyVoc->query($requetesql), $this->paginate());
*/
//todo: edit view

	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ErgoMyVoc.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('ergoMyVoc', $this->ErgoMyVoc->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ErgoMyVoc->create();
			if ($this->ErgoMyVoc->save($this->data)) {
				$this->Session->setFlash(__('The ErgoMyVoc has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoMyVoc could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ErgoMyVoc', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoMyVoc->save($this->data)) {
				$this->Session->setFlash(__('The ErgoMyVoc has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoMyVoc could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoMyVoc->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ErgoMyVoc', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoMyVoc->del($id)) {
			$this->Session->setFlash(__('ErgoMyVoc deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}


	function admin_index() {
		$this->ErgoMyVoc->recursive = 0;
		$this->set('ergoMyVocs', $this->paginate());
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ErgoMyVoc.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('ergoMyVoc', $this->ErgoMyVoc->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->ErgoMyVoc->create();
			if ($this->ErgoMyVoc->save($this->data)) {
				$this->Session->setFlash(__('The ErgoMyVoc has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoMyVoc could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ErgoMyVoc', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoMyVoc->save($this->data)) {
				$this->Session->setFlash(__('The ErgoMyVoc has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoMyVoc could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoMyVoc->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ErgoMyVoc', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoMyVoc->del($id)) {
			$this->Session->setFlash(__('ErgoMyVoc deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>

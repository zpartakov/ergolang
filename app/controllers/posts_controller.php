<?php
/**
* @version        $Id: posts_controller.php v1.0 26.03.2010 14:36:13 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
* 
*/


//Configure::write('debug', 3);

class PostsController extends AppController {

	var $name = 'Posts';
	var $helpers = array('Html', 'Form');

var $components = array('RequestHandler','Auth');
		#criteres de tri
	var $paginate = array(
        'limit' => 25,
        'order' => array(
            'Post.id' => 'desc'
        )
    );
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index','view','flux');
	}	
	
	
	function index() {
		$this->Post->recursive = 0;
		
			$this->ErgoLink->recursive = 0;
			/*fulltext search*/
			if( $this->RequestHandler->isRss() ){
			$posts = $this->Post->find('all', array('limit' => 20, 'order' => 'Post.created DESC'));
			$this->set(compact('posts'));
			} elseif($this->data['Post']['q']) {
					$input = $this->data['Post']['q']; 
					# sanitize the query
					App::import('Sanitize');
					$q = Sanitize::escape($input);
					$options = array(
					"Post.title LIKE '%" .$q ."%'" ." OR Post.body LIKE '%" .$q ."%'"
					);
					
					$this->set(array('posts' => $this->paginate('Post', $options))); 
		} else {
		$this->set('posts', $this->paginate());
	}
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Post.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('post', $this->Post->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Post->create();
			if ($this->Post->save($this->data)) {
				$this->Session->setFlash(__('The Post has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Post could not be saved. Please, try again.', true));
			}
		}
		$tags = $this->Post->Tag->find('list');
		$users = $this->Post->User->find('list');
		$this->set(compact('ergoCategories', 'tags', 'users'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Post', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Post->save($this->data)) {
				$this->Session->setFlash(__('The Post has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Post could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Post->read(null, $id);
		}
		$tags = $this->Post->Tag->find('list');
		$users = $this->Post->User->find('list');
		$this->set(compact('tags','users'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Post', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Post->del($id)) {
			$this->Session->setFlash(__('Post deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
	

}
?>

<?php
/**
* @version        $Id: ergo_ru_phrases_controller.php v1.0 30.12.2009 08:42:36 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
class ErgoRuPhrasesController extends AppController {

	var $name = 'ErgoRuPhrases';
	var $components = array('RequestHandler','Auth');
	
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index','view','search','analyse','delete');
	}
	
	#criteres de tri
	var $paginate = array(
        'limit' => 25,
        'order' => array(
            'ErgoRuPhrase.auteur,ErgoRuPhrase.titre' => 'asc'
        )
    );

	function index() {
		$this->ErgoRuPhrase->recursive = 0;
			if($_SESSION['Auth']['User']['group_id']!=3) { 	//non admin eject
			$options = array(
					"ErgoRuPhrase.titre NOT LIKE '<!-- private -->%'"
					);
					
					#$result=$this->ErgoRufr->find('count', array('conditions' => array('ErgoRufr.foreign LIKE ' => '"$q"'))); 
			$this->set('ergoRuPhrases', $this->paginate($options));
} else {
			$this->set('ergoRuPhrases', $this->paginate());
}		
	}
	
	function search() {
		
	}


	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ErgoRuPhrase.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('ergoRuPhrase', $this->ErgoRuPhrase->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ErgoRuPhrase->create();
			if ($this->ErgoRuPhrase->save($this->data)) {
				/*begin special function to add user id (usefull for killing spam if needed and "my" services */
					$utilisateur= $this->Session->read('Auth.User.username'); //whois the current user
					$utilisateurid= $this->Session->read('Auth.User.id'); //current user id
					$dernierid=mysql_insert_id();
					//store in the new word in the myvocs
					$sql="
					INSERT INTO `akademiach18`.`ergo_my_ru_phrases` (
					`id` ,
					`user_id` ,
					`voc_id` ,
					`date`
					)
					VALUES (
					NULL , 
					'$utilisateurid', 
					'$dernierid',
					CURRENT_TIMESTAMP
					)";
					$sql=mysql_query($sql);
				/*end special function to add user id (usefull for killing spam if needed and "my" services */
				$this->Session->setFlash(__('Enregistrement sauvegardé avec succès', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('L\'enregistrement n\'a pas pu être sauvegardé. Merci d\'essayer à nouveau.', true));
			}
		}
	}

	function edit($id = null) {
		eject_non_admin(); //on autorise pas les non-administrateurs
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ErgoRuPhrase', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoRuPhrase->save($this->data)) {
				$this->Session->setFlash(__('The ErgoRuPhrase has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoRuPhrase could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoRuPhrase->read(null, $id);
		}
	}

	function delete($id) {

			#eject_non_admin(); //on autorise pas les non-administrateurs
		/*	
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ErgoRuPhrase', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoRuPhrase->del($id)) {
			$this->Session->setFlash(__('ErgoRuPhrase deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		* */
	}


	function analyse() {
	}

    function inseremot() {
	}

}
?>

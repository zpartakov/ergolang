<?php
/**
* @version        $Id: ergo_ru_grammaires_controller.php v1.0 13.03.2010 06:17:37 CET $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
* -- Structure de la table `ergo_ru_grammaires`
--

CREATE TABLE IF NOT EXISTS `ergo_ru_grammaires` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `ergo_ru_type_id` int(12) NOT NULL,
  `lib` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `rem` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

*/
?>
<?php
class ErgoRuGrammairesController extends AppController {
			/* on interdit l'effacement au non-admin
		 * */
/**/
	var $name = 'ErgoRuGrammaires';
	var $helpers = array('Html', 'Form');

	var $components = array('RequestHandler','Auth');
	
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('vue');
	}	

	#criteres de tri
	var $paginate = array(
        'limit' => 50,
        'order' => array(
            'ErgoRuGrammaire.id' => 'desc'
        )
    );
	function index() {
		
		if($this->Auth->user('group_id')!=3) {
	$this->Session->setFlash(__('Action not allowed!', true));
	exit;
}
		
		$this->ErgoRuGrammaire->recursive = 0;
		if($this->data['ErgoRuGrammaire']['q']) {
					$input = $this->data['ErgoRuGrammaire']['q']; 
					# sanitize the query
					App::import('Sanitize');
					$q = Sanitize::escape($input);
					$q=trim($q);
#					"ErgoRuGrammaire.lib LIKE '%" .$q ."%'" ." OR ErgoRuGrammaire.text LIKE '%" .$q ."%' OR ErgoRuGrammaire.url LIKE '%" .$q ."%' OR ErgoRuGrammaire.rem LIKE '%" .$q ."%'"
					$options = array(
					"ErgoRuGrammaire.lib LIKE '%" .$q ."%'" ." OR ErgoRuGrammaire.text LIKE '%" .$q ."%' OR ErgoRuGrammaire.rem LIKE '%" .$q ."%'"
					);
					$this->set(array('ErgoRuGrammaires' => $this->paginate('ErgoRuGrammaire', $options))); 
				} else {
		$this->set('ergoRuGrammaires', $this->paginate());
	}
	}

	function view($id = null) {
				if($this->Auth->user('group_id')!=3) {
	$this->Session->setFlash(__('Action not allowed!', true));
	exit;
}
		if (!$id) {
			$this->Session->setFlash(__('Invalid ErgoRuGrammaire.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('ergoRuGrammaire', $this->ErgoRuGrammaire->read(null, $id));
	}
	
	function vue($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ErgoRuGrammaire.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('ergoRuGrammaire', $this->ErgoRuGrammaire->read(null, $id));
	}

	function add() {
				if($this->Auth->user('group_id')!=3) {
	$this->Session->setFlash(__('Action not allowed!', true));
	exit;
}
		if (!empty($this->data)) {
			$this->ErgoRuGrammaire->create();
				#$this->['ergoRuGrammaire']['text']=strip_tags($this->['ergoRuGrammaire']['text'],'<p><a><table><tr><td><strong>');

			if ($this->ErgoRuGrammaire->save($this->data)) {
				$this->Session->setFlash(__('The ErgoRuGrammaire has been saved', true));
				$this->redirect(array('action'=>'add'));
			} else {
				$this->Session->setFlash(__('The ErgoRuGrammaire could not be saved. Please, try again.', true));
			}
		}
		$ergoRuTypes = $this->ErgoRuGrammaire->ErgoRuType->find('list');
		$this->set(compact('ergoRuTypes'));
	}

	function edit($id = null) {
				if($this->Auth->user('group_id')!=3) {
	$this->Session->setFlash(__('Action not allowed!', true));
	exit;
}
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ErgoRuGrammaire', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoRuGrammaire->save($this->data)) {
				$this->Session->setFlash(__('The ErgoRuGrammaire has been saved', true));
				
				#$this->redirect(array('action'=>'index'));
				$this->redirect($_SERVER['HTTP_REFERER']);
				#$this->redirect(javascript.history.go(-2));
			} else {
				$this->Session->setFlash(__('The ErgoRuGrammaire could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoRuGrammaire->read(null, $id);
		}
		$ergoRuTypes = $this->ErgoRuGrammaire->ErgoRuType->find('list');
		$this->set(compact('ergoRuTypes'));
	}

	function delete($id = null) {
				if($this->Auth->user('group_id')!=3) {
	$this->Session->setFlash(__('Action not allowed!', true));
	exit;
}
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ErgoRuGrammaire', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoRuGrammaire->del($id)) {
			$this->Session->setFlash(__('ErgoRuGrammaire deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

/*some custom functions*/
	
	//a function to clean imported html
	function nettoyer () {
		//non admin eject
	if($this->Auth->user('group_id')!=3) {
		$this->Session->setFlash(__('Action not allowed!', true));
		exit;
	}
	}
	
	
	//check all imported files coherence
	function checkimport () {
				if($this->Auth->user('group_id')!=3) {
	$this->Session->setFlash(__('Action not allowed!', true));
	exit;
}
	}
}
?>

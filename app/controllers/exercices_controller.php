<?php
/**
* @version        $Id: exercices_controller.php v1.0 10.03.2010 15:02:39 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
this is the main exercices controller tool
-- Structure de la table `exercices`
--

CREATE TABLE IF NOT EXISTS `exercices` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `ergo_langue_id` int(3) NOT NULL COMMENT 'comput ergo_lang_exercices table',
  `id_exs` int(12) NOT NULL,
  `id_l` int(12) NOT NULL,
  `titreexercice` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `soustitre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `liengrammaire` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `debutcontenu` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `fincontenu` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `source` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `datemod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(12) NOT NULL,
  `prev_ex` int(12) NOT NULL,
  `next_ex` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=66 ;

*/

class ExercicesController extends AppController {

	var $name = 'Exercices';
	var $helpers = array('Html', 'Form');
	var $components = array('RequestHandler','Auth');
function beforeFilter() {
	parent::beforeFilter();
    $this->Auth->allow('lexercice','listeexercices');
 }
	#criteres de tri
	var $paginate = array(
        'limit' => 25,
        'order' => array(
            'Exercice.id' => 'desc'
        )
    );

	function index() {
		$this->Exercice->recursive = 0;
		if($this->data['Exercice']['q']) {
					$input = $this->data['Exercice']['q']; 
					# sanitize the query
					App::import('Sanitize');
					$q = Sanitize::escape($input);
					$q=trim($q);
					#"Exercice.titreexercice LIKE '%" .$q ."%'" ." OR Exercice.liengrammaire LIKE '%" .$q ."%' OR Exercice.source LIKE '%" .$q ."%'"

					$options = array(
					"Exercice.titreexercice LIKE '%" .$q ."%'" ." OR Exercice.liengrammaire LIKE '%" .$q ."%'"
					);
					$this->set(array('Exercices' => $this->paginate('Exercice', $options))); 
				} else {
		$this->set('exercices', $this->paginate());
	}
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Exercice.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('exercice', $this->Exercice->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Exercice->create();
			if ($this->Exercice->save($this->data)) {
				$this->Session->setFlash(__('The Exercice has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Exercice could not be saved. Please, try again.', true));
			}
		}
		$ergoLangues = $this->Exercice->ErgoLangue->find('list');
		$users = $this->Exercice->User->find('list');
		$this->set(compact('ergoLangues', 'users'));
	}

	function edit($id = null) {
		eject_non_admin(); //on autorise pas les non-administrateurs
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Exercice', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Exercice->save($this->data)) {
				$this->Session->setFlash(__('The Exercice has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Exercice could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Exercice->read(null, $id);
		}
		$ergoLangues = $this->Exercice->ErgoLangue->find('list');
		$users = $this->Exercice->User->find('list');
		$this->set(compact('ergoLangues','users'));
	}

	function delete($id = null) {
							eject_non_admin(); //on autorise pas les non-administrateurs

		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Exercice', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Exercice->del($id)) {
			$this->Session->setFlash(__('Exercice deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
	
/* ad hoc ergolang functions */
	function lexercice($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Exercice.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('exercice', $this->Exercice->read(null, $id));
	}


	function listeexercices() {
	}	

}
?>

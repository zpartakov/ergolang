<?php
/**
* @version        $Id: ergo_ru_stopwords_controller.php v1.0 28.06.2010 23:14:38 CEST $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

class ErgoRuStopwordsController extends AppController {

	var $name = 'ErgoRuStopwords';
	var $components = array('RequestHandler','Auth');

	#criteres de tri
	var $paginate = array(
        'limit' => 100,
        'order' => array(
            'ErgoRuStopword.word' => 'asc'
        )
    );
    
	function index() {
		$this->ErgoRuStopword->recursive = 0;
		$this->set('ergoRuStopwords', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(sprintf(__('Invalid %s', true), 'ergo ru stopword'));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('ergoRuStopword', $this->ErgoRuStopword->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ErgoRuStopword->create();
			if ($this->ErgoRuStopword->save($this->data)) {
				$this->Session->setFlash(sprintf(__('The %s has been saved', true), 'ergo ru stopword'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(sprintf(__('The %s could not be saved. Please, try again.', true), 'ergo ru stopword'));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(sprintf(__('Invalid %s', true), 'ergo ru stopword'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoRuStopword->save($this->data)) {
				$this->Session->setFlash(sprintf(__('The %s has been saved', true), 'ergo ru stopword'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(sprintf(__('The %s could not be saved. Please, try again.', true), 'ergo ru stopword'));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoRuStopword->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(sprintf(__('Invalid id for %s', true), 'ergo ru stopword'));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoRuStopword->delete($id)) {
			$this->Session->setFlash(sprintf(__('%s deleted', true), 'Ergo ru stopword'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(sprintf(__('%s was not deleted', true), 'Ergo ru stopword'));
		$this->redirect(array('action' => 'index'));
	}
}
?>

<?php
class ErgoNlfrsController extends AppController {

	var $name = 'ErgoNlfrs';

			var $components = array('RequestHandler','Auth');
	var $helpers = array('Html', 'Form');
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index','view','autocomplete');
		if($this->RequestHandler->isAjax())
		{
			Configure::write('debug', 3);
		}
	}
		#criteres de tri
	var $paginate = array(
        'limit' => 100,
        'order' => array(
            'ErgoNlfr.id' => 'desc'
        )
    );
	
	
	function index() {
		$this->ErgoNlfr->recursive = 0;
						if($this->data['ErgoNlfr']['q']) {
					$input = $this->data['ErgoNlfr']['q']; 
					# sanitize the query
					App::import('Sanitize');
					$q = Sanitize::escape($input);
					$options = array(
					"ErgoNlfr.local LIKE '%" .$q ."%'" ." OR ErgoNlfr.foreign LIKE '%" .$q ."%'"
					);
					$this->set(array('ergoNlfrs' => $this->paginate('ErgoNlfr', $options))); 
		} else {
		$this->set('ergoNlfrs', $this->paginate());
	}
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ergo nlfr', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('ergoNlfr', $this->ErgoNlfr->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ErgoNlfr->create();
			if ($this->ErgoNlfr->save($this->data)) {
				$this->Session->setFlash(__('The ergo nlfr has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ergo nlfr could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ergo nlfr', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoNlfr->save($this->data)) {
				$this->Session->setFlash(__('The ergo nlfr has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ergo nlfr could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoNlfr->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ergo nlfr', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoNlfr->delete($id)) {
			$this->Session->setFlash(__('Ergo nlfr deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Ergo nlfr was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>
<?php
class EmployeesController extends AppController {

	var $name = 'Employees';
	var $components = array('RequestHandler','Security');
	var $helpers = array('Form','Html','Javascript');
	
	function populate() {
		
		$this->Employee->populate();
		
		echo 'Population complete';
		exit;
		
	}
	
	function index() {
	
	}
	
	function beforeFilter() {
		
		parent::beforeFilter();
		
		// ensure our ajax methods are posted
		$this->Security->requirePost('getnodes', 'reorder', 'reparent');
		
	}
	
	function getnodes() {
		
		// retrieve the node id that Ext JS posts via ajax
		$parent = intval($this->params['form']['node']);
		
		// find all the nodes underneath the parent node defined above
		// the second parameter (true) means we only want direct children
		$nodes = $this->Employee->children($parent, true);
		
		// send the nodes to our view
		$this->set(compact('nodes'));
		
	}
	
	function reorder() {
		
		// retrieve the node instructions from javascript
		// delta is the difference in position (1 = next node, -1 = previous node)
		
		$node = intval($this->params['form']['node']);
		$delta = intval($this->params['form']['delta']);
		
		if ($delta > 0) {
			$this->Employee->movedown($node, abs($delta));
		} elseif ($delta < 0) {
			$this->Employee->moveup($node, abs($delta));
		}
		
		// send success response
		exit('1');
		
	}
	
	function reparent(){
		
		$node = intval($this->params['form']['node']);
		$parent = intval($this->params['form']['parent']);
		$position = intval($this->params['form']['position']);
		
		// save the employee node with the new parent id
		// this will move the employee node to the bottom of the parent list
		
		$this->Employee->id = $node;
		$this->Employee->saveField('parent_id', $parent);
		
		// If position == 0, then we move it straight to the top
		// otherwise we calculate the distance to move ($delta).
		// We have to check if $delta > 0 before moving due to a bug
		// in the tree behaviour (https://trac.cakephp.org/ticket/4037)
		
		if ($position == 0) {
			$this->Employee->moveup($node, true);
		} else {
			$count = $this->Employee->childcount($parent, true);
			$delta = $count-$position-1;
			if ($delta > 0) {
				$this->Employee->moveup($node, $delta);
			}
		}
		
		// send success response
		exit('1');
		
	} 

}
?>
<?php
/**
* @version        $Id: ergo_ru_types_controller.php v1.0 13.03.2010 06:35:46 CET $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
-- Structure de la table `ergo_ru_types`
--

CREATE TABLE IF NOT EXISTS `ergo_ru_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lib` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `ergo_ru_types`
--

INSERT INTO `ergo_ru_types` (`id`, `lib`) VALUES
(1, 'Substantifs'),
(2, 'Verbes'),
(3, 'Adjectifs'),
(4, 'Pronoms'),
(5, 'Particules'),
(6, 'Conjonctions et prépositions'),
(7, 'Divers');
*/
?>
<?php
class ErgoRuTypesController extends AppController {

	var $name = 'ErgoRuTypes';
	var $helpers = array('Html', 'Form');
	var $components = array('RequestHandler','Auth');
	
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index','view');
	}

	function index() {
		$this->ErgoRuType->recursive = 0;
		$this->set('ergoRuTypes', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ErgoRuType.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('ergoRuType', $this->ErgoRuType->read(null, $id));
	}

	function add() {
					eject_non_admin(); //on autorise pas les non-administrateurs

		if (!empty($this->data)) {
			$this->ErgoRuType->create();
			if ($this->ErgoRuType->save($this->data)) {
				$this->Session->setFlash(__('The ErgoRuType has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoRuType could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
					eject_non_admin(); //on autorise pas les non-administrateurs

		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ErgoRuType', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoRuType->save($this->data)) {
				$this->Session->setFlash(__('The ErgoRuType has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoRuType could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoRuType->read(null, $id);
		}
	}

	function delete($id = null) {
					eject_non_admin(); //on autorise pas les non-administrateurs

		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ErgoRuType', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoRuType->del($id)) {
			$this->Session->setFlash(__('ErgoRuType deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}



}
?>

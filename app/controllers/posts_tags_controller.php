<?php
class PostsTagsController extends AppController {

	var $name = 'PostsTags';
	var $helpers = array('Html', 'Form');

	function index() {
		$this->PostsTag->recursive = 0;
		$this->set('postsTags', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid PostsTag.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('postsTag', $this->PostsTag->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->PostsTag->create();
			if ($this->PostsTag->save($this->data)) {
				$this->Session->setFlash(__('The PostsTag has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The PostsTag could not be saved. Please, try again.', true));
			}
		}
		$posts = $this->PostsTag->Post->find('list');
		$tags = $this->PostsTag->Tag->find('list');
		$this->set(compact('posts', 'tags'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid PostsTag', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->PostsTag->save($this->data)) {
				$this->Session->setFlash(__('The PostsTag has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The PostsTag could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->PostsTag->read(null, $id);
		}
		$posts = $this->PostsTag->Post->find('list');
		$tags = $this->PostsTag->Tag->find('list');
		$this->set(compact('posts','tags'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for PostsTag', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->PostsTag->del($id)) {
			$this->Session->setFlash(__('PostsTag deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>
<?php
class ErgoEnPhrasesController extends AppController {

	var $name = 'ErgoEnPhrases';

	var $components = array('RequestHandler','Auth');

	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index','view','search');
		#todo corriger analyse (explode!!!)
	}

	#criteres de tri
	var $paginate = array(
        'limit' => 25,
        'order' => array(
            'ErgoEnPhrase.titre' => 'asc'
        )
    );


	function index() {
		$this->ErgoEnPhrase->recursive = 0;
			if($_SESSION['Auth']['User']['group_id']!=3) { 
			$options = array(
					"ErgoEnPhrase.titre NOT LIKE '<!-- private -->%'"
					);
			$this->set('ergoEnPhrases', $this->paginate($options));
		} else {
		$this->set('ergoEnPhrases', $this->paginate());
	}
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ErgoEnPhrase.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('ergoEnPhrase', $this->ErgoEnPhrase->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ErgoEnPhrase->create();
			if ($this->ErgoEnPhrase->save($this->data)) {
				/*begin special function to add user id (usefull for killing spam if needed and "my" services */
					$utilisateur= $this->Session->read('Auth.User.username'); //whois the current user
					$utilisateurid= $this->Session->read('Auth.User.id'); //current user id
					$dernierid=mysql_insert_id();
					//store in the new word in the myvocs
					$sql="
					INSERT INTO `akademiach18`.`ergo_my_ru_phrases` (
					`id` ,
					`user_id` ,
					`voc_id` ,
					`date`
					)
					VALUES (
					NULL , 
					'$utilisateurid', 
					'$dernierid',
					CURRENT_TIMESTAMP
					)";
					$sql=mysql_query($sql);
				/*end special function to add user id (usefull for killing spam if needed and "my" services */
				$this->Session->setFlash(__('Enregistrement sauvegardé avec succès', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('L\'enregistrement n\'a pas pu être sauvegardé. Merci d\'essayer à nouveau.', true));
			}
		}
	}

	function edit($id = null) {
		eject_non_admin(); //on autorise pas les non-administrateurs
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ErgoEnPhrase', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoEnPhrase->save($this->data)) {
				$this->Session->setFlash(__('The ErgoEnPhrase has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoEnPhrase could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoEnPhrase->read(null, $id);
		}
	}

	function delete($id = null) {
						eject_non_admin(); //on autorise pas les non-administrateurs

		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ErgoEnPhrase', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoEnPhrase->del($id)) {
			$this->Session->setFlash(__('ErgoEnPhrase deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

function analyse() {

	}

    function inseremot() {

	}
}
?>

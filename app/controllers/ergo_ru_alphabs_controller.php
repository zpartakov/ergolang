<?php
class ErgoRuAlphabsController extends AppController {

	var $name = 'ErgoRuAlphabs';

	var $components = array('RequestHandler','Auth');
	
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('alphabet','vue', 'audio');
		if($this->RequestHandler->isAjax())
		{
			Configure::write('debug', 3);
		}
	}
	function alphabet() {
		$this->ErgoRuAlphab->recursive = 0;
		$this->set('ergoRuAlphabs', $this->paginate());
	}
	function index() {
		$this->ErgoRuAlphab->recursive = 0;
		$this->set('ergoRuAlphabs', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ergo ru alphab', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('ergoRuAlphab', $this->ErgoRuAlphab->read(null, $id));
	}
	
	function vue($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ergo ru alphab', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('ergoRuAlphab', $this->ErgoRuAlphab->read(null, $id));
	}

	function add() {
		eject_non_admin(); //on autorise pas les non-administrateurs
		
		if (!empty($this->data)) {
			$this->ErgoRuAlphab->create();
			if ($this->ErgoRuAlphab->save($this->data)) {
				$this->Session->setFlash(__('The ergo ru alphab has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ergo ru alphab could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		eject_non_admin(); //on autorise pas les non-administrateurs
		
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ergo ru alphab', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoRuAlphab->save($this->data)) {
				$this->Session->setFlash(__('The ergo ru alphab has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ergo ru alphab could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoRuAlphab->read(null, $id);
		}
	}

	function delete($id = null) {
		eject_non_admin(); //on autorise pas les non-administrateurs
		
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ergo ru alphab', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoRuAlphab->delete($id)) {
			$this->Session->setFlash(__('Ergo ru alphab deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Ergo ru alphab was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	
	function audio() {
	}
}

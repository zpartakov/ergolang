<?php
class ErgoRuStemsController extends AppController {

	var $name = 'ErgoRuStems';

	var $components = array('RequestHandler','Auth');

	#criteres de tri
	var $paginate = array(
        'limit' => 100,
        'order' => array(
            'ErgoRuStem.src' => 'asc'
        )
    );

	function index() {
		$this->ErgoRuStem->recursive = 0;
		$this->set('ergoRuStems', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(sprintf(__('Invalid %s', true), 'ergo ru stem'));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('ergoRuStem', $this->ErgoRuStem->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ErgoRuStem->create();
			if ($this->ErgoRuStem->save($this->data)) {
				$this->Session->setFlash(sprintf(__('The %s has been saved', true), 'ergo ru stem'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(sprintf(__('The %s could not be saved. Please, try again.', true), 'ergo ru stem'));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(sprintf(__('Invalid %s', true), 'ergo ru stem'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoRuStem->save($this->data)) {
				$this->Session->setFlash(sprintf(__('The %s has been saved', true), 'ergo ru stem'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(sprintf(__('The %s could not be saved. Please, try again.', true), 'ergo ru stem'));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoRuStem->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(sprintf(__('Invalid id for %s', true), 'ergo ru stem'));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoRuStem->delete($id)) {
			$this->Session->setFlash(sprintf(__('%s deleted', true), 'Ergo ru stem'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(sprintf(__('%s was not deleted', true), 'Ergo ru stem'));
		$this->redirect(array('action' => 'index'));
	}
	
	function export() {
		$this->layout = '';
		//see export.ctp, for creating the stemming file
	}
}
?>

<?php
/**
* @version        $Id: ergo_fr_vocs_controller.php v1.0 15.04.2010 06:04:00 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.

-- Structure de la table `ergo_fr_vocs`
--

CREATE TABLE IF NOT EXISTS `ergo_fr_vocs` (
  `id` int(14) NOT NULL AUTO_INCREMENT,
  `libelle` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `TYPE` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `genre` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `num` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51878 ;
*/
?>
<?php
class ErgoFrVocsController extends AppController {

	var $name = 'ErgoFrVocs';
	var $helpers = array('Html', 'Form');
	var $components = array('RequestHandler','Auth');

	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index','view','anagramme','mnemotechnique');
	}
	
	

		
		
		#criteres de tri
	var $paginate = array(
        'limit' => 100,
        'order' => array(
            'ErgoFrVoc.id' => 'desc'
        )
    );
    
	function index() {
		$this->ErgoFrVoc->recursive = 0;
			if($this->data['ErgoFrVoc']['q']) {
					$input = $this->data['ErgoFrVoc']['q']; 
					# sanitize the query
					App::import('Sanitize');
					$q = Sanitize::escape($input);
					
					$whole= $_POST['whole']; 
					$debut= $_POST['debut']; 
					$fin= $_POST['fin']; 
					$long= $_POST['long']; 
                    
				if($whole=="whole") {
						/* match exact word */
					$options = "ErgoFrVoc.libelle LIKE '" .$q ."'";
				} else if($debut=="init") {
					/* Debut de mot */
                    $options = "ErgoFrVoc.libelle LIKE '" .$q ."%'";
                  //  $options = "ErgoFrVoc.libelle REGEXP '" .$q ."%'";
				} else if($fin=="end") {
					/* Fin de mot */
					$options = "ErgoFrVoc.libelle LIKE '%" .$q ."'";
				} else {
						/* general default search */
					$options = "ErgoFrVoc.libelle LIKE '%" .$q ."%'";
				}
               //  echo "<br>".$options."<br>"; exit;
                 $options = array($options);
            	$this->set(array('ergoFrVocs' => $this->paginate('ErgoFrVoc', $options))); 
                } else {
                		$this->set('ergoFrVocs', $this->paginate());
                	}
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ErgoFrVoc.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('ergoFrVoc', $this->ErgoFrVoc->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ErgoFrVoc->create();
			if ($this->ErgoFrVoc->save($this->data)) {
				$this->Session->setFlash(__('The ErgoFrVoc has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoFrVoc could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		eject_non_admin(); //on autorise pas les non-administrateurs

		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ErgoFrVoc', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoFrVoc->save($this->data)) {
				$this->Session->setFlash(__('The ErgoFrVoc has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoFrVoc could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoFrVoc->read(null, $id);
		}
	}

	function delete($id = null) {
		eject_non_admin(); //on autorise pas les non-administrateurs

		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ErgoFrVoc', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoFrVoc->del($id)) {
			$this->Session->setFlash(__('ErgoFrVoc deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
	
	/* une fonction pour calculer des anagrammes */
	function anagramme() {
	}
	/* une page pour calculer des éléments mnémotechniques */
	function mnemotechnique() {
	}
	
		/* une fonction pour créer la liste alphabétique des mots (calcul anagrammes) */
	function alphabeta() {
	}
}
?>

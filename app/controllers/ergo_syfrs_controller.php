<?php
class ErgoSyfrsController extends AppController {

	var $name = 'ErgoSyfrs';
		var $components = array('RequestHandler','Auth');
	var $helpers = array('Html', 'Form');
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index','view','autocomplete');
		if($this->RequestHandler->isAjax())
		{
			Configure::write('debug', 3);
		}
	}
		#criteres de tri
	var $paginate = array(
        'limit' => 100,
        'order' => array(
            'ErgoSyfr.id' => 'desc'
        )
    );
	function index() {
		$this->ErgoSyfr->recursive = 0;
						if($this->data['ErgoSyfr']['q']) {
					$input = $this->data['ErgoSyfr']['q']; 
					# sanitize the query
					App::import('Sanitize');
					$q = Sanitize::escape($input);
					$options = array(
					"ErgoSyfr.local LIKE '%" .$q ."%'" ." OR ErgoSyfr.foreign LIKE '%" .$q ."%'"
					);
					$this->set(array('ergoSyfrs' => $this->paginate('ErgoSyfr', $options))); 
		} else {
		$this->set('ergoSyfrs', $this->paginate());
	}
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ergo syfr', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('ergoSyfr', $this->ErgoSyfr->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ErgoSyfr->create();
			if ($this->ErgoSyfr->save($this->data)) {
				$this->Session->setFlash(__('The ergo syfr has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ergo syfr could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ergo syfr', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoSyfr->save($this->data)) {
				$this->Session->setFlash(__('The ergo syfr has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ergo syfr could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoSyfr->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ergo syfr', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoSyfr->delete($id)) {
			$this->Session->setFlash(__('Ergo syfr deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Ergo syfr was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>

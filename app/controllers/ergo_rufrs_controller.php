<?php
/**
* @version        $Id: ergo_rufrs_controller.php v1.0 30.12.2009 08:43:00 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
* 
* 
CREATE TABLE IF NOT EXISTS `ergo_rufrs` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `foreign` text COLLATE utf8_unicode_ci NOT NULL,
  `local` text COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9166 ;
*/
class ErgoRufrsController extends AppController {

	var $name = 'ErgoRufrs';
	var $components = array('RequestHandler','Auth');
		
function beforeFilter() {
	parent::beforeFilter();
    $this->Auth->allow('index','view','autocomplete', 'categories', 'export');
    if($this->RequestHandler->isAjax())
		{
			Configure::write('debug', 3);
		}
}

	#criteres de tri
	var $paginate = array(
        'limit' => 100,
        'order' => array(
            'ErgoRufr.foreign' => 'asc'
        )
    );
    
	function index() {
		$this->ErgoRufr->recursive = 0;
		//var $_GET['user'];
		if($this->data['ErgoRufr']['q']||$_GET['alphabet']) {
					if($_GET['alphabet']) {
						$input = $_GET['alphabet']; 
						
					} else {
						$input = $this->data['ErgoRufr']['q']; 
					}
					if(!$_GET['alphabet']) {
						$boole = $this->data['ErgoRufr']['boole']; 
											} else {
						$boole="begin";

					}
					#echo "<br>boole: " .$boole ."<br>";
					# sanitize the query
					App::import('Sanitize');
					$q = Sanitize::escape($input);
					$options = array(
					"ErgoRufr.foreign LIKE '" .$q ."'"
					);
					
					#$result=$this->ErgoRufr->find('count', array('conditions' => array('ErgoRufr.foreign LIKE ' => '"$q"'))); 
					$result=$this->ErgoRufr->find('count', array('conditions' => $options)); 
					#echo "<font color=red>hits # " .$result ."</font><hr>";
					
					if($result==1) { //exact match
					$this->set(array('ergoRufrs' => $this->paginate('ErgoRufr', $options))); 
					} else { //no exact match
					
					if($boole=="any") {
						$options = array(
							"ErgoRufr.local LIKE '%" .$q ."%'" ." OR ErgoRufr.foreign LIKE '%" .$q ."%'"
						);
					}	elseif($boole=="end") {
						$options = array(
							"ErgoRufr.local LIKE '%" .$q ."'" ." OR ErgoRufr.foreign LIKE '%" .$q ."'"
						);
					}	elseif($boole=="begin") {
						if($_GET['alphabet']) {
													$alphabetCyr=array(
							"а",
							"б",
							"в",
							"г",
							"д",
							"е",
							"ж",
							"з",
							"и",
							"й",
							"к",
							"л",
							"м",
							"н",
							"о",
							"п",
							"р",
							"с",
							"т",
							"у",
							"ф",
							"х",
							"ц",
							"ч",
							"ш",
							"щ",
							"ъ",
							"ы",
							"ь",
							"э",
							"ю",
							"я"
						);
						$q=$alphabetCyr[$input];

							$options = array(
								"ErgoRufr.foreign LIKE '" .$q ."%'"
							);
							#echo "ErgoRufr.foreign LIKE '" .$q ."%'"; exit;
						if($result<1) { //exact match
						echo "yopyop"; exit;
							}	
						} else {
							$options = array(
								"ErgoRufr.local LIKE '" .$q ."%'" ." OR ErgoRufr.foreign LIKE '" .$q ."%'"
							);
						}
						#echo "test"; exit;
					}
					
					$this->set(array('ergoRufrs' => $this->paginate('ErgoRufr', $options))); 
					
					}
} else {
		
		$this->set('ergoRufrs', $this->paginate());
	}
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ErgoRufr.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('ergoRufr', $this->ErgoRufr->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ErgoRufr->create();
			if ($this->ErgoRufr->save($this->data)) {
				/*begin special function to add user id (usefull for killing spam if needed and "my" services */
					$utilisateur= $this->Session->read('Auth.User.username'); //whois the current user
					$utilisateurid= $this->Session->read('Auth.User.id'); //current user id
					$dernierid=mysql_query("SELECT *
						FROM `ergo_rufrs`
						ORDER BY id DESC
						LIMIT 0 , 1");
					$dernierid=mysql_result($dernierid,0,'id');

					//store in the new word in the myvocs
					$sql="
					INSERT INTO `akademiach18`.`ergo_my_ru_vocs` (
					`id` ,
					`user_id` ,
					`ergo_ru_voc_id` ,
					`date`
					)
					VALUES (
					NULL , 
					'$utilisateurid', 
					'$dernierid',
					CURRENT_TIMESTAMP
					)";
					$sql=mysql_query($sql);
				/*end special function to add user id (usefull for killing spam if needed and "my" services */
				/*begin special function to add category */
				if(strlen($_POST['category'])>1){
				$utilisateurid= $this->Session->read('Auth.User.id'); //current user id
				$sql="INSERT INTO `akademiach18`.`ergo_motsassocies` (`id`, `lang`, `user_id`, `ergo_langues_id`, `category_id`, `niveau`, `rem`) VALUES (NULL, 'ru', '".$utilisateurid."', '" .$dernierid ."', '" .$_POST['category'] ."', '0', '')";
				$sql=mysql_query($sql);
				/*end special function to add category */
				}
				$this->Session->setFlash(__('Enregistrement sauvegardé avec succès', true));
				#$this->redirect(array('action'=>'index')); //old
				$this->redirect(array('action'=>'add'));
			} else {
				$this->Session->setFlash(__('L\'enregistrement n\'a pas pu être sauvegardé. Merci d\'essayer à nouveau.', true));
			}
		}
	}

	function edit($id = null) {
		eject_non_admin(); //on autorise pas les non-administrateurs
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ErgoRufr', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {

			if ($this->ErgoRufr->save($this->data)) {
				/*begin special function to add category */
				/*echo phpinfo();
				echo "blabla: " .$_POST['data']['category'];
				if(strlen($_POST['category'])>1){
				$utilisateurid= $this->Session->read('Auth.User.id'); //current user id
				$sql="INSERT INTO `akademiach18`.`categories_ergo_rufrs` (`id`, `category_id`, `ergo_rufr_id`, `user_id`) VALUES (NULL, '" .$_POST['category'] ."', '".$id ."', '".$utilisateurid ."')";
				echo "<br>SQL: " .$sql ."<br>";
				$sql=mysql_query($sql);
				}*/
								/*end special function to add category */

				$this->Session->setFlash(__('The ErgoRufr has been saved', true));
				#$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoRufr could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {

			$this->data = $this->ErgoRufr->read(null, $id);
		}
		
//todo radeff 1.5.2010 11h55 £
		$categories = $this->ErgoRufr->Category->find('list');
		$this->set(compact('categories'));
	}

	function delete($id = null) {
		/*//eject_non_admin(); //£ on autorise pas les non-administrateurs

		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ErgoRufr', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoRufr->del($id)) {
			$this->Session->setFlash(__('ErgoRufr deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		*/
	}


/* local functions
 * */
  
   function autocomplete()
	{
		$recherche = utf8_decode($this->data['ErgoRufr']['q']);
		$lergorufrs = $this->ErgoRufr->find(
			'all',
			array(
				'fields' => 'DISTINCT local',
				'conditions' => "local LIKE '$recherche%'",
				'order' => 'local',
				'limit' => 10
			)
		);
 
		$this->set(compact('lergorufrs', 'recherche'));
	}




function rumtrain() {//a function to random drill on russian vocabulary
}
function govoc() { //function to analyse results from rumtrain
}


function rumtraincat() { //a function to drill on a selected category
}

function myrumtrain() {//a function to random drill on already drilled russian vocabulary
	}

function mydrill() {//a function to print the already drilled russian vocabulary
}	
	
function gomyvoc() { //function to analyse results from myrumtrain
//do not display layout
	$this->layout = '';
}	
	

function categories() {
}

function tri_voc() { 
	//kind a puzzle drill
}


function export() { //sqlexport
}

function exportcsv() { //csvexport
}

function gadd() { //a function to add word translated by google
	$sql= "
	INSERT INTO `akademiach18`.`ergo_rufrs` (
	`id` ,
	`foreign` ,
	`local` ,
	`type` ,
	`date`
	)
	VALUES (
	NULL , '" .$_GET['foreign'] ."', '" .$_GET['local'] ."', '',
	CURRENT_TIMESTAMP
	);
	";
						$sql=mysql_query($sql);

	/*begin special function to add user id (usefull for killing spam if needed and "my" services */
						$utilisateur= $this->Session->read('Auth.User.username'); //whois the current user
						$utilisateurid= $this->Session->read('Auth.User.id'); //current user id
						$dernierid=mysql_query("SELECT *
							FROM `ergo_rufrs`
							ORDER BY id DESC
							LIMIT 0 , 1");
						$dernierid=mysql_result($dernierid,0,'id');

						//store in the new word in the myvocs
						$sql="
						INSERT INTO `akademiach18`.`ergo_my_ru_vocs` (
						`id` ,
						`user_id` ,
						`ergo_ru_voc_id` ,
						`date`
						)
						VALUES (
						NULL , 
						'$utilisateurid', 
						'$dernierid',
						CURRENT_TIMESTAMP
						)";
						$sql=mysql_query($sql);
					/*end special function to add user id (usefull for killing spam if needed and "my" services */
	$this->redirect($this->referer()."#mot" .$_GET['retour'], null, true);
	}

}
?>

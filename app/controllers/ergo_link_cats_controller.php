<?php
/**
* @version        $Id: ergo_link_cats_controller.php v1.0 30.01.2010 08:47:34 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
class ErgoLinkCatsController extends AppController {

	var $name = 'ErgoLinkCats';
	var $helpers = array('Html', 'Form');
		var $components = array('RequestHandler','Auth');
		
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index','view','search');
		#todo corriger analyse (explode!!!)
	}
	#criteres de tri
	var $paginate = array(
        'limit' => 25,
        'order' => array(
            'ErgoLinkCat.lib' => 'asc'
        )
    );
	function index() {
		$this->ErgoLinkCat->recursive = 0;
		$this->set('ergoLinkCats', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ErgoLinkCat.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('ergoLinkCat', $this->ErgoLinkCat->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ErgoLinkCat->create();
			if ($this->ErgoLinkCat->save($this->data)) {
				$this->Session->setFlash(__('The ErgoLinkCat has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoLinkCat could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
				eject_non_admin(); //on autorise pas les non-administrateurs

		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ErgoLinkCat', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoLinkCat->save($this->data)) {
				$this->Session->setFlash(__('The ErgoLinkCat has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoLinkCat could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoLinkCat->read(null, $id);
		}
	}

	function delete($id = null) {
				eject_non_admin(); //on autorise pas les non-administrateurs

		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ErgoLinkCat', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoLinkCat->del($id)) {
			$this->Session->setFlash(__('ErgoLinkCat deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}



}
?>

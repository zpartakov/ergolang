<?php
/**
* @version        $Id: ergo_esfrs_controller.php v1.0 14.10.2010 16:50:51 CEST $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

class ErgoEsfrsController extends AppController {

	var $name = 'ErgoEsfrs';
var $components = array('RequestHandler','Auth');

	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index','view','autocomplete');
		if($this->RequestHandler->isAjax())
		{
			Configure::write('debug', 3);
		}
	}
	
		#criteres de tri
	var $paginate = array(
        'limit' => 100,
        'order' => array(
            'ErgoEsfr.id' => 'desc'
        )
    );

	
	function index() {
		$this->ErgoEsfr->recursive = 0;
			//var $_GET['user'];
		if($this->data['ErgoEsfr']['q']) {
					$input = $this->data['ErgoEsfr']['q']; 
					# sanitize the query
					App::import('Sanitize');
					$q = Sanitize::escape($input);
					$options = array(
					"ErgoEsfr.local LIKE '%" .$q ."%'" ." OR ErgoEsfr.foreign LIKE '%" .$q ."%'"
					);
					$this->set(array('ergoEsfrs' => $this->paginate('ErgoEsfr', $options))); 
		} else {
		$this->ErgoEsfr->recursive = 0;
		$this->set('ergoEsfrs', $this->paginate());
	}
}



	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ergo esfr', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('ergoEsfr', $this->ErgoEsfr->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ErgoEsfr->create();
			if ($this->ErgoEsfr->save($this->data)) {
				
				/*begin special function to add user id (usefull for killing spam if needed and "my" services */
					$utilisateur= $this->Session->read('Auth.User.username'); //whois the current user
					$utilisateurid= $this->Session->read('Auth.User.id'); //current user id
					$dernierid=mysql_query("SELECT *
						FROM `ergo_esfrs`
						ORDER BY id DESC
						LIMIT 0 , 1");
					$dernierid=mysql_result($dernierid,0,'id');
					//store in the new word in the myvocs
					$sql="
					INSERT INTO `akademiach18`.`ergo_my_es_vocs` (
					`id` ,
					`user_id` ,
					`voc_id` ,
					`date`
					)
					VALUES (
					NULL , 
					'$utilisateurid', 
					'$dernierid',
					CURRENT_TIMESTAMP
					)";
					$sql=mysql_query($sql);
				/*end special function to add user id (usefull for killing spam if needed and "my" services */
				
				
				
				$this->Session->setFlash(__('The ergo esfr has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ergo esfr could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ergo esfr', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoEsfr->save($this->data)) {
				$this->Session->setFlash(__('The ergo esfr has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ergo esfr could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoEsfr->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ergo esfr', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoEsfr->delete($id)) {
			$this->Session->setFlash(__('Ergo esfr deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Ergo esfr was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>

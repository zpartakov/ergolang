<?php
class ErgoArafrsController extends AppController {

	var $name = 'ErgoArafrs';
		var $components = array('RequestHandler','Auth');
	var $helpers = array('Html', 'Form');
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index','view','autocomplete');
		if($this->RequestHandler->isAjax())
		{
			Configure::write('debug', 3);
		}
	}
		#criteres de tri
	var $paginate = array(
        'limit' => 100,
        'order' => array(
            'ErgoArafr.id' => 'desc'
        )
    );
    
	function index() {
		$this->ErgoArafr->recursive = 0;
				if($this->data['ErgoArafr']['q']) {
					$input = $this->data['ErgoArafr']['q']; 
					# sanitize the query
					App::import('Sanitize');
					$q = Sanitize::escape($input);
					$options = array(
					"ErgoArafr.local LIKE '%" .$q ."%'" ." OR ErgoArafr.foreign LIKE '%" .$q ."%'"
					);
					$this->set(array('ergoArafrs' => $this->paginate('ErgoArafr', $options))); 
		} else {
		$this->set('ergoArafrs', $this->paginate());
	}
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ergo arafr', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('ergoArafr', $this->ErgoArafr->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ErgoArafr->create();
			if ($this->ErgoArafr->save($this->data)) {
				$this->Session->setFlash(__('The ergo arafr has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ergo arafr could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
//				eject_non_admin(); //on autorise pas les non-administrateurs
eject_non_admin_grp($_SESSION['Auth']['User']['group_id'],$_SESSION['langue']);//on autorise pas les non-administrateurs
	#exit;
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ergo arafr', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoArafr->save($this->data)) {
				$this->Session->setFlash(__('The ergo arafr has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ergo arafr could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoArafr->read(null, $id);
		}
	}

	function delete($id = null) {
eject_non_admin_grp($_SESSION['Auth']['User']['group_id'],$_SESSION['langue']);//on autorise pas les non-administrateurs

		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ergo arafr', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoArafr->delete($id)) {
			$this->Session->setFlash(__('Ergo arafr deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Ergo arafr was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>

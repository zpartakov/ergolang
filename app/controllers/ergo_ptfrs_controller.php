<?php
class ErgoPtfrsController extends AppController {

	var $name = 'ErgoPtfrs';
	var $components = array('RequestHandler','Auth');
			#criteres de tri
			
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index','view','autocomplete');

	}
	var $paginate = array(
        'limit' => 100,
        'order' => array(
            'ErgoPtfr.id' => 'desc'
        )
    );
	function index() {
		$this->ErgoPtfr->recursive = 0;
						if($this->data['ErgoPtfr']['q']) {
					$input = $this->data['ErgoPtfr']['q']; 
					# sanitize the query
					App::import('Sanitize');
					$q = Sanitize::escape($input);
					$options = array(
					"ErgoPtfr.local LIKE '%" .$q ."%'" ." OR ErgoPtfr.foreign LIKE '%" .$q ."%' OR ErgoPtfr.cat LIKE '%" .$q ."%'"
					);
					$this->set(array('ergoPtfrs' => $this->paginate('ErgoPtfr', $options))); 
		} else {
		$this->set('ergoPtfrs', $this->paginate());
		}
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ergo ptfr', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('ergoPtfr', $this->ErgoPtfr->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ErgoPtfr->create();
			if ($this->ErgoPtfr->save($this->data)) {
				$this->Session->setFlash(__('The ergo ptfr has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ergo ptfr could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ergo ptfr', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoPtfr->save($this->data)) {
				$this->Session->setFlash(__('The ergo ptfr has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ergo ptfr could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoPtfr->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ergo ptfr', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoPtfr->delete($id)) {
			$this->Session->setFlash(__('Ergo ptfr deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Ergo ptfr was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}

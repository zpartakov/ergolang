<?php
class ErgoRuDeclinaisonsNomsController extends AppController {

	var $name = 'ErgoRuDeclinaisonsNoms';
	var $helpers = array('Html', 'Form');

	function index() {
		$this->ErgoRuDeclinaisonsNom->recursive = 0;
		$this->set('ergoRuDeclinaisonsNoms', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ErgoRuDeclinaisonsNom.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('ergoRuDeclinaisonsNom', $this->ErgoRuDeclinaisonsNom->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ErgoRuDeclinaisonsNom->create();
			if ($this->ErgoRuDeclinaisonsNom->save($this->data)) {
				$this->Session->setFlash(__('The ErgoRuDeclinaisonsNom has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoRuDeclinaisonsNom could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ErgoRuDeclinaisonsNom', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoRuDeclinaisonsNom->save($this->data)) {
				$this->Session->setFlash(__('The ErgoRuDeclinaisonsNom has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoRuDeclinaisonsNom could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoRuDeclinaisonsNom->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ErgoRuDeclinaisonsNom', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoRuDeclinaisonsNom->del($id)) {
			$this->Session->setFlash(__('ErgoRuDeclinaisonsNom deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>
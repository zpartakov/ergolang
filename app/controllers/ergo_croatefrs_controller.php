<?php
class ErgoCroatefrsController extends AppController {

	var $name = 'ErgoCroatefrs';
		var $components = array('RequestHandler','Auth');

	var $helpers = array('Html', 'Form');
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index','view','export');
		if($this->RequestHandler->isAjax())
		{
			Configure::write('debug', 3);
		}
	}
		#criteres de tri
	var $paginate = array(
        'limit' => 100,
        'order' => array(
            'ErgoCroatefr.id' => 'desc'
        )
    );
	function index() {
		$this->ErgoCroatefr->recursive = 0;
		if($this->data['ErgoCroatefr']['q']) {
					$input = $this->data['ErgoCroatefr']['q']; 
					# sanitize the query
					App::import('Sanitize');
					$q = Sanitize::escape($input);
					$options = array(
					"ErgoCroatefr.local LIKE '%" .$q ."%'" ." OR ErgoCroatefr.foreign LIKE '%" .$q ."%'"
					);
					$this->set(array('ErgoCroatefrs' => $this->paginate('ErgoCroatefr', $options))); 
		} else {
		$this->set('ErgoCroatefrs', $this->paginate());
	}
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ErgoCroatefr.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('ErgoCroatefr', $this->ErgoCroatefr->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ErgoCroatefr->create();
			if ($this->ErgoCroatefr->save($this->data)) {
				$this->Session->setFlash(__('The ErgoCroatefr has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoCroatefr could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ErgoCroatefr', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoCroatefr->save($this->data)) {
				$this->Session->setFlash(__('The ErgoCroatefr has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoCroatefr could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoCroatefr->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ErgoCroatefr', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoCroatefr->del($id)) {
			$this->Session->setFlash(__('ErgoCroatefr deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

function export() { //sqlexport
}
function exportcsv() { //csvexport
}
}
?>

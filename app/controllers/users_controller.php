<?php
/**
* @version        $Id: users_controller.php v1.0 30.12.2009 09:01:43 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
* 
* 
* todo: add cookie http://www.webdevelopment2.com/cakephp-auth-component-tutorial-3/
* http://www.webdevelopment2.com/cakephp-auth-component-tutorial-2/
* 
* CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(40) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `group_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

*/
class UsersController extends AppController {

	var $name = 'Users';
	var $components = array('RequestHandler','Auth');


  function beforeFilter() {
		$this->Auth->allow('login','logout','langue', 'passwordreminder', 'renvoiemail', 'confirmation');
	 }

	function login() {
		/*$this->loadModel('ErgoLangue');*/
		# $langues = $this->ErgoLangue->find('all');
		$this -> Session -> write("variable", "value");
		#$this->redirect(array('page'=>'home'));
	}


    function logout()
    {
	#destroy session language
	$this->Session->destroy("langue");
	$this->Session->setFlash("Vous êtes maintenant déconnecté.");
	$this->redirect($this->Auth->logout());
    } 
    
    	#criteres de tri
	var $paginate = array(
        'limit' => 25,
        'order' => array(
            'User.username' => 'asc'
        )
    );
	function index() {
		eject_non_admin(); //on autorise pas les non-administrateurs
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
	}

	function view($id = null) {
		eject_non_admin(); //on autorise pas les non-administrateurs
		if (!$id) {
			$this->Session->setFlash(__('Invalid User.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('user', $this->User->read(null, $id));
	}

	function add() {
		eject_non_admin(); //on autorise pas les non-administrateurs
		if (!empty($this->data)) {
			$this->User->create();
			if ($this->User->save($this->data)) {
				$this->Session->setFlash(__('The User has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The User could not be saved. Please, try again.', true));
			}
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}

	function edit($id = null) {
		eject_non_admin(); //on autorise pas les non-administrateurs
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid User', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->User->save($this->data)) {
				$this->Session->setFlash(__('The User has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The User could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->User->read(null, $id);
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}

	function delete($id = null) {
		eject_non_admin(); //on autorise pas les non-administrateurs
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for User', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->User->del($id)) {
			$this->Session->setFlash(__('User deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}



### choosen learning language at begin ###
function langue() {
		$lalangue=$_GET['langue'];
		$this -> Session -> write("langue", $lalangue);
		$utilisateur= $this->Session->read('Auth.User.id');
		#echo "langue: " .$lalangue ." / utilisateur: " .$utilisateur; exit;
		if(strlen($utilisateur)>1) {
		//store to db for next login
		maj_session($utilisateur,$lalangue);
	}
#header("Location: /websites/ergolang/cake/".$lalangue);
#exit;	
}

#send password to users
function passwordreminder() {
}





function renvoiemail() {
	
	$email=$_GET['email'];
	if(!$email) {
		echo "<h1>Merci de fournir votre email!";
		echo '<br /><a href="javascript:history.go(-1)">Retour</a></h1>';
		exit;
	}
	$confirm="SELECT * FROM users WHERE email LIKE '" .$email ."'";
		//echo "<br>" .$confirm ."<br>"; //tests
		
		//compte demo
		if($email=="ergolang@akademia.ch") {
					error_reporting(0);
		echo "Vous ne pouvez pas vous faire renvoyer un mot de passe pour le compte démo!";
		echo "<br />Votre adresse IP " .$_SERVER["REMOTE_ADDR"] ." a été enregistrée";
		exit;
		}
		
	$confirm=mysql_query($confirm);
	if(!$confirm) {
		echo "SQL error: " .mysql_error(); exit;
	}
	if(mysql_num_rows($confirm)=="1") { //user email ok
	$login=mysql_result($confirm,0,'username');
	#génère password
		$pass=""; $length=8;
		$vowels = array("a",  "e",  "i",  "o",  "u",  "ae",  "ou",  "io",  
                     "ea",  "ou",  "ia",  "ai"); 
		 // A List of Consonants and Consonant sounds that we can insert
		 // into the password string
		 $consonants = array("b",  "c",  "d",  "g",  "h",  "j",  "k",  "l",  "m",
							 "n",  "p",  "r",  "s",  "t",  "u",  "v",  "w",  
							 "tr",  "cr",  "fr",  "dr",  "wr",  "pr",  "th",
							 "ch",  "ph",  "st",  "sl",  "cl");
		 // For the call to rand(), saves a call to the count() function
		 // on each iteration of the for loop
		 $vowel_count = count($vowels);
		 $consonant_count = count($consonants);
		 // From $i .. $length, fill the string with alternating consonant
		 // vowel pairs.
		 for ($i = 0; $i < $length; ++$i) {
			 $pass .= $consonants[rand(0,  $consonant_count - 1)] .
					  $vowels[rand(0,  $vowel_count - 1)];
		 }
		 
		 // Since some of our consonants and vowels are more than one
		 // character, our string can be longer than $length, use substr()
		 // to truncate the string
			$password=substr($pass,  0,  $length);
	//echo "L'utilisateur avec le mail " .$email ." existe, voici son nouveau mdp: " .$password; //tests
	#$hpassword = Security::hash($password);
	
//hashed mao123=b997ab87506787144928a87c3040f316bbebb937
	//echo "<br>Hashed password = " .$hpassword ."<br>"; //tests
		#$hpassword = Security::hash("mao123");
		$hash=Configure::read('Security.salt');
		$hpassword=sha1($hash.$password);
#echo $hpassword; exit;

	$confirm="UPDATE users SET password = '" .$hpassword ."' WHERE email LIKE '" .$email ."'";
	//echo "<br>".$confirm."<br>"; //tests
	
	$confirm=mysql_query($confirm);
	if(!$confirm) {
		echo "SQL error: " .mysql_error(); exit;
	}
	$textemail="Vous - ou quelqu'un se faisant passer pour vous - a demandé à se faire renvoyer à cet email un mot de passe;
	<br><br>Votre identifiant: " .$login ."<br>Votre nouveau mot de passe: " .$password;
	$textemail.='<br><br>Se connecter à Эrgolang: <a href="http://oblomov.info/websites/ergolang/cake/users/login">http://oblomov.info/websites/ergolang/cake/users/login</a><br>';
	#echo "<br>" .$textemail ."<br>"; //tests
$Destinataire = $email;
$Sujet = "Nouveau mot de passe Эrgolang";
 
$From  = "From: ergolang@akademia.ch\n";
$From .= "MIME-version: 1.0\n";
$From .= "Content-type: text/html; charset= UTF-8\n";
 
$Message = $textemail;
 
$envoie=mail($Destinataire,$Sujet,$Message,$From);
if(!$envoie) { echo "Problem sending email!"; }

			echo '<meta http-equiv="refresh" content="0;URL=/websites/ergolang/cake/users/confirmation">';

	
	} else { //user email not registered, potential hack
	// Désactiver le rapport d'erreurs
		error_reporting(0);
		#echo phpinfo();
		echo "L'email " .$email ." n'est pas enregistré dans notre base de données, votre adresse IP " .$_SERVER["REMOTE_ADDR"] ." a été enregistrée";
		exit;
	}
	}

function confirmation() { //page to redirect after new password
}

}
?>

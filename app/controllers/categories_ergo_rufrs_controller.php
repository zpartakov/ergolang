<?php
class CategoriesErgoRufrsController extends AppController {

	var $name = 'CategoriesErgoRufrs';
	var $helpers = array('Html', 'Form');
		var $components = array('RequestHandler','Auth');

	function index() {
		$this->CategoriesErgoRufr->recursive = 0;
		$this->set('categoriesErgoRufrs', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid CategoriesErgoRufr.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('categoriesErgoRufr', $this->CategoriesErgoRufr->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->CategoriesErgoRufr->create();
			if ($this->CategoriesErgoRufr->save($this->data)) {
				
							if(ereg("ergoRufrs",$_SERVER["HTTP_REFERER"])){
			//redirect to previous page
			$this->redirect($this->referer(), null, true); 
		} else {
				
				$this->Session->setFlash(__('The CategoriesErgoRufr has been saved', true));
				$this->redirect(array('action'=>'index'));
			}
			} else {
				$this->Session->setFlash(__('The CategoriesErgoRufr could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid CategoriesErgoRufr', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->CategoriesErgoRufr->save($this->data)) {
				$this->Session->setFlash(__('The CategoriesErgoRufr has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The CategoriesErgoRufr could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->CategoriesErgoRufr->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for CategoriesErgoRufr', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->CategoriesErgoRufr->del($id)) {
			if(ereg("ergoRufrs",$_SERVER["HTTP_REFERER"])){
			//redirect to previous page
			$this->redirect($this->referer(), null, true); 
		} else {
			$this->Session->setFlash(__('CategoriesErgoRufr deleted', true));
			$this->redirect(array('action'=>'index'));
			
		}

			
		}
	}
	
	function tuer($id = null) {
	}

}
?>

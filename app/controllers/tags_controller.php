<?php
class TagsController extends AppController {

	var $name = 'Tags';
	var $components = array('RequestHandler','Auth');

function beforeFilter() {
	parent::beforeFilter();
    $this->Auth->allow('index','view');
}
	#criteres de tri
	var $paginate = array(
        'limit' => 100,
        'order' => array(
            'Tag.longname' => 'asc'
        )
    );
	function index() {
		$this->Tag->recursive = 1;
		if($this->data['Tag']['q']) {
					$input = $this->data['Tag']['q']; 
					# sanitize the query
					App::import('Sanitize');
					$q = Sanitize::escape($input);
					$options = array(
					"Tag.name LIKE '%" .$q ."%'" ." OR Tag.longname LIKE '%" .$q ."%'"
					);
					$this->set(array('tags' => $this->paginate('Tag', $options))); 
		} else {
		$this->set('tags', $this->paginate());
		}
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(sprintf(__('Invalid %s', true), 'tag'));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('tag', $this->Tag->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Tag->create();
			if ($this->Tag->save($this->data)) {
				$this->Session->setFlash(sprintf(__('The %s has been saved', true), 'tag'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(sprintf(__('The %s could not be saved. Please, try again.', true), 'tag'));
			}
		}
		$ergoLinks = $this->Tag->ErgoLink->find('list');
		$posts = $this->Tag->Post->find('list');
		$this->set(compact('ergoLinks', 'posts'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(sprintf(__('Invalid %s', true), 'tag'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Tag->save($this->data)) {
				$this->Session->setFlash(sprintf(__('The %s has been saved', true), 'tag'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(sprintf(__('The %s could not be saved. Please, try again.', true), 'tag'));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Tag->read(null, $id);
		}
		$ergoLinks = $this->Tag->ErgoLink->find('list');
		$posts = $this->Tag->Post->find('list');
		$this->set(compact('ergoLinks', 'posts'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(sprintf(__('Invalid id for %s', true), 'tag'));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Tag->delete($id)) {
			$this->Session->setFlash(sprintf(__('%s deleted', true), 'Tag'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(sprintf(__('%s was not deleted', true), 'Tag'));
		$this->redirect(array('action' => 'index'));
	}
}
?>

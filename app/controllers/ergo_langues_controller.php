<?php
/**
* @version        $Id: ergo_langues_controller.php v1.0 02.03.2010 13:26:23 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
* 
* CREATE TABLE IF NOT EXISTS `ergo_langues` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `code` varchar(3) NOT NULL,
  `lib` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;
*/


?><?php
class ErgoLanguesController extends AppController {

	var $name = 'ErgoLangues';
	var $helpers = array('Html', 'Form');
	//unallow to everybody
	var $components = array('RequestHandler','Auth');
	function beforeFilter() {
		//forbidden zone to non-admin users
			if($this->Auth->user('group_id')!=3) {
			$this->Session->setFlash(__('Action not allowed!', true));
			exit;
		}
	}	

    	#criteres de tri
	var $paginate = array(
        'limit' => 25,
        'order' => array(
            'ErgoLangue.lib' => 'asc'
        )
    );
	function index() {
		$this->ErgoLangue->recursive = 0;
		$this->set('ergoLangues', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ErgoLangue.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('ergoLangue', $this->ErgoLangue->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ErgoLangue->create();
			if ($this->ErgoLangue->save($this->data)) {
				$this->Session->setFlash(__('The ErgoLangue has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoLangue could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ErgoLangue', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoLangue->save($this->data)) {
				$this->Session->setFlash(__('The ErgoLangue has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoLangue could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoLangue->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ErgoLangue', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoLangue->del($id)) {
			$this->Session->setFlash(__('ErgoLangue deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>

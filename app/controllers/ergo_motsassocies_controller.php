<?php
class ErgoMotsassociesController extends AppController {

	var $name = 'ErgoMotsassocies';
	var $helpers = array('Html', 'Form');
var $components = array('RequestHandler','Auth');

	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index','view');
		#todo corriger analyse (explode!!!)
	}
	
	function index() {
		$this->ErgoMotsassocy->recursive = 0;
		$this->set('ergoMotsassocies', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ErgoMotsassocy.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('ergoMotsassocy', $this->ErgoMotsassocy->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ErgoMotsassocy->create();
			if ($this->ErgoMotsassocy->save($this->data)) {
				$this->Session->setFlash(__('The ErgoMotsassocy has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoMotsassocy could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
						eject_non_admin(); //on autorise pas les non-administrateurs

		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ErgoMotsassocy', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoMotsassocy->save($this->data)) {
				$this->Session->setFlash(__('The ErgoMotsassocy has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoMotsassocy could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoMotsassocy->read(null, $id);
		}
	}

	function delete($id = null) {
						eject_non_admin(); //on autorise pas les non-administrateurs

		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ErgoMotsassocy', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoMotsassocy->del($id)) {
			$this->Session->setFlash(__('ErgoMotsassocy deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}



}
?>

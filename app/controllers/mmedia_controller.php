<?php
class MmediaController extends AppController {

	var $name = 'Mmedia';

	function index() {
		$this->Mmedium->recursive = 0;
		$this->set('mmedia', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(sprintf(__('Invalid %s', true), 'mmedium'));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('mmedium', $this->Mmedium->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Mmedium->create();
			if ($this->Mmedium->save($this->data)) {
				$this->Session->setFlash(sprintf(__('The %s has been saved', true), 'mmedium'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(sprintf(__('The %s could not be saved. Please, try again.', true), 'mmedium'));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(sprintf(__('Invalid %s', true), 'mmedium'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Mmedium->save($this->data)) {
				$this->Session->setFlash(sprintf(__('The %s has been saved', true), 'mmedium'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(sprintf(__('The %s could not be saved. Please, try again.', true), 'mmedium'));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Mmedium->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(sprintf(__('Invalid id for %s', true), 'mmedium'));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Mmedium->delete($id)) {
			$this->Session->setFlash(sprintf(__('%s deleted', true), 'Mmedium'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(sprintf(__('%s was not deleted', true), 'Mmedium'));
		$this->redirect(array('action' => 'index'));
	}
}
?>
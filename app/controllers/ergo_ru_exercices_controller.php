<?php
/**
* @version        $Id: ergo_ru_exercices_controller.php v1.0 07.03.2010 11:15:46 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
* 
* --
-- Structure de la table `ergo_ru_exercices`
--
 
CREATE TABLE IF NOT EXISTS `ergo_ru_exercices` (
  `id_exs` int(12) NOT NULL,
  `id_l` int(12) NOT NULL,
  `enonce` text NOT NULL,
  `reponse` varchar(255) NOT NULL,
  `reponsepost` text NOT NULL,
  `tips` text NOT NULL,
  `rang` float(4,2) NOT NULL DEFAULT '0.00',
  `id` int(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;
*/
#todo: secure app ergo_ru_exercices! open to all logged users!!!

?>
<?php
class ErgoRuExercicesController extends AppController {

	var $name = 'ErgoRuExercices';
	var $helpers = array('Html', 'Form');

	var $components = array('RequestHandler','Auth');
	
/*	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index','view','search');
	}	
*/	
	#criteres de tri
	var $paginate = array(
        'limit' => 25,
        'order' => array(
            'ErgoRuExercice.id' => 'desc'
        )
    );
    
/*	function index() {
		$this->ErgoRuExercice->recursive = 0;
		$this->set('ergoRuExercices', $this->paginate());
	}
*/

	function index() {
		$this->ErgoRuExercice->recursive = 0;
		
		if($this->data['ErgoRuExercice']['q']) {
					$input = $this->data['ErgoRuExercice']['q']; 
					# sanitize the query
					App::import('Sanitize');
					$q = Sanitize::escape($input);
					$q=trim($q);
					$options = array(
					"ErgoRuExercice.enonce LIKE '%" .$q ."%'" ." OR ErgoRuExercice.reponse LIKE '%" .$q ."%' OR ErgoRuExercice.reponsepost LIKE '%" .$q ."%'"
					);
					$this->set(array('ergoRuExercices' => $this->paginate('ErgoRuExercice', $options))); 
				} else {
		
		$this->set('ergoRuExercices', $this->paginate());
	}
	 }







	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ErgoRuExercice.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('ergoRuExercice', $this->ErgoRuExercice->read(null, $id));
	}
	

	function add() {
		if (!empty($this->data)) {
			$this->ErgoRuExercice->create();
			//trim: see model
			if ($this->ErgoRuExercice->save($this->data)) {
				$this->Session->setFlash(__('The ErgoRuExercice has been saved', true));
				$this->redirect(array('action'=>'add#reponse'));
			} else {
				$this->Session->setFlash(__('The ErgoRuExercice could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
						eject_non_admin(); //on autorise pas les non-administrateurs

		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ErgoRuExercice', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoRuExercice->save($this->data)) {
				$this->Session->setFlash(__('The ErgoRuExercice has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoRuExercice could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoRuExercice->read(null, $id);
		}
	}

	function delete($id = null) {
		eject_non_admin(); //on autorise pas les non-administrateurs

		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ErgoRuExercice', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoRuExercice->del($id)) {
			$this->Session->setFlash(__('ErgoRuExercice deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
	
	function exercices() {
	}

//end class
}
?>

<?php
/**
* @version        $Id: ergo_enfrs_controller.php v1.0 01.04.2010 11:20:16 CEST $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.

-- Structure de la table `ergo_enfrs`
--

CREATE TABLE IF NOT EXISTS `ergo_enfrs` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `foreign` text COLLATE utf8_unicode_ci NOT NULL,
  `local` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=38090 ;

*/


class ErgoEnfrsController extends AppController {

	var $name = 'ErgoEnfrs';
	var $components = array('RequestHandler','Auth');

	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index','view','autocomplete');
		if($this->RequestHandler->isAjax())
		{
			Configure::write('debug', 3);
		}
	}
	
		#criteres de tri
	var $paginate = array(
        'limit' => 100,
        'order' => array(
            'ErgoEnfr.id' => 'desc'
        )
    );

	
	function index() {
		$this->ErgoEnfr->recursive = 0;
			//var $_GET['user'];
		if($this->data['ErgoEnfr']['q']) {
					$input = $this->data['ErgoEnfr']['q']; 
					# sanitize the query
					App::import('Sanitize');
					$q = Sanitize::escape($input);
					$options = array(
					"ErgoEnfr.local LIKE '%" .$q ."%'" ." OR ErgoEnfr.foreign LIKE '%" .$q ."%'"
					);
					$this->set(array('ergoEnfrs' => $this->paginate('ErgoEnfr', $options))); 
		} else {
		$this->set('ergoEnfrs', $this->paginate());
/*
$recherche="philo";
	$lesbatiments = $this->ErgoEnfr->find(
			'all',
			array(
				'fields' => 'DISTINCT local',
				'conditions' => "ErgoEnfr.local LIKE '$recherche%'",
				'order' => 'local',
				'limit' => 10
			)
		);

		$this->set('ergoEnfrs', $this->paginate($lesbatiments));
*/



		}
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ErgoEnfr.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('ergoEnfr', $this->ErgoEnfr->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ErgoEnfr->create();
			if ($this->ErgoEnfr->save($this->data)) {
				/*begin special function to add user id (usefull for killing spam if needed and "my" services */
					$utilisateur= $this->Session->read('Auth.User.username'); //whois the current user
					$utilisateurid= $this->Session->read('Auth.User.id'); //current user id
					$dernierid=mysql_query("SELECT *
						FROM `ergo_enfrs`
						ORDER BY `ergo_enfrs`.`id` DESC
						LIMIT 0 , 1");
					$dernierid=mysql_result($dernierid,0,'id');

					//store in the new word in the myvocs
					$sql="
					INSERT INTO `akademiach18`.`ergo_my_en_vocs` (
					`id` ,
					`user_id` ,
					`voc_id` ,
					`date`
					)
					VALUES (
					NULL , 
					'$utilisateurid', 
					'$dernierid',
					CURRENT_TIMESTAMP
					)";
					$sql=mysql_query($sql);
				/*end special function to add user id (usefull for killing spam if needed and "my" services */
				
				
				$this->Session->setFlash(__('Enregistrement sauvegardé avec succès', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('L\'enregistrement n\'a pas pu être sauvegardé. Merci d\'essayer à nouveau.', true));
			}
		}
	}

	function edit($id = null) {
				eject_non_admin(); //on autorise pas les non-administrateurs

		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ErgoEnfr', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoEnfr->save($this->data)) {
				$this->Session->setFlash(__('The ErgoEnfr has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoEnfr could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoEnfr->read(null, $id);
		}
	}

	function delete($id = null) {
				eject_non_admin(); //on autorise pas les non-administrateurs

		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ErgoEnfr', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoEnfr->del($id)) {
			$this->Session->setFlash(__('ErgoEnfr deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

    	//some ajax autocomplete for search engine
    function autocomplete()
	{
		$recherche = utf8_decode($this->data['ErgoEnfr']['q']);
 
		$lesbatiments = $this->ErgoEnfr->find(
			'all',
			array(
				'fields' => 'DISTINCT foreign',
				'conditions' => "foreign LIKE '$recherche%'",
				'order' => 'local',
				'limit' => 10
			)
		);

		$this->set(compact('lesbatiments', 'recherche'));

	}
	
	function rumtrain() {//a function to random drill on vocabulary
	}
	function govoc() { //function to analyse results from rumtrain
	}
	
	function rumtraincat() { //a function to drill on a selected category
	}

	function myrumtrain() {//a function to random drill on already drilled vocabulary
		}
	function gomyvoc() { //function to analyse results from myrumtrain
	//do not display layout
		$this->layout = '';
	}	
    
}
?>

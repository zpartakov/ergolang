<?php
class ErgoCategoriesController extends AppController {

	var $name = 'ErgoCategories';
	var $helpers = array('Html', 'Form');
		var $components = array('RequestHandler','Auth');
		
function beforeFilter() {
	parent::beforeFilter();
    $this->Auth->allow('index','view', 'search');
}
	function index() {
		$this->set('ergoCategories',$this->ErgoCategory->find('all',array('conditions'=>array('parent=0'), $this->paginate())));
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ErgoCategory.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('ergoCategory', $this->ErgoCategory->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ErgoCategory->create();
			if ($this->ErgoCategory->save($this->data)) {
				$this->Session->setFlash(__('The ErgoCategory has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoCategory could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		eject_non_admin(); //on autorise pas les non-administrateurs

		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ErgoCategory', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoCategory->save($this->data)) {
				$this->Session->setFlash(__('The ErgoCategory has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoCategory could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoCategory->read(null, $id);
		}
	}

	function delete($id = null) {
				eject_non_admin(); //on autorise pas les non-administrateurs

		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ErgoCategory', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoCategory->del($id)) {
			$this->Session->setFlash(__('ErgoCategory deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
   function search() {
	$cherche=$this->data['ErgoCategory']['q'];
	$requetesql="SELECT * FROM ergo_categories WHERE `libelle` LIKE '%" .$cherche."%';";
	$this->set('results',$this->ErgoCategory->query($requetesql), $this->paginate());
	}

}
?>

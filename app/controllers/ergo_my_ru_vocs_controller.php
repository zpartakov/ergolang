<?php
/**
* @version        $Id: ergo_my_ru_vocs_controller.php v1.0 26.03.2010 08:48:34 CET $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
* --
-- Structure de la table `ergo_my_ru_vocs`
--

CREATE TABLE IF NOT EXISTS `ergo_my_ru_vocs` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL,
  `ergo_ru_voc_id` int(12) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `voc_id` (`ergo_ru_voc_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=195 ;
*/
?>
<?php
class ErgoMyRuVocsController extends AppController {

	var $name = 'ErgoMyRuVocs';
	var $helpers = array('Html', 'Form');
		var $components = array('RequestHandler','Auth'); //interdit aux non-enregistrés
#criteres de tri
	var $paginate = array(
					'limit' => 25,
        'order' => array(
            'ErgoMyRuVoc.id' => 'desc'
        ),
          //'conditions' => array('user_id =' => $this->Session->read('Auth.User.id')),

    );
	function index() {
		/*begin special function to add user id (usefull for killing spam if needed and "my" services */
		$utilisateurid= $this->Session->read('Auth.User.id'); //current user id
		#$renvoie=$this->set('ergoMyRuVocs', $this->ErgoMyRuVoc->find('all', array('conditions' => array('user_id = ' => $utilisateurid))));
		#$renvoie=find('all', array('conditions' => array('user_id = ' => $utilisateurid)));
		#$this->set('ergoMyRuVocs', $this->paginate($renvoie));
$options = array(
	"ErgoMyRuVoc.user_id = " .$utilisateurid
);
					$this->set(array('ergoMyRuVocs' => $this->paginate('ErgoMyRuVoc', $options))); 
		#$this->set('ergoMyRuVocs', $this->paginate(find('all', array('conditions' => array('user_id = ' => $utilisateurid)))));
		#$this->set('ergoMyRuVocs', $this->paginate(ErgoMyRuVoc->find('all')));
		
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ErgoMyRuVoc.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('ergoMyRuVoc', $this->ErgoMyRuVoc->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ErgoMyRuVoc->create();
			if ($this->ErgoMyRuVoc->save($this->data)) {
				$this->Session->setFlash(__('The ErgoMyRuVoc has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoMyRuVoc could not be saved. Please, try again.', true));
			}
		}
		$ergoRufrs = $this->ErgoMyRuVoc->ErgoRufr->find('list');
		$this->set(compact('ergoRufrs'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ErgoMyRuVoc', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoMyRuVoc->save($this->data)) {
				$this->Session->setFlash(__('The ErgoMyRuVoc has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoMyRuVoc could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoMyRuVoc->read(null, $id);
		}
		$ergoRufrs = $this->ErgoMyRuVoc->ErgoRufr->find('list');
		$this->set(compact('ergoRufrs'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ErgoMyRuVoc', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoMyRuVoc->del($id)) {
			$this->Session->setFlash(__('ErgoMyRuVoc deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>

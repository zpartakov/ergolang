<?php
/**
* @version        $Id: ergo_links_controller.php v1.0 23.03.2010 09:46:35 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

/*
CREATE TABLE IF NOT EXISTS `ergo_links` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL,
  `ergo_langue_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lib` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `rem` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;

*/

class ErgoLinksController extends AppController {

	var $name = 'ErgoLinks';
	#var $helpers = array('Html', 'Form');
		#var $components = array('RequestHandler','Auth');
		/*added http://bakery.cakephp.org/articles/view/pagination*/
	var $components = array('RequestHandler','Auth');
#	var $components = array('RequestHandler','Auth','Pagination');
#    var $helpers = array('Html', 'Form','Pagination');
	

	
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index','view','search', 'tags');
	}	
	
	#criteres de tri
	var $paginate = array(
        'limit' => 25,
        'order' => array(
            'ErgoLink.lib' => 'asc'
        )
    );
  
    
	function index() {
		$this->ErgoLink->recursive = 1;
			/*fulltext search*/
			if($this->data['ErgoLink']['q']) {
					$input = $this->data['ErgoLink']['q']; 
					# sanitize the query
					App::import('Sanitize');
					$q = Sanitize::escape($input);
					$options = array(
					"ErgoLink.url LIKE '%" .$q ."%'" ." OR ErgoLink.lib LIKE '%" .$q ."%'"." OR ErgoLink.rem LIKE '%" .$q ."%'"
					);

					$this->set(array('ergoLinks' => $this->paginate('ErgoLink', $options))); 
					
			/*both language and  category*/
			} elseif($this->data['ErgoLink']['category']&&$this->data['ErgoLink']['lang']) {
							#echo "bla";
					$lang = $this->data['ErgoLink']['lang']; 
					$tag = $this->data['ErgoLink']['category']; 
					$this->redirect(array('action'=>'tags', "q1"=>$tag, "lang"=>$lang));
			/*category*/

			} elseif($this->data['ErgoLink']['category']) {
					$input = $this->data['ErgoLink']['category']; 
					# sanitize the query
					App::import('Sanitize');
					$q1 = Sanitize::escape($input);
					$this->redirect(array('action'=>'tags', "q1"=>$q1));

			/*language*/							
			} elseif($this->data['ErgoLink']['lang']) {
					$input = $this->data['ErgoLink']['lang']; 
					# sanitize the query
					App::import('Sanitize');
					$q1 = Sanitize::escape($input);			
					$options = array(
					"ErgoLink.ergo_langue_id LIKE '%" .$q1 ."%'"
					);
					$this->set(array('ergoLinks' => $this->paginate('ErgoLink', $options))); 				
					
					
				} else {
		$this->set('ergoLinks', $this->paginate());
	}
	}

	function tags() {
		}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ErgoLink.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('ergoLink', $this->ErgoLink->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ErgoLink->create();
			if ($this->ErgoLink->save($this->data)) {
				$this->Session->setFlash(__('The ErgoLink has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoLink could not be saved. Please, try again.', true));
			}
		}
		$tags = $this->ErgoLink->Tag->find('list',array('order'=>'longname'));
		$users = $this->ErgoLink->User->find('list');
		$ergoLangues = $this->ErgoLink->ErgoLangue->find('list');
		$this->set(compact('tags', 'users', 'ergoLangues'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ErgoLink', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoLink->save($this->data)) {
				$this->Session->setFlash(__('The ErgoLink has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The ErgoLink could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoLink->read(null, $id);
		}
		$tags = $this->ErgoLink->Tag->find('list',array('order'=>'longname'));
		$users = $this->ErgoLink->User->find('list');
		$ergoLangues = $this->ErgoLink->ErgoLangue->find('list');
		$this->set(compact('tags','users','ergoLangues'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ErgoLink', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoLink->del($id)) {
			$this->Session->setFlash(__('ErgoLink deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}

			/* bug see http://bakery.cakephp.org/articles/view/quick-tip-doing-ad-hoc-joins-in-model-find
			 * http://bakery.cakephp.org/articles/view/habtm-searching
			 * http://foldifoldi.com/news/?p=271
			 * http://marianoiglesias.com.ar/cakephp/modelizing-habtm-join-tables-in-cakephp-1-2-with-and-auto-with-models/
			 * http://mrphp.com.au/code/code-category/cakephp/cakephp-1-2/working-habtm-form-data-cakephp
			 * http://book.cakephp.org/view/249/Custom-Query-Pagination
			 * http://book.cakephp.org/view/83/hasAndBelongsToMany-HABTM
			 * 
			 * */
?>

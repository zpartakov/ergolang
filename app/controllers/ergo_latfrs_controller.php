<?php
/**
* @version        $Id: ergo_latfrs_controller.php v1.0 13.10.2010 10:25:18 CEST $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

class ErgoLatfrsController extends AppController {

	var $name = 'ErgoLatfrs';
	var $components = array('RequestHandler','Auth');
		
function beforeFilter() {
	parent::beforeFilter();
    $this->Auth->allow('index','view');
    if($this->RequestHandler->isAjax())
		{
			Configure::write('debug', 3);
		}
}

	#criteres de tri
	var $paginate = array(
        'limit' => 100,
        'order' => array(
            'ErgoLatfr.foreign' => 'asc'
        )
    );
	function index() {
				if($this->data['ErgoLatfr']['q']) {
					$input = $this->data['ErgoLatfr']['q']; 
					# sanitize the query
					App::import('Sanitize');
					$q = Sanitize::escape($input);
					$options = array(
					"ErgoLatfr.local LIKE '%" .$q ."%'" ." OR ErgoLatfr.foreign LIKE '%" .$q ."%'"
					);
					$this->set(array('ergoLatfrs' => $this->paginate('ErgoLatfr', $options))); 
		} else {
		$this->ErgoLatfr->recursive = 0;
		$this->set('ergoLatfrs', $this->paginate());
	}
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ergo latfr', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('ergoLatfr', $this->ErgoLatfr->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ErgoLatfr->create();
			if ($this->ErgoLatfr->save($this->data)) {
				
				/*begin special function to add user id (usefull for killing spam if needed and "my" services */
					$utilisateur= $this->Session->read('Auth.User.username'); //whois the current user
					$utilisateurid= $this->Session->read('Auth.User.id'); //current user id
					$dernierid=mysql_query("SELECT *
						FROM `ergo_latfrs`
						ORDER BY id DESC
						LIMIT 0 , 1");
					$dernierid=mysql_result($dernierid,0,'id');					//store in the new word in the myvocs
					$sql="
					INSERT INTO `akademiach18`.`ergo_my_lat_vocs` (
					`id` ,
					`user_id` ,
					`voc_id` ,
					`date`
					)
					VALUES (
					NULL , 
					'$utilisateurid', 
					'$dernierid',
					CURRENT_TIMESTAMP
					)";
					$sql=mysql_query($sql);
				/*end special function to add user id (usefull for killing spam if needed and "my" services */
				
				
				
				$this->Session->setFlash(__('The ergo latfr has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ergo latfr could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ergo latfr', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ErgoLatfr->save($this->data)) {
				$this->Session->setFlash(__('The ergo latfr has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ergo latfr could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ErgoLatfr->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ergo latfr', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ErgoLatfr->delete($id)) {
			$this->Session->setFlash(__('Ergo latfr deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Ergo latfr was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>

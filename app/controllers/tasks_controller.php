<?php
/**
* @version        $Id: tasks_controller.php v1.0 21.03.2010 18:01:14 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
* 
* -- Structure de la table `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `done` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `delay` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `project_id` int(12) NOT NULL,
  `priority` int(1) NOT NULL,
  `user_id` int(12) NOT NULL,
  `client_id` int(12) NOT NULL,
  `description` text NOT NULL,
  `completion` int(3) NOT NULL,
  `parent_phase` int(12) NOT NULL,
  `remarques` text NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;
*/

// Désactiver le rapport d'erreurs
Configure::write('debug', 0);

class TasksController extends AppController {

	var $name = 'Tasks';

	var $helpers = array('Html', 'Form');
	//unallow to everybody
		var $components = array('RequestHandler','Auth');

	function beforeFilter() {
		//forbidden zone to non-admin users
			if($this->Auth->user('group_id')!=3) {
			$this->Session->setFlash(__('Action not allowed!', true));
			exit;
		}
	}	


#criteres de tri
	var $paginate = array(
        'limit' => 50,
        'order' => array(
            'Task.id' => 'desc'
        )
    );
	function index() {
		$this->Task->recursive = 0;
				$done=$_GET['done'];
//full search
		if($this->data['Task']['q']) {
					$input = $this->data['Task']['q']; 
					# sanitize the query
					App::import('Sanitize');
					$q = Sanitize::escape($input);
					echo "cherche: " ."Task.title LIKE '%" .$q ."%'" ." OR Task.description LIKE '%" .$q ."%' OR Task.url LIKE '%" .$q ."%' OR Task.remarques LIKE '%" .$q ."%'";
					$options = array(
					"Task.title LIKE '%" .$q ."%'" ." OR Task.description LIKE '%" .$q ."%' OR Task.url LIKE '%" .$q ."%' OR Task.remarques LIKE '%" .$q ."%'"
									
					);
					
					$this->set(array('tasks' => $this->paginate('Task', $options))); 

//search by statut		
		} elseif(strlen($done)>0) {
					$options = array(
					"Task.done=" .$done								
					);
					$this->set(array('tasks' => $this->paginate('Task', $options))); 	
//no search browse all
				} else {
		$this->set('tasks', $this->paginate());

	}
	}


	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Task.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('task', $this->Task->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Task->create();
			if ($this->Task->save($this->data)) {
				$this->Session->setFlash(__('The Task has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Task could not be saved. Please, try again.', true));
			}
		}
		$projects = $this->Task->Project->find('list');
		$users = $this->Task->User->find('list');
		$clients = $this->Task->Client->find('list');
		$this->set(compact('projects', 'users', 'clients'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Task', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Task->save($this->data)) {
				$this->Session->setFlash(__('The Task has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Task could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Task->read(null, $id);
		}
		$projects = $this->Task->Project->find('list');
		$users = $this->Task->User->find('list');
		$clients = $this->Task->Client->find('list');
		$this->set(compact('projects','users','clients'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Task', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Task->del($id)) {
			$this->Session->setFlash(__('Task deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>

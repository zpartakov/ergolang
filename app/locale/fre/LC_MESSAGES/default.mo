��    2      �  C   <      H  
   I     T     g     m     t     |     �  %   �     �  
   �     �     �     �  	   �     �     �     �  
               
   '     2     ?     M     _     g     l     q     y          �     �     �  s   �       	     
        )  	   0  -   :     h     ~     �     �     �     �     �     �     �    �  
   �     �     �     �     �  
          !        8     >     M  	   Y     c  
   k     v     �     �     �     �     �     �     �     �     �     	     	     	     "	     .	     3	  
   7	     B	     K	  �   S	     �	  	    
  
   

  	   
     
  M   +
     y
     �
     �
     �
     �
     �
     �
     �
     �
        -          )         '   &                       ,                  %      *               #          0                        $                 +                      (   .   
   2      	            /                 1   !      "    %s deleted %s was not deleted Actif Actifs Actions Add %s April Are you sure you want to delete # %s? August Date Debut Date Fin December Delete Delete %s Demande d'équipement Edit Edit %s Equipement Equipements February Invalid %s Invalid User Invalid User. Invalid id for %s January July June List %s March May New %s November October Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end% Role September Soumis par Submit Telephone The %s could not be saved. Please, try again. The %s has been saved User Users Utilisateurs View next previous précédent suivant Project-Id-Version: CakePHP-fr
POT-Creation-Date: 2008-12-17 06:17+0100
PO-Revision-Date: 
Last-Translator: Fred Radeff <fradeff@gmail.com>
Language-Team: LANGUAGE <EMAIL@ADDRESS>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 %s effacé %s n'a pas été effacé Actif Actifs Actions Ajouter %s Avril Confirmer la suppression de # %s? Août Date de début Date de fin Décembre Effacer Effacer %s Demande d'équipement Modifier Modifier %s Équipement Équipements Février Invalide %s Utilisateur invalide Utilisateur invalide id invalide pour %s Janvier Juillet Juin Afficher %s Mars Mai Nouveau %s Novembre Octobre Page %page% de %pages%, montre l'enregistrement %current% d'un total de %count% enregistrements, commence à l'enregistrement %start%, finit à l'enregistrement %end% Rôle Septembre Soumis par Soumettre Téléphone L'enregistrement %s n'a pas pu être sauvegardé. Merci d'essayer à nouveau. %s a été sauvegardé Utilisateur Utilisateurs Utilisateurs Voir suivant précédent précédent suivant 
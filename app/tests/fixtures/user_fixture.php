<?php 
/* SVN FILE: $Id$ */
/* User Fixture generated on: 2009-12-17 15:51:07 : 1261061467*/

class UserFixture extends CakeTestFixture {
	var $name = 'User';
	var $table = 'users';
	var $fields = array(
		'username' => array('type'=>'string', 'null' => false, 'default' => NULL, 'key' => 'unique'),
		'password' => array('type'=>'string', 'null' => false, 'default' => NULL),
		'email' => array('type'=>'string', 'null' => false, 'default' => NULL, 'key' => 'unique'),
		'pseudo' => array('type'=>'string', 'null' => false, 'default' => NULL),
		'role' => array('type'=>'string', 'null' => true, 'default' => 'www', 'length' => 50),
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 12, 'key' => 'primary'),
		'dateIn' => array('type'=>'timestamp', 'null' => false, 'default' => 'CURRENT_TIMESTAMP'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'username' => array('column' => 'username', 'unique' => 1))
	);
	var $records = array(array(
		'username'  => 'Lorem ipsum dolor sit amet',
		'password'  => 'Lorem ipsum dolor sit amet',
		'email'  => 'Lorem ipsum dolor sit amet',
		'pseudo'  => 'Lorem ipsum dolor sit amet',
		'role'  => 'Lorem ipsum dolor sit amet',
		'id'  => 1,
		'dateIn'  => 1
	));
}
?>
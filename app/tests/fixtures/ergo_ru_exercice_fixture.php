<?php 
/* SVN FILE: $Id$ */
/* ErgoRuExercice Fixture generated on: 2010-03-07 10:03:54 : 1267954554*/

class ErgoRuExerciceFixture extends CakeTestFixture {
	var $name = 'ErgoRuExercice';
	var $table = 'ergo_ru_exercices';
	var $fields = array(
		'id_exs' => array('type'=>'text', 'null' => false, 'default' => NULL),
		'id_l' => array('type'=>'text', 'null' => false, 'default' => NULL),
		'enonce' => array('type'=>'text', 'null' => false, 'default' => NULL),
		'reponse' => array('type'=>'string', 'null' => false, 'default' => NULL),
		'rang' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 4),
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 12, 'key' => 'primary'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(array(
		'id_exs'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
		'id_l'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
		'enonce'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
		'reponse'  => 'Lorem ipsum dolor sit amet',
		'rang'  => 1,
		'id'  => 1
	));
}
?>
<?php
/* ErgoArafrs Test cases generated on: 2011-07-10 12:50:36 : 1310295036*/
App::import('Controller', 'ErgoArafrs');

class TestErgoArafrsController extends ErgoArafrsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ErgoArafrsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.ergo_arafr');

	function startTest() {
		$this->ErgoArafrs =& new TestErgoArafrsController();
		$this->ErgoArafrs->constructClasses();
	}

	function endTest() {
		unset($this->ErgoArafrs);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
?>
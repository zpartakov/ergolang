<?php
/* ErgoEsfrs Test cases generated on: 2010-10-14 16:10:39 : 1287067479*/
App::import('Controller', 'ErgoEsfrs');

class TestErgoEsfrsController extends ErgoEsfrsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ErgoEsfrsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.ergo_esfr');

	function startTest() {
		$this->ErgoEsfrs =& new TestErgoEsfrsController();
		$this->ErgoEsfrs->constructClasses();
	}

	function endTest() {
		unset($this->ErgoEsfrs);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
?>
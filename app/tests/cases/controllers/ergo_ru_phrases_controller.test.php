<?php 
/* SVN FILE: $Id$ */
/* ErgoRuPhrasesController Test cases generated on: 2009-12-20 20:12:22 : 1261336822*/
App::import('Controller', 'ErgoRuPhrases');

class TestErgoRuPhrases extends ErgoRuPhrasesController {
	var $autoRender = false;
}

class ErgoRuPhrasesControllerTest extends CakeTestCase {
	var $ErgoRuPhrases = null;

	function startTest() {
		$this->ErgoRuPhrases = new TestErgoRuPhrases();
		$this->ErgoRuPhrases->constructClasses();
	}

	function testErgoRuPhrasesControllerInstance() {
		$this->assertTrue(is_a($this->ErgoRuPhrases, 'ErgoRuPhrasesController'));
	}

	function endTest() {
		unset($this->ErgoRuPhrases);
	}
}
?>
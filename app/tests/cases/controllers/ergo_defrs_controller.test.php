<?php
/* ErgoDefrs Test cases generated on: 2010-10-13 16:10:35 : 1286981015*/
App::import('Controller', 'ErgoDefrs');

class TestErgoDefrsController extends ErgoDefrsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ErgoDefrsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.ergo_defr');

	function startTest() {
		$this->ErgoDefrs =& new TestErgoDefrsController();
		$this->ErgoDefrs->constructClasses();
	}

	function endTest() {
		unset($this->ErgoDefrs);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
?>
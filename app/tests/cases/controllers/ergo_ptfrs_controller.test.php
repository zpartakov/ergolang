<?php
/* ErgoPtfrs Test cases generated on: 2012-03-13 09:53:47 : 1331628827*/
App::import('Controller', 'ErgoPtfrs');

class TestErgoPtfrsController extends ErgoPtfrsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ErgoPtfrsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.ergo_ptfr');

	function startTest() {
		$this->ErgoPtfrs =& new TestErgoPtfrsController();
		$this->ErgoPtfrs->constructClasses();
	}

	function endTest() {
		unset($this->ErgoPtfrs);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}

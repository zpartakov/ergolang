<?php
/* ErgoLatfrs Test cases generated on: 2010-10-12 19:10:08 : 1286903408*/
App::import('Controller', 'ErgoLatfrs');

class TestErgoLatfrsController extends ErgoLatfrsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ErgoLatfrsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.ergo_latfr');

	function startTest() {
		$this->ErgoLatfrs =& new TestErgoLatfrsController();
		$this->ErgoLatfrs->constructClasses();
	}

	function endTest() {
		unset($this->ErgoLatfrs);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
?>
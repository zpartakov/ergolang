<?php
/* ErgoItfrs Test cases generated on: 2010-10-13 11:10:19 : 1286962279*/
App::import('Controller', 'ErgoItfrs');

class TestErgoItfrsController extends ErgoItfrsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ErgoItfrsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.ergo_itfr');

	function startTest() {
		$this->ErgoItfrs =& new TestErgoItfrsController();
		$this->ErgoItfrs->constructClasses();
	}

	function endTest() {
		unset($this->ErgoItfrs);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
?>
<?php
/* ErgoSyfrs Test cases generated on: 2011-07-13 11:36:29 : 1310549789*/
App::import('Controller', 'ErgoSyfrs');

class TestErgoSyfrsController extends ErgoSyfrsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ErgoSyfrsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.ergo_syfr');

	function startTest() {
		$this->ErgoSyfrs =& new TestErgoSyfrsController();
		$this->ErgoSyfrs->constructClasses();
	}

	function endTest() {
		unset($this->ErgoSyfrs);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
?>
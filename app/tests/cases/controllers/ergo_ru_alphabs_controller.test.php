<?php
/* ErgoRuAlphabs Test cases generated on: 2013-10-16 23:36:06 : 1381959366*/
App::import('Controller', 'ErgoRuAlphabs');

class TestErgoRuAlphabsController extends ErgoRuAlphabsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ErgoRuAlphabsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.ergo_ru_alphab');

	function startTest() {
		$this->ErgoRuAlphabs =& new TestErgoRuAlphabsController();
		$this->ErgoRuAlphabs->constructClasses();
	}

	function endTest() {
		unset($this->ErgoRuAlphabs);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}

<?php 
/* SVN FILE: $Id$ */
/* ErgoRuExercice Test cases generated on: 2010-03-07 10:03:54 : 1267954554*/
App::import('Model', 'ErgoRuExercice');

class ErgoRuExerciceTestCase extends CakeTestCase {
	var $ErgoRuExercice = null;
	var $fixtures = array('app.ergo_ru_exercice');

	function startTest() {
		$this->ErgoRuExercice =& ClassRegistry::init('ErgoRuExercice');
	}

	function testErgoRuExerciceInstance() {
		$this->assertTrue(is_a($this->ErgoRuExercice, 'ErgoRuExercice'));
	}

	function testErgoRuExerciceFind() {
		$this->ErgoRuExercice->recursive = -1;
		$results = $this->ErgoRuExercice->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('ErgoRuExercice' => array(
			'id_exs'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'id_l'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'enonce'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'reponse'  => 'Lorem ipsum dolor sit amet',
			'rang'  => 1,
			'id'  => 1
		));
		$this->assertEqual($results, $expected);
	}
}
?>
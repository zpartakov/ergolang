<?php 
/* SVN FILE: $Id$ */
/* User Test cases generated on: 2009-12-17 15:51:12 : 1261061472*/
App::import('Model', 'User');

class UserTestCase extends CakeTestCase {
	var $User = null;
	var $fixtures = array('app.user');

	function startTest() {
		$this->User =& ClassRegistry::init('User');
	}

	function testUserInstance() {
		$this->assertTrue(is_a($this->User, 'User'));
	}

	function testUserFind() {
		$this->User->recursive = -1;
		$results = $this->User->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('User' => array(
			'username'  => 'Lorem ipsum dolor sit amet',
			'password'  => 'Lorem ipsum dolor sit amet',
			'email'  => 'Lorem ipsum dolor sit amet',
			'pseudo'  => 'Lorem ipsum dolor sit amet',
			'role'  => 'Lorem ipsum dolor sit amet',
			'id'  => 1,
			'dateIn'  => 1
		));
		$this->assertEqual($results, $expected);
	}
}
?>
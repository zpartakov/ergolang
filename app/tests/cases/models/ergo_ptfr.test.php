<?php
/* ErgoPtfr Test cases generated on: 2012-03-13 09:53:30 : 1331628810*/
App::import('Model', 'ErgoPtfr');

class ErgoPtfrTestCase extends CakeTestCase {
	var $fixtures = array('app.ergo_ptfr');

	function startTest() {
		$this->ErgoPtfr =& ClassRegistry::init('ErgoPtfr');
	}

	function endTest() {
		unset($this->ErgoPtfr);
		ClassRegistry::flush();
	}

}

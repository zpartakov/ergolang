<?php
/* ErgoRuAlphab Test cases generated on: 2013-10-16 23:35:53 : 1381959353*/
App::import('Model', 'ErgoRuAlphab');

class ErgoRuAlphabTestCase extends CakeTestCase {
	var $fixtures = array('app.ergo_ru_alphab');

	function startTest() {
		$this->ErgoRuAlphab =& ClassRegistry::init('ErgoRuAlphab');
	}

	function endTest() {
		unset($this->ErgoRuAlphab);
		ClassRegistry::flush();
	}

}

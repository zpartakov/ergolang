#a script for creating the package
#dump db structure
downloads=/var/www/websites/ergolang/downloads
echo "Actual versions"
ls -1 $downloads
echo "Input version:"
read version
downloadsv=$downloads/$version
#echo $downloads
mkdir $downloadsv
#exit
mysqldump -d akademiach18 > $downloadsv/ergolang.sql
mysqldump --skip-triggers --compact --no-create-info akademiach18 articles > $downloadsv/articles.sql
mysqldump --skip-triggers --compact --no-create-info akademiach18 ergo_langues > $downloadsv/ergo_langues.sql

cd $downloadsv
find . -type f | while read i
do
	zip $i.zip $i
	rm $i
done


#dump vocs
downloadsvocs=$downloads/vocs
#arabe
mysqldump --skip-triggers --compact --no-create-info akademiach18 ergo_arafrs > $downloadsvocs/arabe.sql
#croate
mysqldump --skip-triggers --compact --no-create-info akademiach18 ergo_croatefrs > $downloadsvocs/croate.sql
#allemand
mysqldump --skip-triggers --compact --no-create-info akademiach18 ergo_defrs  > $downloadsvocs/allemand.sql
#anglais
mysqldump --skip-triggers --compact --no-create-info akademiach18 ergo_enfrs > $downloadsvocs/anglais.sql
#espagnol
mysqldump --skip-triggers --compact --no-create-info akademiach18 ergo_esfrs  > $downloadsvocs/espagnol.sql
#arab
mysqldump --skip-triggers --compact --no-create-info akademiach18 ergo_arafrs > $downloadsvocs/arab.sql
#francais
mysqldump --skip-triggers --compact --no-create-info akademiach18 ergo_fr_vocs  > $downloadsvocs/francais.sql
#italien
mysqldump --skip-triggers --compact --no-create-info akademiach18 ergo_itfrs  > $downloadsvocs/italien.sql
#latin
mysqldump --skip-triggers --compact --no-create-info akademiach18 ergo_latfrs  > $downloadsvocs/latin.sql
#russe
mysqldump --skip-triggers --compact --no-create-info akademiach18 ergo_rufrs  > $downloadsvocs/russe.sql
mysqldump --skip-triggers --compact --no-create-info akademiach18 ergo_ru_stems   > $downloadsvocs/russe_stems.sql
mysqldump --skip-triggers --compact --no-create-info akademiach18 ergo_ru_stopwords   > $downloadsvocs/russe_stopwords.sql


cd $downloadsvocs
find . -type f | while read i
do
	zip $i.zip $i
	rm $i
done

echo "Package ready: please go to http://129.194.18.217/hg/hgwebdir.wsgi to download the latest zip and copy it to " $downloadsv
cd $downloadsv

ergolang

========
collaborative &amp; open-source learning languages / apprentissage de langues collaboratif &amp; open-source

Ergolang - http://oblomov.info/websites/ergolang

Also required:

- cakePHP *1.3 https://github.com/cakephp/cakephp/tags
- jquery
- datepicker
- plugin alaxos

//
// Ergolang is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Ergolang is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PmCake. If not, see <http://www.gnu.org/licenses/>.
 
/**
*
* @package ergolang
* @version $Id: 1.10
* @author Fred Radeff <ergolang@akademia.ch>
* @copyright (c) 2013 Fred Radeff, akademia.ch
* @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
*
*/

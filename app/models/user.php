<?php
/**
* @version        $Id: user.php v1.0 30.12.2009 09:22:43 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
class User extends AppModel {

	var $name = 'User';
			var $displayField = 'username';

		var $actsAs = array('Revision'=> array('limit'=>100), 'Trim');

	#var $display="username";
	var $validate = array(
		'username' => array('notempty',
		   'unique' => array(
				'rule' => array('checkUnique', 'username'),
				'message' => 'User name taken. Use another'
			)
		), 
		'password' => array(
			'notempty' => array(
				'rule' => array('minLenght', 1),
				'required' => true,
				'allowEmpty' => false,
				'message' => 'Password cannot be empty.'
				),
	
			),
		'group_id' => array('numeric'),
		'email' => array(
			'rule'=>'email',
			'required'=>'true',
			'allowEmpty'=>'false',
			'message'=>'Merci de donner un email valide'
			)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
		'Group' => array(
			'className' => 'Group',
			'foreignKey' => 'group_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	var $hasMany = array('Post');
	
	           /*
	            * make sure that the user name entered by the new user has not
been used by some other users*/

	function checkUnique($data, $fieldName) {
     $valid = false;
     if(isset($fieldName) && $this->hasField($fieldName)) {
      $valid = $this->isUnique(array($fieldName => $data));
        }
        return $valid;
}


/*OLDIES*/

#check if the two password fields match*/
/*
 * function checkPasswords($data) {
   if($data['password'] == $this->data['User']
                                         ['password2hashed'])
      return true;
   return false;
}
	var $validate = array(
		'username' => array('notempty',
		   'unique' => array(
				'rule' => array('checkUnique', 'username'),
				'message' => 'User name taken. Use another'
			)
		), 
		'password' => array(
			'notempty' => array(
				'rule' => array('minLenght', 1),
				'required' => true,
				'allowEmpty' => false,
				'message' => 'Password cannot be empty.'
				),
			'passwordSimilar' => array(
			'rule' => 'checkPasswords',
			'message' => 'Different password re entered.'
			)
			),
		'group_id' => array('numeric'),
		'email' => array(
			'rule'=>'email',
			'required'=>'true',
			'allowEmpty'=>'false',
			'message'=>'Merci de donner un email valide'
			)
	);
*/


}
?>

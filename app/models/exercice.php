<?php
class Exercice extends AppModel {

	var $name = 'Exercice';
	var $actsAs = array('Trim');

	var $validate = array(
		'ergo_langue_id' => array('notempty'),
		'ergo_langue_id' => array('numeric'),
		'id_exs' => array('numeric'),
		'id_l' => array('numeric'),
		'titreexercice' => array('notempty'),
		'user_id' => array('numeric')
	);
	


	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
		'ErgoLangue' => array(
			'className' => 'ErgoLangue',
			'foreignKey' => 'ergo_langue_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}
?>

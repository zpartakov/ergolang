<?php
class Personne extends AppModel {

	var $name = 'Personne';
		var $actsAs = array('Trim');

	var $validate = array(
		'nom' => array('notempty'),
		'prenom' => array('notempty'),
		'profession' => array('notempty')
	);

}
?>

<?php
class ErgoRuGrammaire extends AppModel {

	var $name = 'ErgoRuGrammaire';
	var $actsAs = array('Trim');

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
		'ErgoRuType' => array(
			'className' => 'ErgoRuType',
			'foreignKey' => 'ergo_ru_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}
?>

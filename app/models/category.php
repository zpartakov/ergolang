<?php
/**
* @version        $Id: category.php v1.0 09.06.2010 07:59:26 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

class Category extends AppModel {

	var $name = 'Category';
	var $actsAs = array('Revision'=> array('limit'=>100), 'Trim');
	var $displayField = 'libelle';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasAndBelongsToMany = array(
		'ErgoRufr' => array(
			'className' => 'ErgoRufr',
			'joinTable' => 'categories_ergo_rufrs',
			'foreignKey' => 'category_id',
			'associationForeignKey' => 'ergo_rufr_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

}
?>

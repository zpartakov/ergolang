<?php
class Project extends AppModel {

	var $name = 'Project';
	var $actsAs = array('Trim');

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasMany = array(
		'Task' => array(
			'className' => 'Task',
			'foreignKey' => 'project_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>

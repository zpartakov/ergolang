<?php
/**
* @version        $Id: ergo_my_ru_voc.php v1.0 26.03.2010 11:34:45 CET $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
?>
<?php
class ErgoMyRuVoc extends AppModel {

	var $name = 'ErgoMyRuVoc';
		var $actsAs = array('Trim');

  var $displayField = 'ErgoRufr.foreign';
	//The Associations below have been created with all possible keys, those that are not needed can be removed
var $belongsTo = array(

/*		
 * 
var $belongsTo = array(
var $hasMany = array(
var $hasAndBelongsToMany = array(
//ok
var $hasOne = array(
* 
* */
/*
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'id',
			'conditions' => 'ErgoMyRuVoc.user_id=User.id',
			'fields' => '',
			'order' => ''
		),*/
		'ErgoRufr' => array(
			'className' => 'ErgoRufr',
/*			'foreignKey' => 'ergo_ru_voc_id',*/
			'foreignKey' => 'ergo_ru_voc_id',
			'conditions' => 'ErgoMyRuVoc.ergo_ru_voc_id=ErgoRufr.id',
			'order' => ''
)	
	);

}
/*
 *         var $belongsTo = array('Team' =>
                 array('className'    => 'Team',
                     'conditions'   => '',
                     'order'        => '',
                     'foreignKey'   => 'team_id'
                 )
             );

 * 
 * 
 * 
 
 
 * #  var $hasMany = array(
# 'Recette' => Array
# 'className' => 'Recette',
# 'conditions' => array('Recette.acceptee' => '1'),
# 'order' => 'Recette.created DESC'
# )
* */
?>

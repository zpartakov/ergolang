<?php
/**
* @version        $Id: ergo_ru_type.php v1.0 17.03.2010 04:19:44 CET $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
class ErgoRuType extends AppModel {

	var $name = 'ErgoRuType';
	var $actsAs = array('Trim');

	//The Associations below have been created with all possible keys, those that are not needed can be removed
/*	var $hasOne = array(
		'ErgoRuGrammaire' => array(
			'className' => 'ErgoRuGrammaire',
			'foreignKey' => 'ergo_ru_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	var $hasMany = array(
		'ErgoRuGrammaire' => array(
			'className' => 'ErgoRuGrammaire',
			'foreignKey' => 'ergo_ru_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
*/
}
?>

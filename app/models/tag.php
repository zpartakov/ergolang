<?php
class Tag extends AppModel {

	var $name = 'Tag';
var $displayField = 'longname';
	var $actsAs = array('Revision'=> array('limit'=>100), 'Trim');


	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasAndBelongsToMany = array(
		'ErgoLink' => array(
			'className' => 'ErgoLink',
			'joinTable' => 'ergo_links_tags',
			'foreignKey' => 'tag_id',
			'associationForeignKey' => 'ergo_link_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Post' => array(
			'className' => 'Post',
			'joinTable' => 'posts_tags',
			'foreignKey' => 'tag_id',
			'associationForeignKey' => 'post_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

}
?>

<?php
/**
* @version        $Id: ergo_link_cat.php v1.0 30.01.2010 08:47:57 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
class ErgoLinkCat extends AppModel {

	var $name = 'ErgoLinkCat';
		var $actsAs = array('Trim');

	var $validate = array(
		'lib' => array('notempty')
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
/*	var $hasMany = array(
		'ErgoLink' => array(
			'className' => 'ErgoLink',
			'foreignKey' => 'ergo_link_cat_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
*/
}
?>

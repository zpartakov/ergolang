<?php
class ErgoLangue extends AppModel {

	var $name = 'ErgoLangue';
		var $actsAs = array('Trim');

	var $displayField = 'lib';

	var $validate = array(
		'code' => array('notempty'),
		'lib' => array('notempty')
	);

}
?>

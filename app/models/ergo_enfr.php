<?php
/**
* @version        $Id: ergo_enfr.php v1.0 13.10.2010 10:53:29 CEST $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

class ErgoEnfr extends AppModel {

	var $name = 'ErgoEnfr';
	var $actsAs = array('Revision'=> array('limit'=>100), 'Trim');

}
?>

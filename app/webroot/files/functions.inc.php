<?
/* english basic stopwords
 * 
 */

$stopwords="
;a;
;about;
;above;
;across;
;after;
;afterwards;
;again;
;against;
;all;
;almost;
;alone;
;along;
;already;
;also;
;although;
;always;
;am;
;among;
;amongst;
;amoungst;
;amount;
;an;
;and;
;another;
;any;
;anyhow;
;anyone;
;anything;
;anyway;
;anywhere;
;are;
;around;
;as;
;at;
;back;
;be;
;became;
;because;
;become;
;becomes;
;becoming;
;been;
;before;
;beforehand;
;behind;
;being;
;below;
;beside;
;besides;
;between;
;beyond;
;bill;
;both;
;bottom;
;but;
;by;
;call;
;can;
;cannot;
;cant;
;co;
;computer;
;con;
;could;
;couldnt;
;cry;
;de;
;describe;
;detail;
;do;
;done;
;down;
;due;
;during;
;each;
;eg;
;eight;
;either;
;eleven;
;else;
;elsewhere;
;empty;
;enough;
;etc;
;even;
;ever;
;every;
;everyone;
;everything;
;everywhere;
;except;
;few;
;fifteen;
;fify;
;fill;
;find;
;fire;
;first;
;five;
;for;
;former;
;formerly;
;forty;
;found;
;four;
;from;
;front;
;full;
;further;
;get;
;give;
;go;
;had;
;has;
;hasnt;
;have;
;he;
;hence;
;her;
;here;
;hereafter;
;hereby;
;herein;
;hereupon;
;hers;
;herself;
;him;
;himself;
;his;
;how;
;however;
;hundred;
;i;
;ie;
;if;
;in;
;inc;
;indeed;
;interest;
;into;
;is;
;it;
;its;
;itself;
;keep;
;last;
;latter;
;latterly;
;least;
;less;
;ltd;
;made;
;many;
;may;
;me;
;meanwhile;
;might;
;mill;
;mine;
;more;
;moreover;
;most;
;mostly;
;move;
;much;
;must;
;my;
;myself;
;name;
;namely;
;neither;
;never;
;nevertheless;
;next;
;nine;
;no;
;nobody;
;none;
;noone;
;nor;
;not;
;nothing;
;now;
;nowhere;
;of;
;off;
;often;
;on;
;once;
;one;
;only;
;onto;
;or;
;other;
;others;
;otherwise;
;our;
;ours;
;ourselves;
;out;
;over;
;own;
;part;
;per;
;perhaps;
;please;
;put;
;rather;
;re;
;same;
;see;
;seem;
;seemed;
;seeming;
;seems;
;serious;
;several;
;she;
;should;
;show;
;side;
;since;
;sincere;
;six;
;sixty;
;so;
;some;
;somehow;
;someone;
;something;
;sometime;
;sometimes;
;somewhere;
;still;
;such;
;system;
;take;
;ten;
;than;
;that;
;the;
;their;
;them;
;themselves;
;then;
;thence;
;there;
;thereafter;
;thereby;
;therefore;
;therein;
;thereupon;
;these;
;they;
;thick;
;thin;
;third;
;this;
;those;
;though;
;three;
;through;
;throughout;
;thru;
;thus;
;to;
;together;
;too;
;top;
;toward;
;towards;
;twelve;
;twenty;
;two;
;un;
;under;
;until;
;up;
;upon;
;us;
;very;
;via;
;was;
;we;
;well;
;were;
;what;
;whatever;
;when;
;whence;
;whenever;
;where;
;whereafter;
;whereas;
;whereby;
;wherein;
;whereupon;
;wherever;
;whether;
;which;
;while;
;whither;
;who;
;whoever;
;whole;
;whom;
;whose;
;why;
;will;
;with;
;within;
;without;
;would;
;yet;
;you;
;your;
;yours;
;yourself;
;yourselves;
";
#nettoyer les mots pour le vocabulaire
function normaliserMot($mot) {
	$mot=trim($mot);
	$mot=ereg_replace(",","",$mot);
	$mot=str_replace(",","",$mot);
	$mot=ereg_replace("\.*","",$mot);
	$mot=str_replace("?","",$mot);
	$mot=str_replace("the","",$mot);

	return $mot;
}
$sql="SELECT * FROM " .$latable ." WHERE id=" .$id;
$sqlQ=mysql_query($sql);
if(!$sqlQ){
	echo "error sql: " .mysql_error();
	exit;
}
$texte=mysql_result($sqlQ,0,'text');
#the div is needed for ajax google translator
echo "<div id=\"Source\">".$texte."</div>";
?>
<!-- 
Google translator ajax see 
http://blogoscoped.com/archive/2008-03-20-n87.html
unused: 
http://code.google.com/intl/fr/apis/ajaxlanguage/
http://code.google.com/intl/fr/apis/ajaxlanguage/documentation/#Translate
http://code.google.com/intl/fr/apis/ajaxlanguage/documentation/#Examples
 -->
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
google.load("language", "1");
function Translate(sLang) {
	var text = (document.all) ? document.getElementById("Source").innerText : document.getElementById("Source").textContent;
	google.language.detect(text, function(result) {
		if (!result.error && result.language) {
			google.language.translate(text, result.language, sLang,
				function(result) {
					var translated = document.getElementById("Target");
					if (result.translation) {
						translated.innerHTML = "<p><strong>Machine translation:</strong></p>" + result.translation;
					}else{
						translated.innerHTML = "";
					}
				});
		}
	});
}
</script>
<form action="">
<div>
    <!-- possible to select a target language, to keep in mind for later -->
 <!--  <select id="Lang" name="Lang">
    <option value="">Select language...</option>

    <option value="ar">Arabic</option>
    <option value="zh-CN">Chinese(Simplified)</option>
    <option value="zh-TW">Chinese(Traditional)</option>
    <option value="nl">Dutch</option>
    <option value="fr">French</option>
    <option value="de">German</option>

    <option value="it">Italian</option>
    <option value="ja">Japanese</option>
    <option value="ko">Korean</option>
    <option value="pt-PT">Portuguese</option>
    <option value="ru">Russian</option>
    <option value="es">Spanish</option>

  </select>
     <input type="button" value="Traduire" onclick="Translate(this.form.Lang[this.form.Lang.selectedIndex].value);" />
  
   -->
   <input type="button" value="Traduire" onclick="Translate('fr');" />
</div>
</form>
<!-- #the div is needed for ajax google translator -->

<div id="Target" style="background-color: lightblue"></div>

<?

$pieces = explode(" ", $texte);
#todo: fonction pour normaliser les mots (réductions, stopwords, virer , . etc.)
/*
 pistes: 

http://drupal.org/node/85361 

http://snowball.tartarus.org/algorithms/russian/stemmer.html
 */
$touslesmots="";
$j=0;
for($i=0;$i<count($pieces);$i++){
	$mot=$pieces[$i];
	$mot=normaliserMot($mot);
	if(strlen($mot)>1) {
		$j++;
		if(!eregi(";$mot;",$touslesmots)) { //on affiche que si pas un doublon
  		$touslesmots.= ";" .$mot .";"; //on ajoute le mot dans un tableau pour pas avoir de doublons
  		//recherche du mot dans le vocabulaire
  		$all= "SELECT * FROM `" .$latableVoc ."` WHERE `foreign` LIKE '$mot%' LIMIT 0,8";
  		
  		#echo "<pre>" .$all ."</pre>"; //tests
  		$result = mysql_query($all);
  		if(mysql_num_rows($result)>0) {
  			$k=0;
  			$francais="";
  			while($k<mysql_num_rows($result)){;
  			$francais.=mysql_result($result,$k,'local') ."<br>";
  			$k++;
  			}
  		} else {
  			$francais="";
  		}
  		//affichage du résultat
 		echo "<hr>Mot " .$j  ." -- <strong>".$mot ."</strong><br>";
 					if(eregi(";$mot;",$stopwords)){
		$mot="";
		echo "<font color=\"grey\">stopword!</font><br>";
	} else {
 
  		if(strlen($francais)>1) {
  			echo " #hits: " .mysql_num_rows($result)."<br>";
  			echo "<em>francais:</em><br>" .$francais;
  		}
	}
		}
	}
	echo "<br><form action=\"inseremot\"><input type=\"text\" name=\"trad" .$mot ."\"><input type=\"submit\"></form>";
}

echo "<hr>";

//todo: chercher dans voc, proposer d'insérer si n'existe pas, suggérer traductions
  
?>
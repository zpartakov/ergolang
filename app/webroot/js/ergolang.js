/**
* @version        $Id: ergolang.js v1.0 07.03.2010 06:25:39 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*
* Main ergolang javascript
*/

/*
 * a function to play sounds
 */
 function playSound(soundfile) {
 document.getElementById("dummy").innerHTML=
 "<embed src=\""+soundfile+"\" hidden=\"true\" autostart=\"true\" loop=\"false\" />";
 }

 
/* Correction exercice */
function corrige_exercice(id) {
	sid="s"+id;
		var essai = document.getElementById(id).value;
		var verif=document.getElementById(sid).value;
		if(essai!=verif) {
			alert(essai +" = Faux!\nLa bonne réponse était: *** " +verif + " ***");
			document.getElementById(id).className="faux" //Assign the class
		} else {
			document.getElementById(id).className="juste" //Assign the class
		}
}

/* function to correct exercize and give immediate feedback to the user */
/*function corrige_exercice(id,reponse) {
function corrige_exercice(id) {
}*/

function vide_recherche(id) {
	document.getElementById(id).value="";
}
function goto_url(url) {
	document.location=url;
}


function validate_email(form_id,email) {
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
   var address = document.forms[form_id].elements[email].value;
   if(reg.test(address) == false) {
      alert('Email invalide');
      return false;
   }
}

function removeCategory(idcat) {
	/*url="http://129.194.18.197/websites/ergolang/cake/categories_ergo_rufrs/tuer/"+idcat;*/
	url="http://oblomov.info/websites/ergolang/cake/categories_ergo_rufrs/tuer/"+idcat;
	   location.href=url;
}

function rendprive(champ) { //hide the text to other users than administrators
	var letexteprive = document.getElementById("texteprive").checked;
	var titretexteprive = document.getElementById(champ).value;
	if(letexteprive==true) {
		document.getElementById(champ).value="<!-- private -->"+titretexteprive;
		}else{
			var reg_exp= RegExp("<!-- private -->");
			document.getElementById(champ).value = titretexteprive.replace (reg_exp, "");
	}

/* ########### END */
	
			/*
		var verif=document.getElementById(sid).value;
		if(essai!=verif) {
			alert(essai +" = Faux!\nLa bonne réponse était: *** " +verif + " ***");
			document.getElementById(id).className="faux" //Assign the class
		} else {
			document.getElementById(id).className="juste" //Assign the class
		}*/
}

function showHide(shID) {
			document.getElementById(shID).style.display = 'inline';
	}


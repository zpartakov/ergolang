<?php
/**
* @version        $Id: google.php v1.0 09.12.2009 22:08:18 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
/**
* a simple google russian translator using API google
*/

// translate text
$texte = $_POST['translate'];

if($texte){
}

?>

<?
echo "<div id=\"Source\">".$texte."</div>";
?>
<!-- 
Google translator ajax see 
http://blogoscoped.com/archive/2008-03-20-n87.html
unused: 
http://code.google.com/intl/fr/apis/ajaxlanguage/
http://code.google.com/intl/fr/apis/ajaxlanguage/documentation/#Translate
http://code.google.com/intl/fr/apis/ajaxlanguage/documentation/#Examples
 -->
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
google.load("language", "1");
function Translate(sLang) {
	
  if (!result.error) {
    var container = document.getElementById("translation");
    container.innerHTML = result.translation;
  }
});
	
	
	var text = (document.all) ? document.getElementById("Source").innerText : document.getElementById("Source").textContent;
	google.language.detect(text, function(result) {
		if (!result.error && result.language) {
			google.language.translate(text, result.language, sLang,
			/*google.language.translate(text, "ru", "fr",*/
				function(result) {
					var translated = document.getElementById("Target");
					if (result.translation) {
						translated.innerHTML = "<p><strong>Machine translation:</strong></p>" + result.translation;
					}else{
						translated.innerHTML = "";
					}
				});
		}
	});
}
</script>
<form action="">
<div>
    <!-- possible to select a target language, to keep in mind for later -->
 <!--  <select id="Lang" name="Lang">
    <option value="">Select language...</option>

    <option value="ar">Arabic</option>
    <option value="zh-CN">Chinese(Simplified)</option>
    <option value="zh-TW">Chinese(Traditional)</option>
    <option value="nl">Dutch</option>
    <option value="fr">French</option>
    <option value="de">German</option>

    <option value="it">Italian</option>
    <option value="ja">Japanese</option>
    <option value="ko">Korean</option>
    <option value="pt-PT">Portuguese</option>
    <option value="ru">Russian</option>
    <option value="es">Spanish</option>

  </select>
     <input type="button" value="Traduire" onclick="Translate(this.form.Lang[this.form.Lang.selectedIndex].value);" />
  
   -->
   <input type="text" name="translate" id="Source" value="<?php echo $text;?>">

   <input type="button" value="Traduire avec Google" onclick="Translate('fr');" />
</div>
</form>
<!-- #the div is needed for ajax google translator -->

<div id="Target" style="background-color: lightblue"></div>


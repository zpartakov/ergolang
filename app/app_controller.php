<?php
/**
* @version        $Id: app_controller.php v1.0 13.12.2009 10:53:28 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
class AppController extends Controller {
// controller file
var $helpers = array('Html', 'Form', 'Javascript', 'Ajax', 'Session', 'Text');


#useful functions to trim before sending
	function whitespace(&$value, &$key){
		$key = trim($key);
		$value = trim($value);
	}


 function beforeFilter() {
		if(!empty($this->data))
		array_walk_recursive($this->data, array($this, 'whitespace'));

	 setlocale(LC_TIME,array('fr_CH.UTF-8', 'fr_CH', 'fr_FR.UTF-8', 'fr_FR'));

	 $this->Auth->authError="Vous ne pouvez pas accéder à cet espace sans être enregistré";
	 $this->Auth->loginError="Login / mot de passe incorrect";
}

     

	function view() {
	    $this->render('layout file', 'ajax');
	}

#end main class
}

#######

#class to compute unique random numbers, to move in an external file eg inc.classes.php
class UniqueRand{
  var $alreadyExists = array();

  function uRand($min = NULL, $max = NULL){
    $break='false';
    while($break=='false'){
      $rand=mt_rand($min,$max);

      if(array_search($rand,$this->alreadyExists)===false){
        $this->alreadyExists[]=$rand;
        $break='stop';
      }else{
        #echo " $rand already!  ";
        #print_r($this->alreadyExists);
      }
    }
    return $rand;
  }
}

#### radeff ###
function datetime2fr($ladate) {
	$ladate=explode(" ",$ladate);
	$ladate=$ladate[0];
	$ladate=explode("-",$ladate);
	$ladate=$ladate[2]."-".$ladate[1]."-".$ladate[0];
	return $ladate;
}

/* exercices */

//fonctions communes
//fonction exercice de base
function exercicebegin($table,$id_exs,$id_l) {
	echo '<form name="exercice"><table>';
	$sql="SELECT * FROM " .$table ." WHERE id_exs=" .$id_exs ." AND id_l=" .$id_l ." ORDER BY id";
$sql=mysql_query($sql);
if(!$sql) {
	echo "SQL error: " .mysql_error(); exit;
}
#loop sql
$i=0;
while($i<mysql_num_rows($sql)){
	echo "<tr>";
	echo "<td>" .mysql_result($sql,$i,'enonce');
	echo "</td>";
	echo "<td>";
	echo "<input type=\"hidden\" id=\"s" .mysql_result($sql,$i,'id') ."\" value=\"".mysql_result($sql,$i,'reponse') ."\">";
	echo "<input type=\"text\" id=\"" .mysql_result($sql,$i,'id');
	#echo "\" onchange=\"javascript:corrige_exercice(" .mysql_result($sql,$i,'id') ."," .mysql_result($sql,$i,'reponse') .")\">";	
	echo "\" onchange=\"javascript:corrige_exercice(" .mysql_result($sql,$i,'id') .")\">";
	#echo "\" onchange=\"javascript:corrige_exercice(this)\">";
		echo " " .mysql_result($sql,$i,'reponsepost');

	echo "</td>";
	echo "</tr>";
	$i++;
	}
echo "</table>
</form>";
}

//sourceBea
function source($lasource) {
	/* source */
echo '<br><hr>
<a class="source" href="' .$lasource .'" target="_blank">' .$lasource .'</a>';
}
function sourceBea() {
	/* source */
$lasource="http://langues2.ups-tlse.fr/Beatrice/grammaire/GRAMMAIRE.html";
echo ' <hr>
<a class="source" href="' .$lasource .'" target="_blank">' .$lasource .'</a>';
}

function first_words($string, $num, $tail='&nbsp;...')
//source: http://www.phpro.org/examples/First-Words.html
{
        $words = str_word_count($string, 2);
        /*** get the first $num words ***/
        $firstwords = array_slice( $words, 0, $num);

        /** return words in a string **/
        $renvoie=implode(' ', $firstwords);
        if(str_word_count($string, 0)>2) {
			$renvoie.=$tail;
		}
        return $renvoie ;
}

function eject_non_admin() {
	if($_SESSION['Auth']['User']['group_id']!=3) { 	//non admin eject
		/*$this->Session->setFlash(__('Action not allowed!', true));*/
		echo "<h1>Action not allowed!</h1>";
		exit;
	}
}

function eject_non_admin_grp($groupe,$lang) {
		/*$this->Session->setFlash(__('Action not allowed!', true));*/
		#echo "groupe_id" .$groupe ." lang: " .$lang;
		$sql="SELECT * FROM ergo_langues WHERE lib LIKE '" .strtolower($lang) ."'";		
		$sql=mysql_query($sql);
		if(mysql_num_rows($sql)!=1) {
				echo "<h1>Action not allowed!</h1>"; exit;
		}
		$code=mysql_result($sql,0,'code');
		#echo "<br>code: " .$code; exit;		
		$sql="SELECT * FROM groups WHERE id=" .$groupe;		
		$sql=mysql_query($sql);
		if(mysql_num_rows($sql)!=1) {
				echo "<h1>Action not allowed!</h1>"; exit;
		}
		$groupe=mysql_result($sql,0,'name');	

		if(!preg_match("/$code_adm/",$groupe)) { 	//non admin for this language: eject
				echo "<h1>Action not allowed!</h1>"; exit;
		}
}

function generate_password($length){
     // A List of vowels and vowel sounds that we can insert in
     // the password string
     $vowels = array("a",  "e",  "i",  "o",  "u",  "ae",  "ou",  "io",  
                     "ea",  "ou",  "ia",  "ai"); 
     // A List of Consonants and Consonant sounds that we can insert
     // into the password string
     $consonants = array("b",  "c",  "d",  "g",  "h",  "j",  "k",  "l",  "m",
                         "n",  "p",  "r",  "s",  "t",  "u",  "v",  "w",  
                         "tr",  "cr",  "fr",  "dr",  "wr",  "pr",  "th",
                         "ch",  "ph",  "st",  "sl",  "cl");
     // For the call to rand(), saves a call to the count() function
     // on each iteration of the for loop
     $vowel_count = count($vowels);
     $consonant_count = count($consonants);
     // From $i .. $length, fill the string with alternating consonant
     // vowel pairs.
     for ($i = 0; $i < $length; ++$i) {
         $pass .= $consonants[rand(0,  $consonant_count - 1)] .
                  $vowels[rand(0,  $vowel_count - 1)];
     }
     
     // Since some of our consonants and vowels are more than one
     // character, our string can be longer than $length, use substr()
     // to truncate the string
     return substr($pass,  0,  $length);
 
}

############ RUSSIAN ####################


/*method to print a scrolling list of available categories*/
function choisir_categorie($id_mot,$utilisateur) {
	#$utilisateur= $this->Session->read('Auth.User.id');
	#echo $utilisateur; exit;
	/* ****** build sql queries ******/
		//recherche mot -> catégories begin
	#$sql="SELECT * FROM categories_ergo_rufrs WHERE ergo_rufr_id=" .$id_mot;
	$sql="SELECT * FROM categories_ergo_rufrs, categories WHERE ergo_rufr_id=" .$id_mot ." AND categories.id=categories_ergo_rufrs.category_id" ;
	$motcategories=";";$motcategoriessel="";
	#echo "<!-- sql -->$sql";

	#do and check sql
	$sql=mysql_query($sql);
	if(!$sql) {
		echo "SQL error: " .mysql_error(); exit;
	}

	$i=0;
	while($i<mysql_num_rows($sql)){
			#$motcategories.=mysql_result($sql,$i,'category_id').";";
			$motcategoriessel.='<br><input type="checkbox" onClick="javascript:removeCategory(\''.mysql_result($sql,$i,'categories_ergo_rufrs.id').'\')"  checked value="'.mysql_result($sql,$i,'category_id').'">&nbsp;'.mysql_result($sql,$i,'libelle') ."</input>";
			
			$motcategories.=";".mysql_result($sql,$i,'categories.id').";";
				$i++;
		}
	//recherche mot -> catégories fin
	
	//affichage catégories begin
	$affichage_categorie="";
	$sql="SELECT * FROM categories ORDER BY libelle";
	#do and check sql
	$sql=mysql_query($sql);
	if(!$sql) {
		echo "SQL error: " .mysql_error(); exit;
	}
	$i=0;
	while($i<mysql_num_rows($sql)){
		
		
		if(!ereg(";".mysql_result($sql,$i,'id').";",$motcategories)){
			$affichage_categorie.= "<option class=\"categoriesoptions\" value=\"" .mysql_result($sql,$i,'id') ."\"";
			$id2check=";".mysql_result($sql,$i,'id').";";
			if(ereg($id2check,$motcategories)) {
		#		echo ' selected="selected"';
				$affichage_categorie.= ' selected';
			}
			$affichage_categorie.= ">";
			
				if(ereg($id2check,$motcategories)) {
				#echo " selekted ";	echo $id2check ."-"; //tests
				}
			
			$affichage_categorie.=  mysql_result($sql,$i,'libelle') ."</option>";
		}
		$i++;
	}
	
	//affichage catégories fin
	/* ******************************** */
	//begin print
	
	echo "<div class=\"listcategories\">";
	?>
	<form id="CategoriesErgoRufrAddForm" method="post" action="/websites/ergolang/cake/categories_ergo_rufrs/add" accept-charset="utf-8">
	<div style="display:none;"><input type="hidden" name="_method" value="POST" /></div>
	<fieldset>
			<legend>Catégories</legend>
		<div class="input select"><select name="data[CategoriesErgoRufr][category_id]" id="CategoriesErgoRufrCategoryId" size="10" multiple>
	<?
	echo $affichage_categorie;

	//id du mot hidden
	echo '
	<input type="hidden" name="data[CategoriesErgoRufr][ergo_rufr_id]" id="CategoriesErgoRufrErgoRufrId" value="' .$id_mot .'">
	';
	//id du user hidden
	echo '
	<input type="hidden" name="data[CategoriesErgoRufr][user_id]" id="CategoriesErgoRufrUserId" value="' .$utilisateur .'">
	';
	echo "</select></fieldset><fieldset>";
	
	echo "<legend>Catégories sélectionnées</legend><em>Cliquer pour supprimer</em><br>";
	echo $motcategoriessel;
	echo "<br><br><p><input type=\"submit\"></p></fieldset></form></div>";
}




#see other http://zhugo.co.cc/2009/12/dumpexport-mysql-database-with-php-then-zip-it/
#echo CHEMIN; exit;
/* $Id: zip.lib.php,v 1.6 2002/03/30 08:24:04 loic1 Exp $ */

/**
 * Zip file creation class.
 * Makes zip files.
 *
 * Based on :
 *
 *  http://www.zend.com/codex.php?id=535&single=1
 *  By Eric Mueller <eric@themepark.com>
 *
 *  http://www.zend.com/codex.php?id=470&single=1
 *  by Denis125 <webmaster@atlant.ru>
 *
 *  a patch from Peter Listiak <mlady@users.sourceforge.net> for last modified
 *  date and time of the compressed file
 *
 * Official ZIP file format: http://www.pkware.com/appnote.txt
 *
 * @access  public
 */
class zipfile
{
    /**
     * Array to store compressed data
     *
     * @var  array    $datasec
     */
    var $datasec      = array();

    /**
     * Central directory
     *
     * @var  array    $ctrl_dir
     */
    var $ctrl_dir     = array();

    /**
     * End of central directory record
     *
     * @var  string   $eof_ctrl_dir
     */
    var $eof_ctrl_dir = "\x50\x4b\x05\x06\x00\x00\x00\x00";

    /**
     * Last offset position
     *
     * @var  integer  $old_offset
     */
    var $old_offset   = 0;


    /**
     * Converts an Unix timestamp to a four byte DOS date and time format (date
     * in high two bytes, time in low two bytes allowing magnitude comparison).
     *
     * @param  integer  the current Unix timestamp
     *
     * @return integer  the current date in a four byte DOS format
     *
     * @access private
     */
    function unix2DosTime($unixtime = 0) {
        $timearray = ($unixtime == 0) ? getdate() : getdate($unixtime);

        if ($timearray['year'] < 1980) {
        	$timearray['year']    = 1980;
        	$timearray['mon']     = 1;
        	$timearray['mday']    = 1;
        	$timearray['hours']   = 0;
        	$timearray['minutes'] = 0;
        	$timearray['seconds'] = 0;
        } // end if

        return (($timearray['year'] - 1980) << 25) | ($timearray['mon'] << 21) | ($timearray['mday'] << 16) |
                ($timearray['hours'] << 11) | ($timearray['minutes'] << 5) | ($timearray['seconds'] >> 1);
    } // end of the 'unix2DosTime()' method


    /**
     * Adds "file" to archive
     *
     * @param  string   file contents
     * @param  string   name of the file in the archive (may contains the path)
     * @param  integer  the current timestamp
     *
     * @access public
     */
    function addFile($data, $name, $time = 0)
    {
        $name     = str_replace('\\', '/', $name);

        $dtime    = dechex($this->unix2DosTime($time));
        $hexdtime = '\x' . $dtime[6] . $dtime[7]
                  . '\x' . $dtime[4] . $dtime[5]
                  . '\x' . $dtime[2] . $dtime[3]
                  . '\x' . $dtime[0] . $dtime[1];
        eval('$hexdtime = "' . $hexdtime . '";');

        $fr   = "\x50\x4b\x03\x04";
        $fr   .= "\x14\x00";            // ver needed to extract
        $fr   .= "\x00\x00";            // gen purpose bit flag
        $fr   .= "\x08\x00";            // compression method
        $fr   .= $hexdtime;             // last mod time and date

        // "local file header" segment
        $unc_len = strlen($data);
        $crc     = crc32($data);
        $zdata   = gzcompress($data);
        $zdata   = substr(substr($zdata, 0, strlen($zdata) - 4), 2); // fix crc bug
        $c_len   = strlen($zdata);
        $fr      .= pack('V', $crc);             // crc32
        $fr      .= pack('V', $c_len);           // compressed filesize
        $fr      .= pack('V', $unc_len);         // uncompressed filesize
        $fr      .= pack('v', strlen($name));    // length of filename
        $fr      .= pack('v', 0);                // extra field length
        $fr      .= $name;

        // "file data" segment
        $fr .= $zdata;

        // "data descriptor" segment (optional but necessary if archive is not
        // served as file)
        $fr .= pack('V', $crc);                 // crc32
        $fr .= pack('V', $c_len);               // compressed filesize
        $fr .= pack('V', $unc_len);             // uncompressed filesize

        // add this entry to array
        $this -> datasec[] = $fr;
        $new_offset        = strlen(implode('', $this->datasec));

        // now add to central directory record
        $cdrec = "\x50\x4b\x01\x02";
        $cdrec .= "\x00\x00";                // version made by
        $cdrec .= "\x14\x00";                // version needed to extract
        $cdrec .= "\x00\x00";                // gen purpose bit flag
        $cdrec .= "\x08\x00";                // compression method
        $cdrec .= $hexdtime;                 // last mod time & date
        $cdrec .= pack('V', $crc);           // crc32
        $cdrec .= pack('V', $c_len);         // compressed filesize
        $cdrec .= pack('V', $unc_len);       // uncompressed filesize
        $cdrec .= pack('v', strlen($name) ); // length of filename
        $cdrec .= pack('v', 0 );             // extra field length
        $cdrec .= pack('v', 0 );             // file comment length
        $cdrec .= pack('v', 0 );             // disk number start
        $cdrec .= pack('v', 0 );             // internal file attributes
        $cdrec .= pack('V', 32 );            // external file attributes - 'archive' bit set

        $cdrec .= pack('V', $this -> old_offset ); // relative offset of local header
        $this -> old_offset = $new_offset;

        $cdrec .= $name;

        // optional extra field, file comment goes here
        // save to central directory
        $this -> ctrl_dir[] = $cdrec;
    } // end of the 'addFile()' method


    /**
     * Dumps out file
     *
     * @return  string  the zipped file
     *
     * @access public
     */
    function file()
    {
        $data    = implode('', $this -> datasec);
        $ctrldir = implode('', $this -> ctrl_dir);

        return
            $data .
            $ctrldir .
            $this -> eof_ctrl_dir .
            pack('v', sizeof($this -> ctrl_dir)) .  // total # of entries "on this disk"
            pack('v', sizeof($this -> ctrl_dir)) .  // total # of entries overall
            pack('V', strlen($ctrldir)) .           // size of central dir
            pack('V', strlen($data)) .              // offset to start of central dir
            "\x00\x00";                             // .zip file comment length
    } // end of the 'file()' method

} // end of the 'zipfile' class


function urlise($chaine) { //a function to extract urls from pages
	#echo "test urlize: <br>" .$chaine ."<hr>";
	#$chaine=ereg_replace("(http://)(([[:punct:]]|[[:alnum:]]=?)*)","<a href=\"\\0\">\\0</a>",$chaine);
	$chaine = preg_replace("/(https:\/\/)(([[:punct:]]|[[:alnum:]]=?)*)/","<a href=\"\\0\">\\0</a>",$chaine);
	$chaine=preg_replace("/(http:\/\/)(([[:punct:]]|[[:alnum:]]=?)*)/","<a href=\"\\0\">\\0</a>",$chaine);
	//now replace emails
	if(!preg_match("/[a-zA-Z0-9]*\.[a-zA-Z0-9]*@/",$chaine)){
	#$chaine = ereg_replace('[-a-zA-Z0-9!#$%&\'*+/=?^_`{|}~]+@([.]?[a-zA-Z0-9_/-])*','<a href="mailto:\\0">\\0</a>',$chaine);
	#$chaine = preg_replace('/[-a-zA-Z0-9!#$%&\'*+/=?^_`{|}~]+@([.]?[a-zA-Z0-9_\/-])*/','<a href="mailto:\\0">\\0</a>',$chaine);
	}else {
	$chaine = preg_replace('/[-a-zA-Z0-9]*\.[-a-zA-Z0-9!#$%&\'*+\/=?^_`{|}~]+@([.]?[a-zA-Z0-9_\/-])*/','<a href="mailto:\\0">\\0</a>',$chaine);	
	}

	echo nl2br($chaine);
}


function last_session_lang() {
}



function maj_session($user,$lang) { //a function to store session +lang
	$sql="SELECT * FROM sessions WHERE user_id=" .$user;
	#do and check sql
	$sqlq=mysql_query($sql);
	if(!$sqlq) {
		echo "SQL error: " .mysql_error(); exit;
	}
	if(strlen($lang)>1){
	if(mysql_num_rows($sqlq)==1) {
	$sqlsession="UPDATE sessions SET lang = '" .$lang ."' WHERE user_id=" .$user;
	} elseif(mysql_num_rows($sqlq)<1) {
				$sqlsession="
				INSERT INTO sessions (
					`user_id` ,
					`lang`,
					`last_updated`
					)
					VALUES (
					'" .$user ."', '" .$lang ."','" .date("Y-m-d H:i:s") ."'
				)";

		} else {
			echo "SQL error: you have more than one log in the session table: <br><pre>" .$sql ."</pre>"; exit;	
		}
	} else {
		$lalangue=mysql_result($sqlq,0,'lang');
		#$this -> Session -> write("langue", $lalangue);
		$_SESSION['langue'] = $lang;
	}
	
	#do and check sql
	$sql=mysql_query($sqlsession);
	if(!$sql) {
		echo "SQL error: " .mysql_error(); exit;
	}
	
//redirect to language homepage
		#$this -> Session -> write("langue", $lalangue);
	$_SESSION['langue'] = $lang;
#echo "Langue: " .$session->read("langue");
#echo "Langue: " .$session->read("langue"); exit;
header("Location: /websites/ergolang/cake/".$lang);
#header("Location: /websites/ergolang/cake/");

#exit();
}



?>

<?php
	/**
	* @version        $Id: index.ctp v1.0 05.03.2010 06:04:02 CET $
	* @package        Эrgolang
	* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
	* @license        GNU/GPL, see LICENSE.php
	* Эrgolang is free software. This version may have been modified pursuant
	* to the GNU General Public License, and as distributed it includes or
	* is derivative of works licensed under the GNU General Public License or
	* other free or open source software licenses.
	* See COPYRIGHT.php for copyright notices and details.
	*/
	
		if($session->read('Auth.User.group_id')!=3) {
			#echo "sorry, you are not allowed to enter this zone!"; exit;
			header("Location: http://oblomov.info/ergolang");
}
$this->pageTitle = 'Utilisateurs'; 

?>
<div class="users index">
<h1><? echo $this->pageTitle; ?></h1>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('username');?></th>
<!--	<th><?php echo $paginator->sort('password');?></th>-->
	<th><?php echo $paginator->sort('email');?></th>
	<th><?php echo $paginator->sort('group_id');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th><?php echo $paginator->sort('modified');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($users as $user):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $user['User']['id']; ?>
		</td>
		<td>
			<?php echo $user['User']['username']; ?>
		</td>
<!--		<td>
			<?php echo $user['User']['password']; ?>
		</td>-->
		<td>
			<?php echo $user['User']['email']; ?>
		</td>
		<td>
			<?php echo $html->link($user['Group']['name'], array('controller'=> 'groups', 'action'=>'view', $user['Group']['id'])); ?>
		</td>
		<td>
			<?php echo $user['User']['created']; ?>
		</td>
		<td>
			<?php echo $user['User']['modified']; ?>
		</td>

	
			
			
	<?		echo "<td class=\"actions\">"
		 
			.$html->link(
				$html->image('/img/b_view.png', array("alt" => "Détail", "title"=>"Détail")),
				array('action'=>'view', $user['User']['id']),
				array('escape' => false));
				
	echo
				$html->link(
				$html->image('/img/b_edit.png', array("alt" => "Modifier", "title"=>"Modifer")),
				array('action'=>'edit', $user['User']['id']),
				array('escape' => false))
			."<a href=\"delete/" .$user['User']['id'] ."\" onclick=\"return confirm(&#039;Are you sure you want to delete # " .$user['User']['id'] ."?&#039;);\">" .$html->image('/img/b_drop.png', array("alt" => "Supprimer", "title"=>"Supprimer")) ."</a>";

echo		"</td>";
	
?>
			
			
			
			
			
			
			
			
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New User', true), array('action'=>'add')); ?></li>
		<li><?php echo $html->link(__('List Groups', true), array('controller'=> 'groups', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Group', true), array('controller'=> 'groups', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Posts', true), array('controller'=> 'posts', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Post', true), array('controller'=> 'posts', 'action'=>'add')); ?> </li>
	</ul>
</div>

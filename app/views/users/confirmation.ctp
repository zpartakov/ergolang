<?php
/**
* @version        $Id: confirmation.php v1.0 29.03.2010 14:54:50 CEST $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = "Envoyer un nouveau mot de passe - confirmation";
?> 
<h1><? echo $this->pageTitle; ?></h1>
Veuillez consulter votre email, vous avez reçu un nouveau mot de passe.

<?php
/**
* @version        $Id: add.ctp v1.0 05.03.2010 06:06:01 CET $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
		if($session->read('Auth.User.group_id')!=3) {
			echo "sorry, you are not allowed to enter this zone!"; exit;
}
$pass=""; $length=8;
		$vowels = array("a",  "e",  "i",  "o",  "u",  "ae",  "ou",  "io",  
                     "ea",  "ou",  "ia",  "ai"); 
		 // A List of Consonants and Consonant sounds that we can insert
		 // into the password string
		 $consonants = array("b",  "c",  "d",  "g",  "h",  "j",  "k",  "l",  "m",
							 "n",  "p",  "r",  "s",  "t",  "u",  "v",  "w",  
							 "tr",  "cr",  "fr",  "dr",  "wr",  "pr",  "th",
							 "ch",  "ph",  "st",  "sl",  "cl");
		 // For the call to rand(), saves a call to the count() function
		 // on each iteration of the for loop
		 $vowel_count = count($vowels);
		 $consonant_count = count($consonants);
		 // From $i .. $length, fill the string with alternating consonant
		 // vowel pairs.
		 for ($i = 0; $i < $length; ++$i) {
			 $pass .= $consonants[rand(0,  $consonant_count - 1)] .
					  $vowels[rand(0,  $vowel_count - 1)];
		 }
		 
		 // Since some of our consonants and vowels are more than one
		 // character, our string can be longer than $length, use substr()
		 // to truncate the string
			$password=substr($pass,  0,  $length);
?>
<div class="users form">
<?php echo $form->create('User');?>
	<fieldset>
 		<legend><?php __('Add User');?></legend>
	<?php
		echo $form->input('username');
		echo $form->input('password', array("value"=>$password, "type"=>"text"));
		echo $form->input('email');
		echo $form->input('group_id', array("selected"=>5)); //default=user
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Users', true), array('action'=>'index'));?></li>
		<li><?php echo $html->link(__('List Groups', true), array('controller'=> 'groups', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Group', true), array('controller'=> 'groups', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Posts', true), array('controller'=> 'posts', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Post', true), array('controller'=> 'posts', 'action'=>'add')); ?> </li>
	</ul>
</div>

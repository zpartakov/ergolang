<?php
/**
* @version        $Id: login.ctp v1.0 02.03.2010 12:07:38 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

$this->pageTitle = "Login / identification";
?> 
<h1><? echo $this->pageTitle; ?></h1>
 <? if ($session->check('Message.auth')) $session->flash('auth');?> 
 <?=$form->create('User',array('action'=>'login'));?>

 <?php
 /* echo $html->tableCells(array(
 array($form->create('User',array('action'=>'login'))),
 array('Jun 21st, 2007', 'Smart Cookies', 'Yes'),
 array('Aug 1st, 2006', 'Anti-Java Cake', 'No'),
 ));
 */?>
<table>
	   <tr>
		   <td width="10%">
 <label for="UserUsername">Identifiant</label></td>
<td><input name="data[User][username]" type="text" maxlength="255" value="" id="UserUsername" /></td>
</tr>
<tr> 
 <td width="10%"><label for="UserPassword">Mot de passe</label></td>
<td> <input type="password" name="data[User][password]" value="" id="UserPassword" /></td>

  </td>
	   </tr>
   </table>
 <?#=$form->input('username',array('label'=>"Identifiant&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"));?>
 <?#=$form->input('password',array('type'=>'password','label'=>"Mot de passe&nbsp;&nbsp;"));?>
 <?=$form->end('Login');?>
 <br />
<ul>
<li>Mot de passe oublié? <a href="/websites/ergolang/cake/users/passwordreminder">Faites-vous envoyer votre mot de passe</a></li>
<li>Vous n'avez pas encore de compte? <a href="/websites/ergolang/cake/contact/contacts/add">Enregistrez-vous</a></li>
</ul>
<br>
<br>
<a href=logout>Logout</a>

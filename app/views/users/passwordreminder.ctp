<?php
/**
* @version        $Id: passwordreminder.ctp v1.0 29.03.2010 05:00:59 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
//password reminder

$this->pageTitle = "Envoyer un nouveau mot de passe";
?> 
<h1><? echo $this->pageTitle; ?></h1>
Veuillez compléter les champs ci dessous pour obtenir un nouveau mot de passe pour votre compte dans Эrgolang.<br /> 
Un email avec votre nouveau mot de passe vous sera envoyé à l'adresse de courriel utilisée lors de votre enregistrement. 
<br />
<br />
<form id="form_id" action="/websites/ergolang/cake/users/renvoiemail"  onsubmit="javascript:return validate_email('form_id','email');">
Votre email: <input type="text" name="email">
<input type="submit" value="Renvoyer le mot de passe">
</form>
<br />
<ul>
<li>Vous n'avez pas encore de compte?</li>
<li>Le système de renvoi ne marche pas?</li>
<li>Vous avez changé d'email?</li>
</ul>
<br />
-> Veuillez utiliser le <a href="/websites/ergolang/cake/contact/contacts/add">formulaire de contact</a>


<div class="contacts form">
<?php echo $form->create('Contact');?>
	<fieldset>
 		<legend><?php __('Add Contact');?></legend>
	<?php
		echo $form->input('name');
		echo $form->input('address');
		echo $form->input('zip');
		echo $form->input('city');
		echo $form->input('country');
		echo $form->input('phone');
		echo $form->input('email');
		echo $form->input('message');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Contacts', true), array('action'=>'index'));?></li>
	</ul>
</div>

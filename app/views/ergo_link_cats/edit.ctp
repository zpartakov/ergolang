<div class="ergoLinkCats form">
<?php echo $form->create('ErgoLinkCat');?>
	<fieldset>
 		<legend><?php __('Edit ErgoLinkCat');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('lib');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action'=>'delete', $form->value('ErgoLinkCat.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('ErgoLinkCat.id'))); ?></li>
		<li><?php echo $html->link(__('List ErgoLinkCats', true), array('action'=>'index'));?></li>
	</ul>
</div>

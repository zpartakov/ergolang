<div class="ergoLinkCats form">
<?php echo $form->create('ErgoLinkCat');?>
	<fieldset>
 		<legend><?php __('Add ErgoLinkCat');?></legend>
	<?php
		echo $form->input('lib');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List ErgoLinkCats', true), array('action'=>'index'));?></li>
	</ul>
</div>

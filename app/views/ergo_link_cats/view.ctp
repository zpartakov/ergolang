<div class="ergoLinkCats view">
<h2><?php  __('ErgoLinkCat');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoLinkCat['ErgoLinkCat']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Lib'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoLinkCat['ErgoLinkCat']['lib']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit ErgoLinkCat', true), array('action'=>'edit', $ergoLinkCat['ErgoLinkCat']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete ErgoLinkCat', true), array('action'=>'delete', $ergoLinkCat['ErgoLinkCat']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoLinkCat['ErgoLinkCat']['id'])); ?> </li>
		<li><?php echo $html->link(__('List ErgoLinkCats', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New ErgoLinkCat', true), array('action'=>'add')); ?> </li>
	</ul>
</div>

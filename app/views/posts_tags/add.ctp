<div class="postsTags form">
<?php echo $form->create('PostsTag');?>
	<fieldset>
 		<legend><?php __('Add PostsTag');?></legend>
	<?php
		echo $form->input('post_id');
		echo $form->input('tag_id');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List PostsTags', true), array('action'=>'index'));?></li>
	</ul>
</div>

<div class="postsTags view">
<h2><?php  __('PostsTag');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $postsTag['PostsTag']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Post Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $postsTag['PostsTag']['post_id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Tag Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $postsTag['PostsTag']['tag_id']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit PostsTag', true), array('action'=>'edit', $postsTag['PostsTag']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete PostsTag', true), array('action'=>'delete', $postsTag['PostsTag']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $postsTag['PostsTag']['id'])); ?> </li>
		<li><?php echo $html->link(__('List PostsTags', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New PostsTag', true), array('action'=>'add')); ?> </li>
	</ul>
</div>

<div class="postsTags form">
<?php echo $form->create('PostsTag');?>
	<fieldset>
 		<legend><?php __('Edit PostsTag');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('post_id');
		echo $form->input('tag_id');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action'=>'delete', $form->value('PostsTag.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('PostsTag.id'))); ?></li>
		<li><?php echo $html->link(__('List PostsTags', true), array('action'=>'index'));?></li>
	</ul>
</div>

<?php
/**
* @version        $Id: add.ctp v1.0 02.01.2010 07:54:25 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
?>
<div class="ergoMotsassocies form">
<?php echo $form->create('ErgoMotsassocy');?>
	<fieldset>
 		<legend><?php __('Nouvelle association');?></legend>
	<?php
	
//compute available languages
echo '<select id="ErgoMotsassocyLang" name="data[ErgoMotsassocy][lang]">';
$sql="
SELECT *
FROM `ergo_langues`
ORDER BY `ergo_langues`.`lib` ASC";
$sql=mysql_query($sql);
$sqlN=mysql_num_rows($sql);
$i=0;
while($i<$sqlN) {
	echo "<option value=\"" .mysql_result($sql,$i,'code');
	echo "\">";
	echo mysql_result($sql,$i,'lib');
	echo "</option>";
	$i++;
}
echo "</select>";

		#echo $form->input('lang');
		
echo '<input type="hidden" id="ErgoMotsassocyUserId" name="data[ErgoMotsassocy][user_id]" value="' .$session->read('Auth.User.id') .'">';
		#echo $form->input('user_id');
		echo $form->input('ergo_langues_id');
		
		
		#echo $form->input('category_id');
		
//compute available categories
echo '<select id="ErgoMotsassocyCategoryId" name="data[ErgoMotsassocy][category_id]">';
$sql="
SELECT *
FROM `ergo_categories`
ORDER BY `ergo_categories`.`libelle` ASC";
$sql=mysql_query($sql);
$sqlN=mysql_num_rows($sql);
$i=0;
while($i<$sqlN) {
	echo "<option " .mysql_result($sql,$i,'id');
	echo ">";
	if(mysql_result($sql,$i,'parent')!=0){
		echo "<&nbsp;";
	}
	echo mysql_result($sql,$i,'libelle');
	echo "</option>";
	$i++;
}
echo "</select>";	
	
		#echo $form->input('niveau');
//print levels
echo '<div class="input text"><label for="ErgoMotsassocyNiveau">Niveau</label>';
echo '<select id="ErgoMotsassocyNiveau" name="data[ErgoMotsassocy][niveau]">';
for($i=0;$i<7;$i++) {
	echo "<option>" .$i ."</option>";
}
echo "</select>";	

		
		
		echo $form->input('rem');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List ErgoMotsassocies', true), array('action'=>'index'));?></li>
	</ul>
</div>

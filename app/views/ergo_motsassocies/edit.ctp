<div class="ergoMotsassocies form">
<?php echo $form->create('ErgoMotsassocy');?>
	<fieldset>
 		<legend><?php __('Edit ErgoMotsassocy');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('lang');
		echo $form->input('user_id');
		echo $form->input('ergo_langues_id');
		echo $form->input('category_id');
		echo $form->input('niveau');
		echo $form->input('rem');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action'=>'delete', $form->value('ErgoMotsassocy.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('ErgoMotsassocy.id'))); ?></li>
		<li><?php echo $html->link(__('List ErgoMotsassocies', true), array('action'=>'index'));?></li>
	</ul>
</div>

<div class="ergoMotsassocies view">
<h2><?php  __('ErgoMotsassocy');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoMotsassocy['ErgoMotsassocy']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Lang'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoMotsassocy['ErgoMotsassocy']['lang']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('User Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoMotsassocy['ErgoMotsassocy']['user_id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Ergo Langues Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoMotsassocy['ErgoMotsassocy']['ergo_langues_id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Category Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoMotsassocy['ErgoMotsassocy']['category_id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Niveau'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoMotsassocy['ErgoMotsassocy']['niveau']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Rem'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoMotsassocy['ErgoMotsassocy']['rem']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit ErgoMotsassocy', true), array('action'=>'edit', $ergoMotsassocy['ErgoMotsassocy']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete ErgoMotsassocy', true), array('action'=>'delete', $ergoMotsassocy['ErgoMotsassocy']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoMotsassocy['ErgoMotsassocy']['id'])); ?> </li>
		<li><?php echo $html->link(__('List ErgoMotsassocies', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New ErgoMotsassocy', true), array('action'=>'add')); ?> </li>
	</ul>
</div>

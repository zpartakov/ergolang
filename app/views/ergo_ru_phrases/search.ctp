<?php
/**
* @version        $Id: search.ctp v1.0 30.12.2009 21:11:53 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
### def some constants
if($_SERVER["HTTP_HOST"]=="localhost"||$_SERVER["HTTP_HOST"]=="129.194.18.197") { //serveur de développement - tests
$CHEMIN="/ergolang/cake";
} elseif ($_SERVER["HTTP_HOST"]=="oblomov.info"){ //serveur de prod
$CHEMIN="/websites/ergolang/cake";
}
?>
<div class="ergoRuPhrases index">
<h2><?php __('ErgoRuPhrases');?></h2>
<div id="search_box">
<h4>Chercher</h4>
<form id="ergoRuPhraseAddForm" method="GET" action="/websites/ergolang/cake/ergo_ru_phrases/search">
<fieldset style="display:none;">
</fieldset>
<div class="input text">
<input name="search_text" type="text" style="width: 250px;" value="<? echo $_GET['search_text'];?>" id="ergoRuPhraseSearchText" />
</div>
<div class="submit">
<input type="submit" value="Chercher" />
</div>
</form>
</div> 
<?
   
    #echo "blacherche: " .$_GET['search_text');
    $auteur=$_GET['search_text'];
    $cherchetexte=$_GET['search_text'];
    $sql="SELECT * FROM ergo_ru_phrases WHERE (" 
    ."`auteur` LIKE '%" .$auteur ."%'"
    . " OR " ."`titre` LIKE '%" .$cherchetexte ."%'"
    . " OR " ."`url` LIKE '%" .$cherchetexte ."%')";
    $sql="SELECT * FROM ergo_ru_phrases WHERE " 
    ."`auteur` LIKE '%" .$auteur ."%'";
    $sql="SELECT * FROM ergo_ru_phrases WHERE (" 
    ."`auteur` LIKE '%" .$auteur ."%'"
	. " OR 	`titre` LIKE '%" .$cherchetexte ."%')";
		#$sql=utf8_encode($sql);
		
		/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
    $sql="SELECT * FROM ergo_ru_phrases WHERE " 
    ."`auteur` LIKE '%" .$auteur ."%'";
} else {
    $sql="SELECT * FROM ergo_ru_phrases WHERE " 
    ."`auteur` LIKE '%" .$auteur ."%' AND titre NOT LIKE '<!-- private -->%'";
}
    #echo nl2br($sql);
    $sql=mysql_query($sql);
?>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Nouvelle phrase', true), array('action'=>'add')); ?></li>
	</ul>
</div>

<table cellpadding="0" cellspacing="0">
<tr>
	<th>auteur</th>
	<th>titre</th>
	<th>url</th>
	<!--<th>text</th>
	<th>date</th>-->
	<th>modif</th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
#echo "<br>hits: # " .mysql_num_rows($sql) ."<br>";

$i = 0;
while($i<mysql_num_rows($sql)){
	/*$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}*/
?>
	<tr<?php echo $class;?>>

		<td>
			<?php echo mysql_result($sql,$i,'auteur'); ?>
		</td>
				<td>
			<?php echo mysql_result($sql,$i,'titre'); ?>
		</td>
		<td>
		<?
		if(mysql_result($sql,$i,'url')){
		echo "<a href=\"" .mysql_result($sql,$i,'url') ."\" target=\"_blank\">";
echo $html->image('urlextern.png', array("alt"=>"URL Source","title"=>"URL Source"));
echo "</a>";
}
		?>
		
		
			<?php #echo mysql_result($sql,$i,'url'); ?>
		</td>
		<!--<td>
			<?php echo substr(mysql_result($sql,$i,'text'),0,100); ?>
		</td>
		<td>
			<?php echo mysql_result($sql,$i,'date'); ?>
		</td>-->
		<td>
			<?php echo mysql_result($sql,$i,'modif'); ?>
		</td>
		<td class="actions">

			
			<?php 
		echo "<a href=\"" .$CHEMIN ."/ergo_ru_phrases/analyse?id=" .mysql_result($sql,$i,'id') ."&debut=0&fin=0\">";
echo $html->image('b_index.png', array("alt"=>"Analyser","title"=>"Analyser"));
echo "</a>";
echo "&nbsp;";
	
echo "<a href=\"" .$CHEMIN ."/ergo_ru_phrases/view/" .mysql_result($sql,$i,'id') ."\">";
echo $html->image('b_search.png', array("alt"=>"Voir","title"=>"Voir"));
echo "</a>";
echo "&nbsp;";
echo "<a href=\"".$CHEMIN ."/ergo_ru_phrases/edit/" .mysql_result($sql,$i,'id') ."\">";
echo $html->image('b_edit.png', array("alt"=>"Modifier","title"=>"Modifier"));
echo "</a>";
echo "<a onclick=\"javascript:return confirm('Confirmer la suppression ?')\" href=\"" .$CHEMIN ."/ergo_ru_phrases/delete/" .mysql_result($sql,$i,'id') ."\">";
echo $html->image('b_drop.png', array("alt"=>"Effacer","title"=>"Effacer"));
echo "</a>";
?>
		</td>
	</tr>
<?
#$i++;
$i=$i+1;
}
?>
</table>
</div>

<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Nouvelle phrase', true), array('action'=>'add')); ?></li>
	</ul>
</div>

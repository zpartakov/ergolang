<div class="ergoRuPhrases index">
<h2><?php __('ErgoRuPhrases');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('titre');?></th>
	<th><?php echo $paginator->sort('url');?></th>
	<th><?php echo $paginator->sort('text');?></th>
	<th><?php echo $paginator->sort('date');?></th>
	<th><?php echo $paginator->sort('modif');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($ergoRuPhrases as $ergoRuPhrase):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $ergoRuPhrase['ErgoRuPhrase']['id']; ?>
		</td>
		<td>
			<?php echo $ergoRuPhrase['ErgoRuPhrase']['titre']; ?>
		</td>
		<td>
			<?php echo $ergoRuPhrase['ErgoRuPhrase']['url']; ?>
		</td>
		<td>
			<?php echo $ergoRuPhrase['ErgoRuPhrase']['text']; ?>
		</td>
		<td>
			<?php echo $ergoRuPhrase['ErgoRuPhrase']['date']; ?>
		</td>
		<td>
			<?php echo $ergoRuPhrase['ErgoRuPhrase']['modif']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action'=>'view', $ergoRuPhrase['ErgoRuPhrase']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $ergoRuPhrase['ErgoRuPhrase']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $ergoRuPhrase['ErgoRuPhrase']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoRuPhrase['ErgoRuPhrase']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New ErgoRuPhrase', true), array('action'=>'add')); ?></li>
	</ul>
</div>

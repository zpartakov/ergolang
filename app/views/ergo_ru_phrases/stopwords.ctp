<?php
/**
* @version        $Id: stopwords.ctp v1.0 30.12.2009 12:35:05 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

#source: http://snowball.tartarus.org/algorithms/russian/stop.txt
$stopwords="";

#do and check sql
$sql="SELECT * FROM ergo_ru_stopwords ORDER BY word";
$sql=mysql_query($sql);
if(!$sql) {
	echo "SQL error: " .mysql_error(); exit;
}

$i=0;
while($i<mysql_num_rows($sql)){
$stopwords.=";" .mysql_result($sql,$i,'word').";";
	$i++;
	}
	
?>

<?
/**
* @version        $Id: analyse.ctp v1.0 09.12.2009 22:04:25 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
#########
/* fichier pour l'analyse
 * #todo: 
 * syntaxe php doc
 * ajout creative common partout
 * introduire stopwords, eg. the, and etc. dans un fichier séparé
 * ajax pour insérer les traductions sans confirmation, par click ou input + submit
 * textarea pour la traduction de tout le texte, à soumettre à un pair de langue maternelle anglophone
 * prévoir la possibilité de lire un texte phrase par phrase, et d'avoir :
 * un analyseur de textes 
 * -> explode phrases 
 * -> explode mots
 * 
 */
######################################################3
$this->pageTitle = 'Analyse de textes russes'; 
include("incjs.ctp"); //inclusion iframe clavier russe, moteur recherche et scripts DHTML divers
#http://www.php.net/manual/fr/function.chunk-split.php
function UTFChunk($Text,$Len = 100,$End = "\r\n")
{
    if(mb_detect_encoding($Text) == "UTF-8")
    {
        return mb_convert_encoding(
                chunk_split(
                    mb_convert_encoding($Text, "KOI8-R","UTF-8"), $Len,$End
                ),
                "UTF-8", "KOI8-R"
            );
    } else
    {
        return chunk_split($Text,$Len,$End);
    }
}
function splitter($Text,$Len = 300) {
	return str_split($Text,$Len);
}

#russian strtolower
#http://php.net/manual/fr/function.strtolower.php
function strtolower_utf8($string){
  $convert_to = array(
    "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u",
    "v", "w", "x", "y", "z", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï",
    "ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", "û", "ü", "ý", "а", "б", "в", "г", "д", "е", "ё", "ж",
    "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы",
    "ь", "э", "ю", "я"
  );
  $convert_from = array(
    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
    "V", "W", "X", "Y", "Z", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï",
    "Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", "Ü", "Ý", "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж",
    "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ъ",
    "Ь", "Э", "Ю", "Я"
  );

  return str_replace($convert_from, $convert_to, $string);
} 

e($html->css('test', null, array('media' => 'screen')));
e($javascript->link('prototype'));
e($javascript->link('scriptaculous.js?load=effects,controls'));

$id=$_GET['id'];
#echo $id; //tests
$latable="ergo_ru_phrases";
$latableVoc="ergo_rufrs";

//include stopwords
require_once("stopwords.ctp");
//include stemmer
require_once("stemmer.ctp");
$sql="SELECT * FROM " .$latable ." WHERE id=" .$id;
$sqlQ=mysql_query($sql);
if(!$sqlQ){
	echo "error sql: " .mysql_error();
	exit;
}
$texte=mysql_result($sqlQ,0,'text');
############# BEGIN PAGINATION #############
	#pagination des textes longs
	$texteori=$texte;
	$ltexte=strlen($texteori);
	$debut=$_GET['debut'];
	$fin=$_GET['fin'];
	$maxsize=30;
	$npages=$ltexte/$maxsize;

	if(intval($npages)!=$npages){
		$npages=intval($npages)+1;
	}
	if(!$debut){
		$debut=0;
		$fin=$maxsize;
	}

	$pieces = explode(" ", $texte);
	$pieces = array_slice($pieces, $debut, $maxsize); 
	$texte = implode(" ", $pieces);
	echo "<br><div class=\"noteinfo\">Votre texte est trop long pour être analysé en une fois (" .$maxsize ." car. max); il va automatiquement être découpé en tranches</div>";
	#the div is needed for ajax google translator
	echo "<div id=\"Source\">".nl2br($texte)."</div>";

	#pagination
	if(strlen($texteori)>600) {
		$suivantdebut=$fin;
		$suivantfin=$fin+$maxsize;
		$precedentdebut=$debut-$maxsize;
		$precedentfin=$precedentdebut+$maxsize;
			echo "<hr>";	
			if($debut!=0){
				echo "<a href=analyse?id=" .$_GET["id"] ."&debut=0&fin=" .$maxsize ." alt=\"première page\" title=\"première page\">";
				echo $html->image('bd_firstpage.png', array("alt"=>"première page","title"=>"première page"));
				echo "</a>";
				echo "&nbsp;|&nbsp;";
				echo "<a href=analyse?id=" .$_GET["id"] ."&debut=" .$precedentdebut ."&fin=" .$precedentfin ." alt=\"précédent\" title=\"précédent\">";
				echo $html->image('bd_prevpage.png', array("alt"=>"précédent","title"=>"précédent"));	
				echo "</a>";
				echo "&nbsp;|&nbsp;";
			}
			if($fin<$ltexte) {
			echo "<a href=analyse?id=" .$_GET["id"] ."&debut=" .$suivantdebut ."&fin=" .$suivantfin ." alt=\"suivant\" title=\"suivant\">";
				echo $html->image('bd_nextpage.png', array("alt"=>"suivant","title"=>"suivant"));	
			
			echo "</a>";
		}
	}
############# END PAGINATION #############
?>

<!-- #don't remove the div, it is needed for ajax google translator -->

<div id="Target" style="background-color: lightblue"></div>

<?
#remove ponctuation
$texte=eregi_replace(";"," ", $texte);
$texte=eregi_replace("!"," ", $texte);
$texte=eregi_replace("\?"," ", $texte);
$texte=eregi_replace(","," ", $texte);
$texte=eregi_replace("\+"," ", $texte);
$texte=eregi_replace("-"," ", $texte);
$texte=eregi_replace("\("," ", $texte);
$texte=eregi_replace("\)"," ", $texte);
$texte=eregi_replace("&"," ", $texte);
$texte=eregi_replace("\*"," ", $texte);
$texte=eregi_replace("#"," ", $texte);
$texte=eregi_replace("\."," ", $texte);
$texte=preg_replace("/[0-9]*/","",$texte);
$texte=preg_replace("/»/","",$texte);
$texte=preg_replace("/«/","",$texte);

#convert the text into an array of words
$pieces = explode(" ", $texte);

$touslesmots="";
$j=0;
for($i=0;$i<count($pieces);$i++){
	$mot=$pieces[$i];
$motori=$mot; //on garde le mot original
$mot=strtolower_utf8($mot);




//search exact match
  		$all= "SELECT * FROM `" .$latableVoc ."` WHERE `foreign` LIKE '$mot%' LIMIT 0,8";
  		$result = mysql_query($all);
  		if(mysql_num_rows($result)<1) { //no exact match
  		/*
launch stemmer, see
http://drupal.org/node/85361 
http://snowball.tartarus.org/algorithms/russian/stemmer.html
 */
			$mot=normaliserMot($mot);
		}
	


######## SEARCH WORD IN THE DICTIONNARY #########	
	if(strlen($mot)>2&&!ereg(" ",$mot)) {
		$j++;
		if(!eregi(";$mot;",$touslesmots)) { //on affiche que si pas un doublon
  		$touslesmots.= ";" .$mot .";"; //on ajoute le mot dans un tableau pour pas avoir de doublons
  		//recherche du mot dans le vocabulaire
#$motencode=utf8_encode($mot); //encodage utf8 pour le russe
$motencode=$mot; //encodage utf8 pour le russe
  		$all= "SELECT * FROM `" .$latableVoc ."` WHERE `foreign` LIKE '%$motencode%' LIMIT 0,8";
  		
  		#echo "<pre>" .$all ."</pre>"; //tests
  		$result = mysql_query($all);
  		if(mysql_num_rows($result)>0) {
  			$k=0;
  			$francais="";
  			while($k<mysql_num_rows($result)){;
  			  			#$francais.="<input type=\"checkbox\" onClick=\"alert('" .mysql_result($result,$k,'local') ."')\">&nbsp;\n";

  			$francais.="<li>".mysql_result($result,$k,'foreign') .": <em>" .nl2br(mysql_result($result,$k,'local')) ."</em></li>\n\n";
  			$k++;
  			}
  		} else {
			#$link1="http://lingvo.yandex.ru/".json_encode($mot)."/%D0%BF%D0%BE-%D1%84%D1%80%D0%B0%D0%BD%D1%86%D1%83%D0%B7%D1%81%D0%BA%D0%B8/";
			#$link1="http://lingvo.yandex.ru/".json_encode($mot."/по-французски/");
			$lingvo="http://lingvo.yandex.ru/".$motori."/по-французски/";
			#$link1=preg_replace('/"/','',$link1);
			#echo "Search: " .$link1 ."<br>";
  			$francais=" rien trouvé";
			  			#$francais=" <span style=\"background-color: #FFC0CB; padding: 2px\">rien trouvé</span>";
  			
  			#$francais.=" <a target=\"_blank\" href=" .$lingvo.">chercher traduction</a>";
  		}

  		//affichage du résultat
 		echo "<hr><a name=\"mot" .$j ."\"></a><br>Mot " .$j  ." -- <strong>".$mot ."</strong>";
					if($motori!=$mot) {
					echo "&nbsp;&nbsp;<em style=\"color: tomato\">Mot original: " .$motori ."</em>";
					}
 					if(preg_match("/;".$mot.";/i",$stopwords)){
					$mot="";
					echo "<font color=\"grey\"> stopword!</font>";
					} else {
				 
				  		if(strlen($francais)>1) {
				  			echo " #hits: " .mysql_num_rows($result)."";
				  			echo preg_replace("/$mot/","<span style=\"background: #FFFFA9;\">$mot</span>",$francais);
				  		}
					}
echo "<br><br>";


		}
	}
	#echo "<br><form action=\"inseremot\"><input type=\"text\" name=\"trad" .$mot ."\"><input type=\"submit\"></form>";
}

echo "<hr>";

//todo: chercher dans voc, proposer d'insérer si n'existe pas, suggérer traductions
#UTFChunk($texte);
#echo UTFChunk($texte);
#$array=splitter($texte);

$array=str_split($texte,250);
$size=count($array);

	for ($i=0; $i <= $size; $i++){
	echo $array[$i] . "<hr>";

}
#pagination
if(strlen($texteori)>600) {
	$suivantdebut=$fin;
	$suivantfin=$fin+$maxsize;
	$precedentdebut=$debut-$maxsize;
	$precedentfin=$precedentdebut+$maxsize;
		echo "<hr>";	
		if($debut!=0){
			echo "<a href=analyse?id=" .$_GET["id"] ."&debut=0&fin=" .$maxsize ." alt=\"première page\" title=\"première page\">";
			echo $html->image('bd_firstpage.png', array("alt"=>"première page","title"=>"première page"));
			echo "</a>";
			echo "&nbsp;|&nbsp;";
			echo "<a href=analyse?id=" .$_GET["id"] ."&debut=" .$precedentdebut ."&fin=" .$precedentfin ." alt=\"précédent\" title=\"précédent\">";
			echo $html->image('bd_prevpage.png', array("alt"=>"précédent","title"=>"précédent"));	
			echo "</a>";
			echo "&nbsp;|&nbsp;";
		}
		if($fin<$ltexte) {
		echo "<a href=analyse?id=" .$_GET["id"] ."&debut=" .$suivantdebut ."&fin=" .$suivantfin ." alt=\"suivant\" title=\"suivant\">";
			echo $html->image('bd_nextpage.png', array("alt"=>"suivant","title"=>"suivant"));	
		
		echo "</a>";
	}
}

?>


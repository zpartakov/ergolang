<?
/**
		* @version        $Id: view.ctp v1.0 18.12.2009 15:32:15 CET $
		* @package        Эrgolang
		* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
		* @license        GNU/GPL, see LICENSE.php
		* Эrgolang is free software. This version may have been modified pursuant
		* to the GNU General Public License, and as distributed it includes or
		* is derivative of works licensed under the GNU General Public License or
		* other free or open source software licenses.
		* See COPYRIGHT.php for copyright notices and details.
		*/        
		#source code pagination http://bakery.cakephp.org/articles/view/pagination
$this->pageTitle = 'Textes russes: Vue'; 

?>
<div class="ergoRuPhrases view">
<h1><? echo $this->pageTitle; ?></h1>
<!-- ajout traduction automatique google -->
<div id="google_translate_element"></div><script>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'ru'
  }, 'google_translate_element');
}
</script><script src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>




			<?php 
			echo "<ul><li><a href=\"../analyse?id=" .$ergoRuPhrase['ErgoRuPhrase']['id'] ."\">Analyse</a></li></ul>"; 
			?>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuPhrase['ErgoRuPhrase']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Auteur'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuPhrase['ErgoRuPhrase']['auteur']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Titre'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuPhrase['ErgoRuPhrase']['titre']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Url'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuPhrase['ErgoRuPhrase']['url']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Text'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo nl2br($ergoRuPhrase['ErgoRuPhrase']['text']); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Traduction'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo nl2br($ergoRuPhrase['ErgoRuPhrase']['traduction']); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Date'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuPhrase['ErgoRuPhrase']['date']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modif'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuPhrase['ErgoRuPhrase']['modif']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Modifer le texte', true), array('action'=>'edit', $ergoRuPhrase['ErgoRuPhrase']['id'])); ?> </li>
		<li><?php echo $html->link(__('Effacer le texte', true), array('action'=>'delete', $ergoRuPhrase['ErgoRuPhrase']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoRuPhrase['ErgoRuPhrase']['id'])); ?> </li>
		<li><?php echo $html->link(__('Lister les textes', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('Nouveau texte', true), array('action'=>'add')); ?> </li>
	</ul>
</div>

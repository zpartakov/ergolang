<div class="ergoRuPhrases view">
<h2><?php  __('ErgoRuPhrase');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuPhrase['ErgoRuPhrase']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Titre'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuPhrase['ErgoRuPhrase']['titre']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Url'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuPhrase['ErgoRuPhrase']['url']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Text'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuPhrase['ErgoRuPhrase']['text']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Date'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuPhrase['ErgoRuPhrase']['date']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modif'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuPhrase['ErgoRuPhrase']['modif']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit ErgoRuPhrase', true), array('action'=>'edit', $ergoRuPhrase['ErgoRuPhrase']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete ErgoRuPhrase', true), array('action'=>'delete', $ergoRuPhrase['ErgoRuPhrase']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoRuPhrase['ErgoRuPhrase']['id'])); ?> </li>
		<li><?php echo $html->link(__('List ErgoRuPhrases', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New ErgoRuPhrase', true), array('action'=>'add')); ?> </li>
	</ul>
</div>

<?php
/**
* @version        $Id: index.ctp v1.0 20.12.2009 20:33:09 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
### def some constants
if($_SERVER["HTTP_HOST"]=="localhost"||$_SERVER["HTTP_HOST"]=="129.194.18.197") { //serveur de développement - tests
$CHEMIN="/ergolang/cake";
} elseif ($_SERVER["HTTP_HOST"]=="oblomov.info"){ //serveur de prod
$CHEMIN="/websites/ergolang/cake";
}
$this->pageTitle = 'Textes russes'; 

?>
<h1><? echo $this->pageTitle; ?></h1>

<div class="ergoRuPhrases index">
<?
#### news ######
echo '<a href="/websites/ergolang/cake/ergoRuPhrases/index/page:1/sort:modif/direction:desc" alt="Nouveautés" title="Nouveautés">'.$html->image('new.jpg', array("alt"=>"Nouveautés","title"=>"Nouveautés","class"=>"image")).'</a>';
?>
<div id="search_box">
<h4>Chercher</h4>
<!-- begin search form -->
 <table>
	 <tr>
		 <td>
<form id="ergoRuPhraseAddForm" name="" method="GET" action="/websites/ergolang/cake/ergo_ru_phrases/search">
<fieldset style="display:none;">
</fieldset>
<div class="input text">
<input name="search_text" type="text" value="" id="ergoRuPhraseSearchText" class="txttosearch"/>
</div>
</td><td>
<div class="submit">
<input type="button" class="vider" value="Vider" onClick="javascript:vide_recherche('ergoRuPhraseSearchText')" />
<input type="submit" class="chercher" value="Chercher" />
</div>
</form>
</td>
</tr>
</table>
<!-- end search form -->
</div> 

<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Nouveau texte', true), array('action'=>'add')); ?></li>
	</ul>
</div>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('auteur');?></th>
	<th><?php echo $paginator->sort('titre');?></th>
	<th><?php echo $paginator->sort('url');?></th>
	<th><?php echo $paginator->sort('modif');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($ergoRuPhrases as $ergoRuPhrase):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $ergoRuPhrase['ErgoRuPhrase']['id']; ?>
		</td>
		<td>
			<?php echo $ergoRuPhrase['ErgoRuPhrase']['auteur']; ?>
		</td>
				<td>
			<?php echo $ergoRuPhrase['ErgoRuPhrase']['titre']; ?>
		</td>
		<td>
		<?
		if($ergoRuPhrase['ErgoRuPhrase']['url']){
		echo "<a href=\"" .$ergoRuPhrase['ErgoRuPhrase']['url'] ."\" target=\"_blank\">";
echo $html->image('urlextern.png', array("alt"=>"URL Source","title"=>"URL Source"));
echo "</a>";
}
		?>
		
		
		</td>
		<td>
			<?php echo $ergoRuPhrase['ErgoRuPhrase']['modif']; ?>
		</td>
		<td class="actions">

			
			<?php 
		echo "<a href=\"" .$CHEMIN ."/ergo_ru_phrases/analyse?id=" .$ergoRuPhrase['ErgoRuPhrase']['id'] ."&debut=0&fin=0\">";
echo $html->image('b_index.png', array("alt"=>"Analyser","title"=>"Analyser"));
echo "</a>";
echo "&nbsp;";
	
echo "<a href=\"" .$CHEMIN ."/ergo_ru_phrases/view/" .$ergoRuPhrase['ErgoRuPhrase']['id'] ."\">";
echo $html->image('b_search.png', array("alt"=>"Voir","title"=>"Voir"));
echo "</a>";
if($session->read('Auth.User.group_id')==3) {

echo "&nbsp;";
echo "<a href=\"".$CHEMIN ."/ergo_ru_phrases/edit/" .$ergoRuPhrase['ErgoRuPhrase']['id'] ."\">";
echo $html->image('b_edit.png', array("alt"=>"Modifier","title"=>"Modifier"));
echo "</a>";
echo "<a onclick=\"javascript:return confirm('Confirmer la suppression ?')\" href=\"" .$CHEMIN ."/ergo_ru_phrases/delete/" .$ergoRuPhrase['ErgoRuPhrase']['id'] ."\">";
echo $html->image('b_drop.png', array("alt"=>"Effacer","title"=>"Effacer"));
echo "</a>";
}
?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Nouveau texte', true), array('action'=>'add')); ?></li>
	</ul>
</div>

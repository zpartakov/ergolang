<?php
/**
* @version        $Id: add.ctp v1.0 27.06.2010 08:13:19 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Nouveau texte russe'; 
?>

<div class="ergoRuPhrases form">
<?php echo $form->create('ErgoRuPhrase');?>
	<fieldset>
 		<legend><?php echo $this->pageTitle;?></legend>
	<?php
		echo $form->input('auteur');
		echo $form->input('titre');
		echo $form->input('url');
		echo $form->input('text',array('cols' => 80, 'rows'=>55,));
		echo $form->input('traduction',array('cols' => 80, 'rows'=>55,));
		echo $form->input('date');
		echo $form->input('modif');
	?>
	Texte privé?&nbsp;
	<input type="checkbox" id="texteprive" onChange="javascript:rendprive('ErgoRuPhraseTitre');">
	</fieldset>
<?php echo $form->end('Soumettre');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Textes', true), array('action'=>'index'));?></li>
	</ul>
</div>

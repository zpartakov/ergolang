<?php
/**
* @version        $Id: edit.ctp v1.0 27.06.2010 09:19:41 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Modifier texte russe'; 

?>
<div class="ergoRuPhrases form">
<?php echo $form->create('ErgoRuPhrase');?>
	<fieldset>
 		<legend><?php __('Edit ErgoRuPhrase');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('auteur');
		echo $form->input('titre');
		echo $form->input('url');
		echo $form->input('text',array('cols' => 80, 'rows'=>80));
		echo $form->input('traduction',array('cols' => 80, 'rows'=>80));
		echo $form->input('date');
		echo $form->input('modif');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action'=>'delete', $form->value('ErgoRuPhrase.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('ErgoRuPhrase.id'))); ?></li>
		<li><?php echo $html->link(__('List ErgoRuPhrases', true), array('action'=>'index'));?></li>
	</ul>
</div>

<div class="ergoRuPhrases form">
<?php echo $form->create('ErgoRuPhrase');?>
	<fieldset>
 		<legend><?php __('Add ErgoRuPhrase');?></legend>
	<?php
		echo $form->input('titre');
		echo $form->input('url');
		echo $form->input('text');
		echo $form->input('date');
		echo $form->input('modif');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List ErgoRuPhrases', true), array('action'=>'index'));?></li>
	</ul>
</div>

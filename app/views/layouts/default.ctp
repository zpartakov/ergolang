<?php
/**
* @version        $Id: default.ctp v1.0 09.12.2009 06:25:39 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
             "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<?php
$letitrep="Эrgolang";
$CHEMIN="websites/ergolang/cake";
#### end test which server are we running on? ####
$racineweb=$_SERVER["HTTP_HOST"]. "/" .$CHEMIN ."/";
/*top of the page
 * src http://www.dynamicdrive.com/dynamicindex3/scrolltop.htm*/
echo $javascript->link('jquery.min.js');
echo $javascript->link('scrolltopcontrol.js');
?>
<title><? echo $letitrep ." - " .$this->pageTitle ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta name="generator" content="Эrgolang / phpCake" />
   <?php e($scripts_for_layout); ?>
<!--   <?php e($html->css('quickwall')); ?>-->
<?=$html->css('cake.generic');?>
<?#=$html->css('styles');?>
<?=$html->css('hiermenu');?>
<?=$html->css('ergolang');?>
<?=$html->css(array('print'), 'stylesheet', array('media' => 'print'));?>

<?php
e($html->css('autocomplete', null, array('media' => 'screen')));
echo $javascript->link('prototype.js'); //commented radeff because of conflit with jquery see http://oblomov.info/dokuwiki/info:jquery
#echo $javascript->link('scriptaculous.js?load=effects,controls');
echo $javascript->link('scriptaculous.js');
echo $javascript->link('ergolang.js');

/*floating keyboard * */
echo $javascript->link('keyboard.js');
?>
<?#=$html->css('css_menu.css');?>
<?=$html->css('keyboard.css');?>
<link rel="shortcut icon" href="/websites/ergolang/cake/favicon.ico" type="image/x-icon" />
<?php e($html->meta('flux rss posts', '/posts/index.rss'));?>

</head>

<body>
<?
##########################################
//setting language
if($_GET["langue"]) {
$lalang=$_GET["langue"];
}
$utilisateur=$session->read('Auth.User.id');
$langsess=$session->read("langue");


if(strlen($lalang)>1) { //the lang has been passed by a GET
#echo "getlang";
	if(strlen($utilisateur)>0) {//registered user, change default lang
		maj_session($utilisateur,$lalang);
		$this -> Session -> write("langue", $lalang);	//both define language
	} else {
		$this -> Session -> write("langue", $lalang);	//both define language
	}
header("Location: /websites/ergolang/cake/".$lalang);

	} else { //no selected language, check session
#	echo "nogetlang";

	if(strlen($langsess)>1) { //the lang is set in the session, nothing to do
	#echo "nogetlang but session yes";
	} else {
			#echo "£nogetlang and no session, user: " .$utilisateur;

		if(strlen($utilisateur)>0) { //registered user
			$lalang="0";
			#todo£ correct this!!!
			#maj_session($utilisateur,$lalang);
		} else { //anonymous
			#echo "nogetlang and no session anonymous";
			//nothing todo
		}

	}
}

###########################################
?>
<noscript>
<h1>Activez JavaScript! si vous voulez bénéficiez de toutes les fonctionnalités de ce site</h1>
</noscript>

	<div id="logo">

<h1 class="header">
	<?php
	echo '<a href="/websites/ergolang/cake/" title="Accueil Эrgolang">'.$html->image('logoergolang.jpg', array("alt"=>"Accueil Эrgolang","title"=>"Accueil Эrgolang","width"=>"20%","height"=>"20%")).'</a>';
	?>
<span style="position: relative; top: -8px"> apprentissage de langues collaboratif &amp; open-source</span></h1>
	<div class="multiclavier"><input type="text" value="" class="keyboardInput" />

</div>


	</div>

<!-- navigation -->
<div id="leftnav" class="menu">
<?php echo $this->element('menu');?>
</div>

<!-- content -->

		<div id="container">


    <div id="content">

    <br />

    <?php
    //get the authentification informations
    $session->flash();
    $session->flash('auth');

    ?>
    <?php
	#echo "Session langue: " .$_SESSION['langue']; //tests
	?>
         <?=$content_for_layout;?>
     </div>
     </div>
<div id="footer">
<!-- footer -->
<div class="help">
<table>
	<tr>
		<td class="tablepied"><?
echo "<a href=\"/dokuwiki/ergolang:homepage\">";
echo $html->image('help.png', array("alt"=>"Aide","title"=>"Aide","width"=>"50px","height"=>"50px"));
echo "</a>";
?>
 </td>

<td class="tablepied">
<?php
//print page
echo "<a class=\"logoprint\" href=\"javascript:window.print();\">";
echo $html->image('icon-print.jpg', array("alt"=>"Imprimer","title"=>"Imprimer"));
echo "</a>";
?>
</td>
<!-- about -->
<td class="tablepied">
<?php
echo '<a class="contact" href="https://radeff.red/apropos" title="About">'.$html->image('linux/tux_che.jpg', array("alt"=>"About")).'</a>';
?>
</td>
<!-- contact -->
<td class="tablepied">
<?php
echo '<a class="contact" href="https://radeff.red/contact" title="Contact">'.$html->image('ico-contact.gif', array("alt"=>"Contact")).'</a>';
?>
</td>

<td class="tablepied">
<?
//license
echo '<a href="http://www.gnu.org/licenses/gpl.html">'.$html->image('copyleft.jpg', array("alt"=>"GPL License","title"=>"GPL License","width"=>"45","height"=>"45")).'</a>';
?>
</td></tr>
</table>

</div>


</div>

 </body>
 </html>

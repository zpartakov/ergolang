<?php
/**
* @version        $Id: default.ctp v1.0 29.03.2010 06:42:51 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
	<body>
		<?php echo $content_for_layout; ?>
		<hr>
		Cet email vous a été envoyé automatiquement par <a href="http://oblomov.info/websites/ergolang/">Эrgolang</a> - <a href=mailto:ergolang@akademia.ch">ergolang@akademia.ch</a>
	</body>
</html>

<?php 
/* SVN FILE: $Id$ */
/* Project Fixture generated on: 2009-04-03 13:04:18 : 1238757378*/

class ProjectFixture extends CakeTestFixture {
	var $name = 'Project';
	var $table = 'projects';
	var $fields = array(
			'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 8, 'key' => 'primary'),
			'organization' => array('type'=>'integer', 'null' => false, 'default' => '0', 'length' => 8),
			'owner' => array('type'=>'integer', 'null' => false, 'default' => '0', 'length' => 8),
			'priority' => array('type'=>'integer', 'null' => false, 'default' => '0', 'length' => 8),
			'status' => array('type'=>'integer', 'null' => false, 'default' => '0', 'length' => 8),
			'name' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 155),
			'description' => array('type'=>'text', 'null' => true, 'default' => NULL),
			'url_dev' => array('type'=>'string', 'null' => true, 'default' => NULL),
			'url_prod' => array('type'=>'string', 'null' => true, 'default' => NULL),
			'created' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 16),
			'modified' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 16),
			'published' => array('type'=>'string', 'null' => false, 'length' => 1),
			'upload_max' => array('type'=>'string', 'null' => true, 'default' => NULL, 'length' => 155),
			'phase_set' => array('type'=>'integer', 'null' => false, 'default' => '0', 'length' => 8),
			'type' => array('type'=>'string', 'null' => false, 'default' => '0', 'length' => 1),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
			);
	var $records = array(array(
			'id'  => 1,
			'organization'  => 1,
			'owner'  => 1,
			'priority'  => 1,
			'status'  => 1,
			'name'  => 'Lorem ipsum dolor sit amet',
			'description'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,
									phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,
									vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,
									feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.
									Orci aliquet, in lorem et velit maecenas luctus, wisi nulla at, mauris nam ut a, lorem et et elit eu.
									Sed dui facilisi, adipiscing mollis lacus congue integer, faucibus consectetuer eros amet sit sit,
									magna dolor posuere. Placeat et, ac occaecat rutrum ante ut fusce. Sit velit sit porttitor non enim purus,
									id semper consectetuer justo enim, nulla etiam quis justo condimentum vel, malesuada ligula arcu. Nisl neque,
									ligula cras suscipit nunc eget, et tellus in varius urna odio est. Fuga urna dis metus euismod laoreet orci,
									litora luctus suspendisse sed id luctus ut. Pede volutpat quam vitae, ut ornare wisi. Velit dis tincidunt,
									pede vel eleifend nec curabitur dui pellentesque, volutpat taciti aliquet vivamus viverra, eget tellus ut
									feugiat lacinia mauris sed, lacinia et felis.',
			'url_dev'  => 'Lorem ipsum dolor sit amet',
			'url_prod'  => 'Lorem ipsum dolor sit amet',
			'created'  => 'Lorem ipsum do',
			'modified'  => 'Lorem ipsum do',
			'published'  => 'Lorem ipsum dolor sit ame',
			'upload_max'  => 'Lorem ipsum dolor sit amet',
			'phase_set'  => 1,
			'type'  => 'Lorem ipsum dolor sit ame'
			));
}
?>
<?php	
/**
* @version        $Id: auto_complete.ctp v1.0 12.01.2010 14:10:11 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
* adapted from http://book.cakephp.org/view/632/autoComplete
*/
<ul>
 <?php foreach($posts as $post): ?>
     <li><?php echo $post['Post']['title']; ?></li>
 <?php endforeach; ?>
</ul> 
?>

<?php
/**
* @version        $Id: index.ctp v1.0 26.03.2010 14:43:03 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Nouvelles d\'Эrgolang'; 

?>
<div class="posts index">
<h1><?
//license
echo '<a href="http://oblomov.info/websites/ergolang/cake/posts/index.rss">'.$html->image('rss.png', array("alt"=>"Flux RSS","title"=>"Flux RSS")).'</a>';
?><? echo $this->pageTitle; ?></h1>

<!-- begin search form -->
 <table>
	 <tr>
		 <td>
 <div class="input">
<?php echo $form->create('Post', array('class'=>'#cherche', 'url' => array('action' => 'index'))); ?>
		<?php #echo $form->input('q', array('style' => 'width: 250px;', 'label' => false, 'size' => '80')); ?>
		<?php 
				#echo $form->input('q', array('label' => false, 'size' => '50', 'value'=>'Chercher...', 'class'=>'txttosearch')); 
		echo $form->input('q', array('label' => false, 'size' => '50', 'class'=>'txttosearch'));
		?>
		</div>
</td><td>
<input type="button" class="vider" value="Vider" value="Vider" onClick="javascript:vide_recherche('PostQ')" />
<input type="submit" class="chercher" value="Chercher" /> 
</div> 
</td>
</tr>
</table>
<!-- end search form -->

<?
if($session->read('Auth.User.username')){
?>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Nouveau message', true), array('action'=>'add')); ?></li>
	</ul>
</div>
<?
}
?><p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
<!--	<th><?php echo $paginator->sort('id');?></th>-->
	<th><?php echo $paginator->sort('Titre','title');?></th>
	<th><?php echo $paginator->sort('Message','body');?></th>
	<th><?php echo $paginator->sort('Créé','created');?></th>
	<!--<th><?php echo $paginator->sort('modified');?></th>-->
	<th><?php echo $paginator->sort('Utilisateur','user_id');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($posts as $post):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<!--<td>
			<?php echo $post['Post']['id']; ?>
		</td>-->
		<td>
			<?php #echo $post['Post']['title']; ?>
						<?php 
			#echo $html->link(__('View', true), array('action'=>'view', $post['post']['id'])); 
			
			echo "<a href=\"" .CHEMIN ."/posts/view/" .$post['Post']['id'] ."\">";
			echo $post['Post']['title'];
echo "</a>";
			?>
		</td>
		<td>
			<?php 
					$url=$post['Post']['body'];
		//on coupe les libellés trop longs
				if(strlen($url)>270) {
				$url= first_words($url,6);
			}
			echo nl2br($url);
			#echo $post['Post']['body']; ?>
		</td>
		<td>
			<?php echo $post['Post']['created']; ?>
		</td>
		<!--<td>
			<?php echo $post['Post']['modified']; ?>
		</td>-->
		<td>
			<?php 
			#echo $post['Post']['user_id']; 
			echo $post['User']['username']; 
			
			?>
		</td>
		<td class="actions">
			<?php 
			#echo $html->link(__('View', true), array('action'=>'view', $post['post']['id'])); 
			
			echo "<a href=\"" .CHEMIN ."/posts/view/" .$post['Post']['id'] ."\">";
echo $html->image('b_search.png', array("alt"=>"Voir","title"=>"Voir"));
echo "</a>";
			?>
			<?
			/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
	?>
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $post['Post']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $post['Post']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $post['Post']['id'])); ?>
			<?
		}
		?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>

<?
if($session->read('Auth.User.username')){
?>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Nouveau message', true), array('action'=>'add')); ?></li>
	</ul>
</div>
<?
}
?>

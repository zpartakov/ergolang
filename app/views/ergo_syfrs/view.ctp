<div class="ergoSyfrs view">
<h2><?php  __('Ergo Syfr');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoSyfr['ErgoSyfr']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Foreign'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoSyfr['ErgoSyfr']['foreign']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Local'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoSyfr['ErgoSyfr']['local']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?	/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
	?>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ergo Syfr', true), array('action' => 'edit', $ergoSyfr['ErgoSyfr']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Ergo Syfr', true), array('action' => 'delete', $ergoSyfr['ErgoSyfr']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoSyfr['ErgoSyfr']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Ergo Syfrs', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ergo Syfr', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
<?
}
?>
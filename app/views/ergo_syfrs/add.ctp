<div class="ergoSyfrs form">
<?php echo $this->Form->create('ErgoSyfr');?>
	<fieldset>
		<legend><?php __('Add Ergo Syfr'); ?></legend>
	<?php
		echo $this->Form->input('foreign');
		echo $this->Form->input('local');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Ergo Syfrs', true), array('action' => 'index'));?></li>
	</ul>
</div>
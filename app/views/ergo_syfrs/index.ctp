<?php
/**
* @version		$Id: index.ctp v1.0 13.07.2011 11:39:01 CEST $
* @package		Эrgolang
* @copyright	Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Dictionnaire Syrien <=> Français'; 

?>
<div class="ergoSyfrs index">
<h1><? echo $this->pageTitle; ?></h1>
<!-- begin search form -->
 <table>
	 <tr>
		 <td>
 <div class="input">
<?php echo $form->create('ErgoSyfr', array('url' => array('action' => 'index'))); ?>
		<?php #echo $form->input('q', array('style' => 'width: 250px;', 'label' => false, 'size' => '80')); ?>
		<?php echo $form->input('q', array('label' => false, 'size' => '50', 'class'=>'txttosearch')); ?>
		</div>
</td><td>
<input type="button" class="vider" value="Vider" onClick="javascript:vide_recherche('ErgoSyfrQ')" />
<input type="submit" class="chercher" value="Chercher" /> 
</div> 
</td>
</tr>
</table>
<!-- end search form -->
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('foreign');?></th>
			<th><?php echo $this->Paginator->sort('local');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($ergoSyfrs as $ergoSyfr):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $ergoSyfr['ErgoSyfr']['foreign']; ?>&nbsp;</td>
		<td><?php echo $ergoSyfr['ErgoSyfr']['local']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $ergoSyfr['ErgoSyfr']['id'])); ?>
			
						<?php 
						/*	hide from non-admin registred user */
			if($session->read('Auth.User.group_id')==3||$session->read('Auth.User.group_id')==6) {
			echo "&nbsp;";	
			echo $this->Html->link(__('Edit', true), array('action' => 'edit', $ergoSyfr['ErgoSyfr']['id']));
			echo $this->Html->link(__('Delete', true), array('action' => 'delete', $ergoSyfr['ErgoSyfr']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoSyfr['ErgoSyfr']['id'])); 
		}
			?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<?php 
						/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3||$session->read('Auth.User.group_id')==6) {
	?>
	<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Ergo Syfr', true), array('action' => 'add')); ?></li>
	</ul>
</div>
<?
}
?>

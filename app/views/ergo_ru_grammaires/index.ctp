<?php
/**
* @version        $Id: index.ctp v1.0 13.03.2010 06:25:08 CET $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Grammaire Russe'; 

?>
<h1><? echo $this->pageTitle; ?></h1>

/websites/ergolang/cake/ergo_ru_grammaires/vue/

<div class="ergoRuGrammaires index">
<!-- begin search form -->
 <table>
	 <tr>
		 <td>
 <div class="input">
<?php echo $form->create('ErgoRuGrammaire', array('url' => array('action' => 'index'))); ?>
		<?php #echo $form->input('q', array('style' => 'width: 250px;', 'label' => false, 'size' => '80')); ?>
		<?php echo $form->input('q', array('label' => false, 'size' => '50', 'class'=>'txttosearch')); ?>
		</div>
</td><td>
<input type="button" class="vider" value="Vider" onClick="javascript:vide_recherche('ErgoRuGrammaireQ')" />
<input type="submit" class="chercher" value="Chercher" /> 
</div> 
</td>
</tr>
</table>
<!-- end search form -->
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Nouvelle grammaire', true), array('action'=>'add')); ?></li>
	</ul>
</div>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('ergo_ru_type_id');?></th>
	<th><?php echo $paginator->sort('lib');?></th>
	<th><?php echo $paginator->sort('text');?></th>
	<th><?php echo $paginator->sort('url');?></th>
	<th><?php echo $paginator->sort('rem');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($ergoRuGrammaires as $ergoRuGrammaire):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php 
			echo $ergoRuGrammaire['ErgoRuGrammaire']['id']; 
?>
	
		</td>
		<td>
			<?php 
			#echo $ergoRuGrammaire['ErgoRuGrammaire']['ergo_ru_type_id']; 
			#todo: put it in an array / function, here it take too much ressources
			$sql="SELECT * FROM ergo_ru_types WHERE id=" .$ergoRuGrammaire['ErgoRuGrammaire']['ergo_ru_type_id'];
$sql=mysql_query($sql);
	echo mysql_result($sql,0,'lib');

			?>
		</td>
		<td>
			<?php echo $ergoRuGrammaire['ErgoRuGrammaire']['lib']; ?>
		</td>
		<td>
			<?php 
			if(strlen($ergoRuGrammaire['ErgoRuGrammaire']['text'])>0){
				echo "+";
				} else {
					echo "";
				}
				
				 ?>
		</td>
		<td>
			<?php echo $ergoRuGrammaire['ErgoRuGrammaire']['url']; ?>
		</td>
		<td>
			<?php echo $ergoRuGrammaire['ErgoRuGrammaire']['rem']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action'=>'view', $ergoRuGrammaire['ErgoRuGrammaire']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $ergoRuGrammaire['ErgoRuGrammaire']['id'])); ?>
			<br>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $ergoRuGrammaire['ErgoRuGrammaire']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoRuGrammaire['ErgoRuGrammaire']['id'])); ?>
			<?php echo "<a target=\"_blank\" href=\"/websites/ergolang/cake/ergo_ru_grammaires/vue/".$ergoRuGrammaire['ErgoRuGrammaire']['id'] ."\">vue</a>"; ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New ErgoRuGrammaire', true), array('action'=>'add')); ?></li>
	</ul>
</div>

<?php
/**
* @version        $Id: view.ctp v1.0 15.03.2010 11:08:05 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Grammaire Russe- view'; 

?>
<h1><? echo $this->pageTitle; ?></h1>

<div class="ergoRuGrammaires view">
<h2><?php  __('ErgoRuGrammaire');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuGrammaire['ErgoRuGrammaire']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Ergo Ru Type Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php 

$sql="SELECT * FROM ergo_ru_types WHERE id=" .$ergoRuGrammaire['ErgoRuGrammaire']['ergo_ru_type_id'];
$sql=mysql_query($sql);
	echo mysql_result($sql,0,'lib')
?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Lib'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuGrammaire['ErgoRuGrammaire']['lib']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Text'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuGrammaire['ErgoRuGrammaire']['text']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Url'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuGrammaire['ErgoRuGrammaire']['url']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Rem'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuGrammaire['ErgoRuGrammaire']['rem']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit ErgoRuGrammaire', true), array('action'=>'edit', $ergoRuGrammaire['ErgoRuGrammaire']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete ErgoRuGrammaire', true), array('action'=>'delete', $ergoRuGrammaire['ErgoRuGrammaire']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoRuGrammaire['ErgoRuGrammaire']['id'])); ?> </li>
		<li><?php echo $html->link(__('List ErgoRuGrammaires', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New ErgoRuGrammaire', true), array('action'=>'add')); ?> </li>
	</ul>
</div>

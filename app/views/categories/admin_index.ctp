<?php
// {app}/views/categories/admin_index.ctp
$this->pageTitle = "Arborescence des catégories";
 
echo $tree->generate(
	$categories,
	array('element' => 'categories_admin_index')
);
?>

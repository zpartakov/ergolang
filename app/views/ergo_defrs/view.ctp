<?php
/**
* @version        $Id: view.ctp v1.0 14.10.2010 06:05:54 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Dictionnaire Allemand <=> Français: Vue'; 

?>
<div class="ergoDefrs view">
<h1><? echo $this->pageTitle; ?></h1>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoDefr['ErgoDefr']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Foreign'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoDefr['ErgoDefr']['foreign']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Local'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoDefr['ErgoDefr']['local']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Type'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoDefr['ErgoDefr']['type']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Date'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoDefr['ErgoDefr']['date']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?	/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
	?>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ergo Defr', true), array('action' => 'edit', $ergoDefr['ErgoDefr']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Ergo Defr', true), array('action' => 'delete', $ergoDefr['ErgoDefr']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoDefr['ErgoDefr']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Ergo Defrs', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ergo Defr', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
<?
}
?>

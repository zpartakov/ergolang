<?php
/**
* @version        $Id: fck.php v1.0 22.03.2010 06:51:49 CET $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
#todo: not working properly!
*/
?>
<?php 
class FckHelper extends Helper
{
	function load($did, $toolbar = 'Default') {
		/*foreach (explode('/', $id) as $v) {
	 		$did .= ucfirst($v);
		}
*/

	
$CHEMIN="websites/ergolang/cake";

		return <<<FCK_CODE
<script type="text/javascript">
fckLoader_$did = function () {
    var bFCKeditor_$did = new FCKeditor('$did');
    bFCKeditor_$did.BasePath = '/$CHEMIN/js/';
    bFCKeditor_$did.ToolbarSet = '$toolbar';
    bFCKeditor_$did.ReplaceTextarea();
}
fckLoader_$did();
</script>
FCK_CODE;
	}
}

?>

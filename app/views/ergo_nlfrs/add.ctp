<div class="ergoNlfrs form">
<?php echo $this->Form->create('ErgoNlfr');?>
	<fieldset>
 		<legend><?php __('Add Ergo Nlfr'); ?></legend>
	<?php
		echo $this->Form->input('foreign');
		echo $this->Form->input('local');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Ergo Nlfrs', true), array('action' => 'index'));?></li>
	</ul>
</div>
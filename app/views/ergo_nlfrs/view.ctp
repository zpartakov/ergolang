<div class="ergoNlfrs view">
<h2><?php  __('Ergo Nlfr');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoNlfr['ErgoNlfr']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Foreign'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoNlfr['ErgoNlfr']['foreign']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Local'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoNlfr['ErgoNlfr']['local']; ?>
			&nbsp;
		</dd>
	</dl>
	<br/>
<p>Source: <a href="http://www.slowniki.org.pl/holendersko-francuski.pdf">Dictionnaire néerlandais-français Georges Kazojc</a><br/><strong>Licence du eBook</strong>: <a href="http://www.slowniki.org.pl/licencja.pdf">Creative Commons</a></p>
	
</div>
<?	/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
	?>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ergo Nlfr', true), array('action' => 'edit', $ergoNlfr['ErgoNlfr']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Ergo Nlfr', true), array('action' => 'delete', $ergoNlfr['ErgoNlfr']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoNlfr['ErgoNlfr']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Ergo Nlfrs', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ergo Nlfr', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
<?
}
?>

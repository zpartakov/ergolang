<?php
/**
* @version        $Id: index.ctp v1.0 26.03.2010 08:48:44 CET $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$CHEMIN="/websites/ergolang/cake";
?>
<div class="ergoMyRuVocs index">
<h2><?php __('ErgoMyRuVocs');?></h2>
		<li><?php echo $html->link(__('Nouveau mot', true), array('controller'=> 'ergo_rufrs', 'action'=>'add')); ?></li>
<!-- begin search form -->
 
 <table>
	 <tr>
		 <td>
 <div class="input">
<?php echo $form->create('ErgoRufr', array('url' => array('action' => 'index'))); ?>
		<?php echo $form->input('q', array('style' => 'width: 250px;', 'label' => false)); ?>
		
<?php 
/*
e($ajax->autoComplete(
		'ErgoRufr.q',
		'/ergoRufrs/autocomplete',
		array(
			'minChars' => 3,
			'indicator' => 'ajaxloader',
			'style' => 'width: 750px;'
		)
	)); 
	*/
	?> 
<!--
	<div id="ajaxloader" style="display:none;">
		Chargement...
	</div>
-->


</div>
</td><td>
  <?php echo $form->end('Chercher'); ?> 
</div> 
</td>
</tr>
</table>
<!-- end search form -->
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
		<!--<th><?php echo $paginator->sort('id');?></th>
<th><?php echo $paginator->sort('user_id');?></th>
	<th><?php echo $paginator->sort('ergo_ru_voc_id');?></th>-->
	<th><?php echo $paginator->sort('Russe','ErgoRufr.foreign');?></th>
	<th><?php echo $paginator->sort('Français','ErgoRufr.local');?></th>
	<th><?php echo $paginator->sort('date');?></th>
	<?
	/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
	?>
	<th class="actions"><?php __('Actions');?></th>
	<?
}
?>
</tr>
<?php
$i = 0;
foreach ($ergoMyRuVocs as $ergoMyRuVoc):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<!--<td>
			<?php echo $ergoMyRuVoc['ErgoMyRuVoc']['id']; ?>
		</td>
		<td>
			<?php echo $ergoMyRuVoc['ErgoMyRuVoc']['user_id']; ?>
		</td>-->
		<td>
			<?php echo $html->link($ergoMyRuVoc['ErgoRufr']['foreign'], array('controller'=> 'ergo_rufrs', 'action'=>'view', $ergoMyRuVoc['ErgoRufr']['id'])); ?>
		</td>
				<td>
			<?php echo $html->link($ergoMyRuVoc['ErgoRufr']['local'], array('controller'=> 'ergo_rufrs', 'action'=>'view', $ergoMyRuVoc['ErgoRufr']['id'])); ?>
		</td>
		<td>
			<?php 
			#echo $ergoMyRuVoc['ErgoMyRuVoc']['date']; 
			
			$timestamp = strtotime($ergoMyRuVoc['ErgoMyRuVoc']['date']);
e(strftime("%d&nbsp;%b&nbsp;%Y,&nbsp;%Hh%M", $timestamp));?>
		</td>
<?
	/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
	?>
	<td class="actions">
<?php 
echo "<a href=\"" .$CHEMIN ."/ergoRufrs/view/" .$ergoMyRuVoc['ErgoMyRuVoc']['ergo_ru_voc_id'] ."\">";
echo $html->image('b_search.png', array("alt"=>"Voir","title"=>"Voir"));
echo "</a>";


echo "&nbsp;";
echo "<a href=\"".$CHEMIN ."/ergoRufrs/edit/" .$ergoMyRuVoc['ErgoMyRuVoc']['ergo_ru_voc_id'] ."\">";
echo $html->image('b_edit.png', array("alt"=>"Modifier","title"=>"Modifier"));
echo "</a>";
echo "<a onclick=\"javascript:return confirm('Confirmer la suppression ?')\" href=\"" .$CHEMIN ."/ergoRufrs/delete/" .$ergoMyRuVoc['ErgoMyRuVoc']['ergo_ru_voc_id'] ."\">";
echo $html->image('b_drop.png', array("alt"=>"Effacer","title"=>"Effacer"));
echo "</a>";
}

?>
		</td>

		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Nouveau mot', true), array('controller'=> 'ergo_rufrs', 'action'=>'add')); ?></li>
		<!--<li><?php echo $html->link(__('List Ergo Rufrs', true), array('controller'=> 'ergo_rufrs', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Ergo Rufr', true), array('controller'=> 'ergo_rufrs', 'action'=>'add')); ?> </li>-->
	</ul>
</div>

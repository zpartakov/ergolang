<div class="ergoMyRuVocs view">
<h2><?php  __('ErgoMyRuVoc');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoMyRuVoc['ErgoMyRuVoc']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('User Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoMyRuVoc['ErgoMyRuVoc']['user_id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Ergo Rufr'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($ergoMyRuVoc['ErgoRufr']['foreign'], array('controller'=> 'ergo_rufrs', 'action'=>'view', $ergoMyRuVoc['ErgoRufr']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Date'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoMyRuVoc['ErgoMyRuVoc']['date']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit ErgoMyRuVoc', true), array('action'=>'edit', $ergoMyRuVoc['ErgoMyRuVoc']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete ErgoMyRuVoc', true), array('action'=>'delete', $ergoMyRuVoc['ErgoMyRuVoc']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoMyRuVoc['ErgoMyRuVoc']['id'])); ?> </li>
		<li><?php echo $html->link(__('List ErgoMyRuVocs', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New ErgoMyRuVoc', true), array('action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Ergo Rufrs', true), array('controller'=> 'ergo_rufrs', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Ergo Rufr', true), array('controller'=> 'ergo_rufrs', 'action'=>'add')); ?> </li>
	</ul>
</div>

<div class="ergoMyRuVocs form">
<?php echo $form->create('ErgoMyRuVoc');?>
	<fieldset>
 		<legend><?php __('Add ErgoMyRuVoc');?></legend>
	<?php
		echo $form->input('user_id');
		echo $form->input('ergo_ru_voc_id');
		echo $form->input('date');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List ErgoMyRuVocs', true), array('action'=>'index'));?></li>
		<li><?php echo $html->link(__('List Ergo Rufrs', true), array('controller'=> 'ergo_rufrs', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Ergo Rufr', true), array('controller'=> 'ergo_rufrs', 'action'=>'add')); ?> </li>
	</ul>
</div>

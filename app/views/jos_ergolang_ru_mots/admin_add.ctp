<div class="josErgolangRuMots form">
<?php echo $form->create('JosErgolangRuMot');?>
	<fieldset>
 		<legend><?php __('Add JosErgolangRuMot');?></legend>
	<?php
		echo $form->input('russe');
		echo $form->input('francais');
		echo $form->input('phonetique');
		echo $form->input('type');
		echo $form->input('date');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List JosErgolangRuMots', true), array('action'=>'index'));?></li>
	</ul>
</div>

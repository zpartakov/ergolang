<div class="josErgolangRuMots view">
<h2><?php  __('JosErgolangRuMot');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $josErgolangRuMot['JosErgolangRuMot']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Russe'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $josErgolangRuMot['JosErgolangRuMot']['russe']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Francais'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $josErgolangRuMot['JosErgolangRuMot']['francais']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Phonetique'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $josErgolangRuMot['JosErgolangRuMot']['phonetique']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Type'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $josErgolangRuMot['JosErgolangRuMot']['type']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Date'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $josErgolangRuMot['JosErgolangRuMot']['date']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit JosErgolangRuMot', true), array('action'=>'edit', $josErgolangRuMot['JosErgolangRuMot']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete JosErgolangRuMot', true), array('action'=>'delete', $josErgolangRuMot['JosErgolangRuMot']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $josErgolangRuMot['JosErgolangRuMot']['id'])); ?> </li>
		<li><?php echo $html->link(__('List JosErgolangRuMots', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New JosErgolangRuMot', true), array('action'=>'add')); ?> </li>
	</ul>
</div>

<?php
/**
* @version        $Id: vue.ctp v1.0 29.04.2010 06:26:43 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = $article['Article']['title']; 
?>
<div class="articles view">
<h1><? echo $this->pageTitle; ?></h1>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
	<?
	if(strlen($article['Article']['introtext'])>1) {
		?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Introtext'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $article['Article']['introtext']; ?>
			&nbsp;
		</dd>
<?
}
?>


	</dl>
<p>
<?
echo $article['Article']['fulltext'];
?>
</p>
			<?php echo "<span class=\"modification\">Dernière modification: " .$article['Article']['modified'] ."</span>";?>

</div>
<?
/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
?>	
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit Article', true), array('action'=>'edit', $article['Article']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Article', true), array('action'=>'delete', $article['Article']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $article['Article']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Articles', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Article', true), array('action'=>'add')); ?> </li>
	</ul>
</div>
<?
}
?>



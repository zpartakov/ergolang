<?php
/**
* @version        $Id: add.ctp v1.0 29.04.2010 06:22:51 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
?>
<div class="articles form">
<?php echo $form->create('Article');?>
	<fieldset>
 		<legend><?php __('Add Article');?></legend>
	<?php
		echo $form->input('lang');
		echo $form->input('title');
		echo $form->input('introtext');
		#echo $form->input('fulltext');
		?>
		
			<?php echo $form->textarea('fulltext'); ?> 
	<?php echo $fck->replace('fulltext', array('width' => 640, 'height' => 480)); ?> 
<?
		echo $form->input('state',array("value"=>"1"));
		
		echo '<input type="hidden" id="ArticleCreatedBy" name="data[Article][created_by]" value="' .$session->read('Auth.User.id') .'">';

	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Articles', true), array('action'=>'index'));?></li>
	</ul>
</div>

<?php
/**
* @version        $Id: index.ctp v1.0 29.04.2010 06:45:19 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Articles'; 
?>
<div class="articles index">
<h1><? echo $this->pageTitle; ?></h1>

<!-- begin search form -->
 <table>
	 <tr>
		 <td>
 <div class="input">
<?php echo $form->create('Article', array('url' => array('action' => 'index'))); ?>
		<?php #echo $form->input('q', array('style' => 'width: 250px;', 'label' => false, 'size' => '80')); ?>
		<?php echo $form->input('q', array('label' => false, 'size' => '50', 'class'=>'txttosearch')); ?>
		</div>
</td><td>
<input type="button" class="vider" value="Vider" onClick="javascript:vide_recherche('ArticleQ')" />
<input type="submit" class="chercher" value="Chercher" /> 
</div> 
</td>
</tr>
</table>
<!-- end search form -->
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Nouvel article', true), array('action'=>'add')); ?></li>
	</ul>
</div>


<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('lang');?></th>
	<th><?php echo $paginator->sort('title');?></th>
	<th><?php echo $paginator->sort('introtext');?></th>
	<th><?php echo $paginator->sort('fulltext');?></th>
	<th><?php echo $paginator->sort('state');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th><?php echo $paginator->sort('created_by');?></th>
	<th><?php echo $paginator->sort('modified');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($articles as $article):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $article['Article']['id']; ?>
		</td>
		<td>
			<?php echo $article['Article']['lang']; ?>
		</td>
		<td>
			<?php echo $article['Article']['title']; ?>
		</td>
		<td>
			<?php echo $article['Article']['introtext']; ?>
		</td>
		<td>
			<?php echo substr($article['Article']['fulltext'],0,50) ."..."; ?>
		</td>
		<td>
			<?php echo $article['Article']['state']; ?>
		</td>
		<td>
			<?php echo $article['Article']['created']; ?>
		</td>
		<td>
			<?php echo $article['Article']['created_by']; ?>
		</td>
		<td>
			<?php echo $article['Article']['modified']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action'=>'view', $article['Article']['id'])); ?>
			<?php echo $html->link(__('Vue', true), array('action'=>'vue', $article['Article']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $article['Article']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $article['Article']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $article['Article']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Nouvel article', true), array('action'=>'add')); ?></li>
	</ul>
</div>

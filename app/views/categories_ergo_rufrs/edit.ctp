<div class="categoriesErgoRufrs form">
<?php echo $form->create('CategoriesErgoRufr');?>
	<fieldset>
 		<legend><?php __('Edit CategoriesErgoRufr');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('category_id');
		echo $form->input('ergo_rufr_id');
		echo $form->input('user_id');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action'=>'delete', $form->value('CategoriesErgoRufr.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('CategoriesErgoRufr.id'))); ?></li>
		<li><?php echo $html->link(__('List CategoriesErgoRufrs', true), array('action'=>'index'));?></li>
	</ul>
</div>

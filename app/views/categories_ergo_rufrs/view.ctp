<div class="categoriesErgoRufrs view">
<h2><?php  __('CategoriesErgoRufr');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $categoriesErgoRufr['CategoriesErgoRufr']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Category Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $categoriesErgoRufr['CategoriesErgoRufr']['category_id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Ergo Rufr Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $categoriesErgoRufr['CategoriesErgoRufr']['ergo_rufr_id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('User Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $categoriesErgoRufr['CategoriesErgoRufr']['user_id']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit CategoriesErgoRufr', true), array('action'=>'edit', $categoriesErgoRufr['CategoriesErgoRufr']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete CategoriesErgoRufr', true), array('action'=>'delete', $categoriesErgoRufr['CategoriesErgoRufr']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $categoriesErgoRufr['CategoriesErgoRufr']['id'])); ?> </li>
		<li><?php echo $html->link(__('List CategoriesErgoRufrs', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New CategoriesErgoRufr', true), array('action'=>'add')); ?> </li>
	</ul>
</div>

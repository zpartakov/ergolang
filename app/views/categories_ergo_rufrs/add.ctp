<div class="categoriesErgoRufrs form">
<?php echo $form->create('CategoriesErgoRufr');?>
	<fieldset>
 		<legend><?php __('Add CategoriesErgoRufr');?></legend>
	<?php
		echo $form->input('category_id');
		echo $form->input('ergo_rufr_id');
		echo $form->input('user_id');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List CategoriesErgoRufrs', true), array('action'=>'index'));?></li>
	</ul>
</div>

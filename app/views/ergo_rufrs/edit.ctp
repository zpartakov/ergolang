<?php
#phpinfo(); exit;
/**
* @version        $Id: edit.ctp v1.0 02.03.2010 23:10:59 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Modifier dictionnaire'; 
?>

<?php echo $form->create('ErgoRufr');?>

	<fieldset>
 		<legend><? echo $this->pageTitle; ?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('foreign');
		echo $form->input('local');
		echo $form->input('type');
		echo $form->input('date');
//todo kill ergo_motsassocies

	?>
</fieldset>
<?php
	
 echo $form->end('Submit');?>
</div>
<div class="categories">
<?php 	
//id du mot
$id_mot=$_GET["url"];
$id_mot=ereg_replace(".*/","",$id_mot);		
//id du user
$utilisateur= $this->Session->read('Auth.User.id'); //whois the current user
#$utilisateur= $_REQUEST["comment_info"];
#echo "utilisateur: " .$utilisateur ."<br>";


choisir_categorie($id_mot,$utilisateur);
 #echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>

		<li><?php echo $html->link(__('Effacer', true), array('action'=>'delete', $form->value('ErgoRufr.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('ErgoRufr.id'))); ?></li>
		<li><?php echo $html->link(__('Dictionnaire', true), array('action'=>'index'));?></li>
	</ul>
</div>

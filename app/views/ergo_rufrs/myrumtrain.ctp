<?
/**
* @version        $Id: my.ctp v1.0 13.12.2009 09:19:26 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
/* this script is for viewing the words entered by the user himself
 * */

$this->pageTitle = 'Drill Russe - Mes mots'; 
?>
<h2><? echo $this->pageTitle;?></h2>
<h3><a href="/websites/ergolang/cake/ergo_my_ru_vocs">Gérer mes mots</a></h3>

<?
$selectionmots=$_GET['selectionmots'];
if(!$selectionmots) {
	?>
<form method="GET">
<table>
	<tr>
		
		
		
		<?
		echo '<td><input type="radio" name="selectionmots" value="%"';
		if(!$selectionmots||$selectionmots=="%") {
			echo ' checked';
		}
		echo '>Tous mes mots</input></td>
		';
		
		echo '<td><input type="radio" name="selectionmots" value="5"';		
		if($selectionmots=="5") {
			echo ' checked';
		}
		echo '>Les mots que je connais</input></td>';

		echo '<td><input type="radio" name="selectionmots" value="4"';		
		if($selectionmots=="4") {
			echo ' checked';
		}
		echo '>facile</input></td>';
		
		echo '<td><input type="radio" name="selectionmots" value="3"';		
		if($selectionmots=="3") {
			echo ' checked';
		}
		echo '>assez facile</input></td>';
			
		echo '<td><input type="radio" name="selectionmots" value="2"';		
		if($selectionmots=="2") {
			echo ' checked';
		}
		echo '>difficile</input></td>';
		
		echo '<td><input type="radio" name="selectionmots" value="1"';		
		if($selectionmots=="1") {
			echo ' checked';
		}
		echo '>très difficile</input></td>';

		echo '<td><input type="radio" name="selectionmots" value="0"';
		if($selectionmots=="0") {
			echo ' checked';
		}
		echo ' onClick="this.form.submit();">Les mots que je ne connais pas</input> </td>
		';


		
	
		
		
		

		
		

		
		
		
		
		?>	
		
	</tr>
</table>
<br />
<input type="submit">
</form>
<?
} else {

//current user
$utilisateurid=$session->read('Auth.User.id');
$sql="
SELECT * FROM ergo_training_voc
WHERE 
userid=" .$utilisateurid ."  
AND 
note LIKE '".$selectionmots."' 
AND lang= 'ru'";
#echo "<br>".$sql;//tests
$question=$sql;
$questionOri=$question;
$question=mysql_query($question); //finding all words

if(!$question){
	echo "Error mysql: " .mysql_error();
}

$questionN=mysql_num_rows($question);



if($questionN<20) {
	echo "<div style=\"background-color: #FDE0AB; font-style: italic; margin-bottom: 1em; padding: 0.2em; width: 20%\">Note: vous n'avez pas entraîné assez de mots, <a href=\"/websites/ergolang/cake/ergo_rufrs/rumtrain\">exercez-vous!</a></div>"; 
	#exit;
} else {

$rand=new UniqueRand(0,$questionN); //a function to generate a unique random number

$i=0;
$goodword=$rand->uRand(1,$questionN); //creating random number 1 = good word
$id=mysql_result($question,$goodword,'id');
$goodwordForeign=mysql_result($question,$goodword,'foreign');
$goodwordLocal=mysql_result($question,$goodword,'local');
?>
<!-- print game -->
<form method="GET" name="choix_guide" action="gomyvoc">
<form method="GET" name="choix_guide" action="gomyvoc">
<input type="hidden" name="id" value="<? echo $id;?>">
<input type="hidden" name="goodwordForeign" value="<? echo $goodwordForeign;?>">
<input type="hidden" name="goodwordLocal" value="<? echo $goodwordLocal;?>">

Que signifie: <span class="foreign"><? echo  $goodwordForeign;?></span> ? 
<a href="#" onclick="Effect.toggle('toggle_slide', 'appear'); return false;">Traduction</a>
<!-- a scriptaculous effect to show translation -->
<div id="toggle_slide" style="display: none; background:#ccc; margin-top: 1em">
<div>
<? echo $goodwordLocal;?>
</div>
</div>

<table>

 <tr><td>
<select name="sid" size="6" onClick="this.form.submit();">
	<option value="5" selected>--- je connais ce mot ---</option>
	<option value="4"> facile / 8j</option>
	<option value="3">assez facile / 3j</option>
	<option value="2">difficile / 9h</option>
	<option value="1">très difficile / soon</option>
	<option value="0">--- je ne connais pas ce mot ---</option>
</select>
</td>
<td>Remarque facultative<br><textarea name="rem"></textarea></td>

 </tr>
</table>


</form>
<?
}
}
?>
<br /><a href="/websites/ergolang/cake/ergo_rufrs/myrumtrain">Recommencer l'exercice <em>"da capo"</em></a>


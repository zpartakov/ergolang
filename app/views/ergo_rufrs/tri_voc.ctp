<?php
/**
* @version        $Id: tri_voc.ctp v1.0 06.07.2010 15:12:41 CEST $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

$this->pageTitle = 'Exercice de tri Russe <=> Français'; 

####### dragndrop ######
#import dragndrop functions  from http://www.gregphoto.net/sortable/
App::import('Vendor', 'SLLists', array('file' => 'lists'.DS.'SLLists.class.php'));
#require_once('SLLists.class.php');
$sortableLists = new SLLists('scriptaculous');
$sortableLists->addList('categories','categoriesListOrder');
$sortableLists->addList('divContainer','divOrder','div');
$sortableLists->debug = false;
?>
<?=$html->css('lists');?>
<?
$sortableLists->printTopJS();

####### MySQL ####
$nbq=5; //number of questions
$test=0; //initialize test var
$latable="ergo_rufrs";
$limite=" LIMIT 0,100"; //tests
$question="SELECT * FROM " .$latable;
$rand=new UniqueRand(); //a function to generate a unique random number
$question=mysql_query($question); //finding all words
if(!$question){
	echo "Error mysql: " .mysql_error();
}
$questionN=mysql_num_rows($question); 
$w1=$rand->uRand(1,$questionN); //creating random number 1 = good word
$w2=$rand->uRand(1,$questionN); //creating random number 1 = good word
$w3=$rand->uRand(1,$questionN); //creating random number 1 = good word
$w4=$rand->uRand(1,$questionN); //creating random number 1 = good word
$w5=$rand->uRand(1,$questionN); //creating random number 1 = good word
$sql="SELECT * FROM " .$latable ." WHERE (id=" .$w1 ." OR id=" .$w2 ." OR id=" .$w3 ." OR id=" .$w4 ." OR id=" .$w5 .")";
#echo "<br>" .$sql ."<br>"; //test
$sql=mysql_query($sql); //finding all words
if(!$sql){
	echo "Error mysql: " .mysql_error();
}
$questionN=mysql_num_rows($sql); 
//To Pull 7 Unique Random Values Out Of AlphaNumeric
//removed number 0, capital o, number 1 and small L
//Total: keys = 32, elements = 33
$characters = array(
"0", "1","2","3","4");
//make an "empty container" or array for our keys
$keys = array();
//first count of $keys is empty so "1", remaining count is 1-6 = total 7 times
while(count($keys) < 5) {
    //"0" because we use this to FIND ARRAY KEYS which has a 0 value
    //"-1" because were only concerned of number of keys which is 32 not 33
    //count($characters) = 33
    $x = mt_rand(0, count($characters)-1);
    if(!in_array($x, $keys)) {
       $keys[] = $x;
    }
}

$random_chars="";
foreach($keys as $key){
  $random_chars .= $characters[$key];
}

####### Vars ####
$i=0; $foreign=""; $local=""; $ran_key=""; $ordre=""; $idvoc="";

####### loop and stores datas ####
while($i<$questionN) {
	$foreign.="<li class=\"pair\">" .mysql_result($sql,$i,'foreign');
	#$foreign .= " - #" .mysql_result($sql,$i,'id');
	$foreign.="</li>
	";
	$idvoc.=mysql_result($sql,$i,'id') .";";
	$locali=substr($random_chars,$i,1);
	$local.="<li id=\"item_" .($locali+1) ."\">".mysql_result($sql,$locali,'local');
	#$local .= " - #" .mysql_result($sql,$locali,'id');
	
	$local.="</li>
	";
	$ran_key.=($locali+1);
	$ordre.=($i+1);
	$i++;
}
		
function feedbackpuzzle($id) {
				//echo "<br>error mot " .$id;
}


if(isset($_POST['sortableListsSubmitted'])) {
	echo "<h1>";
	$output="";
	$orderArray = SLLists::getOrderArray($_POST['categoriesListOrder'],'categories');
	foreach($orderArray as $item) {
		#$sql = "UPDATE categories set orderid=".$item['order']." WHERE carid=".$item['element'];
		#echo $sql.'<br>';
		$output.=$item['element'];
	}
	#echo $output;
	if($output=="12345") {
		echo "Bravo, tout est juste!";
	} else { //submitted form, give a feedback
		
		$idvoc=$_POST['idvoc'];
		echo "Il y a une ou des erreurs!";

#		echo $idvoc;
$idvoc=explode(";",$idvoc);
#		print_r($idvoc);
		if(substr($output,0,1)!=1) {
			feedbackpuzzle($idvoc[0]);
		}
		if(substr($output,1,1)!=2) {
			feedbackpuzzle($idvoc[1]);
		}
		if(substr($output,2,1)!=3) {
			feedbackpuzzle($idvoc[2]);
		}
		if(substr($output,4,1)!=4) {
			feedbackpuzzle($idvoc[3]);
		}
		if(substr($output,5,1)!=5) {
			feedbackpuzzle($idvoc[4]);
		}
		
	}
	echo "</h1>";
	echo "<h2>Nouvel exercice</h2>";

}

####### Print page ####
?>
	<div style="margin-left:auto;margin-right:auto;text-align:left; width: 50%">
	<table>
		<tr>
			<td> 
				<ul id="foreignsortable" class="sortableListforeign">
					<?
					//left foreign language word to rely
					echo $foreign;
					?>
				</ul>
			</td>
			<td>
				<ul id="categories" class="sortableList">
					<?
					//right column local language to match foreign
					echo $local;
					?>
				</ul>
			</td>
		</tr>
	</table>
<?
$sortableLists->printForm(CHEMIN.'/ergo_rufrs/tri_voc', 'POST', 'Soumettre', 'button',$idvoc);
$sortableLists->printBottomJS();

?>

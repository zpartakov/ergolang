 <?php
/**
* @version        $Id: index.ctp v1.0 13.12.2009 10:46:44 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

//Configure::write('debug', 2);

$this->pageTitle = 'Dictionnaire Russe <=> Français'; 

?>
<div class="ergoRufrs index">
<h1>
<table>
	<tr>
		<td><? echo $this->pageTitle;?> </td><td>
		<? #### news ######
echo '<a href="/websites/ergolang/cake/ergoRufrs/index/page:1/sort:date/direction:desc" alt="Nouveautés" title="Nouveautés">'.$html->image('new.jpg', array("alt"=>"Nouveautés","title"=>"Nouveautés","class"=>"image")).'</a><br />';
  ?>
</td>
	</tr>
</table>

</h1>

<?
	 echo $this->element('russe/alphabet');

echo $html->link(__('Nouveau mot', true), array('action'=>'add'));
?>
<!-- begin search form -->
 <table>
	 <tr>
		 <td>
 <div class="input">
<?php echo $form->create('ErgoRufr', array('url' => array('action' => 'index'))); ?>
		<?php #echo $form->input('q', array('style' => 'width: 250px;', 'label' => false, 'size' => '80')); ?>
		<?php echo $form->input('q', array('label' => false, 'size' => '50', 'class'=>'txttosearch', 'onfocus'=>'this.value=\'\'')); ?>
	<table>
		<tr>
			<td><input type="radio" name="data[ErgoRufr][boole]" value="any" checked>&nbsp;partout (*)</td>
		
			<td><input type="radio" name="data[ErgoRufr][boole]" value="end">&nbsp;fin du texte ($)</td>
		
			<td><input type="radio" name="data[ErgoRufr][boole]" value="begin">&nbsp;début du texte (^)</td>
		</tr>
	</table>	
		</div>
</td>

<td>
<input type="submit" class="chercher" value="Chercher" /> 
</div> 
</td>
</tr>
</table>
<!-- end search form -->
   <?php
/*
 * pas de résultats?
 */
 
if(count($ergoRufrs)==0) {
   	echo "<p style=\"padding: 13px; background-color: lightyellow; width: 50%\">
   	Le moteur Эrgolang ne trouve aucune traduction pour : " .$_REQUEST['data']['ErgoRufr']['q'] ."<br/><br/>";
   	echo "</p>Recherche avec stemming:";
   	$sql="SELECT * FROM ergo_ru_stems ORDER BY src";
   	$sql=mysql_query($sql);
   	$i=0; $longueur=0;
   	while($i<mysql_num_rows($sql)){
   		if(preg_match("/".mysql_result($sql,$i,'src') ."/",$_REQUEST['data']['ErgoRufr']['q'])) {
/* 
 * seeking the longest stem 
 * */
   			$longueur_stem=strlen(mysql_result($sql,$i,'src'));
   			if($longueur_stem > $longueur) {
   				$stem=mysql_result($sql,$i,'src');
   				$remplace=mysql_result($sql,$i,'replace');
   				$longueur=$longueur_stem;
   			}
   		}
   		$i++;
   	}
   	
   	$motcherche=$_REQUEST['data']['ErgoRufr']['q'];
   	$motcherche=preg_replace("/".$stem."/",$remplace, $motcherche);
   	
   	echo "Le stem le plus long qui concorde est: " .$stem 
   	."<br>troncature : ".$_REQUEST['data']['ErgoRufr']['q']
   	." = ".$motcherche   	;
   	
   	$sql="
   	SELECT *
	FROM `ergo_rufrs`
	WHERE `foreign` LIKE '%" .$motcherche ."%'";
   	//echo $sql;
   	$sql=mysql_query($sql);
   	$i=0; $longueur=0;
   	echo "<br/><ul style=\"background-color: #FFF2DA; width: 50%; padding: 5px\">";
   	while($i<mysql_num_rows($sql)){
   		echo "<li>" .mysql_result($sql,$i,'foreign');
   		echo " : ";
   		echo mysql_result($sql,$i,'local');
   		echo "</li>";
   		$i++;  		 
   	}
   	echo "</ul>";

	/*
	 * still no results even with stemming: print dictionnary links
	 */   	
	if(mysql_num_rows($sql)<1) {   	
	   	echo "<br/><br/>Toujours pas de résultat!<br/>Chercher sur <ul>
	   	<li><a target=\"_blank\" href=\"http://slovari.yandex.ru/".$_REQUEST['data']['ErgoRufr']['q']."/fr/#lingvo/\">lingvo</a></li>
	   	<li><a target=\"_blank\" href=\"http://www.multitran.ru/c/m.exe?l1=2&l2=4&s=".$_REQUEST['data']['ErgoRufr']['q']."\">multitran</a></li>
	   </ul>	
	   	";
	}

} else {
	/*
	 * there are some results, print them
	 */
?>   
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<!--<th><?php echo $paginator->sort('id');?></th>-->
	<th><?php echo $paginator->sort('Russe','foreign');?></th>
	<th><?php echo $paginator->sort('Français','local');?></th>
	<th><?php echo $paginator->sort('type');?></th>
	<th><?php echo $paginator->sort('date');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($ergoRufrs as $ergoRufr):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<!--<td>
			<?php echo $ergoRufr['ErgoRufr']['id']; ?>
		</td>-->
		<td>
			<?php echo $ergoRufr['ErgoRufr']['foreign']; ?>
		</td>
		<td>
			<?php echo nl2br(trim($ergoRufr['ErgoRufr']['local'])); ?>
		</td>
		<td>
			<?php echo $ergoRufr['ErgoRufr']['type']; ?>
		</td>
		<td>
			<?php 
				$timestamp = strtotime($ergoRufr['ErgoRufr']['date']);
				e(strftime("%d&nbsp;%b&nbsp;%Y,&nbsp;%Hh%M", $timestamp));
			?>
		</td>
		<td class="actions">
<?php 
echo "<a href=\"" .CHEMIN ."/ergoRufrs/view/" .$ergoRufr['ErgoRufr']['id'] ."\">";
echo $html->image('b_search.png', array("alt"=>"Voir","title"=>"Voir"));
echo "</a>";

echo "<a href=\"http://swac-collections.org/listen/rus/" .$ergoRufr['ErgoRufr']['foreign'] ."\" target=\"_blank\">";
echo $html->image('swac.png', array("alt"=>"Écouter","title"=>"Écouter"));
echo "</a>";
	/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
echo "&nbsp;";
echo "<a href=\"".CHEMIN ."/ergo_rufrs/edit/" .$ergoRufr['ErgoRufr']['id'] ."\">";
echo $html->image('b_edit.png', array("alt"=>"Modifier","title"=>"Modifier"));
echo "</a>";
echo "<a onclick=\"javascript:return confirm('Confirmer la suppression ?')\" href=\"" .CHEMIN ."/ergo_rufrs/delete/" .$ergoRufr['ErgoRufr']['id'] ."\">";
echo $html->image('b_drop.png', array("alt"=>"Effacer","title"=>"Effacer"));
echo "</a>";
}
?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<?php 
if($session->read('Auth.User.group_id')==3) {
?>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Nouveau mot', true), array('action'=>'add')); ?></li>
	</ul>
</div>
<?php 
}
}
?>

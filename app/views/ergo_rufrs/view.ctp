<?php
/**
* @version        $Id: view.ctp v1.0 15.04.2010 05:52:45 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Dictionnaire Russe <=> Français: Vue'; 

?>
<div class="ergoRufrs view">
<h1><? echo $this->pageTitle; ?></h1>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRufr['ErgoRufr']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Foreign'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRufr['ErgoRufr']['foreign']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Local'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRufr['ErgoRufr']['local']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Type'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRufr['ErgoRufr']['type']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Date'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRufr['ErgoRufr']['date']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<?
		/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
	?>
		<li><?php echo $html->link(__('Edit ErgoRufr', true), array('action'=>'edit', $ergoRufr['ErgoRufr']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete ErgoRufr', true), array('action'=>'delete', $ergoRufr['ErgoRufr']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoRufr['ErgoRufr']['id'])); ?> </li>
		<li><?php echo $html->link(__('List ErgoRufrs', true), array('action'=>'index')); ?> </li>
				<?
	}
	?>
<?
			if($session->read('Auth.User.username')) { //show only to registered users
?>
		<li><?php echo $html->link(__('New ErgoRufr', true), array('action'=>'add')); ?> </li>
<?
}
?>		
	</ul>
</div>

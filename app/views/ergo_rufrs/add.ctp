<?php
/**
* @version        $Id: add.ctp v1.0 30.12.2009 08:38:20 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Nouveau mot'; 

?>
<style>
.submit {
	position: absolute; left: 35%; top: 180px;
}
</style>
<a href="../pages/clavierrusse">Clavier russe</a>

<div class="ergoRufrs form">
<?php echo $form->create('ErgoRufr');?>
	<fieldset>
 		<legend><?php echo $this->pageTitle;?></legend>
	<?php
		echo $form->input('foreign');
		echo $form->input('local');
		echo $form->input('type');
		echo $form->input('date');
	?>
	</fieldset>
<?php
$sql="select * FROM ergo_categories ORDER BY libelle";
$sql=mysql_query($sql);
echo "<select name=\"category\"><option value=\"\">-- Catégorie --</option>";
$i=0;
 while($i<mysql_num_rows($sql)){
	 echo "<option value=\"".mysql_result($sql,$i,'id');
	 echo "\">" .mysql_result($sql,$i,'libelle') ."</option>";
 $i++;
 } 
echo "</select>";
 echo $form->end('Submit');?>
 </div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Dictionnaire', true), array('action'=>'index'));?></li>
	</ul>
</div>

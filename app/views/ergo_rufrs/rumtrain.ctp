<?php
/**
* @version        $Id: rumtrain.ctp v1.0 13.07.2010 07:14:47 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Drill Russe'; 

?>
<div class="ErgoRufrs index">
<h2><? echo $this->pageTitle;?></h2>
<?php

######## constants ######
$latable="ergo_rufrs";
#$limite=" LIMIT 0,100"; //tests
$limite=""; //prod

$rand=new UniqueRand(); //a function to generate a unique random number
$question="SELECT * FROM " .$latable;
$question.=$limite;
#see old.ergolang voc.php & govoc.php
$questionOri=$question;
$question=mysql_query($question); //finding all words

if(!$question){
	echo "Error mysql: " .mysql_error();
}

$questionN=mysql_num_rows($question);
$i=0;

$goodword=$rand->uRand(1,$questionN); //creating random number 1 = good word
$goodwordForeign=mysql_result($question,$goodword,'foreign');
$goodwordLocal=mysql_result($question,$goodword,'local');
$badword1=$rand->uRand(1,$questionN); // 
$badword1Foreign=mysql_result($question,$badword1,'foreign');
$badword1Local=mysql_result($question,$badword1,'local');
$badword2=$badword1;//creating random number 2 = bad word 2
while ($badword2==$badword1){ //loop until random number 2 is different from random number 1
$badword2=rand(0,$questionN); //  
}
$badword2Foreign=mysql_result($question,$badword2,'foreign');
$badword2Local=mysql_result($question,$badword2,'local');

#definition de l'ordre des reponses (on fait appel a la classe uRand)
$ordre0= $rand->uRand(1,3); 
$ordre1= $rand->uRand(1,3); 
$ordre2= $rand->uRand(1,3); 
?>
<!-- print game -->
<form method="GET" name="choix_guide" action="govoc">
<input type="hidden" name="goodwordForeign" value="<? echo $goodwordForeign;?>">
<input type="hidden" name="goodwordLocal" value="<? echo $goodwordLocal;?>">
<table>
<tr><th colspan="3">Que signifie: <span class="foreign"><? echo  $goodwordForeign;?></span></th></tr>
</table>

<table>

 <tr><td><select name="sid" size="6">
 <option value="5" selected>--- je connais ce mot ---</option>
 <option value="4"> facile / 8j</option>
  <option value="3">good / 3j</option>
  <option value="2">hard / 9h</option>
  <option value="1">again / soon</option>
  <option value="0">--- je ne connais pas ce mot ---</option>

 </select></td>
<td>Remarque facultative<br><textarea name="rem"></textarea></td>

 </tr>
</table>


<table>
<tr>
<?
#print the answers in the random order (a function would be nicer)
if($ordre0=="1"&&$ordre1=="2"&&$ordre2=="3") { //123
echo "<td><input type=\"radio\" name=\"answer\" value=\"1\" onClick=\"document.forms.choix_guide.submit()\">&nbsp;" .$goodwordLocal ."</input></td>";
echo "<td><input type=\"radio\" name=\"answer\" value=\"0\" onClick=\"document.forms.choix_guide.submit()\">&nbsp;" .$badword1Local ."</input></td>";
echo "<td><input type=\"radio\" name=\"answer\" value=\"0\" onClick=\"document.forms.choix_guide.submit()\">&nbsp;" .$badword2Local ."</input></td>";
} elseif($ordre0=="1"&&$ordre1=="3"&&$ordre2=="2") { //132
echo "<td><input type=\"radio\" name=\"answer\" value=\"1\" onClick=\"document.forms.choix_guide.submit()\">&nbsp;" .$goodwordLocal ."</input></td>";
echo "<td><input type=\"radio\" name=\"answer\" value=\"0\" onClick=\"document.forms.choix_guide.submit()\">&nbsp;" .$badword2Local ."</input></td>";
echo "<td><input type=\"radio\" name=\"answer\" value=\"0\" onClick=\"document.forms.choix_guide.submit()\">&nbsp;" .$badword1Local ."</input></td>";
} elseif($ordre0=="2"&&$ordre1=="1"&&$ordre2=="3") { //213
echo "<td><input type=\"radio\" name=\"answer\" value=\"0\" onClick=\"document.forms.choix_guide.submit()\">&nbsp;" .$badword1Local ."</input></td>";	
echo "<td><input type=\"radio\" name=\"answer\" value=\"1\" onClick=\"document.forms.choix_guide.submit()\">&nbsp;" .$goodwordLocal ."</input></td>";
echo "<td><input type=\"radio\" name=\"answer\" value=\"0\" onClick=\"document.forms.choix_guide.submit()\">&nbsp;" .$badword2Local ."</input></td>";
} elseif($ordre0=="2"&&$ordre1=="3"&&$ordre2=="1") { //231
echo "<td><input type=\"radio\" name=\"answer\" value=\"0\" onClick=\"document.forms.choix_guide.submit()\">&nbsp;" .$badword1Local ."</input></td>";	
echo "<td><input type=\"radio\" name=\"answer\" value=\"0\" onClick=\"document.forms.choix_guide.submit()\">&nbsp;" .$badword2Local ."</input></td>";
echo "<td><input type=\"radio\" name=\"answer\" value=\"1\" onClick=\"document.forms.choix_guide.submit()\">&nbsp;" .$goodwordLocal ."</input></td>";
} elseif($ordre0=="3"&&$ordre1=="1"&&$ordre2=="2") { //312
echo "<td><input type=\"radio\" name=\"answer\" value=\"0\" onClick=\"document.forms.choix_guide.submit()\">&nbsp;" .$badword2Local ."</input></td>";
echo "<td><input type=\"radio\" name=\"answer\" value=\"1\" onClick=\"document.forms.choix_guide.submit()\">&nbsp;" .$goodwordLocal ."</input></td>";
echo "<td><input type=\"radio\" name=\"answer\" value=\"0\" onClick=\"document.forms.choix_guide.submit()\">&nbsp;" .$badword1Local ."</input></td>";	
} elseif($ordre0=="3"&&$ordre1=="2"&&$ordre2=="1") { //321
echo "<td><input type=\"radio\" name=\"answer\" value=\"0\" onClick=\"document.forms.choix_guide.submit()\">&nbsp;" .$badword2Local ."</input></td>";
echo "<td><input type=\"radio\" name=\"answer\" value=\"0\" onClick=\"document.forms.choix_guide.submit()\">&nbsp;" .$badword1Local ."</input></td>";	
echo "<td><input type=\"radio\" name=\"answer\" value=\"1\" onClick=\"document.forms.choix_guide.submit()\">&nbsp;" .$goodwordLocal ."</input></td>";
}
 ?>
 </tr>
 </table>
 


</form>
</div>
<?

$utilisateur=$session->read('Auth.User.email'); 
echo "Utilisateur:" .$utilisateur;

?>

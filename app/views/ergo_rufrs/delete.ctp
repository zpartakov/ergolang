<?php
/**
* @version        $Id: delete.ctp v1.0 17.06.2010 05:53:02 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

$id= preg_replace("/.*\//","",$_REQUEST["url"]);

$sql="SELECT * FROM ergo_rufrs WHERE id='".$id."'";
#do and check sql
$sql=mysql_query($sql);

if(!$sql) {
	echo "SQL error on word selection:<br>" .mysql_error(); exit;
}


//make a backup
$sql1="INSERT INTO `ergo_rufrs_revs` (`id`, `foreign`, `local`, `type`, `date`, `version_id`, `version_created`) VALUES ('".mysql_result($sql,0,'id')."', '".addslashes(mysql_result($sql,0,'foreign'))."', '".addslashes(mysql_result($sql,0,'local'))."', '".mysql_result($sql,0,'type')."', '".mysql_result($sql,0,'date')."', NULL, CURRENT_TIMESTAMP)"; 
$sql=mysql_query($sql1);

if(!$sql) {
	echo "SQL error on backup:<br>" .mysql_error(); exit;
}

//delete main entry from russian voc
$sql="DELETE FROM ergo_rufrs WHERE id='".$id."'";
#do and check sql
$sql=mysql_query($sql);
if(!$sql) {
	echo "SQL error on delete main entry from russian voc:<br>" .mysql_error(); exit;
}

//delete categories if any
$sql="DELETE FROM categories_ergo_rufrs WHERE ergo_rufr_id='".$id."'";
#do and check sql
$sql=mysql_query($sql);
if(!$sql) {
	echo "SQL error on delete categories if any:<br>" .mysql_error();
}

//delete user entry
$sql="DELETE FROM ergo_my_ru_vocs WHERE ergo_ru_voc_id='".$id."'";
#do and check sql
$sql=mysql_query($sql);
if(!$sql) {
	echo "SQL error on delete user entry: <br>" .mysql_error();
}
		
header("Location: " .$_SERVER['HTTP_REFERER']);

?>

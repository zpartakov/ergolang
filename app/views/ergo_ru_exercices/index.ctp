<?php
/**
* @version        $Id: index.ctp v1.0 07.03.2010 11:32:15 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = "Admin exercices russe"; 
?>
<h1><?echo $this->pageTitle ?></h1>

<div class="ergoRuExercices index">

<!-- begin search form -->
 <table>
	 <tr>
		 <td>
 <div class="input">
<?php echo $form->create('ErgoRuExercice', array('url' => array('action' => 'index'))); ?>
		<?php echo $form->input('q', array('style' => 'width: 250px;', 'label' => false)); ?>
		</div>
</td><td>
  <input type="button" class="vider" value="Vider" onClick="javascript:vide_recherche('ErgoRuExerciceQ')" />
<input type="submit" class="chercher" value="Chercher" /> 
</div> 
</td>
</tr>
</table>
<!-- end search form -->


<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New ErgoRuExercice', true), array('action'=>'add')); ?></li>
	</ul>
</div>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id_exs');?></th>
	<th><?php echo $paginator->sort('id_l');?></th>
	<th><?php echo $paginator->sort('enonce');?></th>
	<th><?php echo $paginator->sort('reponse');?></th>
	<th><?php echo $paginator->sort('reponsepost');?></th>
	<th><?php echo $paginator->sort('tips');?></th>
	<th><?php echo $paginator->sort('rang');?></th>
	<th><?php echo $paginator->sort('id');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($ergoRuExercices as $ergoRuExercice):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $ergoRuExercice['ErgoRuExercice']['id_exs']; ?>
		</td>
		<td>
			<?php echo $ergoRuExercice['ErgoRuExercice']['id_l']; ?>
		</td>
		<td>
			<?php echo $ergoRuExercice['ErgoRuExercice']['enonce']; ?>
		</td>
		<td>
			<?php echo $ergoRuExercice['ErgoRuExercice']['reponse']; ?>
		</td>
		<td>
			<?php echo $ergoRuExercice['ErgoRuExercice']['reponsepost']; ?>
		</td>
		<td>
			<?php echo $ergoRuExercice['ErgoRuExercice']['tips']; ?>
		</td>
		<td>
			<?php echo $ergoRuExercice['ErgoRuExercice']['rang']; ?>
		</td>
		<td>
			<?php echo $ergoRuExercice['ErgoRuExercice']['id']; ?>
		</td>
		<td class="actions">
				<a href="/websites/ergolang/cake/exercices/lexercice/
				<?php
				#bug zarb trouver raison id exercice+1
				$idex=$ergoRuExercice['ErgoRuExercice']['id_l']+1; 
				$idex=$ergoRuExercice['ErgoRuExercice']['id_l']; 
				echo $idex; ?>
				">Tout l'exercice</a><br>
<br>
			<?php echo $html->link(__('View', true), array('action'=>'view', $ergoRuExercice['ErgoRuExercice']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $ergoRuExercice['ErgoRuExercice']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $ergoRuExercice['ErgoRuExercice']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoRuExercice['ErgoRuExercice']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New ErgoRuExercice', true), array('action'=>'add')); ?></li>
	</ul>
</div>

<?php
/**
* @version        $Id: view.ctp v1.0 07.03.2010 11:32:58 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
?>
<div class="ergoRuExercices view">
<h2><?php  __('ErgoRuExercice');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id Exs'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuExercice['ErgoRuExercice']['id_exs']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id L'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuExercice['ErgoRuExercice']['id_l']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Enonce'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuExercice['ErgoRuExercice']['enonce']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Reponse'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuExercice['ErgoRuExercice']['reponse']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Reponsepost'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuExercice['ErgoRuExercice']['reponsepost']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Tips'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuExercice['ErgoRuExercice']['tips']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Rang'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuExercice['ErgoRuExercice']['rang']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuExercice['ErgoRuExercice']['id']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit ErgoRuExercice', true), array('action'=>'edit', $ergoRuExercice['ErgoRuExercice']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete ErgoRuExercice', true), array('action'=>'delete', $ergoRuExercice['ErgoRuExercice']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoRuExercice['ErgoRuExercice']['id'])); ?> </li>
		<li><?php echo $html->link(__('List ErgoRuExercices', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New ErgoRuExercice', true), array('action'=>'add')); ?> </li>
	</ul>
</div>

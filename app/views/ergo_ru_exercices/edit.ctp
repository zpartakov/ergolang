<?php
/**
* @version        $Id: edit.ctp v1.0 07.03.2010 11:32:35 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
?>
<div class="ergoRuExercices form">
<?php echo $form->create('ErgoRuExercice');?>
	<fieldset>
 		<legend><?php __('Edit ErgoRuExercice');?></legend>
	<?php
		echo $form->input('id_exs');
		echo $form->input('id_l');
		echo $form->input('enonce');
		echo $form->input('reponse');
		echo $form->input('reponsepost');
		echo $form->input('tips');
		echo $form->input('rang');
		echo $form->input('id');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action'=>'delete', $form->value('ErgoRuExercice.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('ErgoRuExercice.id'))); ?></li>
		<li><?php echo $html->link(__('List ErgoRuExercices', true), array('action'=>'index'));?></li>
	</ul>
</div>

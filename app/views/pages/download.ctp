<?php
/**
* @version        $Id: download.ctp v1.0 26.02.2010 07:34:16 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
?>
<h1>Télécharger le logiciel</h1>

<h2>Version stable</h2>

<h2>Version de développement</h2>

subversion / svn sur sourceforge: <a href="https://sourceforge.net/svn/?group_id=246879" target="_blank">https://sourceforge.net/svn/?group_id=246879</a>

<h2>Installer les dictionnaires</h2>
<ul><li>Dictionnaire français de base</li></ul>
<h3>Dictionnaires bilingues français / langue étrangère</h3>
<ul>
<li>russe</li>
<li>anglais</li>
<li>espagnol</li>
<li>italien</li>
<li>allemand</li>
</ul>

    
<h2>Anciennes versions</h2>

<?php
#echo phpinfo(); exit;
/**
* @version        $Id: home.ctp v1.0 11.12.2009 17:26:10 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Accueil';

#include("check.ctp");

$title_for_layout="Accueil Эrgolang";
#session_destroy();
?>
  <?
//qrcode
echo $html->image('qrcode.ergolang.png', array("style"=>"float: right","alt"=>"QRCode Эrgolang","title"=>"QRCode Эrgolang","width"=>"75","height"=>"75"));
?>

<a name="ergolang"></a>
<h1><? echo $title_for_layout;?>&nbsp;
<?php
	echo '<a href="http://oblomov.info/websites/ergolang/cake/articles/vue/26" title="English version Эrgolang">'.$html->image('languages/en.gif', array("alt"=>"English version Эrgolang","title"=>"English version Эrgolang")).'</a>';
	?></h1>
	<?php echo $this->element('navigator'); ?>
<?

if(strlen($session->read('Auth.User.group_id')>0)) {
		echo "Bienvenue, " .$session->read('Auth.User.username');
	echo "<br>Ton groupe: " .$session->read('Auth.User.group_id')."<br>";
}
?>

<em>Avec Эrgolang, vous pouvez:</em><br />
<br />
<ul>
    <li>consulter les dictionnaires, entrer de nouveaux mots et catégories, entraîner votre vocabulaire</li>
    <li>entrer et reviser des textes, en profitant de l'analyseur automatique de mots</li>
    <li>entrer en contact avec d'autres personnes apprenantes</li>
</ul>
<br />
<em>Les deux idées centrales d'ergolang:</em>
<br />
<br />
<ul>
<li>une communauté d'apprenants, ce qui permet de partager les vocabulaires et de les enrichir</li>
<li>une pédagogie flexible et personnalisée, basée sur les catégories de mots et de textes choisies par l'apprenant</li>
</ul>

<br />
<?
#if($session->read('Auth.User.username')){
	echo "<strong>Choisir votre langue pour la session courante en la sélectionnant dans le menu en haut de la page - vous pouvez changer de langue pendant votre session</strong>";
#	}
	?>
<br /><br />
<?
if(!$session->read('Auth.User.username')){
?>
Pour <strong>utiliser</strong> Эrgolang vous <strong>devez</strong> <a href="users/login" title="login">vous enregistrer</a>; sans être enregistré, vous pourrez uniquement consulter les données publiques (essentiellement les dictionnaires)<br />
<?
}
?>
<br />
La langue de référence est le français<br />
Le projet est open-source, GPL et gratuit<br />
Le projet est encore au stade de prototype, si vous voulez le rejoindre <a href="/contact">contactez-nous</a><br />
<br />
Note: la langue russe propose actuellement le plus de ressources, pour les autres langues prévues actuellement (anglais, italien, allemand, espagnol) il y a un cruel manque de forces!
<br /><br />

<?
   #echo '<br /><pre>session->read()' .nl2br(print_r($session->read())) ."</pre>";

/*echo '<br />session->check()' .$session->check();
echo '<br />session->error()' .$session->error();
echo '<br />session->flash()' .$session->flash();
echo '<br />session->id()' .$session->id();
echo '<br />session->read()' .print_r($session->read());
echo '<br />session->valid()' .$session->valid();
*/
if($session->read("langue")) {
echo "<br /><em>Vous avez choisi comme langue pour cette session: ";
# .$session->read("User.langue");
echo $session->read("langue");
echo "</em>";
}
?>




<?
/*tests
 * */
#echo phpinfo();
?>

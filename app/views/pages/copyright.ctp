<?php
/**
* @version        $Id: copyright.ctp v1.0 09.12.2009 06:52:56 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
?>

Эrgolang derives from copyrighted works licensed under the GNU General
Public License.  This version has been modified pursuant to the
GNU General Public License as of September 15, 2005, and as distributed,
it includes or is derivative of works licensed under the GNU General
Public License or other free or open source software licenses.  Please
see the credits.ctp for a non-exhaustive list of contributors and
copyright holders.  A full text version of the GNU GPL version 2 can be
found in the <a href="license">license</a>

<?php
/**
* @version        $Id: latin.ctp v1.0 13.10.2010 09:37:25 CEST $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = "Latinum";

?>
<h1><? echo $this->pageTitle; ?></h1>
	<?php #echo $this->element('menuen');?>
<h2><a href="/websites/ergolang/cake/ergoLatfrs">Dictionnaire latin-français</a></h2>

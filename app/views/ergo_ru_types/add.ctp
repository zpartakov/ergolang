<?php
/**
* @version        $Id: add.ctp v1.0 17.03.2010 04:21:41 CET $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
?>
<div class="ergoRuTypes form">
<?php echo $form->create('ErgoRuType');?>
	<fieldset>
 		<legend><?php __('Add ErgoRuType');?></legend>
	<?php
		echo $form->input('lib', array("size"=>"80"));
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List ErgoRuTypes', true), array('action'=>'index'));?></li>
	</ul>
</div>

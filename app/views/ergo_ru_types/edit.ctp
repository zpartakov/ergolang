<div class="ergoRuTypes form">
<?php echo $form->create('ErgoRuType');?>
	<fieldset>
 		<legend><?php __('Edit ErgoRuType');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('lib');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action'=>'delete', $form->value('ErgoRuType.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('ErgoRuType.id'))); ?></li>
		<li><?php echo $html->link(__('List ErgoRuTypes', true), array('action'=>'index'));?></li>
	</ul>
</div>

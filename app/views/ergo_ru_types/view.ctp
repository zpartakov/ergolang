<div class="ergoRuTypes view">
<h2><?php  __('ErgoRuType');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuType['ErgoRuType']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Lib'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuType['ErgoRuType']['lib']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit ErgoRuType', true), array('action'=>'edit', $ergoRuType['ErgoRuType']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete ErgoRuType', true), array('action'=>'delete', $ergoRuType['ErgoRuType']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoRuType['ErgoRuType']['id'])); ?> </li>
		<li><?php echo $html->link(__('List ErgoRuTypes', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New ErgoRuType', true), array('action'=>'add')); ?> </li>
	</ul>
</div>

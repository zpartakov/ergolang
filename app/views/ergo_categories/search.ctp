<div class="ergoCategories index">
<h2><?php __('Catégories');?></h2>
<?
 echo $form->create("ErgoCategory",array('action' => 'search'));
    echo $form->input("q", array('label' => 'Chercher'));
    echo $form->end("Chercher"); 
?>
<p>

<?php echo $html->getCrumbs(' > ','Home'); ?>
<?php $html->addCrumb('Catégories', '/ergo_categories'); ?>


<table cellpadding="0" cellspacing="0">

<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('parent');?></th>
	<th><?php echo $paginator->sort('libelle');?></th>
	<th><?php echo $paginator->sort('notes');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;

 foreach ($results as $post): 
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $post['ergo_categories']['id']; ?>
		</td>
		<td>
			<?php echo $post['ergo_categories']['parent']; ?>
		</td>
		<td>
			<?php echo $post['ergo_categories']['libelle']; ?>
		</td>
		<td>
			<?php echo $post['ergo_categories']['notes']; ?>
		</td>
		<td class="actions">
			<?php 
echo "<a href=\"ergo_categories/view/" .$post['ErgoCategory']['id'] ."\">";
echo $html->image('b_search.png', array("alt"=>"Voir","title"=>"Voir"));
echo "</a>";
echo "&nbsp;";
echo "<a href=\"ergo_categories/edit/" .$post['ErgoCategory']['id'] ."\">";
echo $html->image('b_edit.png', array("alt"=>"Modifier","title"=>"Modifier"));
echo "</a>";
echo "<a onclick=\"javascript:return confirm('Confirmer la suppression ?')\" href=\"ergo_categories/delete/" .$post['ErgoCategory']['id'] ."\">";
echo $html->image('b_drop.png', array("alt"=>"Effacer","title"=>"Effacer"));
echo "</a>";

/*
b_drop.png
*/
?>
			<?php 
#echo $html->link(__("$html->image('b_drop.png')", true), array('action'=>'delete', $post['ErgoCategory']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $post['ErgoCategory']['id'])); 
#echo $html->link(__('delete', true), array('action'=>'delete', $post['ErgoCategory']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $post['ErgoCategory']['id'])); 

?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>

<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Nouvelle catégorie', true), array('action'=>'add')); ?></li>
	</ul>
</div>

<?php
/**
* @version        $Id: index.ctp v1.0 13.12.2009 10:40:08 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
require_once("lechemin.php");//compute paths

?>
<?
echo $javascript->link('simpletreemenu.js');
?>
<?=$html->css('simpletree');?>
<div class="ergoCategories index">
<h2><?php __('Catégories');?></h2>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Nouvelle catégorie', true), array('action'=>'add')); ?></li>
	</ul>
</div><?
 	echo $form->create("ErgoCategory",array('action' => 'search'));
    echo $form->input("q", array('label' => 'Chercher'));
    echo $form->end("Chercher"); 

?>
<?php echo $html->getCrumbs(' > ','Home'); ?>
<?php $html->addCrumb('Catégories', '/ergo_categories'); ?>
<p>
<?php
error_reporting(0);
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>


<a href="javascript:ddtreemenu.flatten('treemenu1', 'expand')">Développer</a> | <a href="javascript:ddtreemenu.flatten('treemenu1', 'contact')">Réduire</a>
<ul id="treemenu1" class="treeview">
<?php
foreach ($ergoCategories as $ergoCategory):
?>
<li>
	<?php 
			echo "<a href=\"" .$CHEMIN ."/ergo_categories/view/" .$ergoCategory['ErgoCategory']['id'] ."\">";
			echo $ergoCategory['ErgoCategory']['libelle']; 
			echo "</a>";

######### sous-categories ########
#niveau 2
$sqlsscat="SELECT * FROM ergo_categories WHERE parent = " .$ergoCategory['ErgoCategory']['id'];
#echo $sqlsscat;
$sqlsscat=mysql_query($sqlsscat);
$sqlsscatn=mysql_num_rows($sqlsscat);
$i=0;
if($sqlsscatn>0){
	echo "<ul>";
	
while($i<$sqlsscatn) {
echo "<li>"; 
echo "<a href=\"" .$CHEMIN ."/ergo_categories/view/" .mysql_result($sqlsscat,$i,'id') ."\">";
echo mysql_result($sqlsscat,$i,'libelle');
echo "</a>";
	
#niveau 3
	$sqlsscat3="SELECT * FROM ergo_categories WHERE parent = " .mysql_result($sqlsscat,$i,'id');
	#echo $sqlsscat;
	$sqlsscat3=mysql_query($sqlsscat3);
	$sqlsscatn3=mysql_num_rows($sqlsscat3);
	$i3=0;
		if($sqlsscatn3>0){
				echo "<ul>";

		while($i3<$sqlsscatn3) {
			echo "<li><a href=\"" .$CHEMIN ."/ergo_categories/view/" .mysql_result($sqlsscat3,$i3,'id') ."\">";
			echo mysql_result($sqlsscat3,$i3,'libelle');
			echo "</a>";
	#niveau 4		
				$sqlsscat4="SELECT * FROM ergo_categories WHERE parent = " .mysql_result($sqlsscat3,$i3,'id');
	#echo $sqlsscat;
	$sqlsscat4=mysql_query($sqlsscat4);
	$sqlsscatn4=mysql_num_rows($sqlsscat4);
	$i4=0;
		if($sqlsscatn4>0){
		echo "<ul>";
			while($i4<$sqlsscatn4) {
			echo "<li><a href=\"" .$CHEMIN ."/ergo_categories/view/" .mysql_result($sqlsscat4,$i4,'id') ."\">";
			echo mysql_result($sqlsscat4,$i4,'libelle');
			echo "</a></li>";
						$i4++;
			}
	echo "</ul>";

		}
			
			$i3++;
			echo "</li>";
		}
	echo "</ul>";
		
	}
$i++;
echo "</li>";

}
echo "</ul>";
}

?>

		</li>
<?php endforeach; ?>
</ul>
<script type="text/javascript">

//ddtreemenu.createTree(treeid, enablepersist, opt_persist_in_days (default is 1))

ddtreemenu.createTree("treemenu1", true)
ddtreemenu.createTree("treemenu2", false)

</script>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Nouvelle catégorie', true), array('action'=>'add')); ?></li>
	</ul>
</div>

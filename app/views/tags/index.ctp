<?php
/**
* @version        $Id: index.ctp v1.0 08.06.2010 05:27:02 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Tags / Catégories'; 

?>
<div class="tags index">
	<h2><?php echo $this->pageTitle;?></h2>
	
	<!-- begin search form -->
 <table>
	 <tr>
		 <td>
 <div class="input">
<?php echo $form->create('Tag', array('url' => array('action' => 'index'))); ?>
		<?php #echo $form->input('q', array('style' => 'width: 250px;', 'label' => false, 'size' => '80')); ?>
		<?php echo $form->input('q', array('label' => false, 'size' => '50', 'class'=>'txttosearch')); ?>
		</div>
</td><td>
<input type="reset" style="color: grey" value="Vider" onClick="javascript:vide_recherche('TagQ')" />
<input type="submit" class="chercher" value="Chercher" /> 
</div> 
</td>
</tr>
</table>
<!-- end search form -->
	
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('longname');?></th>
			
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($tags as $tag):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $tag['Tag']['id']; ?>&nbsp;</td>
		<td><?php echo $tag['Tag']['name']; ?>&nbsp;</td>
		<td><?php echo $tag['Tag']['longname']; ?>&nbsp;</td>
		<?
	/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
	?>
		<td class="actions">
			<?php 
			echo "&nbsp;";
			echo "<a href=\"".CHEMIN ."/tags/edit/" .$tag['Tag']['id'] ."\">";
			echo $html->image('b_edit.png', array("alt"=>"Modifier","title"=>"Modifier"));
			echo "</a>";
			echo "<a onclick=\"javascript:return confirm('Confirmer la suppression ?')\" href=\"" .CHEMIN ."/tags/delete/" .$tag['Tag']['id'] ."\">";
			echo $html->image('b_drop.png', array("alt"=>"Effacer","title"=>"Effacer"));
			echo "</a>";

			?>
		</td>
		<?
	}
	?>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<?
	/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
	?>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(sprintf(__('Nouveau %s', true), __('Tag', true)), array('action' => 'add')); ?></li>
			</ul>
</div>
<?
}
?>

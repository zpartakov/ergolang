<div class="ergoPtfrs view">
<h2><?php  __('Ergo Ptfr');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoPtfr['ErgoPtfr']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Cat'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoPtfr['ErgoPtfr']['cat']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Foreign'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoPtfr['ErgoPtfr']['foreign']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Local'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoPtfr['ErgoPtfr']['local']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Rem'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoPtfr['ErgoPtfr']['rem']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Type'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoPtfr['ErgoPtfr']['type']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Date'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoPtfr['ErgoPtfr']['date']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?	/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
	?>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ergo Ptfr', true), array('action' => 'edit', $ergoPtfr['ErgoPtfr']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Ergo Ptfr', true), array('action' => 'delete', $ergoPtfr['ErgoPtfr']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoPtfr['ErgoPtfr']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Ergo Ptfrs', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ergo Ptfr', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
<?
}
?>
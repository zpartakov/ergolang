<?php
/**
* @version        $Id: view.ctp v1.0 10.07.2011 15:01:43 CEST $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
?>
<div class="ErgoCroatefrs view">
<h2><?php  __('ErgoCroatefr');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ErgoCroatefr['ErgoCroatefr']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Foreign'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ErgoCroatefr['ErgoCroatefr']['foreign']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Local'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ErgoCroatefr['ErgoCroatefr']['local']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Type'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ErgoCroatefr['ErgoCroatefr']['type']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Date'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ErgoCroatefr['ErgoCroatefr']['date']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?	/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
	?>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit ErgoCroatefr', true), array('action' => 'edit', $ErgoCroatefr['ErgoCroatefr']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete ErgoCroatefr', true), array('action' => 'delete', $ErgoCroatefr['ErgoCroatefr']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ErgoCroatefr['ErgoCroatefr']['id'])); ?> </li>
		<li><?php echo $html->link(__('List ErgoCroatefrs', true), array('action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New ErgoCroatefr', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
<?
}
?>

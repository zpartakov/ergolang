<?php

$data = array();

foreach ($nodes as $node){
	$data[] = array(
		"text" => $node['Employee']['name'], 
		"id" => $node['Employee']['id'],
		"cls" => "folder",
		"leaf" => ($node['Employee']['lft'] + 1 == $node['Employee']['rght'])
	);
}

echo $javascript->object($data);

?>
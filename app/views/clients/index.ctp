<div class="clients index">
<h2><?php __('Clients');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('name');?></th>
	<th><?php echo $paginator->sort('address1');?></th>
	<th><?php echo $paginator->sort('address2');?></th>
	<th><?php echo $paginator->sort('zip_code');?></th>
	<th><?php echo $paginator->sort('city');?></th>
	<th><?php echo $paginator->sort('country');?></th>
	<th><?php echo $paginator->sort('phone');?></th>
	<th><?php echo $paginator->sort('fax');?></th>
	<th><?php echo $paginator->sort('url');?></th>
	<th><?php echo $paginator->sort('email');?></th>
	<th><?php echo $paginator->sort('comments');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th><?php echo $paginator->sort('extension_logo');?></th>
	<th><?php echo $paginator->sort('owner');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($clients as $client):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $client['Client']['id']; ?>
		</td>
		<td>
			<?php echo $client['Client']['name']; ?>
		</td>
		<td>
			<?php echo $client['Client']['address1']; ?>
		</td>
		<td>
			<?php echo $client['Client']['address2']; ?>
		</td>
		<td>
			<?php echo $client['Client']['zip_code']; ?>
		</td>
		<td>
			<?php echo $client['Client']['city']; ?>
		</td>
		<td>
			<?php echo $client['Client']['country']; ?>
		</td>
		<td>
			<?php echo $client['Client']['phone']; ?>
		</td>
		<td>
			<?php echo $client['Client']['fax']; ?>
		</td>
		<td>
			<?php echo $client['Client']['url']; ?>
		</td>
		<td>
			<?php echo $client['Client']['email']; ?>
		</td>
		<td>
			<?php echo $client['Client']['comments']; ?>
		</td>
		<td>
			<?php echo $client['Client']['created']; ?>
		</td>
		<td>
			<?php echo $client['Client']['extension_logo']; ?>
		</td>
		<td>
			<?php echo $client['Client']['owner']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action'=>'view', $client['Client']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $client['Client']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $client['Client']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $client['Client']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Client', true), array('action'=>'add')); ?></li>
	</ul>
</div>

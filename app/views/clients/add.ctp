<div class="clients form">
<?php echo $form->create('Client');?>
	<fieldset>
 		<legend><?php __('Add Client');?></legend>
	<?php
		echo $form->input('name');
		echo $form->input('address1');
		echo $form->input('address2');
		echo $form->input('zip_code');
		echo $form->input('city');
		echo $form->input('country');
		echo $form->input('phone');
		echo $form->input('fax');
		echo $form->input('url');
		echo $form->input('email');
		echo $form->input('comments');
		echo $form->input('extension_logo');
		echo $form->input('owner');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Clients', true), array('action'=>'index'));?></li>
	</ul>
</div>

<div class="mmedia index">
	<h2><?php __('Mmedia');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('lang');?></th>
			<th><?php echo $this->Paginator->sort('type');?></th>
			<th><?php echo $this->Paginator->sort('date');?></th>
			<th><?php echo $this->Paginator->sort('descr');?></th>
			<th><?php echo $this->Paginator->sort('file');?></th>
			<th><?php echo $this->Paginator->sort('url');?></th>
			<th><?php echo $this->Paginator->sort('ergotyp');?></th>
			<th><?php echo $this->Paginator->sort('ergoid');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($mmedia as $mmedium):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $mmedium['Mmedium']['id']; ?>&nbsp;</td>
		<td><?php echo $mmedium['Mmedium']['lang']; ?>&nbsp;</td>
		<td><?php echo $mmedium['Mmedium']['type']; ?>&nbsp;</td>
		<td><?php echo $mmedium['Mmedium']['date']; ?>&nbsp;</td>
		<td><?php echo $mmedium['Mmedium']['descr']; ?>&nbsp;</td>
		<td><?php echo $mmedium['Mmedium']['file']; ?>&nbsp;</td>
		<td><?php echo $mmedium['Mmedium']['url']; ?>&nbsp;</td>
		<td><?php echo $mmedium['Mmedium']['ergotyp']; ?>&nbsp;</td>
		<td><?php echo $mmedium['Mmedium']['ergoid']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $mmedium['Mmedium']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $mmedium['Mmedium']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $mmedium['Mmedium']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $mmedium['Mmedium']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(sprintf(__('New %s', true), __('Mmedium', true)), array('action' => 'add')); ?></li>
	</ul>
</div>
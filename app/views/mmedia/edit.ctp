<div class="mmedia form">
<?php echo $this->Form->create('Mmedium');?>
	<fieldset>
 		<legend><?php printf(__('Edit %s', true), __('Mmedium', true)); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('lang');
		echo $this->Form->input('type');
		echo $this->Form->input('date');
		echo $this->Form->input('descr');
		echo $this->Form->input('file');
		echo $this->Form->input('url');
		echo $this->Form->input('ergotyp');
		echo $this->Form->input('ergoid');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Mmedium.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Mmedium.id'))); ?></li>
		<li><?php echo $this->Html->link(sprintf(__('List %s', true), __('Mmedia', true)), array('action' => 'index'));?></li>
	</ul>
</div>
<?php
/**
* @version        $Id: missing_view.ctp v1.0 26.03.2010 14:53:06 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Action manquante / Erreur'; 
?>
<h1><? echo $this->pageTitle; ?></h1>

Vous voyez cette page parce que la ressource demandée n'existe pas ou que vous n'avez pas les droits suffisants; si vous voulez vous enregister, merci de <a href="/websites/ergolang/cake/contact/contacts/add">prendre contact</a> avec Эrgolang</em>

<?
echo '<a href="/websites/ergolang/cake">'.$html->image('Peugeot404_s.jpg', array("alt"=>"Erreur 404 / 404 ошибка","title"=>"Erreur 404 / 404 ошибка")).'</a>';

if($session->read("langue")=="russe") {
	?>
	
<h1>404 ошибка</h1>
Документ не найден, Not Found.<br />
Сервер не нашел ничего, соответствующего данному запрашиваемому URI.<br /><br />
<a href="/websites/ergolang/cake">Вернуться на главную страницу</a><br />


	<?
}
?>




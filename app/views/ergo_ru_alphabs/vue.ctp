<div class="ergoRuAlphabs view">
<h2><?php  __('Russe: alphabet vue');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Imprim Maj'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuAlphab['ErgoRuAlphab']['imprim_maj']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Imprim Min'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuAlphab['ErgoRuAlphab']['imprim_min']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Manuscrit Maj'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuAlphab['ErgoRuAlphab']['manuscrit_maj']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Manuscrit Min'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuAlphab['ErgoRuAlphab']['manuscrit_min']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Jpg Gif'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuAlphab['ErgoRuAlphab']['jpg_gif']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Image'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuAlphab['ErgoRuAlphab']['image']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Prononciation'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuAlphab['ErgoRuAlphab']['prononciation']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Sound'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuAlphab['ErgoRuAlphab']['sound']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Exemples'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuAlphab['ErgoRuAlphab']['exemples']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Exemples2'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuAlphab['ErgoRuAlphab']['exemples2']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Exemple Mp3'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuAlphab['ErgoRuAlphab']['exemple_mp3']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Traduction'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuAlphab['ErgoRuAlphab']['Traduction']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php 
if($session->read('Auth.User.group_id')==3) {
?>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ergo Ru Alphab', true), array('action' => 'edit', $ergoRuAlphab['ErgoRuAlphab']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Ergo Ru Alphab', true), array('action' => 'delete', $ergoRuAlphab['ErgoRuAlphab']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoRuAlphab['ErgoRuAlphab']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Ergo Ru Alphabs', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ergo Ru Alphab', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
<?php 
}
?> 
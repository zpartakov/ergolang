<div class="ergoRuAlphabs form">
<?php echo $this->Form->create('ErgoRuAlphab');?>
	<fieldset>
		<legend><?php __('Add Ergo Ru Alphab'); ?></legend>
	<?php
		echo $this->Form->input('imprim_maj');
		echo $this->Form->input('imprim_min');
		echo $this->Form->input('manuscrit_maj');
		echo $this->Form->input('manuscrit_min');
		echo $this->Form->input('jpg_gif');
		echo $this->Form->input('image');
		echo $this->Form->input('prononciation');
		echo $this->Form->input('sound');
		echo $this->Form->input('exemples');
		echo $this->Form->input('exemples2');
		echo $this->Form->input('exemple_mp3');
		echo $this->Form->input('Traduction');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Ergo Ru Alphabs', true), array('action' => 'index'));?></li>
	</ul>
</div>
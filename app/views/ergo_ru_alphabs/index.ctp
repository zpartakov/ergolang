<div class="ergoRuAlphabs index">
	<h2><?php __('Ergo Ru Alphabs');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('imprim_maj');?></th>
			<th><?php echo $this->Paginator->sort('imprim_min');?></th>
			<th><?php echo $this->Paginator->sort('manuscrit_maj');?></th>
			<th><?php echo $this->Paginator->sort('manuscrit_min');?></th>
			<th><?php echo $this->Paginator->sort('jpg_gif');?></th>
			<th><?php echo $this->Paginator->sort('image');?></th>
			<th><?php echo $this->Paginator->sort('prononciation');?></th>
			<th><?php echo $this->Paginator->sort('sound');?></th>
			<th><?php echo $this->Paginator->sort('exemples');?></th>
			<th><?php echo $this->Paginator->sort('exemples2');?></th>
			<th><?php echo $this->Paginator->sort('exemple_mp3');?></th>
			<th><?php echo $this->Paginator->sort('Traduction');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($ergoRuAlphabs as $ergoRuAlphab):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $ergoRuAlphab['ErgoRuAlphab']['id']; ?>&nbsp;</td>
		<td><?php echo $ergoRuAlphab['ErgoRuAlphab']['imprim_maj']; ?>&nbsp;</td>
		<td><?php echo $ergoRuAlphab['ErgoRuAlphab']['imprim_min']; ?>&nbsp;</td>
		<td><?php echo $ergoRuAlphab['ErgoRuAlphab']['manuscrit_maj']; ?>&nbsp;</td>
		<td><?php echo $ergoRuAlphab['ErgoRuAlphab']['manuscrit_min']; ?>&nbsp;</td>
		<td><?php echo $ergoRuAlphab['ErgoRuAlphab']['jpg_gif']; ?>&nbsp;</td>
		<td><?php echo $ergoRuAlphab['ErgoRuAlphab']['image']; ?>&nbsp;</td>
		<td><?php echo $ergoRuAlphab['ErgoRuAlphab']['prononciation']; ?>&nbsp;</td>
		<td><?php echo $ergoRuAlphab['ErgoRuAlphab']['sound']; ?>&nbsp;</td>
		<td><?php echo $ergoRuAlphab['ErgoRuAlphab']['exemples']; ?>&nbsp;</td>
		<td><?php echo $ergoRuAlphab['ErgoRuAlphab']['exemples2']; ?>&nbsp;</td>
		<td><?php echo $ergoRuAlphab['ErgoRuAlphab']['exemple_mp3']; ?>&nbsp;</td>
		<td><?php echo $ergoRuAlphab['ErgoRuAlphab']['Traduction']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $ergoRuAlphab['ErgoRuAlphab']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $ergoRuAlphab['ErgoRuAlphab']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $ergoRuAlphab['ErgoRuAlphab']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoRuAlphab['ErgoRuAlphab']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Ergo Ru Alphab', true), array('action' => 'add')); ?></li>
	</ul>
</div>
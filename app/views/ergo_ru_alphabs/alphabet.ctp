
	<?php
/**
* @version        $Id: alphabet.ctp v1.0 18.10.2013 $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Alphabet Russe'; 


?>

<div class="ergoRuAlphabs index">
	<h2><?php __('Russe: alphabet');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('imprim_maj');?></th>
			<th><?php echo $this->Paginator->sort('imprim_min');?></th>
			<th><?php echo $this->Paginator->sort('Manuscrit','jpg_gif');?></th>
			<th><?php echo $this->Paginator->sort('prononciation');?></th>
			<th><?php echo $this->Paginator->sort('sound');?></th>
			<th><?php echo $this->Paginator->sort('Exemple','exemples2');?></th>
			<th><?php echo $this->Paginator->sort('image');?></th>
			<th><?php echo $this->Paginator->sort('exemple_mp3');?></th>
			<th><?php echo $this->Paginator->sort('Traduction');?></th>
			<?php 
			if($session->read('Auth.User.group_id')==3) {
			?>
			<th class="actions"><?php __('Actions');?></th>
			<?php 
			}
			?> 
	</tr>
	<?php
	$i = 0;
	foreach ($ergoRuAlphabs as $ergoRuAlphab):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $ergoRuAlphab['ErgoRuAlphab']['imprim_maj']; ?>&nbsp;</td>
		<td><?php echo $ergoRuAlphab['ErgoRuAlphab']['imprim_min']; ?>&nbsp;</td>
		<td>
		<?php 
		echo "<a href=\"" .CHEMIN ."/img/audio/ru/alpha/" .$ergoRuAlphab['ErgoRuAlphab']['jpg_gif'] .".gif\">";
		echo $html->image('audio/ru/alpha/'.$ergoRuAlphab['ErgoRuAlphab']['jpg_gif'].".jpg", array("alt"=>"Voir","title"=>"Voir"));
		echo "</a>";
		?>
		</td>
		<td><?php echo $ergoRuAlphab['ErgoRuAlphab']['prononciation']; ?>&nbsp;</td>
		<td>
  	<?php 
		//Audio-Speaker.png  Audio.png  ru/
/*
		echo "<a href=\"javascript:playSound('/files/audio/ru/alpha/"
		.$ergoRuAlphab['ErgoRuAlphab']['sound']."')\">";
		echo $html->image('audio/Audio.png', array('style'=>'width: 50px'));
		echo $ergoRuAlphab['ErgoRuAlphab']['sound'];
		echo "</a>";
*/
		
		
		?>
		<a target="_blank" href="audio?sound=<?php echo $ergoRuAlphab['ErgoRuAlphab']['sound'];?>">mp3</a>
		
		</td>
		<td><?php echo $ergoRuAlphab['ErgoRuAlphab']['exemples2']; ?>&nbsp;</td>
		<td><?php 
		
		echo $html->image('audio/ru/alpha/'.$ergoRuAlphab['ErgoRuAlphab']['image'], array("alt"=>"Voir","title"=>"Voir"));
		
		?>&nbsp;

		</td>
		
		<td>
<!-- 		<div id="<?php echo $ergoRuAlphab['ErgoRuAlphab']['id'];?>">
		<a href="javascript:joue(<?php echo $ergoRuAlphab['ErgoRuAlphab']['id'] .",'" .CHEMIN ."/files/audio/ru/alpha/" .
		
		
		$ergoRuAlphab['ErgoRuAlphab']['exemple_mp3']; ?>')"
		>
		<?php echo CHEMIN ."/files/audio/ru/alpha/" .$ergoRuAlphab['ErgoRuAlphab']['exemple_mp3'] ."'"; ?></a>&nbsp;
		 -->
		<?php 
		echo $ergoRuAlphab['ErgoRuAlphab']['exemple_mp3'];
		?>
		</div>
		</td>
		<td><?php echo $ergoRuAlphab['ErgoRuAlphab']['Traduction']; ?>&nbsp;</td>
		<?php 
		if($session->read('Auth.User.group_id')==3) {
		?>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $ergoRuAlphab['ErgoRuAlphab']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $ergoRuAlphab['ErgoRuAlphab']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $ergoRuAlphab['ErgoRuAlphab']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoRuAlphab['ErgoRuAlphab']['id'])); ?>
		</td>
		<?php 
		}
		?> 
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>


<?php 
if($session->read('Auth.User.group_id')==3) {
?>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Ergo Ru Alphab', true), array('action' => 'add')); ?></li>
	</ul>
</div>
<?php 
}
?> 
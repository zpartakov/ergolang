<div class="ergoEsfrs form">
<?php echo $this->Form->create('ErgoEsfr');?>
	<fieldset>
 		<legend><?php __('Add Ergo Esfr'); ?></legend>
	<?php
		echo $this->Form->input('foreign');
		echo $this->Form->input('local');
		echo $this->Form->input('type');
		echo $this->Form->input('date');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Ergo Esfrs', true), array('action' => 'index'));?></li>
	</ul>
</div>
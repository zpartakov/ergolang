<?php
/**
* @version        $Id: view.ctp v1.0 13.10.2010 11:36:15 CEST $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Dictionnaire Italien <=> Français: Vue'; 

?>
<div class="ergoItfrs view">
<h1><? echo $this->pageTitle; ?></h1>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoItfr['ErgoItfr']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Foreign'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoItfr['ErgoItfr']['foreign']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Local'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoItfr['ErgoItfr']['local']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?	/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
	?>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ergo Itfr', true), array('action' => 'edit', $ergoItfr['ErgoItfr']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Ergo Itfr', true), array('action' => 'delete', $ergoItfr['ErgoItfr']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoItfr['ErgoItfr']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Ergo Itfrs', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ergo Itfr', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
<?
}
?>

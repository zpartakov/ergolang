<div class="ergoItfrs form">
<?php echo $this->Form->create('ErgoItfr');?>
	<fieldset>
 		<legend><?php __('Edit Ergo Itfr'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('foreign');
		echo $this->Form->input('local');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('ErgoItfr.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('ErgoItfr.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Ergo Itfrs', true), array('action' => 'index'));?></li>
	</ul>
</div>
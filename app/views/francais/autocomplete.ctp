<!-- // {app}/views/personnes/autocomplete.ctp -->
<ul>
<?php foreach($professions as $profession): ?> 
     <li><?php e($text->highlight(
     	$profession['ErgoEnfr']['local'],
     	$recherche,
     	'<strong>\1</strong>'
     )); ?></li>
<?php endforeach; ?> 
</ul>
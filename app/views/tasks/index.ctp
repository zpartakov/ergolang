<?php
/**
* @version        $Id: index.ctp v1.0 24.03.2010 05:57:23 CET $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'ToDos'; 

?>
<div class="tasks index">
<h1><? echo $this->pageTitle; ?></h1>
<a href="/websites/ergolang/cake/tasks?done=0"><?php __('On Field') ?></a>
 | <a href="/websites/ergolang/cake/tasks?done=1"><?php __('Ok') ?></a>
 | <a href="/websites/ergolang/cake/tasks/add">New Task</a>

<!-- begin search form -->
 <table>
	 <tr>
		 <td>
 <div class="input">
<?php echo $form->create('Task', array('url' => array('action' => 'index'))); ?>
		<?php #echo $form->input('q', array('style' => 'width: 250px;', 'label' => false, 'size' => '80')); ?>
		<?php echo $form->input('q', array('label' => false, 'size' => '50', 'class'=>'txttosearch')); ?>
		</div>
</td><td>
<input type="button" class="vider" value="Vider" onClick="javascript:vide_recherche('TaskQ')" />
<input type="submit" class="chercher" value="Chercher" /> 
</div> 
</td>
</tr>
</table>
<!-- end search form -->
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('title');?></th>
	<th><?php echo $paginator->sort('done');?></th>
	<th><?php echo $paginator->sort('delay');?></th>
	<th><?php echo $paginator->sort('project_id');?></th>
	<th><?php echo $paginator->sort('priority');?></th>
	<th><?php echo $paginator->sort('user_id');?></th>
	<th><?php echo $paginator->sort('url');?></th>

	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($tasks as $task):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
		<?php echo $html->link(__($task['Task']['id'], true), array('action'=>'edit', $task['Task']['id'])); ?>
		</td>
		<td>
<?php 
#echo $task['Task']['title']; 
echo $html->link(__($task['Task']['title'], true), array('action'=>'view', $task['Task']['id']));
$lurl=$task['Task']['url'];
if(strlen($lurl)>0){
echo "<br><a href=\"$lurl\">url</a>";
}
?>
		</td>
		<td>
			<?php echo $task['Task']['done']; ?>
		</td>

		<td>
			<?php echo $task['Task']['delay']; ?>
		</td>
		<td>
			<?php echo $html->link($task['Project']['name'], array('controller'=> 'projects', 'action'=>'view', $task['Project']['id'])); ?>
		</td>
		<td>
			<?php echo $task['Task']['priority']; ?>
		</td>
		<td>
			<?php echo $html->link($task['User']['username'], array('controller'=> 'users', 'action'=>'view', $task['User']['id'])); ?>
		</td>
				<td>
			<?php 
			if($task['Task']['url']) {
			echo '<a href="';
			#echo $html->link($task['Task']['url'], array('controller'=> 'users', 'action'=>'view', $task['Task']['url'])); 
			echo $task['Task']['url'];
			echo '" target="_blank">www</a>';
		}
			?>
		</td>


		<td class="actions">
<?php 
echo "<a href=\"tasks/view/" .$task['Task']['id'] ."\">";
echo $html->image('b_search.png', array("alt"=>"Voir","title"=>"Voir"));
echo "</a>";
	/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
echo "&nbsp;";
echo "<a href=\"tasks/edit/" .$task['Task']['id'] ."\">";
echo $html->image('b_edit.png', array("alt"=>"Modifier","title"=>"Modifier"));
echo "</a>";
echo "<a onclick=\"javascript:return confirm('Confirmer la suppression ?')\" href=\"tasks/delete/" .$task['Task']['id'] ."\">";
echo $html->image('b_drop.png', array("alt"=>"Effacer","title"=>"Effacer"));
echo "</a>";
}
?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Task', true), array('action'=>'add')); ?></li>
		<li><?php echo $html->link(__('List Projects', true), array('controller'=> 'projects', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Project', true), array('controller'=> 'projects', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Users', true), array('controller'=> 'users', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New User', true), array('controller'=> 'users', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Clients', true), array('controller'=> 'clients', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Client', true), array('controller'=> 'clients', 'action'=>'add')); ?> </li>
	</ul>
</div>

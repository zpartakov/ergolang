<div class="ergoLangues form">
<?php echo $form->create('ErgoLangue');?>
	<fieldset>
 		<legend><?php __('Add ErgoLangue');?></legend>
	<?php
		echo $form->input('code');
		echo $form->input('lib');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List ErgoLangues', true), array('action'=>'index'));?></li>
	</ul>
</div>

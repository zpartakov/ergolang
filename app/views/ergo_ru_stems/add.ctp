<?php
/**
* @version        $Id: add.ctp v1.0 09.07.2010 07:08:31 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Stem - Ajouter'; 
?>
<div class="ergoRuStems form">
<?php echo $this->Form->create('ErgoRuStem');?>
	<fieldset>
 		<legend><?php echo $this->pageTitle;?></legend>
	<?php
		echo $this->Form->input('src');
		echo $this->Form->input('replace');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Ajouter', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(sprintf(__('List %s', true), __('Ergo Ru Stems', true)), array('action' => 'index'));?></li>
	</ul>
</div>

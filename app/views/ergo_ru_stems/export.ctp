<?php
/**
* @version        $Id: export.ctp v1.0 09.07.2010 07:06:26 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<?
$output="
/**
* @version        $Id: stemmer.ctp v1.0 30.12.2009 12:36:37 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
* 
* 
*/
//very light stemmer for ergolang

#nettoyer les mots pour le vocabulaire
function normaliserMot(\$mot) {
\$mot=strtolower_utf8(\$mot);
\$mot=trim(\$mot);";

#do and check sql
$sql="SELECT * FROM ergo_ru_stems ORDER BY src";
$sql=mysql_query($sql);
if(!$sql) {
	echo "SQL error: " .mysql_error(); exit;
}

$i=0;
while($i<mysql_num_rows($sql)){
	$src=mysql_result($sql,$i,'src');
	$replace=mysql_result($sql,$i,'replace');
	$output.= '$mot=preg_replace("/' .$src .'","'.$replace.'",$mot);
	';
	$i++;
}
	$output.="
	
	return \$mot;
}
";
#echo nl2br($output);

$filename = CHEMINFICHIERS.'/app/views/ergo_ru_phrases/stemmer.ctp';
$somecontent = "<?php\n" .$output ."\n?>";

// Assurons nous que le fichier est accessible en écriture
if (is_writable($filename)) {

   // Dans notre exemple, nous ouvrons le fichier $filename en mode d'ajout
   // Le pointeur de fichier est placé à la fin du fichier
   // c'est là que $somecontent sera placé
   if (!$handle = fopen($filename, 'w')) {
         echo "Impossible d'ouvrir le fichier ($filename)";
         exit;
   }

   // Ecrivons quelque chose dans notre fichier.
   if (fwrite($handle, $somecontent) === FALSE) {
       echo "Impossible d'écrire dans le fichier ($filename)";
       exit;
   }
  
   echo "L'écriture dans le <a href=/websites/ergolang/cake/ergo_ru_phrases/test.txt>fichier</a> a réussi";
  
   fclose($handle);
                  
} else {
   echo "Le fichier $filename n'est pas accessible en écriture.";
}

?>


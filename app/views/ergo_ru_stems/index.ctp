<?php
/**
* @version        $Id: index.ctp v1.0 09.07.2010 07:09:59 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Stems'; 

?>
<div class="ergoRuStems index">
	<h2><?php echo $this->pageTitle;?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('src');?></th>
			<th><?php echo $this->Paginator->sort('replace');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($ergoRuStems as $ergoRuStem):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $ergoRuStem['ErgoRuStem']['id']; ?>&nbsp;</td>
		<td><?php echo $ergoRuStem['ErgoRuStem']['src']; ?>&nbsp;</td>
		<td><?php echo $ergoRuStem['ErgoRuStem']['replace']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $ergoRuStem['ErgoRuStem']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $ergoRuStem['ErgoRuStem']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $ergoRuStem['ErgoRuStem']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoRuStem['ErgoRuStem']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(sprintf(__('New %s', true), __('Ergo Ru Stem', true)), array('action' => 'add')); ?></li>
	</ul>
</div>

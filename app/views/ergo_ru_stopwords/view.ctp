<div class="ergoRuStopwords view">
<h2><?php  __('Ergo Ru Stopword');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuStopword['ErgoRuStopword']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Word'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoRuStopword['ErgoRuStopword']['word']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(sprintf(__('Edit %s', true), __('Ergo Ru Stopword', true)), array('action' => 'edit', $ergoRuStopword['ErgoRuStopword']['id'])); ?> </li>
		<li><?php echo $this->Html->link(sprintf(__('Delete %s', true), __('Ergo Ru Stopword', true)), array('action' => 'delete', $ergoRuStopword['ErgoRuStopword']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoRuStopword['ErgoRuStopword']['id'])); ?> </li>
		<li><?php echo $this->Html->link(sprintf(__('List %s', true), __('Ergo Ru Stopwords', true)), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(sprintf(__('New %s', true), __('Ergo Ru Stopword', true)), array('action' => 'add')); ?> </li>
	</ul>
</div>

<div class="ergoRuStopwords form">
<?php echo $this->Form->create('ErgoRuStopword');?>
	<fieldset>
 		<legend><?php printf(__('Edit %s', true), __('Ergo Ru Stopword', true)); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('word');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('ErgoRuStopword.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('ErgoRuStopword.id'))); ?></li>
		<li><?php echo $this->Html->link(sprintf(__('List %s', true), __('Ergo Ru Stopwords', true)), array('action' => 'index'));?></li>
	</ul>
</div>
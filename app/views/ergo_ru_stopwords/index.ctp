<div class="ergoRuStopwords index">
	<h2><?php __('Ergo Ru Stopwords');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('word');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($ergoRuStopwords as $ergoRuStopword):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $ergoRuStopword['ErgoRuStopword']['id']; ?>&nbsp;</td>
		<td><?php echo $ergoRuStopword['ErgoRuStopword']['word']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $ergoRuStopword['ErgoRuStopword']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $ergoRuStopword['ErgoRuStopword']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $ergoRuStopword['ErgoRuStopword']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoRuStopword['ErgoRuStopword']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(sprintf(__('New %s', true), __('Ergo Ru Stopword', true)), array('action' => 'add')); ?></li>
	</ul>
</div>
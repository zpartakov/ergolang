<div class="ergoRuStopwords form">
<?php echo $this->Form->create('ErgoRuStopword');?>
	<fieldset>
 		<legend><?php printf(__('Add %s', true), __('Ergo Ru Stopword', true)); ?></legend>
	<?php
		echo $this->Form->input('word');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(sprintf(__('List %s', true), __('Ergo Ru Stopwords', true)), array('action' => 'index'));?></li>
	</ul>
</div>
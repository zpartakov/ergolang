<?php
/**
* @version        $Id: menu.ctp v1.0 30.12.2009 08:54:11 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
?>

	<div id="cakephp-global-navigation">
<ul id="menuDeroulant">
	<!-- homepage -->
	<li>
		<a href="/websites/ergolang/cake">Accueil</a>
		<ul class="sousMenu">
			<!-- blog -->
		<li><a style="color: PeachPuff" href="/contact">Contact</a></li>
		<li><a href="/websites/ergolang/cake/posts/"><?php __('News') ?></a></li>
				<li><a href="/websites/ergolang/cake/ergo_links/">Liens</a></li>
		<!-- doc -->
			<li><a href="/websites/ergolang/cake/license">License</a></li>
			<li><a href="/websites/ergolang/cake/credits">Crédits</a></li>
			<li><a href="http://radeff.net/dokuwiki/ergolang:ergowiki:homepage">Aide / Wiki</a></li>

	</ul>
	</li>

	<?php
	if($_SESSION['langue']) {
if($_SESSION['langue']=="allemand") {
	 echo $this->element('menuall');
} elseif($_SESSION['langue']=="anglais") {
	 echo $this->element('menuen');
}elseif($_SESSION['langue']=="arabe") {
	 echo $this->element('menuarabe');
} elseif($_SESSION['langue']=="croate") {
	 echo $this->element('menucroate');
} elseif($_SESSION['langue']=="espagnol") {
	 echo $this->element('menuesp');
} elseif($_SESSION['langue']=="francais") {
	 echo $this->element('menufr');
} elseif($_SESSION['langue']=="italien") {
	 echo $this->element('menuit');
} elseif($_SESSION['langue']=="latin") {
	 echo $this->element('menulat');
} elseif($_SESSION['langue']=="portugais") {
	 echo $this->element('menuportugais');
} elseif($_SESSION['langue']=="russe") {
	 echo $this->element('menurusse');
}  elseif($_SESSION['langue']=="syrien") {
	 echo $this->element('menusyrien');
}  elseif($_SESSION['langue']=="hollandais") {
	 echo $this->element('menuhollandais');
}
}
	 ?>




	<?php
	############## ADMIN AREA ##################
	/*	hide from non-admin registred user */
	if($session->read('Auth.User.group_id')==3) {
		?>
	<li>
		<a href="/websites/ergolang/cake/pages/admin"><?php __('Admin') ?></a>
		<ul>
			<li><a href="http://129.194.18.217/intranet/pmcake/pm_projects/view/88" target="_blank">pm</a></li>
		</ul>
		<ul>
		<li>Russe</li>
			<li><a href="/websites/ergolang/cake/exercices/">Exercices</a></li>
				<li><a href="/websites/ergolang/cake/ergo_ru_exercices/">Exercices russe</a></li>
		<hr /><li><a href="/websites/ergolang/cake/articles/">Articles / Pages</a></li>
		<li><a href="<? echo CHEMIN;?>/mmedia">Multimédias</a></li>
		<li><a href="/websites/ergolang/cake/ergo_ru_grammaires/nettoyer">nettoyer fichiers importés</a></li>
		<li><a href="/websites/ergolang/cake/ergo_ru_grammaires/checkimport">check fichiers importés</a></li>
		<li><a href="/ergolangcopy/langues2.ups-tlse.fr/Beatrice/grammaire/!menu_gram.htm">grammaire à copier</a></li>
		</ul>
		<ul class="sousMenu">
		<li><a href="/websites/ergolang/cake/users/">Utilisateurs</a></li>
		<li><a href="/websites/ergolang/cake/contacts/">Contacts (old)</a></li>
		<li><a href="/websites/ergolang/cake/ergoLangues/">Langues</a></li>
		<li><a href="/websites/ergolang/cake/ergo_motsassocies/">Mots associés</a></li>
					<!-- categories -->
	<li><a href="/websites/ergolang/cake/categories/"><?php __('Catégories') ?></a></li>
	<li><a href="/websites/ergolang/cake/categories_ergo_rufrs"><?php __('Catégories / mots') ?></a></li>

</ul>
</li>


	<?
	/* end user admin todo hide from non-admin registred user */
	}
	?>
	<!-- langues -->
	<li style="background-color: #B48308"><a href="/websites/ergolang/cake/ergoLangues/">Langues</a>
		<ul class="sousMenu">
		<li><a href="/websites/ergolang/cake/users/langue/?langue=allemand">allemand</a></li>
		<li><a href="/websites/ergolang/cake/users/langue/?langue=anglais">anglais</a></li>
		<li><a href="/websites/ergolang/cake/users/langue/?langue=arabe">arabe</a></li>
		<li><a href="/websites/ergolang/cake/users/langue/?langue=croate">croate</a></li>
		<li><a href="/websites/ergolang/cake/users/langue/?langue=espagnol">espagnol</a></li>
		<li><a href="/websites/ergolang/cake/users/langue/?langue=francais">français</a></li>
		<li><a href="/websites/ergolang/cake/users/langue/?langue=hollandais">hollandais</a></li>
		<li><a href="/websites/ergolang/cake/users/langue/?langue=italien">italien</a></li>
		<li><a href="/websites/ergolang/cake/users/langue/?langue=latin">latin</a></li>
		<li><a href="/websites/ergolang/cake/users/langue/?langue=portugais">portugais</a></li>
		<li><a href="/websites/ergolang/cake/users/langue/?langue=russe">russe</a></li>
		<li><a href="/websites/ergolang/cake/users/langue/?langue=syrien">syrien</a></li>
		</ul>
	</li>
	<li>
	<?php
	if($session->read('Auth.User.group_id')) {
	?>
	<a href="/websites/ergolang/cake/users/logout">Logout</a>
	<?php
	} else {
			?>
		<a href="/websites/ergolang/cake/users/login">Login</a>

			<?php
	}
	?>

	</li>


</ul>

</div>

<?php
/**
* @version        $Id: menuen.ctp v1.0 27.03.2010 14:57:37 CET $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
?>
	<!-- english -->
			<li><a href="/websites/ergolang/cake/anglais">English</a>
					<ul class="sousMenu">
			<li><a href="/websites/ergolang/cake/ergoEnfrs/">Vocabulaire Anglais</a></li>
			<li><a href="/websites/ergolang/cake/ergoEnPhrases/">Phrases Anglais</a></li>
			<li><a href="/websites/ergolang/cake/ergoEnfrs/rumtrain/">Drill vocabulaire</a></li>
			<li><a href="/websites/ergolang/cake/ergoEnfrs/myrumtrain/">Drill vocabulaire (mes mots)</a></li>
				</ul>
</li>

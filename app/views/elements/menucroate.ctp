<?php
/**
* @version        $Id: menucroate.ctp v1.0 10.01.2011 12:14:59 CEST $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
?>
	<!-- croate -->
			<li><a href="/websites/ergolang/cake/croate">Croate</a>
					<ul class="sousMenu">
<li><a href="/websites/ergolang/cake/ergoCroatefrs">Dictionnaire croate-français</a></li>
<li><a href="/websites/ergolang/cake/articles/vue/16">Prononciation</a></li>		
<li><a href="/websites/ergolang/cake/articles/vue/17">Le nom</a></li>		
<li><a href="/websites/ergolang/cake/articles/vue/18">L'adjectif</a></li>	
<li><a href="/websites/ergolang/cake/articles/vue/19">Les pronoms</a></li>	
<li><a href="/websites/ergolang/cake/articles/vue/20">Les nombres</a></li>	
<li><a href="/websites/ergolang/cake/articles/vue/21">Le verbe</a></li>	
<li><a href="/websites/ergolang/cake/articles/vue/22">Prépositions</a></li>	
<li><a href="/websites/ergolang/cake/articles/vue/23">Divers grammaire</a></li>	
				<li><a href="<? echo CHEMIN; ?>/ergoCroatefrs/exportcsv">Export (csv)</a></li>
	
				</ul>
</li>

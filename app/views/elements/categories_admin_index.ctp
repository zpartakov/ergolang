<?php
// {app}/views/elements/categories_admin_index.ctp
$id = $data['Category']['id'];
?> 
<h4><?php e($data['Category']['libelle']); ?></h4>
<ul class="options">
	<li><?php e($html->link('Monter', 'move_up/'.$id)); ?></li>
	<li><?php e($html->link('Descendre', 'move_down/'.$id)); ?></li>
	<li><?php e($html->link("Modifier", 'edit/'.$id)); ?></li>
	<li><?php e($html->link("Supprimer", 'delete/'.$id, null, "Etes-vous sûr ?")); ?></li>
</ul>

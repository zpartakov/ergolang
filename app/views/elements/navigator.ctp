<noscript>
<div class="noscript">Activez JavaScript! si vous voulez bénéficier de toutes les fonctionnalités de ce site</div>
</noscript>

<?php
 /**
 * @version        $Id: navigator.ctp v1.0 25.03.2010 10:12:00 CET $
 * @package        Эrgolang
 * @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
 * @license        GNU/GPL, see LICENSE.php
 * Эrgolang is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */
  //usefull to identify user's browser source: http://www.allhtml.com/articles/detail/254
   $HTTP_USER_AGENT=$_SERVER["HTTP_USER_AGENT"];
 #echo "<br>Navigateur: " .$HTTP_USER_AGENT ."<br>"; //test purpose
  	$messagenavi="<strong>ATTENTION:</strong> Ce site est prévu pour le navigateur Mozilla/Firefox.<br />";
		$messagenavi.="Votre navigateur actuel: " .$HTTP_USER_AGENT;
		$messagenavi.="<br />Vous pouvez installer firefox en utilisant le lien ci-dessous:<br />";
		$messagenavi.= '<a href="http://www.mozilla-europe.org/fr/"><img src="/websites/ergolang/cake/img/logo_firefox.gif" alt="Télécharger Firefox" /></a>';
$class="messagenavi";
		 if (preg_match("/Firefox/", $HTTP_USER_AGENT)
		 		||preg_match("/.*Epiphany.*/", $HTTP_USER_AGENT)
		 		||preg_match("/opera/i", $HTTP_USER_AGENT)
		 		||preg_match("/chrome/i", $HTTP_USER_AGENT)
		 ) {     // Firefox
			 $class="";
			 $messagenavi="<div style=\"padding: 8px; background-color: lightyellow\">Votre navigateur: " .$HTTP_USER_AGENT ." est compatible</div>";
			//autres navigateurs = notification
		} elseif (eregi('msie', $HTTP_USER_AGENT)) {
			 // Internet Explorer  
		$navigateur="Internet Explorer";
		 } elseif (eregi('Mozilla/4.', $HTTP_USER_AGENT)) {     // Netscape 4.x  
		$navigateur="Netscape 4.x";

		 } else {     // Autres navigateurs  
		$navigateur="Navigateur inconnu";
	 } 
	#echo $HTTP_USER_AGENT ." - " .$navigateur; 
	echo '<div class="'.$class.'">' .$messagenavi .'</div>';
	
?>

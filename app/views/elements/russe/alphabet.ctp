<?php
/**
* @version        $Id: alphabet.ctp v1.0 13.07.2010 05:55:48 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
* 
* note: you HAVE TO make 2 arrays otherwise strange behiavour with the SQL statement for particular russian letters, eg. "в", "и" etc. who return only one result (???!!!)
* 
*/
####### A RUSSIAN ALPHABET ##########
$alphabet=array(
	"0",
	"1",
	"2",
	"3",
	"4",
	"5",
	"6",
	"7",
	"8",
	"9",
	"10",
	"11",
	"12",
	"13",
	"14",
	"15",
	"16",
	"17",
	"18",
	"19",
	"20",
	"21",
	"22",
	"23",
	"24",
	"25",
	"26",
	"27",
	"28",
	"29",
	"30",
	"31"
);

$alphabetCyr=array(
	"а",
	"б",
	"в",
	"г",
	"д",
	"е",
	"ж",
	"з",
	"и",
	"й",
	"к",
	"л",
	"м",
	"н",
	"о",
	"п",
	"р",
	"с",
	"т",
	"у",
	"ф",
	"х",
	"ц",
	"ч",
	"ш",
	"щ",
	"ъ",
	"ы",
	"ь",
	"э",
	"ю",
	"я"
);

echo "<table>
<tr>";
$size=count($alphabet);
	for ($i=0; $i < $size; $i++) {
	echo "<td style=\"text-align: center\">";
	echo '<a href="?alphabet='.$alphabet[$i].'" title="mots commençant par  « '.$alphabetCyr[$i].' »">'.$alphabetCyr[$i] ."</a>";
	echo  "</td>\n";
	if($i==16) {
		echo "</tr><tr>";
	}
}
echo "</tr>
</table>";


?>




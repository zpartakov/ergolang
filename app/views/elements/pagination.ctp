<?php
/**
* @version        $Id: pagination.ctp v1.0 15.04.2010 14:35:06 CEST $
* www.unige.ch
* webmaster@unige.ch
source: http://bakery.cakephp.org/articles/view/pagination / http://bakery.cakephp.org/articles/view/pagination-element
*/
?>    
<div id='pagination'>
<?php
    if($pagination->setPaging($paging)):
    $leftArrow = $html->image("nav/arrowleft.gif", Array('height'=>15));
    $rightArrow = $html->image("nav/arrowright.gif", Array('height'=>15));
	
    $prev = $pagination->prevPage($leftArrow,false);
    $prev = $prev?$prev:$leftArrow;
    $next = $pagination->nextPage($rightArrow,false);
    $next = $next?$next:$rightArrow;

    $pages = $pagination->pageNumbers(" | ");

    echo $pagination->result()."<br />";
    echo $prev." ".$pages." ".$next."<br />";
    echo "Afficher " .$pagination->resultsPerPage(NULL, ' ') ." enregistrements par page";
    endif;
?>
</div>

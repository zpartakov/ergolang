<?php
/**
* @version		$Id: menusyrien.ctp v1.0 13.07.2011 11:48:52 CEST $
* @package		Эrgolang
* @copyright	Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
?>
	<!-- arabe -->
			<li><a href="/websites/ergolang/cake/syrien">Syrien</a>
					<ul class="sousMenu">
<li><a href="/websites/ergolang/cake/ergoSyfrs">Dictionnaire syrien-français</a></li>	
				</ul>
</li>

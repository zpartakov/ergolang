<?php
/**
* @version        $Id: edit.ctp v1.0 10.07.2011 15:00:47 CEST $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
#debug($this->Session);
?>
<div class="ergoArafrs form">
<?php echo $this->Form->create('ErgoArafr');?>
	<fieldset>
		<legend><?php __('Edit Ergo Arafr'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('foreign');
		echo $this->Form->input('local');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('ErgoArafr.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('ErgoArafr.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Ergo Arafrs', true), array('action' => 'index'));?></li>
	</ul>
</div>

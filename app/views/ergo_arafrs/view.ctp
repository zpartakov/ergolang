<?php
/**
* @version        $Id: view.ctp v1.0 10.07.2011 15:01:00 CEST $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
?>
<div class="ergoArafrs view">
<h2><?php  __('Ergo Arafr');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoArafr['ErgoArafr']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Foreign'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoArafr['ErgoArafr']['foreign']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Local'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoArafr['ErgoArafr']['local']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ergo Arafr', true), array('action' => 'edit', $ergoArafr['ErgoArafr']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Ergo Arafr', true), array('action' => 'delete', $ergoArafr['ErgoArafr']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoArafr['ErgoArafr']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Ergo Arafrs', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ergo Arafr', true), array('action' => 'add')); ?> </li>
	</ul>
</div>

<?php
/**
* @version        $Id: lexercice.ctp v1.0 10.03.2010 15:51:22 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = $exercice['Exercice']['titreexercice'];
?>
<div class="exercices view">
<?
echo "<h1><a href=\"/websites/ergolang/cake/ergo_ru_grammaires/vue/" .$exercice['Exercice']['liengrammaire'] ."\" alt=\"Grammaire\" title=\"Grammaire\">" .$this->pageTitle ."</a> | <a href=\"/websites/ergolang/cake/ergo_ru_grammaires/vue/124\">Liste des exercices</a></h1>";
echo "<h2>".$exercice['Exercice']['soustitre']."</h2>";
echo "<p>".$exercice['Exercice']['debutcontenu']."</p>";
//appel de l'exercice
exercicebegin("ergo_ru_exercices",$exercice['Exercice']['id_exs'],$exercice['Exercice']['id_l']);
//contenu final
echo "<p>".$exercice['Exercice']['fincontenu']."</p>";

//previous - next exercises
if($exercice['Exercice']['prev_ex']!=0){
			echo "<a class=\"exprecedent\" href=./" .$exercice['Exercice']['prev_ex'].">Exercice précédent</a>";
}
if($exercice['Exercice']['next_ex']>1){		
		echo "<a class=\"exsuivant\" href=./" .$exercice['Exercice']['next_ex'].">Exercice suivant</a>";
}		

source($exercice['Exercice']['source']);//source

	if($session->read('Auth.User.group_id')==3) {
?>				<li><?php echo $html->link(__('Edit', true), array('action'=>'edit', $exercice['Exercice']['id'])); ?> </li>
<?
	}
	?>
</div>

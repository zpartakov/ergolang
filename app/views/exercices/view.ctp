<div class="exercices view">
<h2><?php  __('Exercice');?></h2>
<a href="/websites/ergolang/cake/ergo_ru_exercices/add?id_exs=<?php echo $exercice['Exercice']['id_exs']; ?>&id_l=<?php echo $exercice['Exercice']['id_l']; ?>">Ajouter un item à cet exercice</a>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $exercice['Exercice']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Ergo Langue'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($exercice['ErgoLangue']['id'], array('controller'=> 'ergo_langues', 'action'=>'view', $exercice['ErgoLangue']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id Exs'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $exercice['Exercice']['id_exs']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id L'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $exercice['Exercice']['id_l']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Titreexercice'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $exercice['Exercice']['titreexercice']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Soustitre'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $exercice['Exercice']['soustitre']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Liengrammaire'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $exercice['Exercice']['liengrammaire']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Debutcontenu'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $exercice['Exercice']['debutcontenu']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Fincontenu'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $exercice['Exercice']['fincontenu']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Source'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $exercice['Exercice']['source']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Datemod'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $exercice['Exercice']['datemod']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('User'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($exercice['User']['id'], array('controller'=> 'users', 'action'=>'view', $exercice['User']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>

<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit Exercice', true), array('action'=>'edit', $exercice['Exercice']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Exercice', true), array('action'=>'delete', $exercice['Exercice']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $exercice['Exercice']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Exercices', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Exercice', true), array('action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Ergo Langues', true), array('controller'=> 'ergo_langues', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Ergo Langue', true), array('controller'=> 'ergo_langues', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Users', true), array('controller'=> 'users', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New User', true), array('controller'=> 'users', 'action'=>'add')); ?> </li>
	</ul>
</div>

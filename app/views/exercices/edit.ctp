<div class="exercices form">
<?php echo $form->create('Exercice');?>
	<fieldset>
 		<legend><?php __('Edit Exercice');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('ergo_langue_id');
		echo $form->input('id_exs');
		echo $form->input('id_l');
		echo $form->input('titreexercice');
		echo $form->input('soustitre');
		echo $form->input('liengrammaire');
		echo $form->input('debutcontenu');
		echo $form->input('fincontenu');
		echo $form->input('source');
		echo $form->input('datemod');
		echo $form->input('user_id');
				echo $form->input('prev_ex');
		echo $form->input('next_ex');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action'=>'delete', $form->value('Exercice.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Exercice.id'))); ?></li>
		<li><?php echo $html->link(__('List Exercices', true), array('action'=>'index'));?></li>
		<li><?php echo $html->link(__('List Ergo Langues', true), array('controller'=> 'ergo_langues', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Ergo Langue', true), array('controller'=> 'ergo_langues', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Users', true), array('controller'=> 'users', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New User', true), array('controller'=> 'users', 'action'=>'add')); ?> </li>
	</ul>
</div>

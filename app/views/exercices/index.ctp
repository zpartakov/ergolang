<?php
/**
* @version        $Id: index.ctp v1.0 19.03.2010 17:37:24 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

/*
function QueryIntoArray($query){
        settype($retval,"array");
        
$result= mysql_query($query);
        if(!$result){
print "Query Failed";
        }        
        for($i=0;$i<mysql_numrows($result);$i++){
                for($j=0;$j<mysql_num_fields($result);$j++){
                        $retval[$i][mysql_field_name($result,$j)] = mysql_result
($result,$i,mysql_field_name($result,$j));
                }//end inner loop
        }//end outer loop
return $retval;
}//end function
settype($myresult,"array");

$query = "SELECT * FROM ergo_langues ORDER BY lib";
$myresult = QueryIntoArray($query);
for($i=0;$i<count($myresult);$i++){
print $myresult[$i]["id"];
print $myresult[$i]["lib"];
}
/** 
$row = mysql_fetch_array($result) or die(mysql_error());
while($row = mysql_fetch_array($result)){
echo $row['id']. " - ". $row['lib'] ."<br>";
}
$row = mysql_fetch_array($result)

*/

$this->pageTitle = "Exercices";

?>
<div class="exercices index">
<h2><? echo $this->pageTitle ?></h2>
		<li><?php echo $html->link(__('Nouvel exercice', true), array('action'=>'add')); ?></li>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<!-- begin search form -->
 
 <table>
	 <tr>
		 <td>
 <div class="input">
<?php echo $form->create('Exercice', array('url' => array('action' => 'index'))); ?>
		<?php echo $form->input('q', array('style' => 'width: 250px;', 'label' => false)); ?>
		</div>
</td><td>
  <?php #echo $form->end('Chercher'); ?> 
<input type="button" class="vider" value="Vider" onClick="javascript:vide_recherche('ExerciceQ')" />
<input type="submit" class="chercher" value="Chercher" /> 

</div> 
</td>
</tr>
</table>
<!-- end search form -->
Syntaxe lien sur exercice: /websites/ergolang/cake/exercices/lexercice/
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('ergo_langue_id');?></th>
	<th><?php echo $paginator->sort('id_exs');?></th>
	<th><?php echo $paginator->sort('id_l');?></th>
	<th><?php echo $paginator->sort('titreexercice');?></th>
	<th><?php echo $paginator->sort('liengrammaire');?></th>
	<th><?php echo $paginator->sort('source');?></th>
	<th><?php echo $paginator->sort('datemod');?></th>
	<th><?php echo $paginator->sort('user_id');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($exercices as $exercice):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $exercice['Exercice']['id']; ?>
		</td>
		<td>
			<?php 
			#echo $exercice['ErgoLangue']['id']; 
			$idl=$exercice['ErgoLangue']['id'];
			if($idl==1){
				echo "ru";
			}elseif($idl==2){
				echo "en";
			}elseif($idl==3){
				echo "es";
			}elseif($idl==4){
				echo "de";
			}elseif($idl==5){
				echo "it";
			}elseif($idl==6){
				echo "fr";
			}
			
			?>
		</td>
		<td>
			<?php echo $exercice['Exercice']['id_exs']; ?>
		</td>
		<td>
			<?php echo $exercice['Exercice']['id_l']; ?>
		</td>
		<td>
			<?php echo $exercice['Exercice']['titreexercice']; ?>
		</td>

		<td>
			<?php 
			echo "<a href=\"/websites/ergolang/cake/ergo_ru_grammaires/vue/" .$exercice['Exercice']['liengrammaire'] ."\">" .eregi_replace(".*/","",$exercice['Exercice']['liengrammaire'])."</a>"; ?>
		</td>

		<td>
			<?php echo "<a href=\"".$exercice['Exercice']['source'] ."\">source</a>"; ?>
			<?php echo "<br>" .$exercice['Exercice']['source']; ?>
		</td>
		<td>
			<?php echo $exercice['Exercice']['datemod']; ?>
		</td>
		<td>
			<?php echo $html->link($exercice['User']['id'], array('controller'=> 'users', 'action'=>'view', $exercice['User']['id'])); ?>
		</td>
		<td class="actions">
		<a href="/websites/ergolang/cake/ergo_ru_exercices/add?id_exs=<?php echo $exercice['Exercice']['id_exs']; ?>&id_l=<?php echo $exercice['Exercice']['id_l']; ?>">Ajouter des questions à cet exercice</a><br>
		<a href="/websites/ergolang/cake/exercices/lexercice/<?php echo $exercice['Exercice']['id_l']; ?>">Vue</a><br>
		<?php echo $html->link(__('View', true), array('action'=>'view', $exercice['Exercice']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $exercice['Exercice']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $exercice['Exercice']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $exercice['Exercice']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Exercice', true), array('action'=>'add')); ?></li>
		<li><?php echo $html->link(__('List Ergo Langues', true), array('controller'=> 'ergo_langues', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Ergo Langue', true), array('controller'=> 'ergo_langues', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Users', true), array('controller'=> 'users', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New User', true), array('controller'=> 'users', 'action'=>'add')); ?> </li>
		
	</ul>
</div>

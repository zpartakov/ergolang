<?
/* fichier pour l'analyse
 * #todo: 
 * syntaxe php doc
 * ajout creative common partout
 * introduire stopwords, eg. the, and etc. dans un fichier séparé
 * ajax pour insérer les traductions sans confirmation, par click ou input + submit
 * textarea pour la traduction de tout le texte, à soumettre à un pair de langue maternelle anglophone
 * prévoir la possibilité de lire un texte phrase par phrase, et d'avoir :
 * un analyseur de textes 
 * -> explode phrases 
 * -> explode mots
 * 
 */
e($html->charset('iso-8859-1'));
e($html->css('test', null, array('media' => 'screen')));
e($javascript->link('prototype'));
e($javascript->link('scriptaculous.js?load=effects,controls'));

$id=$_GET['id'];
echo $id;
$latable="ergo_en_phrases";
$latableVoc="ergo_enfrs";
//include stopwords
require_once("stopwords.ctp");
//include stemmer
require_once("stemmer.ctp");

$sql="SELECT * FROM " .$latable ." WHERE id=" .$id;
$sqlQ=mysql_query($sql);
if(!$sqlQ){
	echo "error sql: " .mysql_error();
	exit;
}
$texte=mysql_result($sqlQ,0,'text');


############# BEGIN PAGINATION #############
	#pagination des textes longs
	$texteori=$texte;
	$ltexte=strlen($texteori);
	$debut=$_GET['debut'];
	$fin=$_GET['fin'];
	$maxsize=30;
	$npages=$ltexte/$maxsize;

	if(intval($npages)!=$npages){
		$npages=intval($npages)+1;
	}
	if(!$debut){
		$debut=0;
		$fin=$maxsize;
	}

	$pieces = explode(" ", $texte);
	$pieces = array_slice($pieces, $debut, $maxsize); 
	#$output = array_slice($input, 0, 3);   // returns "a", "b", and "c"
	$texte = implode(" ", $pieces);

	#if(strlen($texte)<$texteori) {
	#if(strlen($texte)>$maxsize) {
	echo "<br><div class=\"noteinfo\">Votre texte est trop long pour être analysé en une fois (" .$maxsize ." car. max); il va automatiquement être découpé en tranches</div>";
	#echo "<br>Debut: " .$debut ." - fin: " .$fin ."<br>"; //tests
	#}

	#the div is needed for ajax google translator
	echo "<div id=\"Source\">".nl2br($texte)."</div>";

	#pagination
	if(strlen($texteori)>600) {
		$suivantdebut=$fin;
		$suivantfin=$fin+$maxsize;
		$precedentdebut=$debut-$maxsize;
		$precedentfin=$precedentdebut+$maxsize;
			echo "<hr>";	
			if($debut!=0){
				echo "<a href=analyse?id=" .$_GET["id"] ."&debut=0&fin=" .$maxsize ." alt=\"première page\" title=\"première page\">";
				echo $html->image('bd_firstpage.png', array("alt"=>"première page","title"=>"première page"));
				echo "</a>";
				echo "&nbsp;|&nbsp;";
				echo "<a href=analyse?id=" .$_GET["id"] ."&debut=" .$precedentdebut ."&fin=" .$precedentfin ." alt=\"précédent\" title=\"précédent\">";
				echo $html->image('bd_prevpage.png', array("alt"=>"précédent","title"=>"précédent"));	
				echo "</a>";
				echo "&nbsp;|&nbsp;";
			}
			if($fin<$ltexte) {
			echo "<a href=analyse?id=" .$_GET["id"] ."&debut=" .$suivantdebut ."&fin=" .$suivantfin ." alt=\"suivant\" title=\"suivant\">";
				echo $html->image('bd_nextpage.png', array("alt"=>"suivant","title"=>"suivant"));	
			
			echo "</a>";
		}
	}
	/*

	a garder si jamais pour dernière page
	 * bd_lastpage.png
	 * 		;
	*/
############# END PAGINATION #############

?>
<!-- 
Google translator ajax see 
http://blogoscoped.com/archive/2008-03-20-n87.html
unused: 
http://code.google.com/intl/fr/apis/ajaxlanguage/
http://code.google.com/intl/fr/apis/ajaxlanguage/documentation/#Translate
http://code.google.com/intl/fr/apis/ajaxlanguage/documentation/#Examples
 -->
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
google.load("language", "1");
function Translate(sLang) {
	var text = (document.all) ? document.getElementById("Source").innerText : document.getElementById("Source").textContent;
	google.language.detect(text, function(result) {
		if (!result.error && result.language) {
			google.language.translate(text, result.language, sLang,
				function(result) {
					var translated = document.getElementById("Target");
					if (result.translation) {
						translated.innerHTML = "<p><strong>Machine translation:</strong></p>" + result.translation;
					}else{
						translated.innerHTML = "";
					}
				});
		}
	});
}
</script>
<form action="">
<div>
    <!-- possible to select a target language, to keep in mind for later -->
 <!--  <select id="Lang" name="Lang">
    <option value="">Select language...</option>

    <option value="ar">Arabic</option>
    <option value="zh-CN">Chinese(Simplified)</option>
    <option value="zh-TW">Chinese(Traditional)</option>
    <option value="nl">Dutch</option>
    <option value="fr">French</option>
    <option value="de">German</option>

    <option value="it">Italian</option>
    <option value="ja">Japanese</option>
    <option value="ko">Korean</option>
    <option value="pt-PT">Portuguese</option>
    <option value="ru">Russian</option>
    <option value="es">Spanish</option>

  </select>
     <input type="button" value="Traduire" onclick="Translate(this.form.Lang[this.form.Lang.selectedIndex].value);" />
  
   -->
   <input type="button" value="Traduire avec Google" onclick="Translate('fr');" />
</div>
</form>
<!-- #the div is needed for ajax google translator -->

<div id="Target" style="background-color: lightblue"></div>

<?

$pieces = explode(" ", $texte);
#todo: fonction pour normaliser les mots (réductions, stopwords, virer , . etc.)
/*
 pistes: 

http://drupal.org/node/85361 

http://snowball.tartarus.org/algorithms/russian/stemmer.html
 */
$touslesmots="";
$j=0;
for($i=0;$i<count($pieces);$i++){
	$mot=$pieces[$i];
	$mot=normaliserMot($mot);
	if(strlen($mot)>1) {
		$j++;
		if(!eregi(";$mot;",$touslesmots)) { //on affiche que si pas un doublon
  		$touslesmots.= ";" .$mot .";"; //on ajoute le mot dans un tableau pour pas avoir de doublons
  		//recherche du mot dans le vocabulaire
  		$all= "SELECT * FROM `" .$latableVoc ."` WHERE `foreign` LIKE '$mot%' LIMIT 0,8";
  		
  		#echo "<pre>" .$all ."</pre>"; //tests
  		$result = mysql_query($all);
  		if(mysql_num_rows($result)>0) {
  			$k=0;
  			$francais="";
  			while($k<mysql_num_rows($result)){;
  			  			#$francais.="<input type=\"checkbox\" onClick=\"alert('" .mysql_result($result,$k,'local') ."')\">&nbsp;\n";
  			$francais.=mysql_result($result,$k,'local') ."<br>\n\n";
  			$k++;
  			}
  		} else {
  			$francais="";
  		}
  		//affichage du résultat
 		echo "<hr>Mot " .$j  ." -- <strong>".$mot ."</strong><br>";
 					if(eregi(";$mot;",$stopwords)){
		$mot="";
		echo "<font color=\"grey\">stopword!</font><br>";
	} else {
 
  		if(strlen($francais)>1) {
  			echo " #hits: " .mysql_num_rows($result)."<br>";
  			echo "<em>francais:</em><br>" .$francais;
  		}
	}
		}
	}
	#echo "<br><form action=\"inseremot\"><input type=\"text\" name=\"trad" .$mot ."\"><input type=\"submit\"></form>";
}

echo "<hr>";

//todo: chercher dans voc, proposer d'insérer si n'existe pas, suggérer traductions
  
//todo: chercher dans voc, proposer d'insérer si n'existe pas, suggérer traductions
#UTFChunk($texte);
#echo UTFChunk($texte);
#$array=splitter($texte);

$array=str_split($texte,250);
$size=count($array);
#echo "test: " .$array ."<br>size: " .$size; //tests

	for ($i=0; $i <= $size; $i++){
	echo $array[$i] . "<hr>";

}
#pagination
if(strlen($texteori)>600) {
	$suivantdebut=$fin;
	$suivantfin=$fin+$maxsize;
	$precedentdebut=$debut-$maxsize;
	$precedentfin=$precedentdebut+$maxsize;
		echo "<hr>";	
		if($debut!=0){
			echo "<a href=analyse?id=" .$_GET["id"] ."&debut=0&fin=" .$maxsize ." alt=\"première page\" title=\"première page\">";
			echo $html->image('bd_firstpage.png', array("alt"=>"première page","title"=>"première page"));
			echo "</a>";
			echo "&nbsp;|&nbsp;";
			echo "<a href=analyse?id=" .$_GET["id"] ."&debut=" .$precedentdebut ."&fin=" .$precedentfin ." alt=\"précédent\" title=\"précédent\">";
			echo $html->image('bd_prevpage.png', array("alt"=>"précédent","title"=>"précédent"));	
			echo "</a>";
			echo "&nbsp;|&nbsp;";
		}
		if($fin<$ltexte) {
		echo "<a href=analyse?id=" .$_GET["id"] ."&debut=" .$suivantdebut ."&fin=" .$suivantfin ." alt=\"suivant\" title=\"suivant\">";
			echo $html->image('bd_nextpage.png', array("alt"=>"suivant","title"=>"suivant"));	
		
		echo "</a>";
	}
}


?>

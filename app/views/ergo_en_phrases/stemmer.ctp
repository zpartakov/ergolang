<?php
/**
* @version        $Id: stemmer.ctp v1.0 30.12.2009 12:36:37 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
//very light stemmer for ergolang

#nettoyer les mots pour le vocabulaire
function normaliserMot($mot) {
	$mot=trim($mot);
	$mot=ereg_replace(",","",$mot);
	$mot=str_replace(",","",$mot);
	$mot=ereg_replace("\.*","",$mot);
	$mot=str_replace("?","",$mot);
	$mot=str_replace("the","",$mot);


//english stemmer todo
/*$mot=ereg_replace("авьте$","ав",$mot);*/
	return $mot;
}

?>

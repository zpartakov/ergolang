<div class="ergoEnPhrases view">
<h2><?php  __('ErgoEnPhrase');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoEnPhrase['ErgoEnPhrase']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Titre'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoEnPhrase['ErgoEnPhrase']['titre']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Url'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoEnPhrase['ErgoEnPhrase']['url']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Text'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoEnPhrase['ErgoEnPhrase']['text']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Date'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoEnPhrase['ErgoEnPhrase']['date']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modif'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoEnPhrase['ErgoEnPhrase']['modif']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit ErgoEnPhrase', true), array('action'=>'edit', $ergoEnPhrase['ErgoEnPhrase']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete ErgoEnPhrase', true), array('action'=>'delete', $ergoEnPhrase['ErgoEnPhrase']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoEnPhrase['ErgoEnPhrase']['id'])); ?> </li>
		<li><?php echo $html->link(__('List ErgoEnPhrases', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New ErgoEnPhrase', true), array('action'=>'add')); ?> </li>
	</ul>
</div>

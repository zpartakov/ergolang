<style>
/*
.source {
position: abolute;
top: 50px;
left: 10px;
}
.target {
position: abolute;
top: 50px;
left: 210px;
}
*/
</style>
<div class="ergoEnPhrases view">
<h2><?php  __('ErgoEnPhrase');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoEnPhrase['ErgoEnPhrase']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Titre'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoEnPhrase['ErgoEnPhrase']['titre']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Url'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoEnPhrase['ErgoEnPhrase']['url']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Text'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
<table><tr><td>			<?php 
						echo "<div class=\"source\" id=\"Source\">";
			
			echo $ergoEnPhrase['ErgoEnPhrase']['text']; 
						echo "</div>";
			
			?>
			<!-- 
Google translator ajax see 
http://blogoscoped.com/archive/2008-03-20-n87.html
unused: 
http://code.google.com/intl/fr/apis/ajaxlanguage/
http://code.google.com/intl/fr/apis/ajaxlanguage/documentation/#Translate
http://code.google.com/intl/fr/apis/ajaxlanguage/documentation/#Examples
 -->
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
google.load("language", "1");
function Translate(sLang) {
	var text = (document.all) ? document.getElementById("Source").innerText : document.getElementById("Source").textContent;
	google.language.detect(text, function(result) {
		if (!result.error && result.language) {
			google.language.translate(text, result.language, sLang,
				function(result) {
					var translated = document.getElementById("Target");
					if (result.translation) {
						translated.innerHTML = "<p><strong>Machine translation:</strong></p>" + result.translation;
					}else{
						translated.innerHTML = "";
					}
				});
		}
	});
}
</script>
<form action="">
			 <input type="button" value="Traduire" onclick="Translate('fr');" />
			 </form>
</td></tr><tr><td><div class=\"target\" id="Target">&nbsp;</div>
			</td></tr></table>&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Date'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoEnPhrase['ErgoEnPhrase']['date']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modif'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoEnPhrase['ErgoEnPhrase']['modif']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
			<li><?php 
			#echo "<a href=\"ergoEnPhrases/analyse?id=" .$ergoEnPhrase['ErgoEnPhrase']['id'] ."\">Analyse</a>"; 
			echo "<a href=\"../analyse?id=" .$ergoEnPhrase['ErgoEnPhrase']['id'] ."\">Analyse</a>"; 
			?></li>
	
		<li><?php echo $html->link(__('Edit ErgoEnPhrase', true), array('action'=>'edit', $ergoEnPhrase['ErgoEnPhrase']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete ErgoEnPhrase', true), array('action'=>'delete', $ergoEnPhrase['ErgoEnPhrase']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoEnPhrase['ErgoEnPhrase']['id'])); ?> </li>
		<li><?php echo $html->link(__('List ErgoEnPhrases', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New ErgoEnPhrase', true), array('action'=>'add')); ?> </li>
	</ul>
</div>

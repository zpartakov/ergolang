<div class="ergoEnPhrases form">
<?php echo $form->create('ErgoEnPhrase');?>
	<fieldset>
 		<legend><?php __('Edit ErgoEnPhrase');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('titre');
		echo $form->input('url');
		echo $form->input('text');
		echo $form->input('date');
		echo $form->input('modif');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action'=>'delete', $form->value('ErgoEnPhrase.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('ErgoEnPhrase.id'))); ?></li>
		<li><?php echo $html->link(__('List ErgoEnPhrases', true), array('action'=>'index'));?></li>
	</ul>
</div>

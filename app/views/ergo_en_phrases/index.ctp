<?php
/**
* @version        $Id: index.ctp v1.0 29.01.2010 22:56:07 CET $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
### def some constants
if($_SERVER["HTTP_HOST"]=="localhost"||$_SERVER["HTTP_HOST"]=="129.194.18.197") { //serveur de développement - tests
$CHEMIN="/ergolang/cake";
} elseif ($_SERVER["HTTP_HOST"]=="oblomov.info"){ //serveur de prod
$CHEMIN="/websites/ergolang/cake";
}
?><div class="ergoEnPhrases index">
<h2><?php __('ErgoEnPhrases');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('titre');?></th>
	<th><?php echo $paginator->sort('url');?></th>
	<th><?php echo $paginator->sort('text');?></th>
	<!--<th><?php echo $paginator->sort('date');?></th>-->
	<th><?php echo $paginator->sort('modif');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($ergoEnPhrases as $ergoEnPhrase):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $ergoEnPhrase['ErgoEnPhrase']['id']; ?>
		</td>
		<td>
			<?php echo $ergoEnPhrase['ErgoEnPhrase']['titre']; ?>
		</td>
		<td>
			<?php 
			#echo $ergoEnPhrase['ErgoEnPhrase']['url']; 

		if($ergoEnPhrase['ErgoEnPhrase']['url']){
		echo "<a href=\"" .$ergoEnPhrase['ErgoEnPhrase']['url'] ."\" target=\"_blank\">";
echo $html->image('urlextern.png', array("alt"=>"URL Source","title"=>"URL Source"));
echo "</a>";
}
		?>
					</td>
		<td>
			<?php 
			echo substr($ergoEnPhrase['ErgoEnPhrase']['text'],0,100)
			#echo $ergoEnPhrase['ErgoEnPhrase']['text']; 
			?>
		</td>
		<!--<td>
			<?php echo $ergoEnPhrase['ErgoEnPhrase']['date']; ?>
		</td>-->
		<td>
			<?php echo $ergoEnPhrase['ErgoEnPhrase']['modif']; ?>
		</td>
		<td class="actions">
			<?php 
				
		echo "<a href=\"" .$CHEMIN ."/ergoEnPhrases/analyse?id=" .$ergoEnPhrase['ErgoEnPhrase']['id'] ."&debut=0&fin=0\">";
echo $html->image('b_index.png', array("alt"=>"Analyser","title"=>"Analyser"));
echo "</a>";
echo "&nbsp;";
	
echo "<a href=\"" .$CHEMIN ."/ergoEnPhrases/view/" .$ergoEnPhrase['ErgoEnPhrase']['id'] ."\">";
echo $html->image('b_search.png', array("alt"=>"Voir","title"=>"Voir"));
echo "</a>";
echo "&nbsp;";
echo "<a href=\"".$CHEMIN ."/ergoEnPhrases/edit/" .$ergoEnPhrase['ErgoEnPhrase']['id'] ."\">";
echo $html->image('b_edit.png', array("alt"=>"Modifier","title"=>"Modifier"));
echo "</a>";
echo "<a onclick=\"javascript:return confirm('Confirmer la suppression ?')\" href=\"" .$CHEMIN ."/ergoEnPhrases/delete/" .$ergoEnPhrase['ErgoEnPhrase']['id'] ."\">";
echo $html->image('b_drop.png', array("alt"=>"Effacer","title"=>"Effacer"));
echo "</a>";
?>
			
			
			
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New ErgoEnPhrase', true), array('action'=>'add')); ?></li>
	</ul>
</div>

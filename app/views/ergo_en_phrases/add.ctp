<div class="ergoEnPhrases form">
<?php echo $form->create('ErgoEnPhrase');?>
	<fieldset>
 		<legend><?php __('Add ErgoEnPhrase');?></legend>
	<?php
		echo $form->input('titre');
		echo $form->input('url');
		echo $form->input('text');
		echo $form->input('date');
		echo $form->input('modif');
	?>
		Texte privé?&nbsp;
	<input type="checkbox" id="texteprive" onChange="javascript:rendprive('ErgoEnPhraseTitre');">
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List ErgoEnPhrases', true), array('action'=>'index'));?></li>
	</ul>
</div>

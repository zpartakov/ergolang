<div class="ergoEnfrs view">
<h2><?php  __('ErgoEnfr');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoEnfr['ErgoEnfr']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Foreign'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoEnfr['ErgoEnfr']['foreign']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Local'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoEnfr['ErgoEnfr']['local']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit ErgoEnfr', true), array('action'=>'edit', $ergoEnfr['ErgoEnfr']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete ErgoEnfr', true), array('action'=>'delete', $ergoEnfr['ErgoEnfr']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoEnfr['ErgoEnfr']['id'])); ?> </li>
		<li><?php echo $html->link(__('List ErgoEnfrs', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New ErgoEnfr', true), array('action'=>'add')); ?> </li>
	</ul>
</div>

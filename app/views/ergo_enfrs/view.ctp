<?php
/**
* @version        $Id: view.ctp v1.0 13.10.2010 10:43:33 CEST $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Dictionnaire Anglais <=> Français: Vue'; 

?>
<div class="ergoEnfrs view">
<h1><? echo $this->pageTitle; ?></h1>

	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoEnfr['ErgoEnfr']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Foreign'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoEnfr['ErgoEnfr']['foreign']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Local'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoEnfr['ErgoEnfr']['local']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?	/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
	?>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit ErgoEnfr', true), array('action'=>'edit', $ergoEnfr['ErgoEnfr']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete ErgoEnfr', true), array('action'=>'delete', $ergoEnfr['ErgoEnfr']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoEnfr['ErgoEnfr']['id'])); ?> </li>
		<li><?php echo $html->link(__('List ErgoEnfrs', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New ErgoEnfr', true), array('action'=>'add')); ?> </li>
	</ul>
</div>
<?
}
?>

<div class="ergoEnfrs form">
<?php echo $form->create('ErgoEnfr');?>
	<fieldset>
 		<legend><?php __('Edit ErgoEnfr');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('foreign');
		echo $form->input('local');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action'=>'delete', $form->value('ErgoEnfr.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('ErgoEnfr.id'))); ?></li>
		<li><?php echo $html->link(__('List ErgoEnfrs', true), array('action'=>'index'));?></li>
	</ul>
</div>

<div class="ergoEnfrs index">
<h2><?php __('ErgoEnfrs');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('foreign');?></th>
	<th><?php echo $paginator->sort('local');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($ergoEnfrs as $ergoEnfr):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $ergoEnfr['ErgoEnfr']['id']; ?>
		</td>
		<td>
			<?php echo $ergoEnfr['ErgoEnfr']['foreign']; ?>
		</td>
		<td>
			<?php echo $ergoEnfr['ErgoEnfr']['local']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action'=>'view', $ergoEnfr['ErgoEnfr']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $ergoEnfr['ErgoEnfr']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $ergoEnfr['ErgoEnfr']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoEnfr['ErgoEnfr']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New ErgoEnfr', true), array('action'=>'add')); ?></li>
	</ul>
</div>

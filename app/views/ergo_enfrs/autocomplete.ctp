<?php
/**
* @version        $Id: autocomplete.ctp v1.0 22.02.2010 16:45:29 CET $
* www.unige.ch
* webmaster@unige.ch

*/
?>    
<ul>
<?php foreach($lesbatiments as $lebatiment): ?> 
     <li><?php 
     e($text->highlight(
     	$lebatiment['ErgoEnfr']['local'],
     	$recherche,
     	'<strong>\1</strong>'
     )); ?></li>
<?php endforeach; ?>
</ul>


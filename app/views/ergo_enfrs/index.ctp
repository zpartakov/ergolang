<?php
/**
* @version        $Id: index.ctp v1.0 01.04.2010 12:27:37 CEST $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'English <=> French dictionnary'; 
$CHEMIN="/websites/ergolang/cake";

?>

<div class="ErgoEnfrs index">
<h1><? echo $this->pageTitle; ?></h1>
<!-- begin search form -->
 <table>
	 <tr>
		 <td>
 <div class="input">
<?php echo $form->create('ErgoEnfr', array('url' => array('action' => 'index'))); ?>
		<?php #echo $form->input('q', array('style' => 'width: 250px;', 'label' => false, 'size' => '80')); ?>
		<?php echo $form->input('q', array('label' => false, 'size' => '50', 'class'=>'txttosearch')); ?>
		</div>
</td><td>
<input type="button" class="vider" value="Vider" onClick="javascript:vide_recherche('ErgoEnfrQ')" />
<input type="submit" class="chercher" value="Chercher" /> 
</div> 
</td>
</tr>
</table>
<!-- end search form -->

   <?php
/*
   ###### AJAX SEARCH FORM ####
   e($ajax->autoComplete(
		'ErgoEnfr.q',
		'/ergoEnfrs/autocomplete',
		array(
			'minChars' => 3,
			'indicator' => 'ajaxloader',
			'style' => 'width: 250px;'
		)
	)); 
	

	?> 

	<div id="ajaxloader" style="display:none;">
		Chargement...

	</div>
	<?
	* */
    #echo $form->input("q", array('label' => 'Search for'));
   
    ?>
    
   
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New word', true), array('action'=>'add')); ?></li>
	</ul>
</div>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<!--<th><?php echo $paginator->sort('id');?></th>-->
	<th><?php echo "<div id=\"Source\">";
	echo $paginator->sort('foreign');?></div></th>
	<th><?php echo $paginator->sort('local');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($ergoEnfrs as $ergoEnfr):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<!--<td>
			<?php echo $ergoEnfr['ErgoEnfr']['id']; ?>
		</td>-->
		<td>
			<?php echo $ergoEnfr['ErgoEnfr']['foreign']; ?>
		</td>
		<td>
			<?php echo $ergoEnfr['ErgoEnfr']['local']; ?>
		</td>
		<td class="actions">

<?php 

echo "<a href=\"" .$CHEMIN ."/ergo_enfrs/view/" .$ergoEnfr['ErgoEnfr']['id'] ."\">";
echo $html->image('b_search.png', array("alt"=>"Voir","title"=>"Voir"));
echo "</a>";

	/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
echo "&nbsp;";
echo "<a href=\"".$CHEMIN ."/ergo_enfrs/edit/" .$ergoEnfr['ErgoEnfr']['id'] ."\">";
echo $html->image('b_edit.png', array("alt"=>"Modifier","title"=>"Modifier"));
echo "</a>";
echo "<a onclick=\"javascript:return confirm('Confirmer la suppression ?')\" href=\"" .$CHEMIN ."/ergo_enfrs/delete/" .$ergoEnfr['ErgoEnfr']['id'] ."\">";
echo $html->image('b_drop.png', array("alt"=>"Effacer","title"=>"Effacer"));
echo "</a>";
}
?>
		</td>
		
		
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New word', true), array('action'=>'add')); ?></li>
	</ul>
</div>

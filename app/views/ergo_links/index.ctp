<?php
/**
* @version        $Id: index.ctp v1.0 03.03.2010 05:18:45 CET $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

$this->pageTitle = 'Liens Эrgolang'; 

?>

<div class="ergoLinks index">
<h1><? echo $this->pageTitle; ?></h1>
<?
	/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
	?>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Nouveau lien', true), array('action'=>'add')); ?></li>
	</ul>
</div>
<?
}

#### news ######
echo '<a href="/websites/ergolang/cake/ergo_links/index/page:1/sort:date/direction:desc" alt="Nouveautés" title="Nouveautés">'.$html->image('new.jpg', array("alt"=>"Nouveautés","title"=>"Nouveautés","class"=>"image")).'</a>';
?>

<!-- begin search form -->

 <table>
	 <tr>
		 <td>
 <div class="input">
<?php echo $form->create('ErgoLink', array('url' => array('action' => 'index'))); ?>
		<?php echo $form->input('q', array('style' => 'width: 200px;', 'label' => false)); ?>
		</td><td>
<?		#link category
echo '<select name="data[ErgoLink][category]">';
echo "<option value=''>-- catégorie --</option>";

$sqlCat="SELECT * FROM tags ORDER BY name";
$sql=mysql_query($sqlCat);
$i=0;
while($i<mysql_num_rows($sql)){
	echo '<option value="' .mysql_result($sql,$i,'id') .'">' .mysql_result($sql,$i,'name') .'</option>';
	$i++;
}
echo '</select>';
#langue

echo '		</td><td>
<select name="data[ErgoLink][lang]">';
echo "<option value=''>-- langue --</option>";
$sql="SELECT * FROM ergo_langues ORDER BY lib";

$sql=mysql_query($sql);
$i=0;
while($i<mysql_num_rows($sql)){
	echo '<option value="' .mysql_result($sql,$i,'id') .'">' .mysql_result($sql,$i,'lib') .'</option>';
	$i++;
}
echo '</select>';
?>
</div>
</td><td>
<input type="button" class="vider" value="Vider" onClick="javascript:vide_recherche('ErgoLinkQ')" />
  <input type="submit" class="chercher" value="Chercher" /> 

</div> 
</td>
</tr>
</table>
</form>
<!-- end search form -->

<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<!--	<th><?php echo $paginator->sort('id');?></th>
<th><?php echo $paginator->sort('user_id');?></th>-->
	<th><?php echo $paginator->sort('Langue','ergo_langue_id');?></th>
	<th><?php echo $paginator->sort('url');?></th>
	<th><?php echo $paginator->sort('Libellé','lib');?></th>
	<th><?php echo $paginator->sort('date');?></th>
	<th><?php echo $paginator->sort('rem');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($ergoLinks as $ergoLink):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<!--<td>
			<?php echo $ergoLink['ErgoLink']['id']; ?>
		</td>
		<td>
			<?php echo $html->link($ergoLink['User']['id'], array('controller'=> 'users', 'action'=>'view', $ergoLink['User']['id'])); ?>
		</td>-->
		<td>
			<?php echo $html->link($ergoLink['ErgoLangue']['lib'], array('controller'=> 'ergo_langues', 'action'=>'view', $ergoLink['ErgoLangue']['id'])); ?>
		</td>
		<td>
			<?php 
			
			#echo ; 
			
		echo "<a href=\"" .$ergoLink['ErgoLink']['url'] ."\" title=\"voir le site\" target=\"_blank\">".$ergoLink['ErgoLink']['url'] ."</a>"; 
		?>
		</td>
		<td>
			<?php echo $ergoLink['ErgoLink']['lib']; ?>
		</td>
		<td>
			<?php 
			#echo $ergoLink['ErgoLink']['date']; 
			$timestamp = strtotime($ergoLink['ErgoLink']['date']);
e(strftime("%d&nbsp;%b&nbsp;%Y,&nbsp;%Hh%M", $timestamp));?>
		</td>
		<td>
			<?php 
			#echo $ergoLink['ErgoLink']['rem']; 
				$url=$ergoLink['ErgoLink']['rem'];
			if(strlen($url)>70) {
				$url= first_words($url,6);
			}
					/*$rem=mb_substr($ergoLink['ErgoLink']['rem'],0,50);
					$rem=eregi_replace(" .*$","",$rem);

			echo $rem ."...";*/
			echo $url;?>
		</td>
		<td class="actions">
		<?php 
echo "<a href=\"" .CHEMIN ."/ergo_links/view/" .$ergoLink['ErgoLink']['id'] ."\">";
echo $html->image('b_search.png', array("alt"=>"Voir","title"=>"Voir"));
echo "</a>";
	/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
echo "&nbsp;";
echo "<a href=\"".CHEMIN ."/ergo_links/edit/" .$ergoLink['ErgoLink']['id'] ."\">";
echo $html->image('b_edit.png', array("alt"=>"Modifier","title"=>"Modifier"));
echo "</a>";
echo "<a onclick=\"javascript:return confirm('Confirmer la suppression ?')\" href=\"" .CHEMIN ."/ergo_links/delete/" .$ergoLink['ErgoLink']['id'] ."\">";
echo $html->image('b_drop.png', array("alt"=>"Effacer","title"=>"Effacer"));
echo "</a>";
}
?>

		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<?
	/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
	?>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Nouveau lien', true), array('action'=>'add')); ?></li>
	</ul>
</div>
<?
}
?>

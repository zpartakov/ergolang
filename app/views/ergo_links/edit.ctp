<?php
/**
* @version        $Id: edit.ctp v1.0 08.06.2010 06:15:09 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Modifier lien Эrgolang'; 

?>

<div class="ergoLinks form">
<?php echo $form->create('ErgoLink');?>
	<fieldset>
 		<legend><?php echo $this->pageTitle;?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('user_id',array('label'=>'Utilisateur'));
		echo $form->input('ergo_langue_id',array('label'=>'Langue'));
		echo $form->input('url', array('size'=>'80'));
		echo $form->input('lib', array('type'=>'textarea','label'=>'Libellé'));
		echo $form->input('date');
		echo $form->input('rem',array('label'=>'Remarque'));
		echo $form->input('Tag');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action'=>'delete', $form->value('ErgoLink.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('ErgoLink.id'))); ?></li>
		<li><?php echo $html->link(__('List Tags', true), array('controller'=> 'tags', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Tag', true), array('controller'=> 'tags', 'action'=>'add')); ?> </li>
	</ul>
</div>

<?php
/**
* @version        $Id: tags.ctp v1.0 29.03.2010 23:01:39 CEST $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$CHEMIN="/websites/ergolang/cake";
$this->pageTitle = 'Liens Эrgolang'; 

?>

<div class="ergoLinks index">
<h1><? echo $this->pageTitle; ?></h1>
<!-- begin search form -->

 <table>
	 <tr>
		 <td>
 <div class="input">
<?php echo $form->create('ErgoLink', array('url' => array('action' => 'index'))); ?>
		<?php echo $form->input('q', array('style' => 'width: 200px;', 'label' => false)); ?>
		
<?		#link category
echo '<select name="data[ErgoLink][category]">';
echo "<option value=''>-- catégorie --</option>";

$sqlCat="SELECT * FROM tags ORDER BY name";
$sql=mysql_query($sqlCat);
$i=0;
while($i<mysql_num_rows($sql)){
	echo '<option value="' .mysql_result($sql,$i,'id') .'">' .mysql_result($sql,$i,'name') .'</option>';
	$i++;
}
echo '</select>';
#langue

echo '<select name="data[ErgoLink][lang]">';
echo "<option value=''>-- langue --</option>";
$sql="SELECT * FROM ergo_langues ORDER BY lib";

$sql=mysql_query($sql);
$i=0;
while($i<mysql_num_rows($sql)){
	echo '<option value="' .mysql_result($sql,$i,'id') .'">' .mysql_result($sql,$i,'lib') .'</option>';
	$i++;
}
echo '</select>';
?>
</div>
</td><td>
<input type="button" value="Vider" onClick="javascript:vide_recherche('ErgoLinkQ')" />
<?php

   #echo $form->button('Annuler', array('type'=>'reset')); 
  echo $form->end('Chercher'); ?> 
</div> 
</td>
</tr>
</table>
<!-- end search form -->
<?
#echo phpinfo();
$q1=$_REQUEST["url"];
if(!ereg("/lang:",$q1)){
	$category="";
$q1= ereg_replace("^.*:","", $q1);
} else {
$category=ereg_replace("^.*:","", $q1);
$q1= ereg_replace("/lang:.*$","", $q1);
	$q1= ereg_replace("^.*:","", $q1);

}

#echo "<br>Q: " .$q1 ." - Category: " .$category ."<br>"; //tests

	$sql="SELECT ErgoLink.*, Tag.name FROM `ergo_links` AS `ErgoLink` 
						LEFT JOIN ergo_links_tags AS pt ON ErgoLink.id = pt.ergo_link_id 
						LEFT JOIN tags AS Tag ON Tag.id = pt.tag_id 
						WHERE pt.tag_id = " .$q1;
						if(strlen($category)>1){
							$sql.=" AND ergo_langue_id = " .$category;
						}
			/*SELECT ErgoLink.*, Tag.name FROM `ergo_links` AS `ErgoLink` LEFT JOIN ergo_links_tags AS pt ON ErgoLink.id = pt.ergo_link_id LEFT JOIN tags AS Tag ON Tag.id = pt.tag_id WHERE pt.tag_id = 4 AND ergo_langue_id=4			*/
						
#echo $sql; //tests
						
	$sqlt="SELECT COUNT(*) AS total FROM `ergo_links` AS `ErgoLink` 
						LEFT JOIN ergo_links_tags AS pt ON ErgoLink.id = pt.ergo_link_id 
						LEFT JOIN tags AS Tag ON Tag.id = pt.tag_id 
						WHERE pt.tag_id = " .$q1;
						if(strlen($category)>1){
							$sqlt.=" AND ergo_langue_id = " .$category;
						}
						
						#echo $sqlt;
		
		
		$messagesParPage=25; //Nous allons afficher 25 messages par page.

//Une connexion SQL doit être ouverte avant cette ligne...
$retour_total=mysql_query($sqlt); //Nous récupérons le contenu de la requête dans $retour_total
$donnees_total=mysql_fetch_assoc($retour_total); //On range retour sous la forme d'un tableau.
$total=$donnees_total['total']; //On récupère le total pour le placer dans la variable $total.
#echo "total: " .$total;
//Nous allons maintenant compter le nombre de pages.
$nombreDePages=ceil($total/$messagesParPage);

if(isset($_GET['page'])) // Si la variable $_GET['page'] existe...
{
     $pageActuelle=intval($_GET['page']);
     
     if($pageActuelle>$nombreDePages) // Si la valeur de $pageActuelle (le numéro de la page) est plus grande que $nombreDePages...
     {
          $pageActuelle=$nombreDePages;
     }
}
else // Sinon
{
     $pageActuelle=1; // La page actuelle est la n°1    
}

$premiereEntree=($pageActuelle-1)*$messagesParPage; // On calcul la première entrée à lire

// La requête sql pour récupérer les messages de la page actuelle.
$sqlactu=$sql .' ORDER BY ErgoLink.id DESC LIMIT  '.$premiereEntree.', '.$messagesParPage.'';
#echo "<br>Requete sql: " .$sqlactu;
$retour_messages=mysql_query($sqlactu);
echo "<table>";
?>
<tr>
	<!--	<th><a href="/websites/ergolang/cake/ergo_links/index/page:1/sort:id/direction:asc">Id</a></th>
<th><a href="/websites/ergolang/cake/ergo_links/index/page:1/sort:user_id/direction:asc">User</a></th>-->
	<th>Tag</th>

	<th>Url</th>
	<th>Libellé</th>
	<th>Date</th>
	<th class="actions">Actions</th>
</tr>
	
<?
$i=0;
while($donnees_messages=mysql_fetch_assoc($retour_messages)) // On lit les entrées une à une grâce à une boucle
{
	$i++;
	    if ($i%2) {
			$classtr="altrow";
			} else {
				 $classtr="";
			 }
echo '<tr class="' .$classtr .'">';
echo '
                     <td>'.stripslashes($donnees_messages['name']).'</td>
                      <td><a href="' .$donnees_messages['url'] .'" target="_blank">' .$donnees_messages['url'] .'</a></td>
                     <td>'.stripslashes($donnees_messages['lib']).'</td>';
	$timestamp = strtotime($donnees_messages['date']);
	echo "<td>";
echo strftime("%d&nbsp;%b&nbsp;%Y,&nbsp;%Hh%M", $timestamp);
	echo "</td>";
	
	
	echo "<td><a href=\"" .$CHEMIN ."/ergo_links/view/" .$donnees_messages['id'] ."\">";
echo $html->image('b_search.png', array("alt"=>"Voir","title"=>"Voir"));
echo "</a>";
	/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
echo "&nbsp;";
echo "<a href=\"".$CHEMIN ."/ergo_links/edit/" .$donnees_messages['id'] ."\">";
echo $html->image('b_edit.png', array("alt"=>"Modifier","title"=>"Modifier"));
echo "</a>";
echo "<a onclick=\"javascript:return confirm('Confirmer la suppression ?')\" href=\"" .$CHEMIN ."/ergo_links/delete/" .$donnees_messages['id'] ."\">";
echo $html->image('b_drop.png', array("alt"=>"Effacer","title"=>"Effacer"));
echo "</a>";
}
	echo "</td>";
	

echo '                </tr>
';
}
echo "</table>";
echo '<p align="center">Page : '; //Pour l'affichage, on centre la liste des pages
for($i=1; $i<=$nombreDePages; $i++) //On fait notre boucle
{
     //On va faire notre condition
     if($i==$pageActuelle) //Si il s'agit de la page actuelle...
     {
         echo ' [ '.$i.' ] '; 
     }	
     else //Sinon...
     {
          echo ' <a href="?page='.$i.'">'.$i.'</a> ';
     }
}
echo '</p>';

		
		
		
		
		
		
		
?>

<?php
/**
* @version        $Id: view.ctp v1.0 05.07.2010 16:21:23 CEST $
* @package        Эrgolang
* @copyright    Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Lien Эrgolang - Détail'; 

?>

<div class="ergoLinks view">
<h1><? echo $this->pageTitle; ?></h1>

	<dl><?php $i = 0; $class = ' class="altrow"';?>
	
	<?
	/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
	?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoLink['ErgoLink']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('User'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($ergoLink['User']['username'], array('controller'=> 'users', 'action'=>'view', $ergoLink['User']['id'])); ?>
			&nbsp;
		</dd>
		
		<?
}
?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Langue'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($ergoLink['ErgoLangue']['lib'], array('controller'=> 'ergo_langues', 'action'=>'view', $ergoLink['ErgoLangue']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Url'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php urlise($ergoLink['ErgoLink']['url']); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Lib'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoLink['ErgoLink']['lib']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Date'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoLink['ErgoLink']['date']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Rem'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php urlise($ergoLink['ErgoLink']['rem']); ?>
			<?php #echo $ergoLink['ErgoLink']['rem']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
	<?
	/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
	?>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit ErgoLink', true), array('action'=>'edit', $ergoLink['ErgoLink']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete ErgoLink', true), array('action'=>'delete', $ergoLink['ErgoLink']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoLink['ErgoLink']['id'])); ?> </li>
		<li><?php echo $html->link(__('List ErgoLinks', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New ErgoLink', true), array('action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Users', true), array('controller'=> 'users', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New User', true), array('controller'=> 'users', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Ergo Langues', true), array('controller'=> 'ergo_langues', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Ergo Langue', true), array('controller'=> 'ergo_langues', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Tags', true), array('controller'=> 'tags', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Tag', true), array('controller'=> 'tags', 'action'=>'add')); ?> </li>
	</ul>
</div>

<div class="related">
	<h3><?php __('Related Tags');?></h3>
	<?php if (!empty($ergoLink['Tag'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Longname'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($ergoLink['Tag'] as $tag):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $tag['id'];?></td>
			<td><?php echo $tag['name'];?></td>
			<td><?php echo $tag['longname'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller'=> 'tags', 'action'=>'view', $tag['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller'=> 'tags', 'action'=>'edit', $tag['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller'=> 'tags', 'action'=>'delete', $tag['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $tag['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $html->link(__('New Tag', true), array('controller'=> 'tags', 'action'=>'add'));?> </li>
		</ul>
	</div>
</div>
<?
}
?>

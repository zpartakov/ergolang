<?php
/**
* @version        $Id: add.ctp v1.0 08.06.2010 06:18:17 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
* 
*/
$this->pageTitle = 'Nouveau lien Эrgolang'; 

?>
<div class="ergoLinks form">
<?php echo $form->create('ErgoLink');?>
	<fieldset>
 		<legend><?php echo $this->pageTitle;?></legend>
	<?php
		echo $form->input('user_id',array('label'=>'Utilisateur'));
		echo $form->input('ergo_langue_id',array('label'=>'Langue'));
		echo $form->input('url');
		echo $form->input('lib', array('type'=>'textarea','label'=>'Libellé'));
		echo $form->input('date');
		echo $form->input('rem',array('label'=>'Remarque'));
		echo $form->input('Tag');
	?>
	</fieldset>
<?php echo $form->end('Ajouter');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List ErgoLinks', true), array('action'=>'index'));?></li>
		<li><?php echo $html->link(__('List Users', true), array('controller'=> 'users', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New User', true), array('controller'=> 'users', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Ergo Langues', true), array('controller'=> 'ergo_langues', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Ergo Langue', true), array('controller'=> 'ergo_langues', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Tags', true), array('controller'=> 'tags', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Tag', true), array('controller'=> 'tags', 'action'=>'add')); ?> </li>
	</ul>
</div>

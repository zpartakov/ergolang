<div class="projects form">
<?php echo $form->create('Project');?>
	<fieldset>
 		<legend><?php __('Add Project');?></legend>
	<?php
		echo $form->input('organization');
		echo $form->input('owner');
		echo $form->input('priority');
		echo $form->input('status');
		echo $form->input('name');
		echo $form->input('description');
		echo $form->input('url_dev');
		echo $form->input('url_prod');
		echo $form->input('published');
		echo $form->input('upload_max');
		echo $form->input('phase_set');
		echo $form->input('type');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Projects', true), array('action'=>'index'));?></li>
	</ul>
</div>

<?php
/**
* @version        $Id: mnemotechnique.ctp v1.0 09.07.2011 06:07:18 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*
* PROGRAMME POUR CODER LES NUMEROS DE TELEPHONE EN PHRASE MNEMOTECHNIQUE ET POUR CODER LES JOURS D'UNE ANNEE EN PHRASE MNEMOTECHNIQUE
*/
Configure::write('debug', 0);
$CHEMIN="/websites/ergolang/cake";
$this->pageTitle = 'Mnémotechnique';

#requete mysql
function requeteMySQL($lelibelle) {
$sql="SELECT libelle FROM ergo_fr_vocs WHERE num='$lelibelle' ORDER BY libelle";
#echo "<br>" .$sql ."<hr>"; //tests
$result = mysql_query("$sql");
 return($result);
}
?>
<h1><? echo $this->pageTitle; ?></h1>

 <form method="get">
  <table border=1>
    <tr>
      <th colspan="2">
        <h1>Associer un enregistrement</h1>
      </th>
    </tr>
    <tr>
      <td>Entrer le numéro de
        téléphone / la date<br>
        ou toute autre séquence numérique
        à associer
        <br/>
        <em><span style="font-size: smaller">ex. 7794321 -> accaparement</span></em>
        </td>
      <td width=400>
        <input type=text name='lelibelle' value=''>
      </td>
    </tr>
    <tr>
      <td width="288">
        <input type=reset value='Annuler' name="reset">
      </td>
      <td width="297">
        <input type=submit value='Introduire' name="submit">
      </td>
    </tr>
  </table>
</form>
 <form method="get">
  <table border=1>
    <tr>
      <th colspan="2">
        <h1>Trouver une phrase mnémotechnique pour les jours d'une année</h1>
      </th>
    </tr>
    <tr>
      <td>Entrer l'année</td>
      <td width=400>
        <select name='annee'>
        <option value=\"\">--- Choisir l'année à coder ---</option>
<?
$s=date("Y");
$i=0;
while ($i<10) {
echo "<option>$s</option>";
$s++;
$i++;
}
?>
        </select>
      </td>
    </tr>
    <tr>
      <td width="288">
        <input type=reset value='Annuler' name="reset">
      </td>
      <td width="297">
        <input type=submit value='Introduire' name="submit">
      </td>
    </tr>
  </table>
</form>
<?php
$lelibelle=$_GET['lelibelle'];
$annee=$_GET['annee'];
if(is_numeric($annee)) {
	$lelibelle=$annee;
/* ######## recherche codage années ######## */
	echo "<h2>Dates des premiers dimanches de chaque mois pour l'année: " .$annee ."</h2>";
	echo "<div class=\"resultats\">";
	$result=requeteMySQL($lelibelle);

	#make a loop on the months
	for($i=1; $i<13; $i++) {
		$nixdate=mktime(0,0,0,$i,1,$annee);
		$humandate= date("l",$nixdate);
		$ddate= date("d",$nixdate);
		$readabledate=date("d-m-Y",$nixdate);

		$test=0;
		while($test==0) {
			if($humandate=="Sunday") {
				$string.=$ddate;
				$test=1;
			} else {
				$nixdate=$nixdate+24*3600;
				$humandate= date("l",$nixdate);
				$ddate= date("d",$nixdate);
			}
		}
	}

	$printemps=substr($string,0,6);
	$printemps=eregi_replace("0","",$printemps);
	$lelibelle=$printemps;
	$result=requeteMySQL($lelibelle);

	$nb=mysql_num_rows($result);
	$i=0;
	if ($nb>0) {
	  $test=1;
		while ($i < $nb) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
		$lePrintemps.= "<span class=$bgcolor>" .mysql_result($result,$i,libelle) ."</span><br>";
		$i++;
		}
	} else {
		$lePrintemps= "<br>Désolé, pas de résultat";
	}

	$ete=substr($string,6,6);
	$ete=eregi_replace("0","",$ete);
	$lelibelle=$ete;
	$result=requeteMySQL($lelibelle);

	$nb=mysql_num_rows($result);
	$i=0;
	if ($nb>0) {
	  $test=1;
		while ($i < $nb) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
		$leEte.= "<span class=$bgcolor>" .mysql_result($result,$i,libelle) ."</span><br>";
		$i++;
		}
	} else {
		$leEte= "<br>Désolé, pas de résultat";
	}

	$automne=substr($string,12,6);
	$automne=eregi_replace("0","",$automne);
	$lelibelle=$automne;
	$result=requeteMySQL($lelibelle);

	$nb=mysql_num_rows($result);
	$i=0;
	if ($nb>0) {
	  $test=1;
		while ($i < $nb) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
		$leautomne.= "<span class=$bgcolor>" .mysql_result($result,$i,libelle) ."</span><br>";
		$i++;
		}
	} else {
		$leautomne= "<br>Désolé, pas de résultat";
	}

	$hiver=substr($string,18,6);
	$hiver=eregi_replace("0","",$hiver);
	$lelibelle=$hiver;
	$result=requeteMySQL($lelibelle);

	$nb=mysql_num_rows($result);
	$i=0;
	if ($nb>0) {
	  $test=1;
		while ($i < $nb) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
		$lehiver.= "<span class=$bgcolor>" .mysql_result($result,$i,libelle) ."</span><br>";
		$i++;
		}
	} else {
		$lehiver= "<br>Désolé, pas de résultat";
	}

	echo "<h1>$titre</h1><table border=1 padding=5><tr><th>Printemps $printemps</th><th>Eté $ete</th><th>Automne $automne</th><th>Hiver $hiver</th></tr>";
	echo "<tr><td valign=top>$lePrintemps</td><td valign=top>$leEte</td><td valign=top>$leautomne</td><td valign=top>$lehiver</td></tr>";
	echo "</table>";





	echo "</div>";
}

if($lelibelle&&strlen($lelibelle)>1&&!is_numeric($annee)) {
/* ########  recherche correspondance sur date ######## */
	echo "<div class=\"resultats\"><h2>Votre recherche:</h2>";
	$lelibelleori=$lelibelle;
	$lelibelle=ereg_replace("\.","",$lelibelle);
	$lelibelle=ereg_replace(" ","",$lelibelle);
	$lelibelle=ereg_replace("-","",$lelibelle);
	$lelibelle=ereg_replace("/","",$lelibelle);
	$result=requeteMySQL($lelibelle);
	########
	#POKER (tout ok)
	$affiche= "<table>";
	$nb=mysql_num_rows($result);
	$i=0;
	if ($nb>0) {
	  $test=1;
	echo "<br>Poker<br>";
		$affiche.= "<tr><td class=\"resultats\" valign=top>";
		while ($i < $nb) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
		$affiche.= "<br>" .mysql_result($result,$i,libelle);
		$i++;
		}
		$affiche.= "</td></tr></table>";
	$afficheTout.=$affiche;
	#exit;
	#########
	} else {
	#n-1
	  echo "<br>Pas de cryptage pour la séquence entière <font color=red>$lelibelle</font>, essai avec $leibelleL-1<br>";
	 $libelleNDroiteDebut=substr($lelibelle,0,$leibelleL-1);
	 $libelleNDroiteFin=substr($lelibelle,$leibelleL-1,$leibelleL);
	 $libelleNGaucheDebut=substr($lelibelle,0,1);
	 $libelleNGaucheFin=substr($lelibelle,1,$leibelleL);
	#droite
	# echo "<hr>DROITE<br>Niveau 1: " .$lelibelle ." = Droite: " .$libelleNDroiteDebut ."+" .$libelleNDroiteFin;
	#appel fonction mysql
	$result=requeteMySQL($libelleNDroiteDebut);
	$result2=requeteMySQL($libelleNDroiteFin);
	$affiche= "<table border=1>";
	$nb=mysql_num_rows($result);
	$nb2=mysql_num_rows($result2);
	$i=0;
	if ($nb>0&&$nb2>0) {
	  $test=1;
	echo "<br>Poker  à droite n-1<br>";
		$affiche.= "<tr><td valign=top>";
		while ($i < $nb) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
		$affiche.= "<br><span class=$bgcolor>" .mysql_result($result,$i,libelle) ."</span>";
		$i++;
		}
		$affiche.= "</td></tr></table>";
	$afficheTout.=$affiche;
	#exit;
	 } else {
	#droite n-2
	   echo "<br>Niveau 2: Pas de cryptage pour la séquence $leibelleL-1 à droite, essai avec $leibelleL-2<br>";
	 $libelleNDroiteDebut=substr($lelibelle,0,$leibelleL-2);
	 $libelleNDroiteFin=substr($lelibelle,$leibelleL-2,$leibelleL);
	# echo $lelibelle ." = Droite: " .$libelleNDroiteDebut ."+" .$libelleNDroiteFin;
	#appel fonction mysql
	$result=requeteMySQL($libelleNDroiteDebut);
	$result2=requeteMySQL($libelleNDroiteFin);
	$affiche= "<table border=1>";
	$nb=mysql_num_rows($result);
	$nb2=mysql_num_rows($result2);
	if ($nb>0&&$nb2>0) {
	  $test=1;
	echo "<br>Poker à droite n-2<br>";
		$affiche.= "<tr><td valign=top>";
		$i=0;
		while ($i < $nb) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
		$affiche.= "<br><span class=$bgcolor>" .mysql_result($result,$i,libelle) ."</span>";
		$i++;
		}
		$affiche.= "</td><td>";
		$i=0;
		while ($i < $nb2) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
		$affiche.= "<br><span class=$bgcolor>" .mysql_result($result2,$i,libelle) ."</span>";
		$i++;
		}
		$affiche.= "</td></tr></table>";
		$afficheTout.=$affiche;
	#exit;
				  } else {
	   echo "<br>Niveau 3: Pas de cryptage pour la séquence $leibelleL-2 à droite, essai avec $leibelleL-3<br>";
	   $libelleNDroiteDebut=substr($lelibelle,0,$leibelleL-3);
	   $libelleNDroiteFin=substr($lelibelle,$leibelleL-3,$leibelleL);
	#   echo $lelibelle ." = Droite: " .$libelleNDroiteDebut ."+" .$libelleNDroiteFin;
		#appel fonction mysql
	$result=requeteMySQL($libelleNDroiteDebut);
	$result2=requeteMySQL($libelleNDroiteFin);
	$affiche= "<table border=1>";
	$nb=mysql_num_rows($result);
	$nb2=mysql_num_rows($result2);
	if ($nb>0&&$nb2>0) {
	  $test=1;
	echo "<br>Poker à droite n-3<br>";
		$affiche.= "<tr><td valign=top>";
		$i=0;
		while ($i < $nb) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
		$affiche.= "<br><span class=$bgcolor>" .mysql_result($result,$i,libelle) ."</span>";
		$i++;
		}
		$affiche.= "</td><td>";
		$i=0;
		while ($i < $nb2) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
		$affiche.= "<br><span class=$bgcolor>" .mysql_result($result2,$i,libelle) ."</span>";
		$i++;
		}
		$affiche.= "</td></tr></table>";
		$afficheTout.=$affiche;
	#exit;
	 } else {
	#  echo "<br>Niveau 4: Pas de cryptage pour la séquence $leibelleL-3 à droite, essai avec $leibelleL-4<br>";
	   $libelleNDroiteDebut=substr($lelibelle,0,$leibelleL-4);
	   $libelleNDroiteFin=substr($lelibelle,$leibelleL-4,$leibelleL);
	#   echo $lelibelle ." = Droite: " .$libelleNDroiteDebut ."+" .$libelleNDroiteFin;
		#appel fonction mysql
	#todo
		#appel fonction mysql
	$result=requeteMySQL($libelleNDroiteDebut);
	$result2=requeteMySQL($libelleNDroiteFin);
	$affiche= "<table border=1>";
	$nb=mysql_num_rows($result);
	$nb2=mysql_num_rows($result2);
	if ($nb>0&&$nb2>0) {
	  $test=1;
	echo "<br>Poker à droite n-4<br>";
		$affiche.= "<tr><td valign=top>";
		$i=0;
		while ($i < $nb) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
		$affiche.= "<br><span class=$bgcolor>" .mysql_result($result,$i,libelle) ."</span>";
		$i++;
		}
		$affiche.= "</td><td>";
		$i=0;
		while ($i < $nb2) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
		$affiche.= "<br><span class=$bgcolor>" .mysql_result($result2,$i,libelle) ."</span>";
		$i++;
		}
		$affiche.= "</td></tr></table>";
		$afficheTout.=$affiche;
	#exit;
	 }
	 }

	}
	}
	########
	#gauche niveau 1
	# echo "<hr>GAUCHE<br>Niveau 1: " .$libelleNGaucheDebut ."+" .$libelleNGaucheFin;
	#appel fonction mysql
	$result=requeteMySQL($libelleNGaucheDebut);
	$result2=requeteMySQL($libelleNGaucheFin);
	$nb=mysql_num_rows($result);
	$nb2=mysql_num_rows($result2);

	$affiche= "<table border=1>";
	if ($nb>0&&$nb2>0) {
	  $test=1;
	echo "<br>Poker  à gauche n-1<br>";
		$affiche.= "<tr><td valign=top>";
	$i=0;
		while ($i < $nb) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
		$affiche.= "<br><span class=$bgcolor>" .mysql_result($result,$i,libelle) ."</span>";
		$i++;
		}
		$affiche.= "</td><td>";
		$i=0;
		while ($i < $nb2) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
		$affiche.= "<br><span class=$bgcolor>" .mysql_result($result2,$i,libelle) ."</span>";
		$i++;
		}
		$affiche.= "</td></tr></table>";
	$afficheTout.=$affiche;
	#exit;
	 } else {
	#   echo "<br>Niveau 2: Pas de cryptage pour la séquence $leibelleL-1,à gauche essai avec $leibelleL-2<br>";
	#gauche niveau 2
	 $libelleNGaucheDebut=substr($lelibelle,0,2);
	 $libelleNGaucheFin=substr($lelibelle,2,$leibelleL);
	# echo $lelibelle ." = Gauche: " .$libelleNGaucheDebut ."+" .$libelleNGaucheFin;
	#appel fonction mysql
	$result=requeteMySQL($libelleNGaucheDebut);
	$result2=requeteMySQL($libelleNGaucheFin);
	$affiche= "<table border=1>";
	$nb=mysql_num_rows($result);
	$nb2=mysql_num_rows($result2);

	if ($nb>0&&$nb2>0) {
	  $test=1;
	echo "<br>Poker  à gauche n-2<br>";
		$affiche.= "<tr><td valign=top>";
		$i=0;
		while ($i < $nb) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
		$affiche.= "<br><span class=$bgcolor>" .mysql_result($result,$i,libelle) ."</span>";
		$i++;
		}
		$affiche.= "</td><td>";
		$i=0;
		while ($i < $nb2) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
		$affiche.= "<br><span class=$bgcolor>" .mysql_result($result2,$i,libelle) ."</span>";
		$i++;
		}
		$affiche.= "</td></tr></table>";
	$afficheTout.=$affiche;
	 } else {
	   echo "<br>Niveau 3: Pas de cryptage pour la séquence $leibelleL-2,à gauche essai avec $leibelleL-3<br>";
	#gauche niveau 3
	 $libelleNGaucheDebut=substr($lelibelle,0,3);
	 $libelleNGaucheFin=substr($lelibelle,3,$leibelleL);
	# echo $lelibelle ." = Gauche: " .$libelleNGaucheDebut ."+" .$libelleNGaucheFin;
	#appel fonction mysql
	$result=requeteMySQL($libelleNGaucheDebut);
	$result2=requeteMySQL($libelleNGaucheFin);
	$affiche= "<table border=1>";
	$nb=mysql_num_rows($result);
	$nb2=mysql_num_rows($result2);
	$i=0;
	if ($nb>0&&$nb2>0) {
	  $test=1;
	echo "<br>Poker  à gauche n-3<br>";
		$affiche.= "<tr><td valign=top>";
		while ($i < $nb) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
			if(!eregi(mysql_result($result,$i,libelle),$afficheTout)) {
		$affiche.= "<br><span class=$bgcolor>" .mysql_result($result,$i,libelle) ."</span>";
			}
		$i++;

		}
		$affiche.= "</td><td>";
		$i=0;
		while ($i < $nb2) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
		if(!eregi(mysql_result($result,$i,libelle),$afficheTout)) {
		$affiche.= "<br><span class=$bgcolor>" .mysql_result($result2,$i,libelle) ."</span>";
		}
		$i++;
		}
		$affiche.= "</td></tr></table>";
	$afficheTout.=$affiche;
	 } else {
	  echo "<br>Niveau 4: Pas de cryptage pour la séquence $leibelleL-3 à gauche, essai avec $leibelleL-4<br>";
	#gauche niveau 4
	 $libelleNGaucheDebut=substr($lelibelle,0,4);
	 $libelleNGaucheFin=substr($lelibelle,4,$leibelleL);
	# echo $lelibelle ." = Gauche: " .$libelleNGaucheDebut ."+" .$libelleNGaucheFin;
	#appel fonction mysql
	#appel fonction mysql
	$result=requeteMySQL($libelleNGaucheDebut);
	$result2=requeteMySQL($libelleNGaucheFin);
	$affiche= "<table border=1>";
	$nb=mysql_num_rows($result);
	$nb2=mysql_num_rows($result2);
	$i=0;
	if ($nb>0&&$nb2>0) {
	  $test=1;
	echo "<br>Poker  à gauche n-4<br>";
		$affiche.= "<tr><td valign=top>";
		while ($i < $nb) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
		$affiche.= "<br><span class=$bgcolor>" .mysql_result($result,$i,libelle) ."</span>";
		$i++;
		}
		$affiche.= "</td><td>";
		$i=0;
		while ($i < $nb2) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
		$affiche.= "<br><span class=$bgcolor>" .mysql_result($result2,$i,libelle) ."</span>";
		$i++;
		}
		$affiche.= "</td></tr></table>";
	$afficheTout.=$affiche;
	 }
	 }
	 }
	}

	}

	$affiche.= "</td></tr></table>";
	#pas marché avec 2, recherche 3
	if($test!=1&&$leibelleL==7) {
	  echo "<br>Pas de cryptage pour la séquence <font color=red>$lelibelle</font>,en 2 mots, essai avec 3 mots<br>";
	 $libelleDroite=substr($lelibelle,0,3);
	 $libelleMilieu=substr($lelibelle,3,3);
	 $libelleFin=substr($lelibelle,5,3);
	# echo $libelleDroite ."<br>" .$libelleMilieu ."<br>" .$libelleFin;

	$sql1="SELECT libelle FROM voc WHERE num='$libelleDroite' ORDER BY libelle";
	$sql2="SELECT libelle FROM voc WHERE num='$libelleMilieu' ORDER BY libelle";
	$sql3="SELECT libelle FROM voc WHERE num='$libelleFin' ORDER BY libelle";
	#echo "<br>" .$sql1 ."<br>" .$sql2 ."<br>" .$sql3 ."<br>" ."<hr>";

	$result1 = mysql_query("$sql1");
	$result2 = mysql_query("$sql2");
	$result3 = mysql_query("$sql3");
	$nb1=mysql_num_rows($result1);
	$nb2=mysql_num_rows($result2);
	$nb3=mysql_num_rows($result3);

	#echo "<br>" .$nb1 ."<br>" .$nb2 ."<br>" .$nb3 ."<br>" ."<hr>";
	if ($nb1>0&&$nb2>0&&$nb3>0) {

	  $affiche1="";
	  $affiche2="";
	  $affiche3="";
	echo "<br>Poker avec 3+2+2";
	  $test=1;
	#mot1
	  $i=0;
			while ($i < $nb1) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
		$affiche1.= "<br><span class=$bgcolor>" .mysql_result($result1,$i,libelle) ."</span>";
		$i++;
		}

	#mot1
	  $i=0;
			while ($i < $nb2) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
		$affiche2.= "<br><span class=$bgcolor>" .mysql_result($result2,$i,libelle) ."</span>";
		$i++;
		}

	#mot1
	  $i=0;
			while ($i < $nb3) {
			//loop over each field
			if (intval($i/2)==($i/2)) {
			$bgcolor="mot";
			} else {
			$bgcolor="mot1";
			}
		$affiche3.= "<br><span class=$bgcolor>" .mysql_result($result3,$i,libelle) ."</span>";
		$i++;
		}


	$afficheTout.="<table border=1><tr><td valign=top>$affiche1</td><td valign=top>$affiche2</td><td valign=top>$affiche3</td></tr></table></div>";
	#exit;
	 }
	 }

	if($test=="1") {
	echo $afficheTout;
	 } else {
	  echo "Désolé pas de cryptage possible";
	 }


}
?>



<hr/>
ex. 7794321 accaparement<br />
<br />
Pour mémoire (cf. <a href=https://radeff.red/akademia/doc/tc/Methodes/IndexMethod_html.php#Syst%C3%A8mes%20mn%C3%A9motechniques>Systèmes mnémotechniques akademia.ch</a>
<TABLE BORDER WIDTH="" >
<TR>
<TD>Chiffre</TD>

<TD>Lettre</TD>
</TR>

<TR>
<TD>1</TD>

<TD>t, d (dentales)</TD>
</TR>

<TR>
<TD>2</TD>

<TD>n (2 barres)</TD>
</TR>

<TR>
<TD>3</TD>

<TD>m (3 barres)</TD>
</TR>

<TR>

<TD>4</TD>

<TD>r</TD>
</TR>

<TR>
<TD>5</TD>

<TD>l</TD>
</TR>

<TR>
<TD>6</TD>

<TD>ch, j</TD>
</TR>

<TR>
<TD>7</TD>

<TD>k, g</TD>
</TR>

<TR>
<TD>8</TD>

<TD>v, f, ph</TD>
</TR>

<TR>
<TD>9</TD>

<TD>p, b</TD>
</TR>

<TR>
<TD>0</TD>

<TD>s, z</TD>

</TR>
</TABLE>
On peut ainsi coder des num&eacute;ros de t&eacute;l&eacute;phone, des dates etc..., en intercalant simplement des voyelles entres les consonnes.<br />
<br />
<br />Exemple: (parfois, les mill&eacute;naires
sont omis)
<br />Thal&egrave;s de Milet: Chat noir largu&eacute; (624-547 av. J.-C.)
<br />Socrate: Riche pas, mais
papa (469-399 av. J.-C.) - 70 ans

<br />Platon: Arnaque homérique
(427-347) - 80 ans
<br />Aristote: Amphore maniérée
(384-324) - 60 ans
<br /><br /><hr /><br />
<p>En savoir plus sur la méthode Aimé Paris: <a href="https://fr.wikipedia.org/wiki/Mn%C3%A9motechnique#M.C3.A9thode_d.27Aim.C3.A9_Paris">Méthode d'Aimé Paris</a></p>
<p>Voir aussi: <a href="https://radeff.red/dokuwiki/doku.php/akademia:methodes">https://radeff.red/dokuwiki/doku.php/akademia:methodes</a></p>

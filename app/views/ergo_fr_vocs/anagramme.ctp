<?php
/**
* @version        $Id: anagramme.ctp v1.0 09.07.2011 06:07:21 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$CHEMIN="/websites/ergolang/cake";
$this->pageTitle = 'Anagrammes'; 


function sortString(&$s) {
  $s = str_split($s);
  sort($s);
  $s = implode($s);
}

function wd_remove_accents($str, $charset='utf-8')
{
	/*
	 * source: http://www.weirdog.com/blog/php/supprimer-les-accents-des-caracteres-accentues.html
	 * un gars futé! car problème de l'utf 8... il convertit en html_entities puis gère les cas particuliers
*/
    $str = htmlentities($str, ENT_NOQUOTES, $charset);
    
    $str = preg_replace('#&([A-za-z])(?:acute|cedil|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
    $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères
    
    return $str;
}
?>
<h1><? echo $this->pageTitle; ?></h1>
<form method="GET">
Chercher un anagramme pour: <input type="text" name="mot">
<input type="reset">
<input type="submit">
&nbsp;<em>ex: maire</em>
</form>
<?php
$libelle=$_GET['mot'];
if($libelle) {
	$affiche='';
	echo "<h2>Recherche anagramme pour " .$libelle ."</h2>";
		
	$string=wd_remove_accents($libelle);
	sortString($string, strlen($string));

$sql="SELECT * FROM ergo_fr_vocs WHERE alphab LIKE '" .$string ."'";
#echo "<br><em>$sql</em><br>";
$sql=mysql_query($sql);
$nb=mysql_num_rows($sql);
if ($nb>1) {
echo "<p>Hits: " .mysql_num_rows($sql) ."</p><br />";

	$affiche.= "<tr><td valign=top><ol>";
$i = 0;
	while ($i < mysql_num_rows($sql)) {
	  if (mysql_result($sql,$i,'libelle')!=$libelle) {
//loop over each field
		if (intval($i/2)==($i/2)) {
		$bgcolor="mot";
		} else {
		$bgcolor="mot1";
		}
	$affiche.= "<li><span class=$bgcolor>" .mysql_result($sql,$i,'libelle') ."</span>";
	  }	
	  $i++;

	}
	$affiche.= "</ol></td></tr>";
echo $affiche;
} else {
  echo "Sorry, no results<br>";
}



}
?>

<?php


/**
* @version        $Id: index.ctp v1.0 15.04.2010 05:49:08 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$CHEMIN="/websites/ergolang/cake";
$this->pageTitle = 'Dictionnaire Français'; 
?>
<div class="ergoFrVocs index">
<h1><? echo $this->pageTitle; ?></h1>
<p>Source principale: Lexique (<a href="http://www.lexique.org">http://www.lexique.org</a>),  Boris New & Christophe Pallier</p> 
<?php echo $html->link(__('Nouveau mot', true), array('action'=>'add')); ?>

<!-- begin search form -->
 <table>
	 <tr>
		 <td>
 <div class="input">
<?php echo $form->create('ErgoFrVoc', array('url' => array('action' => 'index'))); ?>
		<?php echo $form->input('q', array('label' => false, 'size' => '50', 'class'=>'txttosearch')); ?>
		
  <table>
    <tr> 
      <td> Mot entier
        <input type="radio" name="whole" value="whole">

      </td>
      <td> Debut de mot
        <input type="radio" name="debut" value="init">
      </td>
      <td> Fin de mot
        <input type="radio" name="fin" value="end">
      </td>
	  </tr><tr>
      <td>Nombre de lettres:<input type=text name='long' value='' size=2>
</td>
    </tr>
  </table>
		
		</div>
</td>


<td>
<input type="button" class="vider" value="Vider" onClick="javascript:vide_recherche('ErgoFrVocQ')" />
<input type="submit" class="chercher" value="Chercher" /> 
</div> 
</td>
</tr>
</table>







<!-- end search form -->
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('libelle');?></th>
	<th><?php echo $paginator->sort('TYPE');?></th>
	<th><?php echo $paginator->sort('genre');?></th>
	<th><?php echo $paginator->sort('num');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
//print_r($ergoFrVocs); exit;
foreach ($ergoFrVocs as $ergoFrVoc):
    
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $ergoFrVoc['ErgoFrVoc']['id']; ?>
		</td>
		<td>
			<?php 
			#echo "<br>plain" .$ergoFrVoc['ErgoFrVoc']['libelle']; 
			echo $ergoFrVoc['ErgoFrVoc']['libelle']; 
			#echo "<br>utf8_decode : " .utf8_decode($ergoFrVoc['ErgoFrVoc']['libelle']); 
			#echo "<br>utf8encode_utf8_decode : " .utf8_encode(utf8_decode($ergoFrVoc['ErgoFrVoc']['libelle'])); 
			#echo "<br>utf8decode_utf8_encode : " .utf8_decode(utf8_encode($ergoFrVoc['ErgoFrVoc']['libelle'])); 
			?>
		</td>
		<td>
			<?php echo $ergoFrVoc['ErgoFrVoc']['TYPE']; ?>
		</td>
		<td>
			<?php echo $ergoFrVoc['ErgoFrVoc']['genre']; ?>
		</td>
		<td>
			<?php echo $ergoFrVoc['ErgoFrVoc']['num']; ?>
		</td>
		<td class="actions">
	<?php 
echo "<a href=\"" .$CHEMIN ."/ergoFrVocs/view/" .$ergoFrVoc['ErgoFrVoc']['id'] ."\">";
echo $html->image('b_search.png', array("alt"=>"Voir","title"=>"Voir"));
echo "</a>";

	/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
echo "&nbsp;";
echo "<a href=\"".$CHEMIN ."/ergoFrVocs/edit/" .$ergoFrVoc['ErgoFrVoc']['id'] ."\">";
echo $html->image('b_edit.png', array("alt"=>"Modifier","title"=>"Modifier"));
echo "</a>";
echo "<a onclick=\"javascript:return confirm('Confirmer la suppression ?')\" href=\"" .$CHEMIN ."/ergoFrVocs/delete/" .$ergoFrVoc['ErgoFrVoc']['id'] ."\">";
echo $html->image('b_drop.png', array("alt"=>"Effacer","title"=>"Effacer"));
echo "</a>";
}
?>
		</td>
	</tr>
<?php 



endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
<?php echo $html->link(__('Nouveau mot', true), array('action'=>'add')); ?>
	</ul>
</div>
<p>Source principale: Lexique (<a href="http://www.lexique.org">http://www.lexique.org</a>),  Boris New & Christophe Pallier</p> 

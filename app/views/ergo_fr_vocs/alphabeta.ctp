<?php
/**
* @version        $Id: alphabeta.ctp v1.0 09.07.2011 07:14:28 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
		/* une fonction pour créer la liste alphabétique des mots (calcul anagrammes) */
		
		/*source: http://www.jqueryin.com/2010/04/30/a-quick-implementation-of-string-sort-in-php/*/
function sortString(&$s) {
  $s = str_split($s);
  sort($s);
  $s = implode($s);
}

function wd_remove_accents($str, $charset='utf-8')
{
	/*
	 * source: http://www.weirdog.com/blog/php/supprimer-les-accents-des-caracteres-accentues.html
	 * un gars futé! car problème de l'utf 8... il convertit en html_entities puis gère les cas particuliers
*/
    $str = htmlentities($str, ENT_NOQUOTES, $charset);
    
    $str = preg_replace('#&([A-za-z])(?:acute|cedil|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
    $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères
    
    return $str;
}

$sql="SELECT * FROM ergo_fr_vocs WHERE LENGTH(alphab) <1 ORDER BY libelle";
$sql=mysql_query($sql);
if(!$sql) {
	echo "SQL bug: " .mysql_error();
	exit;
}
$i=0;
while($i<mysql_num_rows($sql)){
	$arr[]="";
	echo mysql_result($sql,$i,'libelle');
	$string=mysql_result($sql,$i,'libelle');
	$string=wd_remove_accents($string);
	echo "<br>";
	sortString($string, strlen($string));
echo $string . "\n";
$insere="UPDATE ergo_fr_vocs SET alphab='".$string."' WHERE id=".mysql_result($sql,$i,'id');
echo "<br><em>" .$insere."</em><br>";
	$insere=mysql_query($insere);
if(!$insere) {
	echo "<br>SQL bug: " .mysql_error();
}

	echo "<hr>";
	$i++;
}
?>

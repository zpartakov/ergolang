<div class="ergoFrVocs form">
<?php echo $form->create('ErgoFrVoc');?>
	<fieldset>
 		<legend><?php __('Edit ErgoFrVoc');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('libelle');
		echo $form->input('TYPE');
		echo $form->input('genre');
		echo $form->input('num');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action'=>'delete', $form->value('ErgoFrVoc.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('ErgoFrVoc.id'))); ?></li>
		<li><?php echo $html->link(__('List ErgoFrVocs', true), array('action'=>'index'));?></li>
	</ul>
</div>

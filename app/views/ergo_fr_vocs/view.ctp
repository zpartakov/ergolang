<?php
/**
* @version        $Id: view.ctp v1.0 15.04.2010 05:52:52 CEST $
* @package        Эrgolang
* @copyright      Copyright (C) 2009 - 2013 Open Source Matters. All rights reserved.
* @license        GNU/GPL, see LICENSE.php
* Эrgolang is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
$this->pageTitle = 'Dictionnaire Français: Vue'; 

?>
<div class="ergoFrVocs view">
<h1><? echo $this->pageTitle; ?></h1>

<p>Source principale: Lexique (<a href="http://www.lexique.org">http://www.lexique.org</a>),  Boris New & Christophe Pallier</p> 
<p>&nbsp;</p>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoFrVoc['ErgoFrVoc']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Libelle'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoFrVoc['ErgoFrVoc']['libelle']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('TYPE'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoFrVoc['ErgoFrVoc']['TYPE']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Genre'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoFrVoc['ErgoFrVoc']['genre']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Num'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $ergoFrVoc['ErgoFrVoc']['num']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
	<?
		/*	hide from non-admin registred user */
if($session->read('Auth.User.group_id')==3) {
	?>
		<li><?php echo $html->link(__('Edit ErgoFrVoc', true), array('action'=>'edit', $ergoFrVoc['ErgoFrVoc']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete ErgoFrVoc', true), array('action'=>'delete', $ergoFrVoc['ErgoFrVoc']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $ergoFrVoc['ErgoFrVoc']['id'])); ?> </li>
		<li><?php echo $html->link(__('List ErgoFrVocs', true), array('action'=>'index')); ?> </li>
		<?
	}
	?>
		<?
			if($session->read('Auth.User.username')) { //show only to registered users
?>
		<li><?php echo $html->link(__('New ErgoFrVoc', true), array('action'=>'add')); ?> </li>
		<?
}
?>
	</ul>
</div>

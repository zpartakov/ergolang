<?php
/**
 * Short description for file.
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/views/pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));

	/* some redirections for pretty urls */
Router::connect('/installation', array('controller' => 'articles', 'action' => 'vue','1'));
Router::connect('/license', array('controller' => 'articles', 'action' => 'vue','2'));
Router::connect('/credits', array('controller' => 'articles', 'action' => 'vue','3'));

Router::connect('/pages/todos', array('controller' => 'articles', 'action' => 'vue','11'));

/* languages redirections */

	Router::connect('/allemand', array('controller' => 'articles', 'action' => 'vue','9'));
	Router::connect('/anglais', array('controller' => 'articles', 'action' => 'vue','4'));
	Router::connect('/arabe', array('controller' => 'articles', 'action' => 'vue','24'));
	Router::connect('/croate', array('controller' => 'articles', 'action' => 'vue','13'));
	Router::connect('/espagnol', array('controller' => 'articles', 'action' => 'vue','10'));
	Router::connect('/francais', array('controller' => 'articles', 'action' => 'vue','15'));
	Router::connect('/italien', array('controller' => 'articles', 'action' => 'vue','8'));
	Router::connect('/latin', array('controller' => 'pages', 'action' => 'display', 'lat/latin'));
	Router::connect('/russe', array('controller' => 'articles', 'action' => 'vue','5'));
	Router::connect('/russe/exercices', array('controller' => 'articles', 'action' => 'vue','7'));
	Router::connect('/syrien', array('controller' => 'articles', 'action' => 'vue','25'));
	Router::connect('/portugais', array('controller' => 'articles', 'action' => 'vue','33'));
	Router::connect('/hollandais', array('controller' => 'articles', 'action' => 'vue','36'));




/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
	#http://www.formation-cakephp.com/28/generer-un-flux-rss-avec-cakephp
	Router::parseExtensions('rss');
		?>
